<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Terceros extends Model
{
  use HasFactory;
    protected $table='terceros';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //Id
        'id',
        //nombre
        'tip_per',
        //tipo persona juridica o natural
        'tip_ident',
        //tipo identifi
        'N_documet',
        //numero Iden
        'nit',
        ///nit de la empresa
        'expedido',
        //eliminar
        'name',
        //nombre persona
        'secname',
        //segundo Nombre
        'lastname',
        //apellido
        'seclastname',
        //segundo apellido
        'nom_representante',
        'correo_representante',
        'phone_representante',
        'nom_comercial',
        'addres',
        //Direccion
        'pais',
        'departamento',
        'municipio',
        'city',
        //Ciudad
        'correo',
        //correo electronico
        'phone',
        //numero de telefono
        'mobile',
        //numero de Celular
        'banco',
        'tipo_cuenta',
        'cuenta_bancaria',
        'nombre_titular',
        'regimen',
        'actividad_economica',
        'res_fiscal',
        'direlocal',
        //direccion del local
        'contra',
        //tipo Arrendatario
        'sigla',
        //tamaño de la Empresa
        'deleted',
        //estado
    ];


    public function departamentopdf(){
      return $this->HasOne("App\Models\Departament","id_departamento","departamento");
    }

    public function municipiopdf(){
      return $this->HasOne("App\Models\Municipio","id_municipio","municipio");
    }

    public function paispdf(){
      return $this->HasOne("App\Models\Pais","id","pais");
    }

    public function banco_rela(){
      return $this->HasOne("App\Models\Bank","bank_id","banco");
    }

    public function economica_rela(){
      return $this->HasOne("App\Models\ActividadEconomica","codigo","actividad_economica");
    }

    public function res_rela(){
      return $this->HasOne("App\Models\ResponsabilidadFiscal","id","res_fiscal");
    }

}
