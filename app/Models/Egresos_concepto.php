<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Egresos_concepto extends Model
{
    use HasFactory;

    protected $table = 'cuenta_contable';

    protected $fillable = [
        'id',
        'concepto_cont',
        'valor',
        'impuesto',
        'observaciones_cont',
        'id_egresos',
        'deleted',
    ];
    public function egresos()
    {
      return $this->HasOne("App\Models\Egresos","id","id_egresos");
    }
}
