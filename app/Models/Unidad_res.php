<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unidad_res extends Model
{
    use HasFactory;
    protected $table = 'unidad_res';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_res',
        'tip_inm',
        'nombre_res',
        'deleted',
    ];

    public function tipo_inmueble_res(){
        return $this->belongsTo("App\Models\TipoInmueble","tip_inm");
    }
}
