<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sub_cuentas extends Model
{
    use HasFactory;

    protected $table = 'sub_cuentas';

    protected $fillable = [
        'id',
        'concepto',
        'codigo',
        'cuenta_id',
        'deleted',
    ];
    public function cuentas_id(){
        return $this->HasOne("App\Models\Cat_cuentas","id","cuenta_id");
    }

    public function nivel1(){
       return $this->HasMany("App\Models\Nivel_1","idSubCuentas","id")->where('deleted', 0);
   }

}
