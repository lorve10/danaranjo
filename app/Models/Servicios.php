<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servicios extends Model
{
    use HasFactory;
    protected $table = 'servicios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_servicios',
        'nombre_servic',
        'cat_id',
        'deleted',

    ];

    public function Categoria_id(){
        return $this->HasOne("App\Models\CatServicios","id_cat","cat_id");
    }
}
