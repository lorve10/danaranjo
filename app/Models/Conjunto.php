<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conjunto extends Model
{
    use HasFactory;

    protected $table = 'conjunto';

    protected $fillable = [
        'id',
        'nombre',
        'direccion',
        'departamento',
        'municipio',
        'telefono',
        'logo',
        'pais',
        'moneda',
        'deleted',
    ];

    public function ver_moneda(){
        return $this->belongsTo("App\Models\Moneda","moneda");
    }
}
