<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminData extends Model
{
    use HasFactory;

    protected $table = 'datadmin';

    protected $fillable = [
        'id',
        'nombre',
        'firma',
        'telefono',
        'correo',
        'deleted',
    ];
}
