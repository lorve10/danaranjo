<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    use HasFactory;

    protected $table="pagos";

    protected $fillable = [
        'id',
        'fecha',
        'mes',
        'n_document',
        'inmueble',
        'tip_pago',
        'valor',
        'concepto',
        'interes',
        'observa',
        'deleted',
    ];

    public function inmueble(){
        return $this->belongsTo("App\Models\Inmueble","inmueble");
    }
    public function concept(){
      return $this->HasOne("App\Models\Servicios","id_servicios","concepto");
    }


}
