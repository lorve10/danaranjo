<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nivel_5 extends Model
{
    use HasFactory;


    protected $table = 'nivel_5';

    protected $fillable = [
        'id',
        'nombre',
        'codigo',
        'descripcion',
        'tipo',
        'naturaleza',
        'factura',
        'nit',
        'mvot',
        'cxc',
        'proveedores',
        'inventario',
        'activos',
        'saldo',
        'interes',
        'mora',
        'descuento',
        'retefuente',
        'base',
        'porcentaje',
        'nivelcuatro',
        'deleted'
    ];

}
