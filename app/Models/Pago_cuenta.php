<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pago_cuenta extends Model
{
    use HasFactory;
    protected $table="pagos_cuentas";

    protected $fillable = [
        'id',
        'concepto',
        'valor',
        'impuesto',
        'observacion',
        'id_pago',
        'deleted'
    ];
    public function pagos()
    {
      return $this->HasOne("App\Models\Pago","id","id_pago");
    }
}
