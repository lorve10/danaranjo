<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departament extends Model
{
  use HasFactory;
    protected $table='departamentos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //Id de la categoria
        'id_departamento',
        //nombre
        'departamento',
    ];
    public function municipios()
    {
      return $this->hasMany('App\Models\Municipio','id_departamento','departamento_id');
    }
}
