<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nivel_4 extends Model
{
    use HasFactory;


    protected $table = 'nivel_4';

    protected $fillable = [
        'id',
        'nombre',
        'codigo',
        'descripcion',
        'tipo',
        'naturaleza',
        'factura',
        'nit',
        'mvot',
        'cxc',
        'proveedores',
        'inventario',
        'activos',
        'saldo',
        'interes',
        'mora',
        'descuento',
        'retefuente',
        'base',
        'porcentaje',
        'niveltres',
        'deleted'
    ];

    public function nivel5(){
       return $this->HasMany("App\Models\Nivel_5","nivelcuatro","id")->where("deleted", 0);
   }
}
