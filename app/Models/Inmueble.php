<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inmueble extends Model
{
    use HasFactory;
    protected $table = 'inmueble';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'tip_inmueble',
        'torre',
        'parqueadero',
        'num_parq',
        'deposito',
        'num_depo',
        'numero',
        'area_mt2',
        //'terceros',
        'tarifa',
        'estado',
        'deleted',

    ];

    public function tipo_inmueble(){
        return $this->belongsTo("App\Models\TipoInmueble","tip_inmueble");
    }

    public function tipo_unidad(){
        return $this->HasOne("App\Models\Unidad_res", "id_res", "torre");
    }
    // recomendacion poner nombre diferente al campo por ejemplo tercero(nombre de la funcion)==terceros(campo) es incoreecto
    // entp
    public function propietario(){
        return $this->belongsTo("App\Models\Terceros","terceros");
    }
    // esa tiene que poner nombre diferente
    public function tarifa_inmueble(){
        return $this->belongsTo("App\Models\Tarifa","tarifa");
    }

    //Esta trae los cobros = tarifa por mes
    public function cobro_inmueble(){
        return $this->HasOne("App\Models\Cobros","inmueble","id");
    }
}
