<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Serv_contable extends Model
{
    use HasFactory;
    protected $table = 'serv_contable';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'id_cat_servi',
        'id_cat_cuent',
        'id_sub_cuent',
        'id_cuenta_sub',
        'id_sub_cuen2',
        'id_sub_cuent3',
        'deleted',

    ];



}
