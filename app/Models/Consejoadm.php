<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Consejoadm extends Model
{
    use HasFactory;

    protected $table = 'consejoadm';

    protected $fillable = [
        'id',
        'fechaini',
        'fechafin',
        'lider',
        'telefono',
        'cargo',
        'telefonocargo',
        'deleted',
    ];
}
