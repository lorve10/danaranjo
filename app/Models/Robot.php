<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Robot extends Model
{
    use HasFactory;
    protected $table = 'robot';

    protected $fillable = [
        'id',
        'estado',
        'fechaini',
        'fechaend',
        'concepto'
    ];
}
