<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cat_cuentas extends Model
{
    use HasFactory;

    protected $table = 'cat_cuentas';

    protected $fillable = [
        'id',
        'nombre_cuenta',
    ];
}
