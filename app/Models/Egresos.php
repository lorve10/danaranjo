<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Egresos extends Model
{
    use HasFactory;

    protected $table = 'egresos';

    protected $fillable = [
        'id',
        'fecha',
        'mes',
        'ndocumento',
        'terceros',
        'concepto',
        'mt_pago',
        'valor',
        'interes',
        'observaciones',
        'deleted',
    ];

    public function propietario(){
        return $this->belongsTo("App\Models\Terceros","terceros");
    }
    public function concep(){
      return $this->HasOne("App\Models\Servicios","id_servicios" ,"concepto")->where('deleted',0);
    }
}
