<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Propietarios extends Model
{
    use HasFactory;

    protected $table='propietarios';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
        'id', //Id
        'tip_prop', //tipo propietario
        'tip_ident', //tipo identidad
        'n_document', //numero Iden
        'expedido', //expedicion del documento
        'foto', ///Foto del propietario
        'name', //nombre persona
        'secname', //segundo Nombre
        'lastname', //apellido
        'seclastname', //segundo apellido
        'addres', //Direccion
        'departamento',
        'municipio',
        'city', //Ciudad
        'correo', //correo electronico
        'phone', //numero de telefono
        'mobile',  //numero de Celular
        'profesion', //Profesion de la persona
        'estado_civil', //Estado civil de la persona
        'tip_inmueble', //Tipo de inmueble: 1. Apartamento / 2. Parqueadero / 3. Deposito / 4. Casas
        'torre', //torre de la persona
        'vehiculo', ///Vehiculo: 1 - carro / 2 - moto
        'marca', ///Marca del vehículo
        'placa', ///Placa del vehículo
        'color', ///Color del vehículo
        'numero_per', //Numero de personas que viven en el inmueble
        'documento', ///Certificado libertad
        'name_file', //Nombre del documento de certificado libertad
        'prop_inmu',
        'arrendatario',
        'vive',
        'deleted',
        //estado
    ];


    public function departamentopdf(){
      return $this->HasOne("App\Models\Departament","id_departamento","departamento");
    }

    public function municipiopdf(){
      return $this->HasOne("App\Models\Municipio","id_municipio","municipio");
    }

    public function tipoInmueble(){
      return $this->HasOne("App\Models\TipoInmueble", "id","tip_inmueble");
    }

    public function torreRes(){
      return $this->HasOne("App\Models\Unidad_res", "id_res","torre");
    }
}
