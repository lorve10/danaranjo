<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nivel_1 extends Model
{
    use HasFactory;


    protected $table = 'nivel_1';

    protected $fillable = [
        'id',
        'nombre',
        'codigo',
        'descripcion',
        'tipo',
        'naturaleza',
        'nit',
        'mvot',
        'factura',
        'cxc',
        'proveedores',
        'inventario',
        'activos',
        'saldo',
        'interes',
        'mora',
        'descuento',
        'retefuente',
        'base',
        'porcentaje',
        'efectivo',
        'idSubCuentas',
        'nivel',
        'deleted'
    ];






}
