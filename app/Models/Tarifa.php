<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarifa extends Model
{
    use HasFactory;
    protected $table = 'tarifa';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'nombre',
        'tarifa',
        'valor_interes',
        'id_concepto',
        'deleted',
    ];

    public function concepto(){
        return $this->HasMany("App\Models\Servicios","id_servicios","id_concepto");
    }
}
