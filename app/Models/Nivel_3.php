<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nivel_3 extends Model
{
    use HasFactory;


    protected $table = 'nivel_3';

    protected $fillable = [
        'id',
        'nombre',
        'codigo',
        'descripcion',
        'tipo',
        'naturaleza',
        'factura',
        'nit',
        'mvot',
        'cxc',
        'proveedores',
        'inventario',
        'activos',
        'saldo',
        'interes',
        'mora',
        'descuento',
        'retefuente',
        'base',
        'porcentaje',
        'idNivelTwo',
        'deleted'
    ];

    public function nivel4(){
       return $this->HasMany("App\Models\Nivel_4","niveltres","id")->where("deleted", 0);
   }
}
