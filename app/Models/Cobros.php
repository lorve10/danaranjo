<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cobros extends Model
{
    use HasFactory;

    protected $table = 'cobros';

    protected $fillable = [
        'id',
        'cobro',
        'concepto',
        'descuento',
        'inmueble',
        'fecha_gene',
        'mes_admin',
        'estado_pago',
        'fecha_pago',
        'pago_id',
        'deleted',
    ];

    public function cobros(){
        return $this->HasOne("App\Models\Inmueble","id","inmueble");
    }
    public function servi(){
      return $this->HasOne("App\Models\Servicios","id_servicios","concepto");      
    }

}
