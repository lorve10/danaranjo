<?php

namespace App\Models\FIModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grupos extends Model
{
    use HasFactory;

    protected $table='grupos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'codigo',
        'nombre_grupo',
        'clase_id',
        'deleted',
    ];

    public function rela_clases(){
        return $this->belongsTo("App\Models\FIModels\Clases","clase_id");
    }
}
