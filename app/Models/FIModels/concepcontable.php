<?php

namespace App\Models\FIModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class concepcontable extends Model
{
    use HasFactory;

    protected $table='concepcontable';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'nombre',
        'perteGrupo',
        'pertenNivel',
        'deleted',
    ];

    public function grupo(){
        return $this->HasOne("App\Models\Sub_cuentas","id","perteGrupo");
    }
    public function niveles(){
        return $this->HasOne("App\Models\Nivel_1","id","pertenNivel");
    }
}
