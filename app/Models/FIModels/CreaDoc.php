<?php

namespace App\Models\FIModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreaDoc extends Model
{
    use HasFactory;


        protected $table = 'creadocumento';

        protected $fillable = [
            'id',
            'tipo',
            'fecha',
            'datos',
            'deleted'
        ];

        public function persona(){
            return $this->belongsTo("App\Models\Terceros","tercero");
        }
        public function cuenta(){
            return $this->HasOne("App\Models\Nivel_1", "id","cuenta");
        }
        public function tipo(){
            return $this->belongsTo("App\Models\FIModels\TipoDoc","tipo");
        }
}
