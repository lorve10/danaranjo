<?php

namespace App\Models\FIModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoDoc extends Model
{
    use HasFactory;

    protected $table='tipodocumento';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'prefijo',
        'nombre',
        'titulo',
        'conInicial',
        'conFinal',
        'comprobante',
        'resolucion',
        'num_inicial',
        'num_final',
        'fecha_vig',
        'fecha_res',
        'deleted',
    ];
}
