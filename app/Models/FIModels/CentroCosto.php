<?php

namespace App\Models\FIModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CentroCosto extends Model
{
    use HasFactory;

    protected $table = 'centrocosto';

    protected $fillable = [
        'id',
        'codigo',
        'nombre',
        'tipo',
        'id_centro',
        'deleted'
    ];
}
