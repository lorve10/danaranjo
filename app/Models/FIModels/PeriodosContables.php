<?php

namespace App\Models\FIModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PeriodosContables extends Model
{
    use HasFactory;

    protected $table='periodo_contable';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fecha_inicial',
        'fecha_final',
        'deleted',
    ];
}
