<?php

namespace App\Models\FIModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuentas extends Model
{
    use HasFactory;

    protected $table='cuentas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'codigo_cuenta',
        'nombre_cuenta',
        'grupo_id',
        'clase_id',
        'deleted',
    ];

    public function rela_clases(){
        return $this->belongsTo("App\Models\FIModels\Clases","clase_id");
    }

    public function rela_grupos(){
        return $this->HasOne("App\Models\FIModels\Grupos", "codigo", "grupo_id");
    }
}
