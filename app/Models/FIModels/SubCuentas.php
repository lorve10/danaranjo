<?php

namespace App\Models\FIModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCuentas extends Model
{
    use HasFactory;

    protected $table='subcuentas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'codigo_subcuenta',
        'nombre_subcuenta',
        'cuenta_id',
        'grupo_id',
        'clase_id',
        'nivel',
        'deleted',
    ];

    public function rela_clases(){
        return $this->belongsTo("App\Models\FIModels\Clases","clase_id");
    }

    public function rela_grupos(){
        return $this->HasOne("App\Models\FIModels\Grupos", "codigo", "grupo_id");
    }

    public function rela_cuentas(){
        return $this->HasOne("App\Models\FIModels\Cuentas", "codigo_cuenta", "cuenta_id");
    }
}
