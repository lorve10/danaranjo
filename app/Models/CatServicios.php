<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatServicios extends Model
{
    use HasFactory;
    protected $table = 'cat_servic';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_cat',
        'nombre_cat',
        'deleted',

    ];
}
