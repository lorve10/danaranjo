<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cargosconjunto extends Model
{
    use HasFactory;

    protected $table = 'cargosconjunto';

    protected $fillable = [
        'id',
        'cargo',
        'nombre',
        'telefono',
        'sub_id',
        'correo',
        'fechaini',
        'fechafin',
        'deleted',
    ];
}
