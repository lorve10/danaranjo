<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nivel_2 extends Model
{
    use HasFactory;


    protected $table = 'nivel_2';

    protected $fillable = [
        'id',
        'nombre',
        'codigo',
        'descripcion',
        'tipo',
        'naturaleza',
        'nit',
        'mvot',
        'factura',
        'cxc',
        'proveedores',
        'inventario',
        'activos',
        'saldo',
        'interes',
        'mora',
        'descuento',
        'retefuente',
        'base',
        'porcentaje',
        'idNivelOne',
        'deleted'
    ];
    public function nivel3(){
       return $this->HasMany("App\Models\Nivel_3","idNivelTwo","id")->where("deleted", 0);
   }

}
