<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Auth;
use PDF;
use Mail;
use App\Models\Robot;
use App\Models\Cobros;
use App\Models\Inmueble;
use Log;


class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:SendEmails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio masivo de correos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $consulta = Robot::where('estado',0)->get();
        foreach ($consulta as $value) {
          $consulta_cobros = Cobros::with('cobros.tarifa_inmueble', 'cobros.tipo_inmueble', 'cobros.propietario')->where('concepto','=',$value->concepto)
          ->whereDate('mes_admin','>=',$value->fechaini)->whereDate('mes_admin','<=',$value->fechaend)->get();

          foreach ($consulta_cobros as $value_cobros) {
            $datato = [];
            array_push($datato, $value_cobros);
            $data = [
              'cobros' => $datato
            ];

            $data['correo'] = $value_cobros->cobros->propietario->correo;
            Log::info(json_encode($data));
            $pdf = PDF::loadView('admon.vista-pdf', $data);
            Mail::send([], $data, function ($mail) use ($data ,$pdf) {
                $mail->from('centroscomercialesone@gmail.com', 'Danaranjo');
                $mail->subject('Cuenta de cobro');
                $mail->to($data['correo'] );
                $mail->attachData($pdf->output(), 'cobro.pdf');
            });

          }

        }
    }
}
