<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use DB;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AdmonController2 extends Controller
{
  public function prueba (){
    $data = DB::table('prueba')
    ->whereIn('cuenta_id', [8,9])
    ->get();

    foreach($data as $value){
      $concepto = preg_replace('/[0-9]+/', '', $value->concepto);
      $codigo = preg_replace('/[^0-9]/', '', $value->concepto);
      //Log::info("$concepto => $codigo");
      DB::table('prueba')->where('id',$value->id)->update([
        'concepto' => $concepto,
        'codigo' => $codigo,
      ]);
    }

  }
   
}
