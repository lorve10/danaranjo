<?php

namespace App\Http\Controllers;

use App\Models\Arrendatarios;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArrendatarioController extends Controller
{
    //
    public function guard_arrendatario( Request $request){
        if ($request['id'] > 0) {

          $img = $request->file('foto')->store('fotos_propietarios','public');
          $img = "storage/".$img;

          if($request['vive'] == null){
            $arrend = "";
            $estado = 0;
          }else {
            $arrend = $request['vive'];
            $estado = 1;
          }

          Arrendatarios::where('id',$request['id'])->update([
            'tip_prop' => 2,
            'tip_ident' => $request['tip_ident'],
            'n_document' => $request['n_document'],
            'name' => $request['name'],
            'secname' => $request['secname'],
            'lastname' => $request['lastname'],
            'seclastname' => $request['seclastname'],
            'expedido' => $request['expedido'],
            'foto' => $img,
            'addres' => $request['addres'],
            'departamento' => $request['departamento'],
            'municipio' => $request['municipio'],
            'phone' => $request['phone'],
            'mobile' => $request['mobile'],
            'profesion' => $request['profesion'],
            'estado_civil' => $request['estado_civil'],
            'tip_inmueble' => $request['tip_inmueble'],
            'torre' => $request['torre'],
            'correo' => $request['correo'],
            'vehiculo' => $request['vehiculo'],
            'marca' => $request['marca'],
            'placa' => $request['placa'],
            'color' => $request['color'],
            'numero_per' => $request['numero_per'],
            'arrendatario' => $estado,
            'vive' => $arrend
            
      
      
          ]);
        }
        else{

          $img = $request->file('foto')->store('fotos_propietarios','public');
          $img = "storage/".$img;

          if($request['vive'] == null){
            $arrend = "";
            $estado = 0;
          }else {
            $arrend = $request['vive'];
            $estado = 1;
          }
          
          Arrendatarios::create([
            'tip_prop' => 2,
            'tip_ident' => $request['tip_ident'],
            'n_document' => $request['n_document'],
            'name' => $request['name'],
            'secname' => $request['secname'],
            'lastname' => $request['lastname'],
            'seclastname' => $request['seclastname'],
            'expedido' => $request['expedido'],
            'foto' => $img,
            'addres' => $request['addres'],
            'departamento' => $request['departamento'],
            'municipio' => $request['municipio'],
            'phone' => $request['phone'],
            'mobile' => $request['mobile'],
            'profesion' => $request['profesion'],
            'estado_civil' => $request['estado_civil'],
            'tip_inmueble' => $request['tip_inmueble'],
            'torre' => $request['torre'],
            'correo' => $request['correo'],
            'vehiculo' => $request['vehiculo'],
            'marca' => $request['marca'],
            'placa' => $request['placa'],
            'color' => $request['color'],
            'numero_per' => $request['numero_per'],
            'arrendatario' => $estado,
            'vive' => $arrend
      
          ]);
        }
        $response = true;
        return $response;
      }

      public function obtain_arrendatario(){
        $terce = Arrendatarios::with("departamentopdf","municipiopdf","tipoInmueble","torreRes")->get();
        return $terce;
      }

      public function deleted_arrend (Request $request){
        Arrendatarios::where('id',$request['id'])->update([
        'deleted'=>1
      ]);
      $response = true;
      return $response;
      }
    
      public function habilited_arrend (Request $request){
        Arrendatarios::where('id',$request['id'])->update([
        'deleted'=>0
      ]);
      $response = true;
      return $response;
      }
}
