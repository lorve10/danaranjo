<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Inmueble;
use App\Models\Tarifa;
use App\Models\Terceros;
use App\Models\TipoInmueble;
use App\Models\Cobros;
use App\Models\Unidad_res;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InmuebleController extends Controller
{
  public function guard_inmu(Request $request){

    try {
      $numeroInmu = Inmueble::where('numero', '=', $request['numero'])->where('torre','=',$request['torre'])->first();

      $id = $request['id'];

      $num_par = Inmueble::where('num_parq','=', $request['num_parq'])->where('num_parq', '!=',0)->first();
      $num_depo = Inmueble::where('num_depo','=', $request['num_depo'])->where('num_depo', '!=',0)->first();

      $num_par2 = Inmueble::where('num_parq','=', $request['num_parq'])->where('id','!=',$id)->where('num_parq','!=',0)->first();
      $num_depo2 = Inmueble::where('num_depo','=', $request['num_depo'])->where('id','!=',$id)->where('num_depo','!=',0)->first();

      $data['tip_inmueble'] = $request['tip_inmueble'];
      $data['torre'] = $request['torre'];
      $data['parqueadero'] = $request['parqueadero'];
      $data['deposito'] = $request['deposito'];
      $data['num_depo'] = $request['num_depo'];
      $data['num_parq'] = $request['num_parq'];
      $data['numero'] = $request['numero'];
      $data['area_mt2'] = $request['area_mt2'];
      $data['tarifa'] = $request['tarifa'];

      if($id > 0){
        if ($num_par2) {
          $response = 3;
          return $response;
        }
        else if ($num_depo2) {
          $response = 4;
          return $response;
        }
        else {
        Inmueble::find($id)->update($data);
        }
      }
      else{
        if($numeroInmu){
          $response = 2;
          return $response;
        }
        else if ($num_par) {
          $response = 3;
          return $response;
        }
        else if ($num_depo) {
          $response = 4;
          return $response;
        }
        else {
          Inmueble::create($data);
          return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);
        }
      }

    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false, 'linea' => $e->getLine()], 500);
    }

  }

    public function guard_inmu_massive(Request $request){
      try {
        $masivo = json_decode($request['data']);


        foreach ($masivo as $value) {
          $datanum = Inmueble::where('numero', '=', $value->numero_inmueble)
          ->where('torre', '=', $value->torre)->first();

          $dataTipo = Inmueble::where('tip_inmueble','=', $value->tipo_inmueble)->first();
          // $datares = TipoInmueble::where('nombre_res','=', $value->torre)->first();
          ///lolsito ???

          $data['tip_inmueble'] = $value->tipo_inmueble;
          $data['torre'] = $value->torre;
          $data['parqueadero'] = $value->parqueadero;
          $data['num_parq'] = $value->numero_parqueadero;
          $data['deposito'] = $value->deposito;
          $data['num_depo'] = $value->numero_deposito;
          $data['numero'] = $value->numero_inmueble;
          $data['area_mt2'] = $value->area;
          $data['tarifa'] = $value->tarifa;

          if($datanum != null){
            $response  = 2;
            return $response;
          }
          else{
            Inmueble::create($data);
          }

        }
        return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

      }  catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false, 'linea' => $e->getLine()], 500);

      }

    }

    public function guard_tipInmueble(Request $request){
      log::info("entroooo tipo de inmueble");
      $data = TipoInmueble::where('nombre', '=', $request['nombre'])->first();

        if ($request['id'] > 0) {
          TipoInmueble::where('id',$request['id'])->update([
            'nombre' => $request['nombre'],
          ]);
          $response = true;
          return $response;
        }
        else if($data == null){
          TipoInmueble::create([
            'nombre' => $request['nombre'],

          ]);
          $response = true;
          return $response;
        }else {
          $response = false;
          return $response;
        }

    }

    public function guard_unidadRes(Request $request){
      log::info("entroooo 2");
      $data = Unidad_res::where('nombre_res', '=', $request['nombre_res'])
      ->where('tip_inm','=',$request['tip_inm']) ->first();

        if ($request['id_res'] > 0) {
          Unidad_res::where('id_res',$request['id_res'])->update([
            'tip_inm' => $request['tip_inm'],
            'nombre_res' => $request['nombre_res'],
          ]);
          $response = true;
          return $response;
        }
        else if($data == null){
          Unidad_res::create([
            'tip_inm' => $request['tip_inm'],
            'nombre_res' => $request['nombre_res'],

          ]);
          $response = true;
          return $response;
        }else {
          $response = false;
          return $response;
        }

    }

    public function obtain_tarifa(){
      $tarifa = Tarifa::where('deleted', 0)->get();
      return $tarifa;
    }

    public function obtain_tipo(){
      $tipo = TipoInmueble::where('deleted', 0)->get();
      return $tipo;
    }

    public function obtain_unidad(){
      $tipoUnidad = Unidad_Res::with("tipo_inmueble_res")->where('deleted', 0)->get();
      return $tipoUnidad;
    }

    public function obtain_Inmu(){


      $data = Inmueble::with("tipo_inmueble", "tipo_unidad", "propietario", "tarifa_inmueble","cobro_inmueble")->orderBy('tip_inmueble', 'asc')->orderBy('torre', 'asc')->orderBy('numero', 'asc')->get();

      return $data;
    }

    public function deleted_inmueble(Request $request){

      if(Cobros::where('inmueble', $request['id'])->exists()){
        Log::info("no se elimino");

      }
      else{
        Log::info("se elimino");
        Inmueble::where('id', $request['id'])->update([
          'deleted'=>1
        ]);
        $response = true;
        return $response;
      }


    }

    public function vistaInm()
    {
      return view('admon.InmuebleAdmon');

    }

    public function habilited_inmueble(Request $request){
        $response = Inmueble::where('id', $request['id'])->update([
          'deleted'=> 0
        ]);
        $response = true;
        return $response;
    }




}
