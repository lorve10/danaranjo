<?php

namespace App\Http\Controllers\Financiero;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FIModels\CreaDoc;
use App\Models\FIModels\PeriodosContables;
use App\Models\Nivel_1;
use Log;
use DB;
use Auth;
use PDF;
use Mail;

class CrearDocumentosController extends Controller
{
    //
    public function CrearDocumentos()
    {
        return view('financiero.CrearDocumentosFinanciero');
    }
    public function pdf()
    {
        return view('financiero.vistaPdf');
    }

    public function obtain_doc() {
        $data = CreaDoc::with("tipo","persona","cuenta")->where("deleted",0)->get();
        return $data;
    }

    public function obtain_nivel()
    {
      $data = Nivel_1::where("deleted",0)->get();
      return $data;
    }

    public function crear(Request $request){

      try {

        $fechaDocu = $request['fecha'];

        $fechaPeriodo = PeriodosContables::where('deleted',0)
                                        ->whereDate('fecha_inicial', '<=', $fechaDocu)
                                        ->whereDate('fecha_final', '>=', $fechaDocu)
                                        ->count();

        Log::info($fechaPeriodo);

        $lista = json_decode($request['lista']);

        $data['tipo'] = $request['tipo'];
        $data['fecha'] = $request['fecha'];
        $data['datos'] = $request['lista'];
          //crear cargos
          if($fechaPeriodo == 0){
            $response = 2;
            return $response;
          }else{
            CreaDoc::create($data);
          }

        return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);


      } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false, 'linea' => $e->getLine()], 500);
      }
    }

    public function imprimir(Request $request){

      $data = [
        'data' => json_decode($request['data'])
      ];

      Log::info($data);
       $pdf = PDF::loadView('financiero.vistaPdf', $data);
       return $pdf->stream('descargar.pdf');
    }

}
