<?php

namespace App\Http\Controllers\Financiero;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;

use App\Models\FIModels\PeriodosContables;

class PeriodosContablesController extends Controller
{
    //
    public function PeriodosContables()
    {
        return view('financiero.PeriodosContablesFinanciero');
    }

    public function guardPeriodoContable(Request $request) {
        try {
            $prop = PeriodosContables::where('fecha_inicial', '=', $request['fecha_inicial'])->first();
            $prop2 = PeriodosContables::where('fecha_final', '=', $request['fecha_final'])->first();

            $id = $request['id'];

            $data['fecha_inicial'] = $request['fecha_inicial'];
            $data['fecha_final'] = $request['fecha_final'];

            if($prop){
                $response = 2;
                return $response;
            }else if($prop2){
                $response = 3;
                return $response;
            }else{
                PeriodosContables::create($data);
            }

            return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

        } catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage(), 'success' => false, 'linea' => $e->getLine()], 500);
        }
    }

    public function obtain_PeriodosContables(Request $request) {
        $data = PeriodosContables::get();
        return $data;
    }

    public function habilited_periodoContable (Request $request){
        PeriodosContables::where('id',$request['id'])->update([
            'deleted'=> 0
        ]);
        $response = true;
        return $response;
    }

    public function deleted_periodoContable (Request $request){
        PeriodosContables::where('id',$request['id'])->update([
            'deleted'=> 1
        ]);
        $response = true;
        return $response;
    }
}
