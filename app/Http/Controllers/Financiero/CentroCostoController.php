<?php

namespace App\Http\Controllers\Financiero;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FIModels\CentroCosto;


class CentroCostoController extends Controller
{
  public function CentroCosto()
  {
    return view('financiero.CentroCostoFinanciero');
  }

  public function guard_costo(Request $request)
  {
    try {

      $data['codigo'] = $request['codigo'];
      $data['nombre'] = $request['nombre'];
      $data['tipo'] = $request['tipo'];
      $data['id_centro'] = $request['id_centro'];

      CentroCosto::create($data);
      return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);



    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false, 'linea' => $e->getLine()], 500);
    }
  }

  public function obtener_costo(){
    $data = CentroCosto::where('deleted',0)->get();
    return $data;
  }

  public function obtener_centro(){
    $data = CentroCosto::where('tipo',1)->get();
    return $data;
  }

  public function deletedCentro(Request $request){
    try {
        CentroCosto::where('id',$request['id'])->update([
        'deleted'=>1
        ]);
        return response()->json([ 'message' => "Successfully deleted", 'success' => true ], 200);
        }catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }

  }

}
