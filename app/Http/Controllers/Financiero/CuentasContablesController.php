<?php

namespace App\Http\Controllers\Financiero;

use Log;
use App\Models\FIModels\Clases;
use App\Models\FIModels\Grupos;
use App\Models\FIModels\Cuentas;
use App\Models\FIModels\SubCuentas;
use App\Models\Sub_cuentas;
use App\Models\Nivel_1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CuentasContablesController extends Controller
{
    //
    public function obtain_Cuentas(Request $request){
        $data = Cuentas::with("rela_clases", "rela_grupos")->get();

        return $data;
    }

    public function obtain_Subcuentas(){
        $data = SubCuentas::with("rela_clases", "rela_grupos", "rela_cuentas")->get();

        return $data;
    }

    public function obtain_grupo(Request $request){
        $data = Grupos::with("rela_clases")->where('clase_id', '=', $request['id'])->get();
        return $data;
    }

    public function obtain_Nivel(){
      $res = Nivel_1::where("deleted",0)->get();
      return $res;
    }

    public function filtro_nivel(Request $request){
      $res = Nivel_1::where("codigo", $request['codigo'])->get();
      return $res;
    }

    //////// pasando a parametrizacion
    public function CuentaContable()
    {
      return view('admon.CuentaContableAdmon');
    }
    public function obtain_cuenta ()
    {
      $data =  Sub_cuentas::with("cuentas_id")->where("deleted", 0)->get();
      return $data;
    }

    public function guard_Niveles(Request $request){

      try {

        $tipo_nivel = $request['tipo_nivel'];
        $id = $request['id'];


        $data['nombre'] = $request['nombre'];
        $data['codigo'] = $request['codigo'];
        $data['tipo'] = $request['tipo'];
        $data['naturaleza'] = $request['naturaleza'];
        $data['nit'] = $request['nit'];
        $data['mvot'] = $request['mvot'];
        $data['factura'] = $request['factura'];
        $data['cxc'] = $request['cxc'];
        $data['proveedores'] = $request['proveedores'];
        $data['inventario'] = $request['inventario'];
        $data['activos'] = $request['activos'];
        $data['saldo'] = $request['saldo'];
        $data['interes'] = $request['interes'];
        $data['mora'] = $request['mora'];
        $data['descuento'] = $request['descuento'];
        $data['retefuente'] = $request['retefuente'];
        $data['base'] = $request['base'];
        $data['porcentaje'] = $request['porcentaje'];
        $data['efectivo'] = $request['efectivo'];


        if($id > 0){
              Nivel_1::find($id)->update($data);
        }
        else{
              $data['idSubCuentas'] = $request['idSubCuentas'];
              $data['nivel'] = $request['nivel'];
              Nivel_1::create($data);
          }

        return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

      }catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }
    }

    // public function deshabilitar_One(Request $request){
    //     Nivel_1::where('id',$request['id'])->update([
    //     'deleted'=>1
    //     ]);
    //
    //   $response['success'] = true;
    //   return $response;
    // }

    public function traer_codigo(){
      $res = Nivel_1::where("deleted",0)->get();
      return $res;
    }

    public function obtenerCodigo(Request $request){
      $resNiv1 = Nivel_1::where("codigo",'=',$request['codigo'])->first();
      $sub =  SubCuentas::where("codigo_subcuenta",'=',$request['codigo'])->first();

      $response = [];

      if($resNiv1 == null){
        $response = SubCuentas::where("codigo_subcuenta",'=',$request['codigo'])->get();
        return   $response;
      }
      if($sub == null) {
        $response = Nivel_1::where("codigo",'=',$request['codigo'])->get();
        return   $response;
      }
    }
}
