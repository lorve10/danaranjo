<?php

namespace App\Http\Controllers\Financiero;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FIModels\concepcontable;


class ConceptoController extends Controller
{
  public function Concepto()
  {
    return view('financiero.ConceptoFinanciero');
  }

  public function guardConcepto(Request $request) {
      try {
          $id = $request['id'];

          $data['nombre'] = $request['nombre'];
          $data['pertenNivel'] = $request['pertenNivel'];

          concepcontable::create($data);

          return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

      } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false, 'linea' => $e->getLine()], 500);
      }
  }

  public function getConcep(){
    $data = concepcontable::with("niveles")->where('deleted',0)->get();
    return $data;
  }

  public function deleted(Request $request){
    try {
        concepcontable::where('id',$request['id'])->update([
        'deleted'=>1
        ]);
        return response()->json([ 'message' => "Successfully deleted", 'success' => true ], 200);
        }catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }

  }



}
