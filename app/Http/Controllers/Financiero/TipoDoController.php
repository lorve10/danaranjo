<?php

namespace App\Http\Controllers\Financiero;

use Log;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FIModels\TipoDoc;


class TipoDoController extends Controller
{
  public function documento()
  {
    return view('financiero.DocumentoFinanciero');
  }


  public function guardDoct(Request $request) {
      try {
          $id = $request['id'];

          $data['prefijo'] = $request['prefijo'];
          $data['nombre'] = $request['nombre'];
          $data['titulo'] = $request['titulo'];
          $data['conInicial'] = $request['conInicial'];
          $data['conFinal'] = $request['conFinal'];
          $data['comprobante'] = $request['comprobante'];
          $data['resolucion'] = $request['resolucion'];
          $data['num_inicial'] = $request['num_inicial'];
          $data['num_final'] = $request['num_final'];
          $data['fecha_vig'] = $request['fecha_vig'];
          $data['fecha_res'] = $request['fecha_res'];

          TipoDoc::create($data);

          return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

        } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false, 'linea' => $e->getLine()], 500);
        }
    }
    public function obtain_documento(Request $request) {
        $data = TipoDoc::where("deleted",0)->get();
        return $data;
    }

    public function notification(Request $request){
        $fechaHoy = date('Y-m-d');
        Log::info($fechaHoy);
        $fecha = TipoDoc::whereDate('fecha_vig','=', $fechaHoy)->count();

        return $fecha;
        return response()->json([ 'message' => "Successfully created", 'success' => true, 'res' => $fecha ], 200);

    }
}
