<?php

namespace App\Http\Controllers;

use Log;
use PDF;
use Mail;
use App\Models\Pago;
use App\Models\Cobros;
use App\Models\Pago_cuenta;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PurseController extends Controller
{
  public function guard_purse(Request $request){



    log::info("entroooo");
    if ($request['id'] > 0) {
      echo "no tiene actulizar prro";
    }
    $data = Cobros::with('cobros.tarifa_inmueble', 'cobros.tipo_inmueble', 'cobros.propietario')->where('inmueble','=',$request['inmueble'])->where('concepto','=',$request['concepto'])
    ->whereDate('mes_admin','>=',$request['fechaini'])->whereDate('mes_admin','<=',$request['fechaend'])->first();
    Log::info("aqui cobroooo");
    Log::info($data);
    if($data != null){
      $pago = $data->cobro - $request['valor'];
      Log::info($pago);


       if($pago == null){
        Log::info("entro a no hacer pago");
        $response = false;
        return $response;
      }
      else if($pago == 0){
            $pagoTotal = Pago::create([
              'fecha' => $request['fecha'],
              'mes' => $request['mes'],
              'inmueble' => $request['inmueble'],
              'tip_pago' => $request['tip_pago'],
              'valor' => $request['valor'],
              'concepto' => $request['concepto'],
              'interes' => $request['interes'],
              'observa' => $request['observa'],
            ]);

           Cobros::where('id',$data->id)->update([
            'estado_pago' => 1,
            'fecha_pago' => $request['mes'],
          ]);
      }
      else {
        $pagoTotal = Pago::create([
          'fecha' => $request['fecha'],
          'mes' => $request['mes'],
          'inmueble' => $request['inmueble'],
          'tip_pago' => $request['tip_pago'],
          'valor' => $request['valor'],
          'concepto' => $request['concepto'],
          'interes' => $request['interes'],
          'observa' => $request['observa'],
        ]);

         Cobros::where('id',$data->id)->update([
         'cobro' => $pago,
         'estado_pago' => 2,
         'fecha_pago' => $request['mes'],
         ]);
      }
        // $data = Cobro::with('cobros.tarifa_inmueble', 'cobros.tipo_inmueble', 'cobros.propietario')
        // ->where('inmueble', $request['inmueble'])->where('mes_admin', $request['mes'])->first();

      $response = true;
      return $response;

    }
    else{

          $response = false;
          return $response;
    }



  }

    public function pagos()
    {
      return view('admon.PagosAdmon');

    }

    public function obtain_pago(){
      ///$data = Pago::with("inmueble.tipo_inmueble","inmueble.propietario","pagos_cuent")->where('deleted',0)->get();
      $data = Pago::with("inmueble.cobro_inmueble","concept","inmueble.tipo_inmueble","inmueble.propietario.departamentopdf", "inmueble.propietario.municipiopdf")->where('deleted', 0)->get();
      return $data;
      // $data = Pago::with( "inmueble.tipo_inmueble","inmueble.propietario", "concept")->where('deleted', 0)->get();
      // return $data;
    }
    public function delete_purse(Request $request){
        Pago::where('id',$request['id'])->update([
        'deleted'=>1
      ]);
    }

    public function pagos_pdf(Request $request)
    {
      function formatMoney($number, $fractional=false) {
          if ($fractional) {
              $number = sprintf('%.2f', $number);
          }
          while (true) {
              $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
              if ($replaced != $number) {
                  $number = $replaced;
              } else {
                  break;
              }
          }
          return $number;
      }

     $respuesta = json_decode($request['data']);
     $valor = $respuesta->valor;
     $cobro = $respuesta->inmueble->cobro_inmueble->cobro;


     $data = [
       'pago' => $respuesta,
       'valor' => formatMoney($valor),
       'cobro' => formatMoney($cobro)


     ];
     Log::info($data);
     $pdf = PDF::loadView('admon.vista-pdf-pago', $data);
     return $pdf->stream('descargar.pdf');
    }
    public function enviar_pago(Request $request){

      $respuesta = json_decode($request['data']);

      $data = [
        'pago' => $respuesta
      ];
      $data['correo'] = $respuesta->inmueble->propietario->correo;
      Log::info($data['correo']);

       $pdf = PDF::loadView('admon.vista-pdf-pago', $data);
       Mail::send([], $data, function ($mail) use ($data ,$pdf) {
           $mail->from('centroscomercialesone@gmail.com', 'Danaranjo');
           $mail->to($data['correo'] );
           $mail->attachData($pdf->output(), 'pago.pdf');
       });
       echo "<script>window.confirm('Resibo de pago enviado exitosamente');
         window.history.back();
       </script>";
    }



}
