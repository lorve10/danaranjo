<?php

namespace App\Http\Controllers;

use Log;
use DB;
use Auth;
use PDF;
use Mail;
use App\Models\Cobros;
use App\Models\Inmueble;
use App\Models\Robot;
use App\Models\Admon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
///use Barryvdh\DomPDF\Facade as PDF;


class CobrosController extends Controller
{
    //

    public function obtain_cobros_mes(Request $request)
    {
      Log::info($request);
      $response['data'] = Cobros::with('servi','cobros.tarifa_inmueble','cobros.tipo_inmueble','cobros.propietario')->where('inmueble',$request['id'])->where('concepto','=',$request['concepto'])
      ->whereDate('mes_admin','>=',$request['fechaini'])->whereDate('mes_admin','<=',$request['fechaend'])->get();
      $response['success'] = true;
      return $response;
    }
    public function obtain_cobros_print(Request $request)
    {
      $response['data'] = Cobros::with('servi','cobros.tarifa_inmueble', 'cobros.tipo_inmueble', 'cobros.propietario')->where('concepto','=',$request['concepto'])
      ->whereDate('mes_admin','>=',$request['fechaini'])->whereDate('mes_admin','<=',$request['fechaend'])->get();
      $response['success'] = true;
      return $response;
    }

    public function vistaCobros()
    {
      return view('admon.CobrosAdmon');
    }

    public function obtain_Inmu(){
      $data = Inmueble::with("tipo_inmueble","propietario","tarifa_inmueble")->where('deleted',0)->get();
      return $data;
    }
////->tipo_inmueble->nombre;
    public function guard_Cobro(Request $request)
    {
      $data = json_decode($request['data']);
      $response['success'] = false;
      Log::info($data);

      foreach ($data as $value) {
        $valida = $value->id;
      }


      if(!Cobros::where('concepto','=', $request['concepto'])->where('inmueble','=', $valida)->whereDate('mes_admin','>=',$request['fechaini'])->whereDate('mes_admin','<=',$request['fechaend'])->exists()){
        if($request['cobro'] == null){
          foreach ($data as $value) {
            Log::info('entro a crear');
            Log::info(json_encode($value));
                Cobros::create([
                  'cobro' => $value->tarifa_inmueble->tarifa,
                  'inmueble' => $value->id,
                  'descuento' => $request['descuento'],
                  'concepto' => $request['concepto'],
                  'fecha_gene' => $request['fecha_gene'],
                  'mes_admin' => $request['mes_admin'],
                ]);
                $response = true;
          }
          return $response;

        }else{
          foreach ($data as $value) {
            Log::info('entro a crear');
            Log::info(json_encode($value));
                Cobros::create([
                  'cobro' => $request['cobro'],
                  'inmueble' => $value->id,
                  'descuento' => $request['descuento'],
                  'concepto' => $request['concepto'],
                  'fecha_gene' => $request['fecha_gene'],
                  'mes_admin' => $request['mes_admin'],
                ]);
                $response = true;
          }
          return $response;

        }

      }
  }

    public function obtain_cobro(){
      $data = Cobros::with('servi','cobros.tarifa_inmueble', 'cobros.tipo_inmueble', 'cobros.propietario')->where('deleted', 0)->get();
      return $data;
    }
    ////generar pdf

    public function download(Request $request)
    {
      function formatMoney($number, $fractional=false) {
          if ($fractional) {
              $number = sprintf('%.2f', $number);
          }
          while (true) {
              $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1.$2', $number);
              if ($replaced != $number) {
                  $number = $replaced;
              } else {
                  break;
              }
          }
          return $number;
      }

    $res = json_decode($request['data']);
    setlocale(LC_MONETARY, 'es_CO');

    foreach ($res as $value) {
        $valor = $value->cobro;
        $descuento = $value->descuento;
    }
      $restar =   $valor * $descuento/100;
      $result = $valor - $restar;

     $data = [
       'cobros' => $res,
        'pagar' => formatMoney($valor),
        'descuento' => $descuento,
        'saldo' => formatMoney($result),
       // 'd' => $_GET[$valor],
     ];
    $pdf = PDF::loadView('admon.vista-pdf', $data);
    return $pdf->stream('descargar.pdf');
    }

///enviar correos pdf uno `por uno`
    public function enviar(Request $request){

      $data = [
        'cobros' => json_decode($request['data'])
      ];
      $data2 = json_decode($request['data']);
      foreach ($data2 as  $value) {
        $data['correo'] = $value->cobros->propietario->correo;
      };
       $pdf = PDF::loadView('admon.vista-pdf', $data);
       Mail::send([], $data, function ($mail) use ($data ,$pdf) {
           $mail->from('centroscomercialesone@gmail.com', 'Danaranjo');
           $mail->to($data['correo'] );
           $mail->attachData($pdf->output(), 'cobro.pdf');
       });
       echo "<script>window.confirm('Cuenta de cobro enviado exitosamente');
         window.history.back();
       </script>";
    }

    public function enviar_masivo(Request $request){
    /// pruba de enviar masivamente
      $data = [
        'cobros' => json_decode($request['data'])
      ];
      $data2 = json_decode($request['data']);
      foreach ($data2 as  $value) {
        $data['correo'] = $value->cobros->propietario->correo;
      };
       $pdf = PDF::loadView('admon.vista-pdf', $data);
       Mail::send([], $data, function ($mail) use ($data ,$pdf) {
           $mail->from('danaranjo@gmail.com', 'Danaranjo');
           $mail->to($data['correo'] );
           $mail->attachData($pdf->output(), 'cobro.pdf');
       });
       echo "<script>window.confirm('Cuenta de cobro enviado exitosamente');
         window.history.back();
       </script>";
    }

////Dar ordenenes al robot para ejecutar
    public function robot(Request $request){
        Robot::create([
          'fechaini' => $request['fechaini'],
          'fechaend' => $request['fechaend'],
          'concepto' => $request['concepto'],
        ]);

      $response = true;
      return $response;

    }
}
