<?php

namespace App\Http\Controllers;

use Log;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tarifa;

class CuotasController extends Controller
{
    //
    public function vistaCuot()
    {
      return view('admon.DuesAdmon');
    }

    public function guard_cuotas(Request $request){
        log::info("entroooo");
        if ($request['id'] > 0) {
          Tarifa::where('id',$request['id'])->update([
            'nombre' => $request['nombre'],
            'tarifa' => $request['tarifa'],
            'valor_interes' => $request['valor_interes'],
            'id_concepto' => $request['id_concepto'],
          ]);
        }
        else{
          Tarifa::create([
            'nombre' => $request['nombre'],
            'tarifa' => $request['tarifa'],
            'valor_interes' => $request['valor_interes'],
            'id_concepto' => $request['id_concepto'],
          ]);
        }
        $response = true;
        return $response;
    }

    public function obtain_Cuotas(){
        $tarifa = Tarifa::with("concepto")->where('deleted',0)->get();
        return $tarifa;
    }

    public function deleted_cuotas(Request $request){
        Tarifa::where('id', $request['id'])->update([
          'deleted'=>1
        ]);
    }

}
