<?php

namespace App\Http\Controllers;
use Log;
use DB;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Impuesto;
use App\Models\Egresos;
use App\Models\Egresos_concepto;

class EgresosController extends Controller
{
    //
    public function vistaEgresos()
    {
      return view('admon.EgresosAdmon');
    }
    public function obtain_Impu(){
      $data = Impuesto::where('deleted',0)->get();
      return $data;
    }
    public function obtain_egresos(){
      ///$data = Pago::with("inmueble.tipo_inmueble","inmueble.propietario","pagos_cuent")->where('deleted',0)->get();
      $data = Egresos::with("propietario", "concep")->where('deleted',0)->get();
      return $data;
    }
    public function guard_egreso(Request $request){


      if ($request['id'] > 0) {
        echo "hola mundo";
      }
      else{
       $egreso = Egresos::create([
          'fecha' => $request['fecha'],
          'mes' => $request['mes'],
          'ndocumento' => $request['n_documento'],
          'terceros' => $request['tercero'],
          'concepto' => $request['concepto'],
          'mt_pago' => $request['tip_pago'],
          'valor' => $request['valor'],
          'interes' => $request['interes'],
          'observaciones' => $request['observaciones'],
        ]);

      }
      $response = true;
      return $response;
    }
}
