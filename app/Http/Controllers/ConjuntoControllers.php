<?php

namespace App\Http\Controllers;

use Log;
use DB;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Conjunto;
use App\Models\AdminData;
use App\Models\Consejoadm;
use App\Models\Cargosconjunto;
use App\Models\Moneda;



class ConjuntoControllers extends Controller
{
  public function conjunto()
  {
    return view('admon.ConjuntoAdmon');
  }

  public function datosConjunto()
  {
    return view('admon.DatosConjuntoAdmon');
  }

  public function guard_conjunto (Request $request){
    log::info("entroooo");

    if ($request['id'] > 0) {

      if($request->file('file') == null ){
        $file = "";
      }else {
        $file = $request->file('file')->store('imagenes_conjunto','public');
        $file = "storage/".$file;
      }

      Conjunto::where('id',$request['id'])->update([
        'nombre' => $request['nombre'],
        'direccion' => $request['direccion'],
        'departamento' => $request['departamento'],
        'municipio' => $request['municipio'],
        'telefono' => $request['telefono'],
        'logo' => $file,
        'pais' => $request['pais'],
        'moneda' => $request['moneda']
      ]);
    }
    else{
      if($request->file('file') == null ){
        $file = "";
      }else {
        $file = $request->file('file')->store('imagenes_conjunto','public');
        $file = "storage/".$file;
      }
      Conjunto::create([
        'nombre' => $request['nombre'],
        'direccion' => $request['direccion'],
        'departamento' => $request['departamento'],
        'municipio' => $request['municipio'],
        'telefono' => $request['telefono'],
        'logo' => $file,
        'pais' => $request['pais'],
        'moneda' => $request['moneda']
      ]);
    }
    $response = true;
    return $response;

  }
  public function guard_datadmin (Request $request){
    log::info("entroooo");

    if ($request['id'] > 0) {
      $file = $request->file('firma')->store('imagen_firma','public');
      $file = "storage/".$file;
      AdminData::where('id',$request['id'])->update([
        'nombre' => $request['nombre'],
        'firma' => $file,
        'telefono' => $request['telefono'],
        'correo' => $request['correo'],
      ]);
    }
    else{
      $file = $request->file('firma')->store('imagen_firma','public');
      $file = "storage/".$file;
      AdminData::create([
        'nombre' => $request['nombre'],
        'firma' => $file,
        'telefono' => $request['telefono'],
        'correo' => $request['correo'],
      ]);
    }
    $response = true;
    return $response;

  }
  public function guard_consejo (Request $request){
    try {

      $lista = json_decode($request['cargo']);
      foreach ($lista as $value) {

        $data['fechaini'] = $request['fechaini'];
        $data['fechafin'] = $request['fechafin'];
        $data['cargo'] = $value->cargo;
        $data['nombre'] = $value->nombre;
        $data['telefono'] = $value->telefono;
        $data['correo'] = $value->correo;

        //crear cargos
        Cargosconjunto::create($data);
      }
      return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);


    } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }
  }
  public function guardar_cargos (Request $request){

      foreach ($data as $value) {
        Cargosconjunto::create([
           'cargo' => $value->cargo,
           'nombre' => $value->nombre,
           'telefono' =>$value->telefono,
         ]);
      }
      $response = true;
      return $response;
  }

  public function obtain_conjunto (){
    $data = Conjunto::with('ver_moneda')->where('deleted', 0)->get();
    return $data;
  }

  public function obtain_moneda (){
    $data = Moneda::where('deleted', 0)->get();
    return $data;
  }

  public function obtain_admin (){
    $data = AdminData::where('deleted', 0)->get();
    return $data;
  }
  public function obtain_consejo (){
    $data = Consejoadm::where('deleted', 0)->get();
    return $data;
  }
  public function obtain_cargos (){
    $data = Cargosconjunto::where('deleted', 0)->get();
    return $data;
  }
  public function deleted_cargos (Request $request){
    Cargosconjunto::where('id',$request['id'])->update([
    'deleted'=>1
  ]);
    $response = true;
    return $response;
  }

  public function Propietarios()
  {
    return view('admon.PropietariosAdmon');
  }
}
