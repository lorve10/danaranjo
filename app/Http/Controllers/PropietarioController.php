<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Propietarios;
use App\Models\Inmueble;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PropietarioController extends Controller
{
    //

    public function guard_propietario(Request $request){

          try {

            $prop = Propietarios::where('n_document', '=', $request['n_document'])->first();

            $id = $request['id'];
            $tip_prop = $request['tip_prop'];

            if ($request->file('foto')==null && $id==0)
            {
              return response()->json([ 'message' => "La imagen es obligatoria", 'success' => false ], 200);
            }

            $torres = json_decode($request['torre']);

            if ($request->file('documento')==null && $id==0 && $tip_prop==1)
            {
              return response()->json([ 'message' => "El documento es obligatorio", 'success' => false ], 200);
            }

            $data['tip_prop'] = $tip_prop;
            $data['tip_ident'] = $request['tip_ident'];
            $data['n_document'] = $request['n_document'];
            $data['name'] = $request['name'];
            $data['secname'] = $request['secname'];
            $data['lastname'] = $request['lastname'];
            $data['seclastname'] = $request['seclastname'];
            $data['expedido'] = $request['expedido'];
            $data['addres'] = $request['addres'];
            $data['departamento'] = $request['departamento'];
            $data['municipio'] = $request['municipio'];
            $data['phone'] = $request['phone'];
            $data['mobile'] = $request['mobile'];
            $data['profesion'] = $request['profesion'];
            $data['estado_civil'] = $request['estado_civil'];
            $data['tip_inmueble'] = $request['tip_inmueble'];
            $data['torre'] = $request['torre'];
            $data['correo'] = $request['correo'];
            $data['vehiculo'] = $request['vehiculo'];
            $data['marca'] = $request['marca'];
            $data['placa'] = $request['placa'];
            $data['color'] = $request['color'];
            $data['numero_per'] = $request['numero_per'];
            $data['prop_inmu'] = $request['prop_inmu'];
            $data['arrendatario'] = $request['arrendatario'];
            $data['vive'] = $request['vive'];

            if ($request->file('foto'))
            {
              // almacena y captura el nombre del archivo
              $image = $request->file('foto');
              $data['foto'] = $image->store('fotos_propietarios','public');
            }

            if ($request->file('documento'))
            {
              // almacena y captura el nombre del archivo
              $file = $request->file('documento');
              $data['documento'] = $file->store('documentos','public');
              $data['name_file'] = $request['name_file'];
            }

            if($id > 0){
              Propietarios::find($id)->update($data);
            }
            else{
              if($prop){
                $response = 2;
                return $response;
              }else {
                if($request['tip_prop']==1){
                  foreach ($torres as $value) {
                    Log::info($value->value);
                    Inmueble::find($value->value)->update([
                      "estado" => 1
                    ]);
                    Log::info("entrooo a guardar aqui");
                    Propietarios::create($data);
                  }
                }else {
                  Propietarios::create($data);
                }
              }
            }
            return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

          } catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage(), 'success' => false, 'linea' => $e->getLine()], 500);
          }
        }

      public function obtain_propietarios(){
        $terce = Propietarios::with("departamentopdf","municipiopdf","tipoInmueble","torreRes")->get();
        return $terce;
      }

      public function deleted_prop (Request $request){
        Propietarios::where('id',$request['id'])->update([
        'deleted'=>1
      ]);
      $response = true;
      return $response;
      }

      public function habilited_prop (Request $request){
        Propietarios::where('id',$request['id'])->update([
        'deleted'=>0
      ]);
      $response = true;
      return $response;
      }
}
