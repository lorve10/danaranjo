<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;
use DB;
use Auth;
use App\Models\Terceros;
use App\Models\Conjunto;
use App\Models\Departament;
use App\Models\Municipio;

class Home extends Controller
{
    public function terceros (){
        $data = Terceros::where('deleted', 0)->get();
        return $data;
    }

    public function imgConjunto (){
        $data = Conjunto::where('deleted', 0)->get();
        return $data;
    }

    public function obtain_departament()
    {
      $departament = Departament::get();
      return $departament;
    }

    public function obtain_municipio()
    {
      $municipio = Municipio::get();
      return $municipio;
    }

}
