<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use DB;
use Auth;
use App\Models\Admon;
use App\Models\Inmueble;
use App\Models\Terceros;
use App\Models\TipoInmueble;
use App\Models\Tarifa;
use App\Models\Departament;
use App\Models\Municipio;
use App\Models\Bank;
use App\Models\Pais;
use App\Models\ActividadEconomica;
use App\Models\ResponsabilidadFiscal;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use App\Notifications\InvoicePaid;



class AdmonController extends Controller
{
  public function encrypted()
  {
    $pass = "123456";
    $encript = Hash::make($pass);
    echo $encript;
  }

  public function form_log_admon()
  {
    return view('admon.LoginAdmon');
  }

  public function login_admon(Request $request)
  {
    Log::info("prueba");
    Log::info($request);
    $user = Admon::first();
    $login=Auth::guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]);
    if ($login) {
      Log::info("hecho");
      Auth::guard('admin')->login($user);
       return response()->json('Authenticated',200);
     }else{
       return response()->json(['errors'=>'Email o contrasena incorrectos.'],422);
     }

  }

  public function logout_admin()
  {
    Auth::guard('admin')->logout();
     return redirect('/');
  }

  public function home()
  {
    return view('admon.HomeAdmon');

  }

  public function guard_person( Request $request){

    try {

      $prop = Terceros::where('N_documet', '=', $request['N_documet'])->first();
      log::info($prop);

      Log::info($request);
      $id = $request['id'];

      $data['tip_per'] = $request['tip_per'];
      $data['tip_ident'] = $request['tip_ident'];
      $data['nit'] = $request['nit'];
      $data['N_documet'] = $request['N_documet'];
      $data['name'] = $request['name'];
      $data['secname'] = $request['secname'];
      $data['lastname'] = $request['lastname'];
      $data['seclastname'] = $request['seclastname'];
      $data['nom_comercial'] = $request['nom_comercial'];
      $data['expedido'] = $request['expedido'];
      $data['addres'] = $request['addres'];
      $data['pais'] = $request['pais'];
      $data['departamento'] = $request['departamento'];
      $data['municipio'] = $request['municipio'];
      $data['phone'] = $request['phone'];
      $data['mobile'] = $request['mobile'];
      $data['correo'] = $request['correo'];
      $data['banco'] = $request['banco'];
      $data['tipo_cuenta'] = $request['tipo_cuenta'];
      $data['cuenta_bancaria'] = $request['cuenta_bancaria'];
      $data['nombre_titular'] = $request['nombre_titular'];
      $data['nom_representante']= $request['nom_representante'];
      $data['correo_representante']= $request['correo_representante'];
      $data['phone_representante']= $request['phone_representante'];
      $data['regimen'] = $request['regimen'];
      $data['actividad_economica'] = $request['actividad_economica'];
      $data['res_fiscal'] = $request['res_fiscal'];
      $data['direlocal'] = $request['direlocal'];
      $data['contra'] = $request['contra'];
      $data['sigla'] = $request['sigla'];

      if($id > 0){
        Terceros::find($id)->update($data);
      }else{
        if($prop){
          $response = 2;
          return $response;
        }else {
          Terceros::create($data);
        }
      } 

      return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
    }
  }

  public function obtain_terce(){
    $terce = Terceros::with("departamentopdf","municipiopdf","paispdf","banco_rela","economica_rela","res_rela")->get();
    return $terce;
  }

  public function deleted_ter (Request $request){
    Terceros::where('id',$request['id'])->update([
    'deleted'=>1
  ]);
    $response = true;
    return $response;
  }

  public function habilited_ter (Request $request){
    Terceros::where('id',$request['id'])->update([
    'deleted'=> 0
  ]);
    $response = true;
    return $response;
  }

  ///Modulo terceros
  public function vistaTerceros()
  {
    return view('admon.TercerosAdmon');

  }

  ///Modulo Inicio
  public function InicioAdmon()
  {
    return view('admon.InicioAdmon');

  }

  ///Modulo Financiero
  public function Financiera()
  {
    return view('admon.FinancieraAdmon');
  }

  ///Modulo parametrizacion
  public function Parametrizacion()
  {
    return view('admon.ParametrizacionAdmon');
  }

  ///Modulo Cartera
  public function Cartera()
  {
    return view('admon.CarteraAdmon');
  }

  ///Modulo Contabilidad
  public function Contabilidad()
  {
    return view('admon.ContabilidadAdmon');
  }

  ///Modulo Tesoreria
  public function Tesoreria()
  {
    return view('admon.TesoreriaAdmon');
  }

  ///Modulo Configuracion
  public function Configuracion()
  {
    return view('admon.CreateUserAdmon');
  }

  public function guard_newUser(Request $request){
    $password = Hash::make($request['password']);
    $name = "admin";
      Admon::create([
        'name' => $name,
        'email' =>$request['email'],
        'password' => $password,
      ]);

    $response = true;
    return $response;
  }

  public function mostrarUsers (Request $request){
    $data = Admon::where('deleted',0)->get();
      return $data;
  }

  public function deleted_users(Request $request){
    Admon::where('id', $request['id'])->update([
      'deleted'=>1
    ]);
  }

  public function recovery(Request $request){
    Log::info($request);
    $user = Admon::first();
    $login=Auth::guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('pass')]);
    $password = Hash::make($request['password']);
    log::info("pruebas");
    log::info($login);
    if($login){
      Log::info("entro aqui perra 2222");
     $res = Admon::where('email', $request['email'])->update([
        'password'=> $password
      ]);
    }
    $response = true;
    return $response;
    }

    public function obtain_departament()
    {
      $departament = Departament::get();
      return $departament;
    }

    public function obtain_municipio()
    {
      $municipio = Municipio::get();
      return $municipio;
    }

    public function obtain_bank()
    {
      $bank = Bank::get();
      return $bank;
    }

    public function obtain_pais()
    {
      $pais = Pais::get();
      return $pais;
    }

    public function obtain_actividadEconomica()
    {
      $actividad = ActividadEconomica::get();
      return $actividad;
    }

    public function obtain_res()
    {
      $res = ResponsabilidadFiscal::get();
      return $res;
    }

    /////// recuperar contraseña

    public function obtain_user(Request $request)
    {
      $user = Admon::where('email',$request['correo'])->first();
      if ($user) {
        $code = rand(1000, 9999);
        $mensaje = "Su código de verificación: ".$code;
        Notification::route('mail', $request['correo'])->notify(new InvoicePaid($mensaje));

        Admon::where('id',$user->id)->update([
          'codigo'=>$code
        ]);
        $response['success'] = true;
      }else {
        $response['success'] = false;
      }
      return $response;
    }

    public function valida_use(Request $request)
    {
      $user = Admon::where('email', $request['email'])->first();

      if ($user->codigo == $request['code']) {

        $response['success'] = true;
      }else {
        $response['success'] = false;
      }

      return $response;
    }

    public function edit_pass(Request $request)
    {
        $password = Hash::make($request['password']);
        $user = Admon::where('email',$request['correo'])->update([
          'password'=>$password
        ]);
        if ($user) {
          $response['success'] = true;
        }else {
          $response['success'] = false;
        }
        return $response;
    }



}
