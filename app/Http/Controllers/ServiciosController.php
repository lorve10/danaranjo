<?php

namespace App\Http\Controllers;

use Log;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CatServicios;
use App\Models\Cat_cuentas;
use App\Models\Sub_cuentas;
use App\Models\Cuentas_sub;
use App\Models\Sub_cuenta2;
use App\Models\Sub_cuenta3;
use App\Models\Serv_contable;




use App\Models\Servicios;

class ServiciosController extends Controller
{
    //
    public function vistaConceptos()
    {
      return view('admon.ConceptosAdmon');
    }

    public function obtain_categoria(){
        $categoria = CatServicios::where('deleted', 0)->get();
        return $categoria;
    }

    public function obtain_categoriaCobro(){
      $categoria = Servicios::with("Categoria_id")->where('deleted', 0)->get();
      return $categoria;
  }

    public function guard_servicio(Request $request){
        log::info("entroooo");
        if ($request['id_servicios'] > 0) {
          Servicios::where('id_servicios',$request['id_servicios'])->update([
            'nombre_servic' => $request['nombre_servic'],
            'cat_id' => $request['cat_id'],
          ]);
          Serv_contable::where('id_cat_servi',$request['id_servicios'])->update([
            'id_cat_cuent' => $request['id_cat_cuent'],
            'id_sub_cuent' => $request['id_sub_cuent'],
            'id_cuenta_sub' => $request['id_cuenta_sub'],
            'id_sub_cuen2' => $request['id_sub_cuen2'],
            'id_sub_cuent3' => $request['id_sub_cuent3'],

          ]);
        }
        else{
          $servi = Servicios::create([
            'nombre_servic' => $request['nombre_servic'],
            'cat_id' => $request['cat_id'],
          ]);
          Serv_contable::create([
            'id_cat_servi' => $servi->id,
            'id_cat_cuent' => $request['id_cat_cuent'],
            'id_sub_cuent' => $request['id_sub_cuent'],
            'id_cuenta_sub' => $request['id_cuenta_sub'],
            'id_sub_cuen2' => $request['id_sub_cuen2'],
            'id_sub_cuent3' => $request['id_sub_cuent3'],

          ]);
        }
        $response = true;
        return $response;
    }

    public function guard_categoria(Request $request){
        log::info("entroooo");
        if ($request['id_cat'] > 0) {
          CatServicios::where('id_cat',$request['id_cat'])->update([
            'nombre_cat' => $request['nombre_cat'],
          ]);
        }
        else{
          CatServicios::create([
            'nombre_cat' => $request['nombre_cat'],

          ]);
        }
        $response = true;
        return $response;
    }

    public function list_Servicios(){
        $data = Servicios::with("Categoria_id")->where('deleted', 0)->get();
        return $data;
    }

    public function deleted_servicio(Request $request){
        Servicios::where('id_servicios', $request['id_servicios'])->update([
          'deleted'=>1
        ]);
    }
    public function traer_cuent(Request $request){
      $data = Sub_cuentas::with("cuentas_id","nivel1")->where("cuenta_id",'=', $request['id'])->get();
      return $data;
    }

}
