<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('login');
});


Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix'=>'admon'],function(){
  /////////////ADMINISTRADOR///////////////
  Route::get('/encrypted/pass', [App\Http\Controllers\AdmonController::class, 'encrypted']);
  Route::get('/home', [App\Http\Controllers\AdmonController::class, 'home'])->middleware(['auth:admin']);
  /////////////Inicio ADMINISTRADOR ///////////////7
  Route::get('/inicio', [App\Http\Controllers\AdmonController::class, 'InicioAdmon'])->middleware(['auth:admin']);
  Route::post('/login_administrador', [App\Http\Controllers\AdmonController::class, 'login_admon']);
  Route::get('/logout', [App\Http\Controllers\AdmonController::class, 'logout_admin'])->name('admin.logout');
  Route::get('/encrypted', [App\Http\Controllers\AdmonController::class, 'encrypted'])->name('admin.logout');


  ///PRUEBA///
  Route::get('/prueba', [App\Http\Controllers\AdmonController2::class, 'prueba'])->middleware(['auth:admin']);

  ////TERCEROOOOOSS////////////////////
  Route::post('/guard_person', [App\Http\Controllers\AdmonController::class, 'guard_person'])->middleware(['auth:admin']);
  Route::get('/obtain_terce', [App\Http\Controllers\AdmonController::class, 'obtain_terce'])->middleware(['auth:admin']);
  Route::post('/deleted_ter', [App\Http\Controllers\AdmonController::class, 'deleted_ter'])->middleware(['auth:admin']);
  Route::post('/habilited_ter',[App\Http\Controllers\AdmonController::class, 'habilited_ter'])->middleware(['auth:admin']);
  Route::get('/terceros', [App\Http\Controllers\AdmonController::class, 'vistaTerceros'])->middleware(['auth:admin']);
  Route::get('/municipio', [App\Http\Controllers\AdmonController::class, 'obtain_municipio'])->middleware(['auth:admin']);
  Route::get('/departament', [App\Http\Controllers\AdmonController::class, 'obtain_departament'])->middleware(['auth:admin']);
  Route::get('/bank', [App\Http\Controllers\AdmonController::class, 'obtain_bank'])->middleware(['auth:admin']);
  Route::get('/pais', [App\Http\Controllers\AdmonController::class, 'obtain_pais'])->middleware(['auth:admin']);
  Route::get('/actividadEconomica', [App\Http\Controllers\AdmonController::class, 'obtain_actividadEconomica'])->middleware(['auth:admin']);
  Route::get('/ResponsabilidadFiscal', [App\Http\Controllers\AdmonController::class, 'obtain_res'])->middleware(['auth:admin']);



  ///Inmuebles////
  Route::get('/inmueble', [App\Http\Controllers\InmuebleController::class, 'vistaInm'])->middleware(['auth:admin']);
  Route::post('/guard_inmu', [App\Http\Controllers\InmuebleController::class, 'guard_inmu'])->middleware(['auth:admin']);
  Route::post('/guard_inmu_massive', [App\Http\Controllers\InmuebleController::class, 'guard_inmu_massive'])->middleware(['auth:admin']);
  Route::get('/obtain_tarifa', [App\Http\Controllers\InmuebleController::class, 'obtain_tarifa'])->middleware(['auth:admin']);
  Route::get('/obtain_tipo', [App\Http\Controllers\InmuebleController::class, 'obtain_tipo'])->middleware(['auth:admin']);
  Route::get('/obtain_unidad', [App\Http\Controllers\InmuebleController::class, 'obtain_unidad'])->middleware(['auth:admin']);
  Route::get('/obtain_Inmu', [App\Http\Controllers\InmuebleController::class, 'obtain_Inmu'])->middleware(['auth:admin']);
  Route::post('/deleted_inmueble',[App\Http\Controllers\InmuebleController::class, 'deleted_inmueble'])->middleware(['auth:admin']);
  Route::post('/habilited_inmueble',[App\Http\Controllers\InmuebleController::class, 'habilited_inmueble'])->middleware(['auth:admin']);

  Route::post('/guard_tipInmueble', [App\Http\Controllers\InmuebleController::class, 'guard_tipInmueble'])->middleware(['auth:admin']);
  Route::post('/guard_unidadRes', [App\Http\Controllers\InmuebleController::class, 'guard_unidadRes'])->middleware(['auth:admin']);

  /////Cartera////
  Route::get('/cartera', [App\Http\Controllers\AdmonController::class, 'Cartera'])->middleware(['auth:admin']);
  Route::get('/contabilidad', [App\Http\Controllers\AdmonController::class, 'Contabilidad'])->middleware(['auth:admin']);


  Route::prefix("parametrizacion")->group(function(){

    Route::get('/cuentas', [App\Http\Controllers\Financiero\CuentasContablesController::class, 'CuentaContable'])->middleware(['auth:admin']);
    Route::get('/obtain_cuenta', [App\Http\Controllers\Financiero\CuentasContablesController::class, 'obtain_cuenta'])->middleware(['auth:admin']);
    Route::get('/sub_cuenta2', [App\Http\Controllers\Financiero\CuentasContablesController::class, 'sub_cuenta2'])->middleware(['auth:admin']);

    Route::get('/obtain_sub', [App\Http\Controllers\Financiero\CuentasContablesController::class, 'obtain_sub'])->middleware(['auth:admin']);
    Route::post('/guard_Niveles', [App\Http\Controllers\Financiero\CuentasContablesController::class, 'guard_Niveles'])->middleware(['auth:admin']);

    ////Cartera deshabilitar
    //Route::post('/deshabilitar_One', [App\Http\Controllers\CuentasContables\CuentasController::class, 'deshabilitar_One'])->middleware(['auth:admin']);
    ///filtro por codigo
    Route::get('/traer_codigo', [App\Http\Controllers\Financiero\CuentasContablesController::class, 'traer_codigo'])->middleware(['auth:admin']);
    Route::post('/obtenerCodigo', [App\Http\Controllers\Financiero\CuentasContablesController::class, 'obtenerCodigo'])->middleware(['auth:admin']);

  });




  ////Print Cobro ////
  Route::get('/imprimir', [App\Http\Controllers\printCobroController::class, 'PrintCobro'])->middleware(['auth:admin']);
  Route::get('/obtain_cobro', [App\Http\Controllers\CobrosController::class, 'obtain_cobro'])->middleware(['auth:admin']);

  ////Pagos///
  Route::get('/pagos', [App\Http\Controllers\PurseController::class, 'pagos'])->middleware(['auth:admin']);
  Route::post('/guard_purse', [App\Http\Controllers\PurseController::class, 'guard_purse'])->middleware(['auth:admin']);
  Route::get('/obtain_pago', [App\Http\Controllers\PurseController::class, 'obtain_pago'])->middleware(['auth:admin']);
  Route::post('/delete_purse', [App\Http\Controllers\PurseController::class, 'delete_purse'])->middleware(['auth:admin']);
  Route::post('/descargar/pagos', [App\Http\Controllers\PurseController::class, 'pagos_pdf'])->middleware(['auth:admin']);
  Route::post('/enviar_pago', [App\Http\Controllers\PurseController::class, 'enviar_pago'])->middleware(['auth:admin']);

  ///Cuotas///
  Route::get('/cuotas', [App\Http\Controllers\CuotasController::class, 'vistaCuot'])->middleware(['auth:admin']);
  Route::get('/obtain_Cuotas', [App\Http\Controllers\CuotasController::class, 'obtain_Cuotas'])->middleware(['auth:admin']);
  Route::post('/guard_cuotas', [App\Http\Controllers\CuotasController::class, 'guard_cuotas'])->middleware(['auth:admin']);
  Route::post('/deleted_cuotas',[App\Http\Controllers\CuotasController::class, 'deleted_cuotas'])->middleware(['auth:admin']);

  ///Cobros///
  Route::get('/cobros', [App\Http\Controllers\CobrosController::class, 'vistaCobros'])->middleware(['auth:admin']);
  Route::post('/guard_Cobro', [App\Http\Controllers\CobrosController::class, 'guard_Cobro'])->middleware(['auth:admin']);
  Route::post('/descargar/cobros', [App\Http\Controllers\CobrosController::class, 'download'])->middleware(['auth:admin']);
  Route::post('/obtain_cobros_mes', [App\Http\Controllers\CobrosController::class, 'obtain_cobros_mes'])->middleware(['auth:admin']);
  Route::post('/obtain_cobros_print', [App\Http\Controllers\CobrosController::class, 'obtain_cobros_print'])->middleware(['auth:admin']);

  Route::get('/vista-pdf', [App\Http\Controllers\CobrosController::class, 'pdf'])->middleware(['auth:admin']);
  Route::post('/enviar', [App\Http\Controllers\CobrosController::class, 'enviar'])->middleware(['auth:admin']);
  Route::post('/robot', [App\Http\Controllers\CobrosController::class, 'robot'])->middleware(['auth:admin']);

  ///vista del pdf



  ///Servicios///
  Route::get('/conceptos', [App\Http\Controllers\ServiciosController::class, 'vistaConceptos'])->middleware(['auth:admin']);
  Route::get('/obtain_categoria', [App\Http\Controllers\ServiciosController::class, 'obtain_categoria'])->middleware(['auth:admin']);
  Route::get('/obtain_categoriaCobro', [App\Http\Controllers\ServiciosController::class, 'obtain_categoriaCobro'])->middleware(['auth:admin']);
  Route::post('/guard_servicio', [App\Http\Controllers\ServiciosController::class, 'guard_servicio'])->middleware(['auth:admin']);
  Route::get('/list_Servicios', [App\Http\Controllers\ServiciosController::class, 'list_Servicios'])->middleware(['auth:admin']);
  Route::post('/guard_categoria', [App\Http\Controllers\ServiciosController::class, 'guard_categoria'])->middleware(['auth:admin']);
  Route::post('/deleted_servicio', [App\Http\Controllers\ServiciosController::class, 'deleted_servicio'])->middleware(['auth:admin']);
  Route::get('/cat_cuentas', [App\Http\Controllers\ServiciosController::class, 'cat_cuentas'])->middleware(['auth:admin']);
  Route::post('/traer_cuent', [App\Http\Controllers\ServiciosController::class, 'traer_cuent'])->middleware(['auth:admin']);
  Route::post('/traer_cuent2', [App\Http\Controllers\ServiciosController::class, 'traer_cuent2'])->middleware(['auth:admin']);
  Route::post('/traer_cuent3', [App\Http\Controllers\ServiciosController::class, 'traer_cuent3'])->middleware(['auth:admin']);
  Route::post('/traer_cuent4', [App\Http\Controllers\ServiciosController::class, 'traer_cuent4'])->middleware(['auth:admin']);

  ///Egresos///
  Route::get('/egresos', [App\Http\Controllers\EgresosController::class, 'vistaEgresos'])->middleware(['auth:admin']);
  Route::get('/obtain_Impu', [App\Http\Controllers\EgresosController::class, 'obtain_Impu'])->middleware(['auth:admin']);
  Route::post('/guard_egreso', [App\Http\Controllers\EgresosController::class, 'guard_egreso'])->middleware(['auth:admin']);
  Route::get('/obtain_egresos', [App\Http\Controllers\EgresosController::class,'obtain_egresos'])->middleware(['auth:admin']);


  ///CONFIGURACION ////////////////////////////
  Route::get('/Configuracion', [App\Http\Controllers\AdmonController::class, 'Configuracion']);
  Route::get('/mostrarUsers', [App\Http\Controllers\AdmonController::class, 'mostrarUsers']);
  Route::post('/guard_newUser', [App\Http\Controllers\AdmonController::class, 'guard_newUser']);
  Route::post('/recovery', [App\Http\Controllers\AdmonController::class, 'recovery']);
  Route::post('/deleted_users', [App\Http\Controllers\AdmonController::class, 'deleted_users']);


  ///Financiera //////////
  Route::get('/Financiera', [App\Http\Controllers\AdmonController::class, 'Financiera'])->middleware(['auth:admin']);


  ///Tesoreria //////////
  Route::get('/Tesoreria', [App\Http\Controllers\AdmonController::class, 'Tesoreria'])->middleware(['auth:admin']);

  //Parametrizacion
  Route::get('/parametrizacion', [App\Http\Controllers\AdmonController::class, 'Parametrizacion'])->middleware(['auth:admin']);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 //// Modulo de Conjunto ////////
 Route::get('/conjunto', [App\Http\Controllers\ConjuntoControllers::class, 'conjunto'])->middleware(['auth:admin']);
 Route::get('/datos/conjunto', [App\Http\Controllers\ConjuntoControllers::class, 'datosConjunto'])->middleware(['auth:admin']);
 Route::post('/guard_conjunto', [App\Http\Controllers\ConjuntoControllers::class, 'guard_conjunto'])->middleware(['auth:admin']);
 Route::get('/obtain_conjunto', [App\Http\Controllers\ConjuntoControllers::class, 'obtain_conjunto'])->middleware(['auth:admin']);
 Route::get('/obtain_moneda', [App\Http\Controllers\ConjuntoControllers::class, 'obtain_moneda'])->middleware(['auth:admin']);
 Route::get('/conjunto/propietarios', [App\Http\Controllers\ConjuntoControllers::class, 'Propietarios'])->middleware(['auth:admin']);

 ////Propietario conjunto
 Route::post('/guard_propietario', [App\Http\Controllers\PropietarioController::class, 'guard_propietario'])->middleware(['auth:admin']);
 Route::get('/obtain_propietarios', [App\Http\Controllers\PropietarioController::class, 'obtain_propietarios'])->middleware(['auth:admin']);
 Route::post('/deleted_prop', [App\Http\Controllers\PropietarioController::class, 'deleted_prop'])->middleware(['auth:admin']);
  Route::post('/habilited_prop',[App\Http\Controllers\PropietarioController::class, 'habilited_prop'])->middleware(['auth:admin']);

////Propietario conjunto
 Route::post('/guard_arrendatario', [App\Http\Controllers\ArrendatarioController::class, 'guard_arrendatario'])->middleware(['auth:admin']);
 Route::get('/obtain_arrendatario', [App\Http\Controllers\ArrendatarioController::class, 'obtain_arrendatario'])->middleware(['auth:admin']);
 Route::post('/deleted_arrend', [App\Http\Controllers\ArrendatarioController::class, 'deleted_arrend'])->middleware(['auth:admin']);
  Route::post('/habilited_arrend',[App\Http\Controllers\ArrendatarioController::class, 'habilited_arrend'])->middleware(['auth:admin']);

///datos administrador del conjunto

 Route::post('/guard_datadmin', [App\Http\Controllers\ConjuntoControllers::class, 'guard_datadmin'])->middleware(['auth:admin']);
 Route::get('/obtain_admin', [App\Http\Controllers\ConjuntoControllers::class, 'obtain_admin'])->middleware(['auth:admin']);

 ///datos consejo Conjunto
 Route::post('/guard_consejo', [App\Http\Controllers\ConjuntoControllers::class, 'guard_consejo'])->middleware(['auth:admin']);
 Route::get('/obtain_consejo', [App\Http\Controllers\ConjuntoControllers::class, 'obtain_consejo'])->middleware(['auth:admin']);
 Route::get('/obtain_cargos', [App\Http\Controllers\ConjuntoControllers::class, 'obtain_cargos'])->middleware(['auth:admin']);
 Route::post('/deleted_cargos', [App\Http\Controllers\ConjuntoControllers::class, 'deleted_cargos'])->middleware(['auth:admin']);
 Route::post('/guardar_cargos', [App\Http\Controllers\ConjuntoControllers::class, 'guardar_cargos'])->middleware(['auth:admin']);



//recuperar contraseña
 Route::post('/obtain_user', [App\Http\Controllers\AdmonController::class, 'obtain_user'])->middleware(['auth:admin']);
 Route::post('/valida_use', [App\Http\Controllers\AdmonController::class, 'valida_use'])->middleware(['auth:admin']);
 Route::post('/edit_pass', [App\Http\Controllers\AdmonController::class, 'edit_pass'])->middleware(['auth:admin']);


 Route::prefix("parametrizacion")->group(function(){
   ////Centro de costo
   Route::get('/centroCosto', [App\Http\Controllers\Financiero\CentroCostoController::class, 'CentroCosto'])->middleware(['auth:admin']);
   Route::post('/guard_costo', [App\Http\Controllers\Financiero\CentroCostoController::class, 'guard_costo'])->middleware(['auth:admin']);
   Route::get('/obtener_costo', [App\Http\Controllers\Financiero\CentroCostoController::class, 'obtener_costo'])->middleware(['auth:admin']);
   Route::get('/obtener_centro', [App\Http\Controllers\Financiero\CentroCostoController::class, 'obtener_centro'])->middleware(['auth:admin']);
   Route::post('/deletedCentro', [App\Http\Controllers\Financiero\CentroCostoController::class, 'deletedCentro'])->middleware(['auth:admin']);

   //////Concepto
   Route::get('/concepto', [App\Http\Controllers\Financiero\ConceptoController::class, 'Concepto'])->middleware(['auth:admin']);
   Route::post('/guardConcepto', [App\Http\Controllers\Financiero\ConceptoController::class, 'guardConcepto'])->middleware(['auth:admin']);
   Route::get('/getConcep', [App\Http\Controllers\Financiero\ConceptoController::class, 'getConcep'])->middleware(['auth:admin']);
   Route::post('/deleted', [App\Http\Controllers\Financiero\ConceptoController::class, 'deleted'])->middleware(['auth:admin']);


   Route::get('/TiposCIIU', [App\Http\Controllers\Financiero\TiposCIUController::class, 'TiposCIU'])->middleware(['auth:admin']);

   ///Periodos contables/////
   Route::get('/periodos-contables', [App\Http\Controllers\Financiero\PeriodosContablesController::class, 'PeriodosContables'])->middleware(['auth:admin']);
   Route::get('/obtain_PeriodosContables', [App\Http\Controllers\Financiero\PeriodosContablesController::class, 'obtain_PeriodosContables'])->middleware(['auth:admin']);
   Route::post('/guardPeriodoContable', [App\Http\Controllers\Financiero\PeriodosContablesController::class, 'guardPeriodoContable'])->middleware(['auth:admin']);
   Route::post('/deleted_periodoContable', [App\Http\Controllers\Financiero\PeriodosContablesController::class, 'deleted_periodoContable'])->middleware(['auth:admin']);
   Route::post('/habilited_periodoContable', [App\Http\Controllers\Financiero\PeriodosContablesController::class, 'habilited_periodoContable'])->middleware(['auth:admin']);

   Route::get('/tipoDoc', [App\Http\Controllers\Financiero\TipoDoController::class, 'documento'])->middleware(['auth:admin']);
   Route::post('/guardDoct', [App\Http\Controllers\Financiero\TipoDoController::class, 'guardDoct'])->middleware(['auth:admin']);
   Route::get('/obtain_tipo', [App\Http\Controllers\Financiero\TipoDoController::class, 'obtain_tipo'])->middleware(['auth:admin']);
   Route::get('/obtain_documento', [App\Http\Controllers\Financiero\TipoDoController::class, 'obtain_documento'])->middleware(['auth:admin']);
   Route::get('/notification', [App\Http\Controllers\Financiero\TipoDoController::class, 'notification'])->middleware(['auth:admin']);


   //// Aqui Cuentas contables ////
   Route::get('/obtain_Cuentas', [App\Http\Controllers\Financiero\CuentasContablesController::class, 'obtain_Cuentas'])->middleware(['auth:admin']);
   Route::post('/obtain_grupo', [App\Http\Controllers\Financiero\CuentasContablesController::class, 'obtain_grupo'])->middleware(['auth:admin']);
   Route::get('/obtain_Subcuentas', [App\Http\Controllers\Financiero\CuentasContablesController::class, 'obtain_Subcuentas'])->middleware(['auth:admin']);
   Route::get('/obtain_Nivel', [App\Http\Controllers\Financiero\CuentasContablesController::class, 'obtain_Nivel'])->middleware(['auth:admin']);
   Route::post('/filtro_nivel', [App\Http\Controllers\Financiero\CuentasContablesController::class, 'filtro_nivel'])->middleware(['auth:admin']);

 });

  ///Crear documentos/////
  Route::get('/crear-documentos', [App\Http\Controllers\Financiero\CrearDocumentosController::class, 'CrearDocumentos'])->middleware(['auth:admin']);
  Route::get('/obtain_nivel', [App\Http\Controllers\Financiero\CrearDocumentosController::class, 'obtain_nivel'])->middleware(['auth:admin']);
  Route::post('/crear', [App\Http\Controllers\Financiero\CrearDocumentosController::class, 'crear'])->middleware(['auth:admin']);
  Route::get('/obtain_doc', [App\Http\Controllers\Financiero\CrearDocumentosController::class, 'obtain_doc'])->middleware(['auth:admin']);
  Route::post('/imprimir', [App\Http\Controllers\Financiero\CrearDocumentosController::class, 'imprimir'])->middleware(['auth:admin']);
  Route::get('/vistapdf', [App\Http\Controllers\Financiero\CrearDocumentosController::class, 'pdf'])->middleware(['auth:admin']);












});
