<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/terceros_app', [App\Http\Controllers\API\Home::class, 'terceros']);
Route::get('/Conjunto', [App\Http\Controllers\API\Home::class, 'imgConjunto']);
Route::get('/obtain_departament', [App\Http\Controllers\API\Home::class, 'obtain_departament']);
Route::get('/obtain_municipio', [App\Http\Controllers\API\Home::class, 'obtain_municipio']);
