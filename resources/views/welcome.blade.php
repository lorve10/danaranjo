<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="{{ asset('images/favicon64x64.ico') }}" sizes="128x128">
  <link rel="icon" type="image/png" href="{{ asset('images/favicon16x16.png') }}" sizes="16x16">
  <link rel="icon" type="image/png" href="{{ asset('images/favicon32x32.png') }}" sizes="32x32">
  <link rel="icon" type="image/png" href="{{ asset('images/favicon64x64.png') }}" sizes="64x64">
  <title>Gestor de conjuntos residenciales | OneTurpial</title>
  

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
  rel="stylesheet">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;1,300&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>
</head>

<body style = "overflow-x: hidden;">

  <!-- React root DOM -->
  <div id="HomePage" class = "full-page">
  </div>

  <!-- React JS -->
  <script src="{{ mix('js/app.js') }}" defer></script>

</body>
<style>

body {
    background-color: #1E2747;
    font-family: "Open Sans", sans-serif;
}


.p-footer{
  color: white;
  border-right: 1px solid white;
}
.p-footer-2{
  color: white;
}
.p-footer-3{
  color: #e0ad24;
}
.footer{
  position: absolute;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 90px;
  box-shadow: -16px -102px 218px 67px rgb(0 0 0 / 64%);
  border-top: 2px solid white;
}

</style>
</html>
