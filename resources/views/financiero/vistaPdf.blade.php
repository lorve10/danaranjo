<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title></title>
    <script>

      </script>
  </head>
  <body>

        <div class="container">
            <div class="">

              <div class="justify-content-center text-center ">
                <img src="{{ public_path('images/logo_pdf.png') }}" class="img"/>
                <p>Docuemtos</p>
              </div>

            </div>
              <div class="container mt-4">
                <div class=" col-6">
                  <p class=""> <strong>Fecha de creacion: </strong>  {{$data->fecha}} </p>
                  <p class=""> <strong>Tipo de documento: </strong>  {{$data->tipo->nombre}} </p>
                </div>
              <br>
              <br>
            <div class="col-md-12">
              <table>
                <thead>
                  <tr>
                    <th>Cuenta contable</th>
                    <th>Tercero</th>
                    <th>Detalle</th>
                    <th>Base</th>
                    <th>Debito</th>
                    <td>Credito</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach (json_decode($data->datos) as $da)

                  <tr>
                    <td>{{$da->cuenta->label}}</td>
                    <td>{{$da->tercero->label}}</td>
                    <td>{{$da->detalle}}</td>
                    <td>{{$da->base}}</td>
                    <td>{{$da->debito}}</td>
                    <td>{{$da->credito}}</td>
                  </tr>
                  @endforeach

                </tbody>

              </table>

            </div>

        </div>

  </body>

  <style media="screen">
    .margin{
      margin-left: 80%;
      margin-top: -56%;
    }
    .margin_dos{
      margin-left: 70%;
      margin-top: -56%;
    }
    .line{
      color: black;
    }
    .img{
      width: 400px;
      height: 100px;
    }

    table, th, td {
      border: 1px solid black;
      border-collapse: collapse;
    }

  </style>
</html>
