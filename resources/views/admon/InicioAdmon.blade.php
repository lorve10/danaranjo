<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('images/favicon64x64.ico') }}" sizes="128x128">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon16x16.png') }}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon64x64.png') }}" sizes="64x64">
    <title>Gestor de conjuntos residenciales | OneTurpial</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>

<body style = "overflow-x: hidden;">
  <div className="col-md-12" style="background-color:#D9D9D9" >
    <div class="col-md-6 justify-content-center centrar">
      <img src="{{ asset('images/OneturpialHome.png') }}" class="img">

    </div>
  </div>

    <!-- React root DOM -->
    <div id="InicioAdmon" class = "full-page">
    </div>

    <!-- React JS -->
    <!--<script src="{{ mix('js/app.js') }}" defer></script>-->
    <script src="{{ asset('js/app.js') }}" defer></script>

</body>
<style>
.full-page{
  height:80%;
}
body {
  font-family: "Open Sans", sans-serif;
  height: 100vh;
  background-size: cover;
  background-color: #ffffff;
}
.img{
  height: 60;
  margin-left: 83px;
  margin-top: -32px;
}

.centrar{
  margin: auto;
  height: 109px;
}

</style>
</html>
