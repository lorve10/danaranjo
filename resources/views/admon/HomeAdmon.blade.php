<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('images/favicon64x64.ico') }}" sizes="128x128">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon16x16.png') }}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon64x64.png') }}" sizes="64x64">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>

<body >

    <div className="hola">
      <div id="HomePage" class = "full-page">
      </div>

    </div>

    <!-- React JS -->
    <!--<script src="{{ mix('js/app.js') }}" defer></script>-->
    <script src="{{ asset('js/app.js') }}" defer></script>

    

</body>
<style>

body{
  background-color: white;
}
.hola{
  position: fixed;
  left: 0px;
}
.img{
  width: 50%;
}
.footer{
  background: #D9D9D9;
  margin-left: 80%;
  border-left: thick solid #D9D9D9;
  height: 622px;
}
</style>
</html>
