<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title></title>
    <script>
    alert("asassasas");

      </script>
  </head>
  <body>
    
        <div class="container">
            <div class="">

              <div class="justify-content-center text-center ">
                <img src="{{ public_path('images/logo_pdf.png') }}" class="img"/>
                <p>Recibos de pago danaranjo</p>
              </div>

            </div>
              <div class="container mt-4">
                <div class=" col-6">
                  <p class=""> <strong>Mes de pago: </strong>  {{$pago->mes}} </p>
                  <p class=""> <strong>Fecha de generación: </strong>  {{$pago->fecha}} </p>
                </div>
                <div class="margin">

                  <p class=""><strong>Recibo de pago</strong></p>
                  <p class=""><strong>No.  </strong>{{$pago->id}} </p>
                </div>
                <hr class="line">

                <div class=" col-8">
                  <p class=""> <strong>Nombre:  </strong>  {{$pago->inmueble->propietario->name}} {{$pago->inmueble->propietario->secname}}  {{$pago->inmueble->propietario->lastname}} {{$pago->inmueble->propietario->seclastname}}  </p>

                </div>
                <div class="col-6">
                  <p class=""> <strong>Direccion:  </strong>  {{$pago->inmueble->propietario->addres}}</p>
                  <p class=""><strong>E-mail: </strong> {{$pago->inmueble->propietario->correo}}
                  <p class=""><strong>Inmueble: </strong> {{$pago->inmueble->tipo_inmueble->nombre}} - {{$pago->inmueble->numero}}</p>
                  <p class=""><strong>Celular: </strong> {{$pago->inmueble->propietario->mobile}} </p>
                </div>
                <div class="margin_dos">
                  <p class=""> <strong>Nit/c.c:</strong> {{$pago->inmueble->propietario->N_documet}}-{{$pago->inmueble->propietario->nit}}</p>
                  <p class=""> <strong>Codigo: </strong> 0001 </p>
                  <p class=""> <strong>Ciudad: </strong>
                    {{$pago->inmueble->propietario->municipiopdf->municipio}}
                  </p>
                </div>
              <br>
              <br>
              <div class="col-md-12">
                <table align="center" class="table table-striped">
                  <thead style="background-color: #FFDE59">
                    <tr align="center">
                      <th scope="col" style="width:300px"> Concepto </th>
                      <th scope="col">Saldo </th>
                      <th scope="col">Cuota </th>
                      <th scope="col">Nuevo Saldo </th>
                    </tr>
                  </thead>
                  <tbody>

                    <tr scope="row">

                      <td>Cuota De {{$pago->concept->nombre_servic}}</td>
                      <td>0</td>
                      <td>$ {{$valor}}</td>
                      <td id = "new_saldo" >$ {{$cobro}}</td>
                    </tr>

                  </tbody>
                </table>

              </div>
        </div>

  </body>

  <style media="screen">
    .margin{
      margin-left: 80%;
      margin-top: -56%;
    }
    .margin_dos{
      margin-left: 70%;
      margin-top: -56%;
    }
    .line{
      color: black;
    }
    .img{
      width: 400px;
      height: 100px;
    }

    table, th, td {
      border: 1px solid black;
      border-collapse: collapse;
    }

  </style>
</html>
