import React,{useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2';
import Header from '../../Ui/Header';
import NewCuotas from './NewCuotas';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import useful from '../util';


function Cuotas () {
  const [loading, setLoading] = useState(false)
  const [text, setText] = useState('')
  const [ listCuotas, setListCuotas ] = useState([]);
  const [backCuo, setBackCuo] = useState(listCuotas)
  //const [backInmu2, setBackInmu2] = useState(listCuotas)
  const [id, setId] = useState(null);
  const [data, setData] = useState({})
  const [marginval, setMarginVal] = useState(false)
  const [estado, setEstado] = useState(1);


  const searchInput = async(value) => {
    await setText(value)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData = backCategories2.filter(function(item2){
      var name = ((item2.cat_nombre).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var tarifa = ((item2.tarifa).toString()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var restarifa = tarifa.indexOf(inputSearch) > -1
      var restarifaLasT = tarifa.indexOf(inputSearch) > -1
      var res = false
      if(resNameLasT||resNameLasT){
        res = true
      }

      if(restarifa||restarifaLasT){
        res = true
      }

      return res;
    })

    await setCurrentPage(1)
    await setBackCuo(newData)
    const dataNew = Pagination.paginate(newData,1,postsPerPage)
    await setListCuotas(dataNew)
    await setLoading(false)
  }

  const mostrarCuotas = async() =>{
    axios.get(ip+'admon/obtain_Cuotas').then(response=>{
      console.log(response);
      var res = response.data;
        setListCuotas(response.data);
    })
  }

  const dataUpdateCuots = async (data) => {
    console.log(data);

    var nombre = data.nombre
    var concepto = data.id_concepto
    var tarifa = data.tarifa
    var valor_interes = data.valor_interes

    const dataForAll = {

      nombre,
      concepto,
      tarifa,
      valor_interes,
    }
    await setId(data.id)
    await setData(dataForAll)
    await setEstado(3)
    console.log(dataForAll);
  }

  const deshabilitar = (value) =>{
    var message = 'Quieres eliminar esta cuota?'
    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,

    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value.id)
        axios.post(ip+'admon/deleted_cuotas',data).then(response=>{

          Swal.fire('Eliminado correctamente!', '', 'success')
          mostrarCuotas();
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }

  useEffect(()=>{
    mostrarCuotas();
  }, [])

  const goback = async () => {
    await setEstado(1)
    setId(null)
  }

  const gobackSave = () => {
    setEstado(1)
    setId(null)
    setData({})
    mostrarCuotas();
  }

    const propHeader = (value) => {
      console.log("entro",value);
      setMarginVal(value)
    }


    const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
      <a
      style= {{cursor:'pointer'}}
        ref={ref}
        onClick={(e) => {
          e.preventDefault();
          onClick(e);
        }}
      >
           <span className = "material-icons">more_vert</span>
      </a>
    ));

  return(
    <div className="content-wrapper"  style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:74}} >
    <Header section={8} marginval = {(value)=>propHeader(value)}/>
    <div className="verticar-footer">
          <div className="deshabilitar" onClick = {()=>setEstado(3)}>
              <h6 className="form-section d-flex align-items-center " style={{backgroundColor:'#FFDE59', borderRadius:15, marginTop:44, cursor:'pointer'}}><i className="material-icons" style= {{fontSize:'170%'}}>control_point</i>Agregar cuota</h6>
          </div>
          <h5 className="form-section habilitar" onClick = {()=>setEstado(3)}><i className="material-icons" style= {{fontSize:'170%', marginLeft:14, marginTop:44, backgroundColor:'#FFDE59', borderRadius:15, width:66}}> control_point</i></h5>
          <img className="imagen" src={ip+'images/footer.png'}/>
    </div>


    {
      estado == 1 ?
      <div >
        <div className="mt-3">
        <h6 style={{marginLeft:'60px'}} className="mt-2">/<a href = {ip+'admon/cartera'}>Cartera</a>/Cuotas</h6>

          <h1 className="text-center" style={{marginLeft:"-81%"}}>Cuotas</h1>
        </div>
        <div className="d-flex col-8">
        <div className="row mt-3 d-flex "  style={{marginLeft:51}} >
            <div className="col-12 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid ', backgroundColor:'white',borderRadius:12}}>
            <input  onChange = {(e)=>searchInput(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Consultar Cuotas...  "/>
            {
              text.length == 0 ?
              <span style = {{color:'black', cursor:'pointer'}}  className= "material-icons">search</span>
              :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
            }
            </div>
        </div>
        </div>
      </div>
      :null
        }

        <div className="col-md-10" style={{marginLeft:50}}>
        <div className="row">
    {
      estado == 1?

        listCuotas.map((item, index)=>{

          console.log(item.concepto);

          return (
            <div className="col-md-3 d-flex" key = {index}>
                <div className="card d-flex" style={{border: '1px solid #d9d9d9'}}>
                <div className="card-header" style={{backgroundColor: '#d9d9d9', fontSize:'20px', fontWeight: "bold"}}>
                    <div className = "d-flex justify-content-between">
                        <span>
                        {item.nombre}
                        </span>


                  {/*<div className="dropdown dropright d-flex justify-content-end">
                    <a className="dropdown-toggle" style = {{color:'black'}} role="button" href="#"  id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span className = "material-icons">more_vert</span>
                    </a>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a href="#" className="dropdown-item"  onClick = {()=>dataUpdateCuots(item)} >Editar</a>
                    <a href="#" className="dropdown-item"  onClick = {()=>deshabilitar(item)} >Eliminar</a>
                  </div>
                  </div>*/}

                  <Dropdown>
                    <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
                      Dropdown Button
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item href="#" onClick = {()=>dataUpdateCuots(item)}>Editar</Dropdown.Item>
                      <Dropdown.Item href="#"  onClick = {()=>deshabilitar(item)}>Eliminar</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>

                </div>
                </div>
                <div className="card-body" style={{backgroundColor: '#d9d9d9'}}>
                    <h5 className="card-title" style={{fontWeight: "bold"}}>Valor cuota: $ {useful.convertMoney(item.tarifa)}</h5>
                    <h5 className="card-title" style={{fontWeight: "bold"}}>Valor interes: $ {useful.convertMoney(item.valor_interes)}</h5>
                    <h5 className="card-title" style={{fontWeight: "bold"}}>Concepto: {item.concepto.map(item2=>{
                      return(
                        <div>{item2.nombre_servic}</div>
                      )
                    })}
                    </h5>
                </div>
                </div>
            </div>
          );
        })
      :null
    }
    </div>
    </div>



    {estado == 3 ?
            <div>
              <NewCuotas data={data} gobackSave = {()=>gobackSave()} goback = {()=>goback()}  id = {id} />
            </div>
      :null
    }
  </div>



);

}
export default Cuotas;
if (document.getElementById('Cuotas')) {
  ReactDOM.render(<Cuotas />, document.getElementById('Cuotas'));
}
