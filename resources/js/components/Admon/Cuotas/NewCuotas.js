import React, {useState, useEffect, Fragment} from 'react';
import {ip} from '../../ApiRest';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import axios from 'axios';
import Swal from 'sweetalert2';
import '../../../../css/app.scss';
import makeAnimated from 'react-select/animated';
import { data } from 'jquery';

function NewCuotas (props) {
    const [loading, setLoading] = useState(false);
    const [text, setText] = useState('');
    const [nombre, setNombre] = useState('');
    const [id, setId] = useState(null)
    const [tarifa, setTarifa] = useState(null);
    const [valor_interes, setvalorInteres] = useState([]);
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);

    const [concepto, setConcepto] = useState(null);
    const [typeConcepSelect, setTypeConcepSelect] = useState([]);

    useEffect(()=>{
        validar();
        conceptoPago();
    }, [])

    const conceptoPago = () =>{
      axios.get(ip+'admon/list_Servicios').then(response=>{
          var res = response.data
          var concep = []
          var filtrado = res.filter(e=>e.cat_id == 2)
          console.log(filtrado);
          filtrado.map(item=>{
            const data = {
              value:item.id_servicios,
              label:item.nombre_servic
            }
            concep.push(data)
          })
          setTypeConcepSelect(concep)
          if(props.id>0){
            var filtrado = res.filter(e=>e.cat_id == 2)

            var filterconcepto = filtrado.filter(e=>e.id_servicios == props.data.concepto)
            console.log("esta es la props concepto");
            console.log(filterconcepto);
            const data = {
                value:filterconcepto[0].id_servicios,
                label:filterconcepto[0].nombre_servic
            }
            setConcepto(data)
        }
        })

  }

    console.log(props.id);

    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

    const saveCuotas = async () => {
      var message = ''
      var error = false
    if(tarifa == ''){
      error = true
      message = "La cuota no puede ser vacío "
    }else if(valor_interes == ''){
      error = true
      message = "El valor interes no puede ser vacío "
    }else if(concepto == ''){
      error = true
      message = "El concepto no puede estar vacío "
    }
    if(error){
      MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('id',props.id)
      data.append('nombre', nombre)
      data.append('tarifa', tarifa)
      data.append('valor_interes', valor_interes)
      data.append('id_concepto', concepto.value)
      axios.post(ip+'admon/guard_cuotas',data).then(response=>{
        if (props.id == null) {
          MessageSuccess("Cuota creada correctamente");
          props.gobackSave();
        }
        else{
          MessageSuccess("Cuota editada correctamente");
          props.gobackSave();
        }
      })
    }

  }

  const validar = async () =>{
    console.log("entro aquiii");
    if(props.id > 0 ){
      console.log("entro al if");
      setNombre(props.data.nombre);
      setTarifa(props.data.tarifa);
      setvalorInteres(props.data.valor_interes);
    }
  }

    return(

      <div className="mt-5 col-12" style={{marginLeft: '45px'}}>
        <div className="col-md-5">
          <h1>{props.id > 0 ? 'Editar Cuota':'Agregar Cuota'}</h1>
        </div>
        <div className="btn container col-1 " onClick = {()=>props.goback()}>
          <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
        </div>
        <br></br>
          <div className="row col-12">
            <div className="col-md-8 d-flex">
              <label className="d-flex col-3 mr-3 align-items-center color" style={{width: '120px'}}>Nombre: </label>
              <input className="form-control col-6 d-flex" placeholder="Ingresa nombre de la cuota" value={nombre} type="text" onChange={(event)=>setNombre(event.target.value)}/>
            </div>

            <div className="col-md-12 d-flex">
            <label className="d-flex col-2 mr-2 align-items-center color" style={{width: '120px'}} >Concepto: </label>
            <Select
            className="col-4"
            style={{ backgroundColor:'#FDA71A'}}
            value={concepto}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={typeConcepSelect}
            onChange={(e)=>setConcepto(e)}
            placeholder = "Seleccionar Tipo Concepto"
            name="colors"
            />
            </div>

            <div className="col-md-8 d-flex">
            <label className="d-flex col-3 mr-3 align-items-center color" style={{width: '120px'}}>Valor Cuota:</label>
            <input className="form-control col-6 d-flex" placeholder="Ingresa el valor de la cuota" value={tarifa} type="number" onChange={(event)=>setTarifa(event.target.value)}/>
            </div>

            <div className="col-md-8 d-flex" >
            <label className="d-flex col-3 mr-3 align-items-center color" style={{width: '120px'}}>Valor Interés:</label>
            <input className="form-control col-6 d-flex" placeholder="Ingresa el valor del interés" value={valor_interes} type="number" onChange={(event)=>setvalorInteres(event.target.value)} />
            </div>
        </div>
          <div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{marginLeft:'100px'}}>
            <button className="btn btn-success" style={{border:'1px solid #FDA71A', backgroundColor:'#FFDE59', color:'black'}} onClick = {()=>saveCuotas()}>Guardar cuota</button>
          </div>

    </div>

    );
}

export default NewCuotas;
if(document.getElementById('NewCuotas')) {
    ReactDOM.render(<NewCuotas />, document.getElementById('NewCuotas'));
}
