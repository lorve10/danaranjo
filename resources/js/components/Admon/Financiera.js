import React, {useState} from 'react'
import ReactDOM from 'react-dom'
import {ip} from '../../components/ApiRest';
import Header from '../../components/Ui/Header';
import '../../../css/app.scss';

function Financiera () {
  const [marginval, setMarginVal] = useState(false)

  const propHeader = (value) => {
      console.log("entro",value);
      setMarginVal(value)
  }


  return(
    <div className="content-wrapper " style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}}>
      <Header section={4}  marginval = {(value)=>propHeader(value)}/>
      <div className="verticar-footer">
            <img className="imagen" src={ip+'images/footer.png'} style={{ marginTop:367}}/>
      </div>
      <div className="container mt-5"  >
          <div className="text-center">
          <h4><strong>Módulo Financiero</strong></h4>
          </div>
        <div className="row mb-3">
        <div className="col-md-9 row mb-3 p-0 mt-4" style={{marginLeft: '110px'}}>

        <div className="col-md-4 mb-3">
        <a href={ip+"admon/terceros"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/Terceros.png'}/>
                </div>
                <div className="mt-2">
                  <p className="m-0 interlineado">Terceros</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>

        <div className="col-md-4 mb-3">
        <a href={ip+"admon/cartera"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/Cartera.png '}/>
                </div>
                <div className="mt-2">
                  <p className="m-0 interlineado">Cartera</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>

        <div className="col-md-4 mb-3 ">
          <a href={ip+"admon/Tesoreria"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/Cuentasporpagar.png'}/>
                </div>
                <div className="mt-2">
                  <p className="m-0 interlineado">Cuentas por pagar</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>

        <div className="col-md-4 mb-3">
        <a href={ip+"admon/terceros"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/Tesorería.png'}/>
                </div>
                <div className="mt-2">
                  <p className="m-0 interlineado">Tesorería</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>

        <div className="col-md-4 mb-3">
        <a href={ip+"admon/contabilidad"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/Contabilidad(1).png '}/>
                </div>
                <div className="mt-2">
                  <p className="m-0 interlineado">Contabilidad</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>

        <div className="col-md-4 mb-3 ">
          <a href={ip+"admon/Tesoreria"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/ActivosFijos.png'}/>
                </div>
                <div className="mt-2">
                  <p className="m-0 interlineado">Activos fijos</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>

        <div className="col-md-4 mb-3">
        <a href={ip+"admon/terceros"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/informes.png'}/>
                </div>
                <div className="mt-2">
                  <p className="m-0 interlineado">Informes</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>
      </div>
      </div>
    </div>
    </div>
  )

}

export default Financiera;

if(document.getElementById('Financiera')){
  ReactDOM.render(<Financiera />, document.getElementById('Financiera'));

}
