import React, {useState, useEffect, Fragment} from 'react';
import {ip} from '../../ApiRest';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import axios from 'axios';
import Swal from 'sweetalert2';
import '../../../../css/app.scss';
import makeAnimated from 'react-select/animated';
import { data } from 'jquery';

function NewCobroServicios (props) {
    const [loading, setLoading] = useState(false);
    const [text, setText] = useState('');
    const [id, setId] = useState(null)
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);
    const [fecha, setFecha] = useState('');
    const [mes, setMes] = useState('');
    const [documento, setDocumento] = useState('');
    const [valor, setValor] = useState('');
    const [interes, setInteres] = useState('');
    const [observa, setObserva] = useState('')
    const [inmueble, setInmueble] = useState(null);
    const [inmuebleSelect, setInmuebleSelect] = useState([]);
    const [pago, setPago] = useState(null)
    const [typoPago, setTypoPago] = useState([
      {
        value:'1',
        label:'Efectivo'
      },
      {
        value:'2',
        label:'Banco'
      }
    ])
    const [categoria, setCategoria ] = useState(null);
    const [categoriaSelect, setCategoriaSelect] = useState([]);

    useEffect(()=>{
        obtain_categoriaCobro();
        mostrarInmu();
    }, [])

    const mostrarInmu = () =>{
        axios.get(ip+'admon/obtain_Inmu').then(response=>{
          console.log(response);
            var res = response.data
            var inmueble = []
            res.map(item=>{
              data = {
                value:item.id,
                label:item.tipo_inmueble.nombre+" - "+item.numero
              }
              inmueble.push(data)
            })
            setInmuebleSelect(inmueble)
  
            if(props.id>0){
                var filterInmueble = res.filter(e=>e.id == props.data.inmueble.id)
                console.log("esta es la props terceros");
                console.log(filterInmueble);
                const data = {
                    value:filterInmueble[0].id,
                    label:filterInmueble[0].tipo_inmueble.nombre+" - "+filterInmueble[0].numero
                }
                setInmueble(data)
            }
        })
      }

    const obtain_categoriaCobro = () => {
        axios.get(ip+'admon/obtain_categoriaCobro').then(response=>{
            console.log(response.data);
            var res = response.data;
            var cat = res.filter(e=>e.cat_id==2)
            console.log(cat)
            var categoria = []
            cat.map(item=>{
              data = {
                value:item.id_servicios,
                label:item.nombre_servic
              }
              categoria.push(data)
            })
            setCategoriaSelect(categoria);
        })
    }

    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

    const saveCobroServicio = async () => {
      var message = ''
      var error = false
      if (nombreServ == ''){
      error = true
      message = "El nombre del servicio no puede ser vacío "  
    }else if (categoria == null){
        error = true
        message = "Selecciona la categoria "  
    }
    if(error){
        MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('id_servicios',props.id)
      data.append('nombre_servic', nombreServ)
      data.append('cat_id', categoria.value)
      axios.post(ip+'admon/guard_cobroservicio',data).then(response=>{
        if (props.id == null) {
          MessageSuccess("Servicio creado correctamente");
          props.gobackSave();
        }
        else{
          MessageSuccess("Servicio editado correctamente");
          props.gobackSave();
        }
      })
    }

  }

    return(
        <div className="mt-5 col-12 ">

        <div className="btn container col-1 " onClick = {()=>props.goback()}>
        <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
        </div>
  
          <div  className="container col-12 mt-4">
            <div className="row col-12">
              <div className="col-md-6 d-flex ">
              <label className="d-flex col-4 mr-3 align-items-center color">Fecha: </label>
              <input className="form-control d-flex"  value={fecha}  type="date" onChange={(event)=>setFecha(event.target.value)}/>
              </div>
  
              <div className="col-md-6 d-flex">
              <label className="d-flex col-4 mr-3 color"> Mes:</label>
              <input className="form-control d-flex"  value={mes} type="month" onChange={(event)=>setMes(event.target.value)}/>
              </div>
  
              <div className="col-md-12 d-flex">
              <label className="d-flex col-4 align-items-center color" >Inmueble: </label>
              <Select
              className="col-8"
              style={{ backgroundColor:'#FDA71A'}}
              value={inmueble}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={inmuebleSelect}
              onChange={(e)=>setInmueble(e)}
              placeholder = "Seleccionar Tipo Inmueble"
              name="colors"
              />
              </div>

              <div className="col-md-12 d-flex">
              <label className="d-flex col-4 align-items-center color" >Servicio: </label>
              <Select
              className="col-8"
              style={{ backgroundColor:'#FDA71A'}}
              value={categoria}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={categoriaSelect}
              onChange={(e)=>setCategoria(e)}
              placeholder = "Seleccionar tipo servicio"
              name="colors"
              />
              </div>

              <div className="col-md-12 d-flex">
              <label className="d-flex col-4 color"> Tipo Pago:</label>
              <Select
              className="col-8"
              style={{ backgroundColor:'#FDA71A'}}
              value={pago}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={typoPago}
              onChange={(e)=>setPago(e)}
              placeholder = "Seleccionar Tipo Pago"
              name="colors"
              />
              </div>
  
              <div className="col-md-5 d-flex"  >
              <label className="d-flex col-6 mr-3 color" >Valor:</label>
              <input className="form-control col-10  d-flex " placeholder="Digite el valor" value={valor} type="number" onChange={(event)=>setValor(event.target.value)} />
              </div>

              <div className="col-md-12 d-flex"  >
              <textarea className="form-control  d-flex" placeholder="Observaciones" value={observa} type="text" style={{ border:'1px solid #FFDE59'}} onChange={(event)=>setObserva(event.target.value)} />
              </div>
  
  
  
          </div>
          <div className="my-2 d-flex col-md-12 justify-content-end px-0" style = {{float:'left'}}>
            <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:20, color:'black'}} onClick = {()=>saveCobroServicio()}>Añadir cobro de servicio</button>
          </div>
        </div>
  
      </div>
    );
}

export default NewCobroServicios;
if(document.getElementById('NewCobroServicios')) {
    ReactDOM.render(<NewCobroServicios />, document.getElementById('NewCobroServicios'));
}