import React, {useState, useEffect, Fragment} from 'react';
import {ip} from '../../ApiRest';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import axios from 'axios';
import Swal from 'sweetalert2';
import '../../../../css/app.scss';
import makeAnimated from 'react-select/animated';
import { data } from 'jquery';
import { Divider } from '@material-ui/core';
import obtener from './Cuenta_conex';



function NewServicios (props) {

    const [loading, setLoading] = useState(false);
    const [text, setText] = useState('');
    const [id, setId] = useState(null)
    const [number_cat, setNumber_cat] = useState(props.servicios.length+1)

    const [categoria, setCategoria] = useState(null);
    const [nombreServ, setNombreServ] = useState(null);
    const [nombreCat, setNombreCat] = useState(null);
    const [categoriaSelect, setCategoriaSelect] = useState([]);
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);

    const [cuentas, setCuentas ] = useState(null);
    const [cuentaSelect, setCuentaSelect] = useState([]);
    const [subCuentas, setSubCuentas] = useState(null);
    const [subCuentaSelect, setSubCuentaSelect] = useState([]);
    const [ cuentaCont, setCuentaCont] = useState([]);
    //////
    const [listaCuent, setListaCuent ] = useState([])
    ///
    const [estado, setEstado ] = useState(1);
    const [estado2, setEstado2 ] = useState(1);
    const [estado3, setEstado3 ] = useState(1);


    const [subCuenta2, setSubCuenta2] = useState(null);
    const [subCuenta2Select, setSubCuenta2Select ] = useState([])
    ///
    const [subCuenta3, setSubCuenta3] = useState(null);
    const [subCuenta3Select, setSubCuenta3Select ] = useState([])
    //
    const [subCuenta4, setSubCuenta4] = useState(null);
    const [subCuenta4Select, setSubCuenta4Select ] = useState([])

    useEffect(()=>{
        obtain_categoria();
        validar();
        validarCategoria();
        cuentas_cont()
    }, [])

    const obtain_categoria = () => {
        axios.get(ip+'admon/obtain_categoria').then(response=>{
            console.log(response);
            var res = response.data;
            var categoria = []
            res.map(item=>{
              data = {
                value:item.id_cat,
                label:item.nombre_cat
              }
              categoria.push(data)
            })
            setCategoriaSelect(categoria);
            if(props.id>0){
                var filtercategoria = res.filter(e=>e.id_cat == props.data.categoria.id_cat)
                console.log("esta es la props tarifa");
                console.log(props.data);
                console.log(filtercategoria);
                const data = {
                    value:filtercategoria[0].id_cat,
                    label:filtercategoria[0].nombre_cat
                }
                setCategoria(data)
            }
        })
    }

    const cuentas_cont = () => {
      axios.get(ip+'admon/cat_cuentas').then(response=>{
        console.log(response.data);
        var res = response.data;
        var cuenta = []
        res.map(item=>{
          data = {
            value:item.id,
            label:item.nombre_cuenta
          }
          cuenta.push(data)
        })
        setCuentaSelect(cuenta);
      })

    }

    const envia = async (e) =>{
      setCuentas(e)
      var data = await obtener.cuentas(e.value);
      setListaCuent(data)

      console.log("aquiii obtener valor");
      console.log(data);
      var cuentas = [];
      data.map(item => {
            const data2 = {
              value: item.id,
              label: item.codigo + " - " + item.concepto
            }
            cuentas.push(data2)
      })
      setSubCuentaSelect(cuentas);
    }

    const sub = async (valor) =>{
      console.log(valor);
        setSubCuentas(valor)
        setEstado(2)

        const data = new FormData()
        data.append('id', valor.value)
        await axios.post(ip+'admon/traer_cuent2',data).then(response=>{
            console.log(response.data)
            var res = response.data;
            var cuentas2 = [];

            res.map(item => {
                  const data2 = {
                    value: item.id,
                    label: item.codigo + " - " + item.nombre
                  }
                  cuentas2.push(data2)
            })
            setSubCuenta2Select(cuentas2);
        })
    }
    const sub2 = async (valor) =>{
        setSubCuenta2(valor)
        setEstado2(2)
        const data = new FormData()
        data.append('id', valor.value)
        await axios.post(ip+'admon/traer_cuent3',data).then(response=>{
            console.log(response.data)
            var res = response.data;
            var cuentas3 = [];
            console.log("aqui dataaa 3322222");
            res.map(item => {
                  const data2 = {
                    value: item.id,
                    label: item.codigo + " - " + item.nombre
                  }
                  cuentas3.push(data2)
            })
            setSubCuenta3Select(cuentas3);
        })
    }
    const sub3 = async (valor) =>{
        setSubCuenta3(valor)
        setEstado3(2)
        const data = new FormData()
        data.append('id', valor.value)
        await axios.post(ip+'admon/traer_cuent4',data).then(response=>{
            console.log(response.data)
            var res = response.data;
            var cuentas3 = [];
            console.log("aqui dataaa 3322222");
            res.map(item => {
                  const data2 = {
                    value: item.id,
                    label: item.codigo + " - " + item.nombre
                  }
                  cuentas3.push(data2)
            })
            setSubCuenta4Select(cuentas3);
        })
    }


    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

    const saveServicio = async () => {
      var message = ''
      var error = false
      if (nombreServ == ''){
      error = true
      message = "El nombre del servicio no puede ser vacío "
    }else if (categoria == null){
        error = true
        message = "Selecciona la categoria "
    }
    if(error){
        MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('id_servicios',props.id)
      data.append('nombre_servic', nombreServ)
      data.append('cat_id', categoria.value)
      ///
      data.append('id_cat_cuent', cuentas.value)
      data.append('id_sub_cuent', subCuentas.value)
      data.append('id_cuenta_sub', subCuenta2.value)
      data.append('id_sub_cuen2', subCuenta3.value)
      data.append('id_sub_cuent3', subCuenta4.value)



      axios.post(ip+'admon/guard_servicio',data).then(response=>{
        if (props.id == null) {
          MessageSuccess("Servicio creado correctamente");
          props.gobackSave();
        }
        else{
          MessageSuccess("Servicio editado correctamente");
          props.gobackSave();
        }
      })
    }

  }

  const saveCategoria = async () => {
    var message = ''
    var error = false
    if (nombreCat == ''){
    error = true
    message = "Escribe el nombre de la categoría "
  }
  if(error){
      MessageError(message)
  }
  else{
    const data = new FormData()
    data.append('id_cat',props.id)
    data.append('nombre_cat', nombreCat)
    axios.post(ip+'admon/guard_categoria',data).then(response=>{
      if (props.id == null) {
        MessageSuccess("Categoria creada correctamente");
        props.gobackSave();
      }
      else{
        MessageSuccess("Servicio editado correctamente");
        props.gobackSave();
      }
    })
  }

}

  const validar = async () =>{
    console.log("entro aquiii");
    if(props.id > 0 ){
      console.log("entro al if");
      setNombreServ(props.data.nombre_servic);
      setCategoria(props.data.cat_id);

    }
  }

  const validarCategoria = async () =>{
    console.log("entro aquiii");
    if(props.id > 0 ){
      console.log("entro al if");
      setNombreCat(props.data.nombre_cat);

    }
  }

    return(
      <div className="mt-5 col-12">
        <div className="btn container col-1 " onClick = {()=>props.goback()}>
          <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
        </div>
      <div className="mt-3" style={{marginLeft:4}}>
      <h4>Agregar Servicios</h4>
      </div>


        <div  className="form-group">
          <div className="row col-12">

          <div className="col-md-8 d-flex">
            <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '220px'}}>Categoría: </label>
            <Select
            className="col-md-8"
            value={categoria}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={categoriaSelect}
            onChange={(e)=>setCategoria(e)}
            placeholder = "Seleccione categoría"
            name="colors"
            />
            </div>
            <div className="col-md-8 d-flex">
            <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '220px'}}>Cuentas Contables: </label>
            <Select
            className="col-md-8"
            value={cuentas}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={cuentaSelect}
            onChange={(e)=>envia(e)}
            placeholder = "Seleccione cuenta contable"
            name="colors"
            />
            </div>

            <div className="col-md-8 d-flex">
            <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '220px'}}>Sub cuentas contables: </label>
            <Select
            className="col-md-8"
            value={subCuentas}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={subCuentaSelect}
            onChange={(e)=>sub(e)}
            placeholder = "Seleccione subcuenta contable"
            name="colors"
            />
            </div>
            { estado == 2 ?
              <div className="col-md-8 d-flex">
              <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '220px'}}>Sub cuentas 2 contables: </label>
              <Select
              className="col-md-8"
              value={subCuenta2}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={subCuenta2Select}
              onChange={(e)=>sub2(e)}
              placeholder = "Seleccione subcuenta contable"
              name="colors"
              />
              </div>
              : null
              }
              { estado2 == 2 ?

              <div className="col-md-8 d-flex">
              <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '220px'}}>Sub cuentas 3 contables: </label>
              <Select
              className="col-md-8"
              value={subCuenta3}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={subCuenta3Select}
              onChange={(e)=>sub3(e)}
              placeholder = "Seleccione subcuenta contable"
              name="colors"
              />
              </div>
              :null

             }
             { estado3 == 2 ?

             <div className="col-md-8 d-flex">
             <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '220px'}}>Sub cuentas 4 contables: </label>
             <Select
             className="col-md-8"
             value={subCuenta4}
             closeMenuOnSelect={true}
             components={animatedComponents}
             options={subCuenta4Select}
             onChange={(e)=>setSubCuenta4(e)}
             placeholder = "Seleccione subcuenta contable"
             name="colors"
             />
             </div>
             :null

            }



            <div className="col-md-8 d-flex" >
            <label className="d-flex col-6 mr-3 align-items-center color" style={{width: '220px'}}>Nombre del servicio:</label>
            <input className="form-control col-md-8 d-flex" placeholder="Ingrese el nombre del servicio" style={{ border:'1px solid colors', width: '324px'}} value={nombreServ} onChange={(event)=>setNombreServ(event.target.value)} type="text" />
            </div>


        </div>
          <div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
            <button className="btn btn-success" style={{border:'1px solid #FFDE59', backgroundColor:'#FFDE59', color:'black'}} onClick = {()=>saveServicio()}>Guardar servicio</button>
          </div>
      </div>
      <div className="col-12">
      <div className="divider" style={{marginTop:'10%'}}>
      <Divider className="col-md-10 mt-5 my-3" style={{backgroundColor:'black'}} />
      </div>
          <h4 className="form-section d-flex align-items-center">Agregar Categoría</h4>
        </div>
      <div  className="form-group">
          <div className="row col-12">
            <div className="col-md-12 d-flex" >
            <label className="d-flex col-4 mr-3 align-items-center color" style={{width: '200px'}}>Nombre de la categoría:</label>
            <input className="form-control col-md-6 mr-3 d-flex" placeholder="Ingrese el nombre de la categoría" style={{ border:'1px solid colors'}} value={nombreCat} onChange={(event)=>setNombreCat(event.target.value)} type="text" />
            <button className="btn btn-success col-md-3" style={{border:'1px solid #FFDE59', backgroundColor:'#FFDE59', color:'black', height:'37px'}} onClick = {()=>saveCategoria()}>Guardar Categoria</button>
            </div>
            <div className="col-md-8 d-flex" style = {{float:'right', marginTop: '96'}}>

            </div>
        </div>

      </div>

    </div>

    );
}

export default NewServicios;
if(document.getElementById('NewServicios')) {
    ReactDOM.render(<NewServicios />, document.getElementById('NewServicios'));
}
