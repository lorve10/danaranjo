import React,{useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Header from '../../Ui/Header';
import Footer from '../../Ui/Footer';
import Swal from 'sweetalert2';
import '../../../../css/app.scss';
import NewServicios from './NewServicios';
import NewCobroServicios from './NewCobroServicios';
import Pagination from '../../pagination/Pagination'
import PaginationButton from '../../pagination/Pagination-button'


function Conceptos () {
  const [loading, setLoading] = useState(false)
  const [text, setText] = useState('')
  const [listServicios, setListServicios ] = useState([]);
  const [backServ, setBackServ] = useState(listServicios);
  const [backServ2, setBackServ2] = useState(listServicios);
  const [backServ3, setBackServ3] = useState(listServicios);
  const [id, setId] = useState(null);
  const [data, setData] = useState({})
  const [showForm, setShowForm] = useState(1)
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [marginval, setMarginVal] = useState(false)
  const [estado, setEstado] = useState(1);


  const updateCurrentPage = async (number) => {
    await setListServicios([])
    await setCurrentPage(number)
    const dataNew = Pagination.paginate(backServ,number,postsPerPage)
    await setListServicios(dataNew)
  }

  const searchInput = async(value) => {
    console.log(backServ3)
    await setText(value)
    await setLoading(true)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData = backServ3.filter(function(item2){
      var servicio = ((item2.nombre_servic).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var resServicio = servicio.indexOf(inputSearch) > -1
      var resServicioLasT = servicio.indexOf(inputSearch) > -1
      var res = false
      if(resServicio||resServicioLasT){
        res = true
      }

      return res;
    })
      await setCurrentPage(1)
      await setBackServ(newData)
      const dataNew = Pagination.paginate(newData,1,postsPerPage)
      await setListServicios(dataNew)
      await setLoading(false)

  }

  const mostrarServicios = async() =>{
    axios.get(ip+'admon/list_Servicios').then(response=>{
      console.log(response);
      var res = response.data;
      var filter = res.filter(e=>e.deleted == 0)
      setBackServ(filter)
      setBackServ2(filter)
      setBackServ3(res)
      const dataNew = Pagination.paginate(filter,currentPage,postsPerPage)
      setListServicios(dataNew)
    })
  }

  const deshabilitar = (value) =>{
    var message = '¿Quieres eliminar este servicio?'
    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,

    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id_servicios', value.id_servicios)
        axios.post(ip+'admon/deleted_servicio',data).then(response=>{

        Swal.fire('Servicio Eliminado correctamente!', '', 'success')
        mostrarServicios();
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }

  useEffect(()=>{
    mostrarServicios();
  }, [])

const goback = async () => {
    await setEstado(1)
    setId(null)
  }

  const gobackSave = () => {
    setEstado(1)
    setId(null)
    setData({})
    mostrarServicios();
  }

  const propHeader = (value) => {
    console.log("entro",value);
    setMarginVal(value)
  }


  return(
    <div className="content-wrapper" style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}} >
    <Header section ={7}  marginval = {(value)=>propHeader(value)}/>
    <div className="verticar-footer">
          <div className="deshabilitar" onClick = {()=>setEstado(3)} style={{marginTop:50}}>
              <h6 className="form-section d-flex align-items-center  my-3" style={{backgroundColor:'#FFDE59', borderRadius:15, marginTop:50, cursor:'pointer'}}><i className="material-icons" style= {{fontSize:'170%'}}>control_point</i>Añadir Servicio</h6>
          </div>
          <div className="" onClick={()=>setEstado(3)} style={{marginTop:20}}>
              <h5 className="form-section habilitar" onClick = {()=>setEstado(3)}><i className="material-icons" style= {{fontSize:'150%', marginLeft:14, marginTop:15, backgroundColor:'#FFDE59', borderRadius:15, width:66}}> control_point</i></h5>
          </div>
          <img className="imagen" src={ip+'images/footer.png'} style={{marginTop:200}}/>
    </div>
    {
      estado == 1 ?
      <div >
      <div className="mt-2">
      <h6 className="mt-2" style={{marginLeft:'60px'}}>/<a href = {ip+'admon/cartera'}>Cartera</a>/Conceptos</h6>

        <h4 className="text-center" style={{marginLeft:'-86%'}}>Conceptos</h4>
      </div>
        <div className="col-8 d-flex">

        <div className="ml-4 col-md-12 mt-1 d-flex "  style={{marginLeft:120}} >
            <div className="col-12 mt-5 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #D9D9D9', backgroundColor:'white',borderRadius:12}}>
            <input  onChange = {(e)=>searchInput(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Consultar conceptos... "/>
            {
              text.length == 0 ?
              <span style = {{color:'black', cursor:'pointer'}}  className= "material-icons">search</span>
              :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
            }
            </div>
        </div>
        </div>
      </div>
      :null
    }
    {
      estado == 1?
      <div className="col-md-10" style={{marginLeft:35}}>
        <table class="table table-striped">
        <thead style={{backgroundColor: '#FFDE59' }}>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Categoria</th>
            <th scope="col">Servicio</th>
            <th scope="col">Deshabilitar</th>
          </tr>
        </thead>
        <tbody>

        {
          listServicios.map((item)=>{
            return(
              <tr>
                <th scope="row">{item.id_servicios}</th>
                <td>{item.categoria_id.nombre_cat}</td>
                <td>{item.nombre_servic }</td>
                <td>
                  <div>
                  <button onClick = {()=>deshabilitar(item)} className="btn "><i class = "material-icons">delete</i></button>
                  </div>
                </td>
              </tr>
            )
          })
        }

        </tbody>
      </table>
      <div className="d-flex col-md-12 col-12 justify-content-end">
        <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backServ.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
      </div>
    </div>
      :null
    }

    {estado == 3 ?
      <div>
            <div className=" col-md-8" style={{marginTop: 50, marginLeft:46}}>
              <NewServicios servicios = {backServ3} data={data} gobackSave = {()=>gobackSave()} goback = {()=>goback()}  id = {id}/>
            </div>
        </div>

      :null
    }

    {estado == 4 ?
      <div>
            <div className=" col-md-8" style={{marginTop: 50, marginLeft:46}}>
              <h4>Agregar cobro de servicios</h4>
              <NewCobroServicios servicios = {backServ3} data={data} gobackSave = {()=>gobackSave()} goback = {()=>goback()}  id = {id}/>
            </div>
        </div>

      :null
    }

  </div>


);

}
export default Conceptos;
if (document.getElementById('Conceptos')) {
  ReactDOM.render(<Conceptos />, document.getElementById('Conceptos'));
}
