import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import {ip} from '../../ApiRest';
import Divider from '@material-ui/core/Divider';
import '../../../../css/app.scss';
import Swal from 'sweetalert2'



function Conceptos(props) {

  const [datos1, setDatos1]=useState([])
  const [datos2, setDatos2]=useState([])
  const [datos3, setDatos3]=useState([])
  const [datos4, setDatos4]=useState([])
  const [datos5, setDatos5]=useState([])
  const [datos6, setDatos6]=useState([])
  const [datos7, setDatos7]=useState([])
  const [datos8, setDatos8]=useState([])
  const [datos9, setDatos9]=useState([])

  const [showForm, setShowForm] = useState(1)
  const [showForm2, setShowForm2] = useState(1)
  const [data, setData] = useState({})
  const [data2, setData2] = useState({})



  const [text, setText] = useState('')
  const [cuentas, setCuentas] =useState([])
  const [marginval, setMarginVal] = useState(false)
  //////////estados
  const [estado , setEstado] = useState(false)
  const [patrimonio , setPatrimonio] = useState(false)
  const [ingresos, setIngresos ] = useState(false)
  const [gastos, setGastos ] = useState(false)
  const [venta, setVenta ] = useState(false)

  const [estado2 , setEstado2] = useState(false)
  const [estado3 , setEstado3] = useState(false)
  const [estado4 , setEstado4] = useState(false)

  const [estado5 , setEstado5] = useState(false)
  const [estado6 , setEstado6] = useState(false)
  const [estado7 , setEstado7] = useState(false)
  const [estado8 , setEstado8] = useState(false)
  //
  const [estado9 , setEstado9] = useState(false)
  const [estado10 , setEstado10] = useState(false)
  const [estado11 , setEstado11] = useState(false)
  const [estado12 , setEstado12] = useState(false)
  ///
  const [estado13 , setEstado13] = useState(false)
  const [estado14 , setEstado14] = useState(false)
  const [estado15 , setEstado15] = useState(false)
  const [estado16 , setEstado16] = useState(false)
  //
  const [estado17 , setEstado17] = useState(false)
  const [estado18 , setEstado18] = useState(false)
  const [estado19 , setEstado19] = useState(false)
  const [estado20 , setEstado20] = useState(false)
  ///
  const [estado21 , setEstado21] = useState(false)
  const [estado22 , setEstado22] = useState(false)
  const [estado23 , setEstado23] = useState(false)
  const [estado24 , setEstado24] = useState(false)
  ///
  const [costo, setCosto ] = useState(false)
  const [costo2, setCosto2 ] = useState(false)
  const [costo3, setCosto3 ] = useState(false)
  const [costo4, setCosto4 ] = useState(false)
  ///
  const [ orden, setOrden] = useState(false)
  const [ orden2, setOrden2] = useState(false)
  const [ orden3, setOrden3] = useState(false)
  const [ orden4, setOrden4] = useState(false)

  ///
  const [acreedoras, setAcreedoras] = useState(false)
  const [acreedoras2, setAcreedoras2] = useState(false)
  const [acreedoras3, setAcreedoras3] = useState(false)
  const [acreedoras4, setAcreedoras4] = useState(false)






  //////////fin estado
  const [valor, setValor] = useState(null)
  const [id, setId] = useState(null)






  useEffect(()=>{
    obtain_cuenta()
  },[])
  //props.object_data(id)

  const concep = (data, nombre) =>{
    props.object_data(data, nombre)

  }


  const obtain_cuenta = async () =>{
    axios.get(ip+'admon/obtain_cuenta').then(response=>{
      setCuentas(response.data);
      var cuenta = response.data;
      setDatos1(cuenta.filter(e=>e.cuenta_id == 1))
      setDatos2(cuenta.filter(e=>e.cuenta_id == 2))
      setDatos3(cuenta.filter(e=>e.cuenta_id == 3))
      setDatos4(cuenta.filter(e=>e.cuenta_id == 4))
      setDatos5(cuenta.filter(e=>e.cuenta_id == 5))
      setDatos6(cuenta.filter(e=>e.cuenta_id == 6))
      setDatos7(cuenta.filter(e=>e.cuenta_id == 7))
      setDatos8(cuenta.filter(e=>e.cuenta_id == 8))
      setDatos9(cuenta.filter(e=>e.cuenta_id == 9))
    })
  }
  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }

  return(
    <div className="row">

    <div className="row">
    {
    showForm == 1?

    <div className="col-md-11" style={{marginLeft:35}}>
    <div>

    </div>
    <div className="contenerdor-list">
    <div className="col-12 d-flex" style={{backgroundColor:'#FFDE59'}} onClick={()=>setEstado(!estado)}>
    <strong className="d-flex" ><p className="d-flex" >Activo</p></strong>
    <i className="material-icons">expand_more</i>
    </div>
    <div className="col-md-12">
    <Divider />

    {
    datos1.map((item, index)=>{
    return(
    <div className={!estado ? 'tabla-deshabilitar ':'tabla-habilitar'}>
    <div className="d-flex" key={index}>
    <p className="col-3"  style={{marginLeft:'5%'}} onClick={()=>setEstado2(estado2 == item.id ? false:item.id)}>{item.concepto}</p>
    <div className="hola" style={{marginLeft:'40%'}}>
    <button  className="btn "><i className = "material-icons" onClick={()=>concep(item.id, item.concepto)} >more_horiz</i></button>
    </div>
    </div>
    <Divider style={{backgroundColor:'black'}}/>
    { item.sub.map((itemsub, indexsub)=>{
    return (

    <div className={estado2 == item.id ? 'tabla-deshabilitar ':'tabla-habilitar'} style={{marginLeft:'30%'}} key={indexsub}>
    <div className="d-flex"  >

    <p className="d-flex col-3"  onClick={()=>setEstado3(estado3 == itemsub.id ? false:itemsub.id)}>{itemsub.nombre}</p>
    <div className=" " style={{marginLeft:'30%'}}>
    <button  className="btn "><i className = "material-icons" onClick={()=>concep(itemsub.id, itemsub.nombre)}>more_horiz</i></button>
    </div>
    </div>
    <Divider style={{backgroundColor:'black'}}/>
    <div className="" style={{marginLeft:'20%'}}>
    {itemsub.sub2.map((itemsub2, indexsub2)=>{
    return(

      <div className={estado3 === itemsub.id ? 'tabla-habilitar':'tabla-deshabilitar '}>

      <div className="d-flex" key={indexsub2}>
      <p className="d-flex col-3" onClick={()=>setEstado4(estado4 == itemsub2.id ? false:itemsub2.id)}>{itemsub2.nombre}</p>
      <div className="" style={{marginLeft:'4%'}}>
      <button  className="btn "><i className = "material-icons" onClick={()=>concep(itemsub2.id, itemsub2.nombre)}>more_horiz</i></button>
      </div>
      </div>
      <Divider style={{backgroundColor:'black'}} />

      {itemsub2.sub3.map((itemsub3, indexsub3)=>{
                  return(
                    <div className={estado4 === itemsub2.id ? 'tabla-habilitar':'tabla-deshabilitar '} style={{marginLeft:'30%'}} key={indexsub3}  >
                    <div className="d-flex">
                    <p className="d-flex col-3">{itemsub3.nombre}</p>

                    <div className="" style={{marginLeft:'4%'}}>
                    <button  className="btn "><i className = "material-icons" onClick={()=>concep(itemsub3.id, itemsub3.nombre)}>more_horiz</i></button>

                    </div>
                    </div>
                    <Divider style={{backgroundColor:'black'}}/>
                    </div>
                  )
                })
      }
      </div>
    )
    })
    }
    </div>

    </div>

    );
    })
    }
    </div>
    )
    })
    }
    </div>
    </div>
    <div className="contenerdor-list mt-2">
    <div className="col-12 d-flex" style={{backgroundColor:'#FFDE59'}}>
    <strong className="d-flex" onClick={()=>setEstado5(!estado5)}><p>Pasivo</p></strong>
    <i className="material-icons">expand_more</i>

    </div>
    <div className="col-md-12">
    {
    datos2.map((item, index)=>{
    return(
    <div className={!estado5 ? 'tabla-deshabilitar ':'tabla-habilitar'}>
    <div className="d-flex" key={index}>
    <p className="col-3 d-flex" onClick={()=>setEstado6(estado6 == item.id ? false:item.id)} style={{marginLeft:'5%'}}>{item.concepto}
    </p>
    <div className="hola d-flex" style={{marginLeft:'40%'}}>
    <button  className="btn "><i class = "material-icons"  onClick={()=>concep(item.id, item.concepto)} >more_horiz</i></button>
    </div>

    </div>
    <Divider/>

    <div className="" style={{marginLeft:'25%'}}>

    { item.sub.map((itemsub, indexsub)=>{
    return (
    <div className={estado6 === item.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub} >
    <div className="d-flex" >
    <p className="d-flex col-3" onClick={()=>setEstado7(estado7 == itemsub.id ? false:itemsub.id)}>{itemsub.nombre}</p>
    <div className=" " style={{marginLeft:'25%'}}>
    <button  className="btn "><i class = "material-icons"  onClick={()=>concep(itemsub.id, itemsub.nombre)}>more_horiz</i></button>
    <Divider/>
    </div>
    <Divider/>
    </div>
    <Divider/>

    <div className="" style={{marginLeft:'20%'}}>
    {itemsub.sub2.map((itemsub2, indexsub2 )=>{
      return(
        <div className={estado7 === itemsub.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub2} >
        <div className="d-flex">
        <p className="d-flex col-3" onClick={()=>setEstado8(estado8 == itemsub2.id ? false:itemsub2.id)}>{itemsub2.nombre}</p>
        <div className="" style={{marginLeft:'4%'}}>
        <button  className="btn "><i class = "material-icons"  onClick={()=>concep(itemsub2.id,itemsub2.nombre)}>more_horiz</i></button>
        <Divider/>
        </div>
        </div>
        <Divider/>

        {itemsub2.sub3.map(itemsub3=>{
                    return(
                      <div style={{marginLeft:'40%'}}  className={estado8 === itemsub2.id ? 'tabla-habilitar':'tabla-deshabilitar '}>
                      <div className="d-flex">
                      <p className="d-flex col-3">{itemsub3.nombre}</p>

                      <div className="" style={{marginLeft:'4%'}}>
                      <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub3.id, itemsub3.nombre)}>more_horiz</i></button>

                      <Divider/>
                      </div>
                      </div>
                      <Divider/>
                      </div>
                    )
                  })
        }
        </div>
      )
    })

    }
    </div>

    </div>

    );
    })

    }

    </div>
    </div>
    )
    })
    }
    </div>
    </div>
    <div className="contenerdor-list mt-2">
    <div className="col-12 d-flex" style={{backgroundColor:'#FFDE59'}}>
    <strong className="d-flex" onClick={()=>setEstado9(!estado9)}><p>Patrimonio</p></strong>
    <i className="material-icons">expand_more</i>

    </div>
    <div className="col-md-12">
    {
    datos3.map((item, index)=>{
    return(
    <div className={!estado9 ? 'tabla-deshabilitar ':'tabla-habilitar'}>
    <div className="d-flex" key={index}>
    <p className="col-3 d-flex" onClick={()=>setEstado10(estado10 == item.id ? false:item.id)} style={{marginLeft:'5%'}}>{item.concepto}
    </p>
    <div className="hola d-flex" style={{marginLeft:'40%'}}>
    <button  className="btn "><i class = "material-icons" onClick={()=>concep(item.id, item.concepto)}>more_horiz</i></button>

    </div>

    </div>
    <Divider/>

    <div className="" style={{marginLeft:'25%'}}>

    { item.sub.map((itemsub, indexsub)=>{
    return (
    <div className={estado10 === item.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub} >
    <div className="d-flex" >
    <p className="d-flex col-3" onClick={()=>setEstado11(estado11 == itemsub.id ? false:itemsub.id)}>{itemsub.nombre}</p>
    <div className=" " style={{marginLeft:'25%'}}>
    <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub.id, itemsub.nombre)}>more_horiz</i></button>

    <Divider/>
    </div>
    <Divider/>
    </div>
    <Divider/>

    <div className="" style={{marginLeft:'20%'}}>
    {itemsub.sub2.map((itemsub2, indexsub2 )=>{
      return(
        <div className={estado11 === itemsub.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub2} >
        <div className="d-flex">
        <p className="d-flex col-3" onClick={()=>setEstado12(estado12 == itemsub2.id ? false:itemsub2.id)}>{itemsub2.nombre}</p>
        <div className="" style={{marginLeft:'4%'}}>
        <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub2.id,itemsub2.nombre)}>more_horiz</i></button>

        <Divider/>
        </div>
        </div>
        <Divider/>

        {itemsub2.sub3.map(itemsub3=>{
                    return(
                      <div style={{marginLeft:'40%'}}  className={estado12 === itemsub2.id ? 'tabla-habilitar':'tabla-deshabilitar '}>
                      <div className="d-flex">
                      <p className="d-flex col-3">{itemsub3.nombre}</p>

                      <div className="" style={{marginLeft:'4%'}}>
                      <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub3.id, itemsub3.nombre)}>more_horiz</i></button>

                      <Divider/>
                      </div>
                      </div>
                      <Divider/>
                      </div>
                    )
                  })
        }
        </div>
      )
    })

    }
    </div>

    </div>

    );
    })

    }

    </div>
    </div>
    )
    })
    }
    </div>
    </div>
    <div className="contenerdor-list mt-2">
    <div className="col-12 d-flex" style={{backgroundColor:'#FFDE59'}}>
    <strong className="d-flex" onClick={()=>setEstado13(!estado13)}><p>Ingresos</p></strong>
    <i className="material-icons">expand_more</i>

    </div>
    <div className="col-md-12">
    {
    datos4.map((item, index)=>{
    return(
    <div className={!estado13 ? 'tabla-deshabilitar ':'tabla-habilitar'}>
    <div className="d-flex" key={index}>
    <p className="col-3 d-flex" onClick={()=>setEstado14(estado14 == item.id ? false:item.id)} style={{marginLeft:'5%'}}>{item.concepto}
    </p>
    <div className="hola d-flex" style={{marginLeft:'40%'}}>
    <button  className="btn "><i class = "material-icons" onClick={()=>concep(item.id, item.concepto)}>more_horiz</i></button>

    </div>

    </div>
    <Divider/>

    <div className="" style={{marginLeft:'25%'}}>

    { item.sub.map((itemsub, indexsub)=>{
    return (
    <div className={estado14 === item.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub} >
    <div className="d-flex" >
    <p className="d-flex col-3" onClick={()=>setEstado15(estado15 == itemsub.id ? false:itemsub.id)}>{itemsub.nombre}</p>
    <div className=" " style={{marginLeft:'25%'}}>
    <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub.id, itemsub.nombre)}>more_horiz</i></button>

    <Divider/>
    </div>
    <Divider/>
    </div>
    <Divider/>

    <div className="" style={{marginLeft:'20%'}}>
    {itemsub.sub2.map((itemsub2, indexsub2 )=>{
      return(
        <div className={estado15 === itemsub.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub2} >
        <div className="d-flex">
        <p className="d-flex col-3" onClick={()=>setEstado16(estado16 == itemsub2.id ? false:itemsub2.id)}>{itemsub2.nombre}</p>
        <div className="" style={{marginLeft:'4%'}}>
        <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub2.id, itemsub2.nombre)}>more_horiz</i></button>

        <Divider/>
        </div>
        </div>
        <Divider/>

        {itemsub2.sub3.map(itemsub3=>{
                    return(
                      <div style={{marginLeft:'40%'}}  className={estado16 === itemsub2.id ? 'tabla-habilitar':'tabla-deshabilitar '}>
                      <div className="d-flex">
                      <p className="d-flex col-3">{itemsub3.nombre}</p>

                      <div className="" style={{marginLeft:'4%'}}>
                      <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub3.id, itemsub3.nombre)}>more_horiz</i></button>

                      <Divider/>
                      </div>
                      </div>
                      <Divider/>
                      </div>
                    )
                  })
        }
        </div>
      )
    })

    }
    </div>

    </div>

    );
    })

    }

    </div>
    </div>
    )
    })
    }
    </div>
    </div>
    <div className="contenerdor-list mt-2">
    <div className="col-12 d-flex" style={{backgroundColor:'#FFDE59'}}>
    <strong className="d-flex" onClick={()=>setEstado17(!estado17)}><p>Gastos</p></strong>
    <i className="material-icons">expand_more</i>

    </div>
    <div className="col-md-12">
    {
    datos4.map((item, index)=>{
    return(
    <div className={!estado17 ? 'tabla-deshabilitar ':'tabla-habilitar'}>
    <div className="d-flex" key={index}>
    <p className="col-3 d-flex" onClick={()=>setEstado18(estado18 == item.id ? false:item.id)} style={{marginLeft:'5%'}}>{item.concepto}
    </p>
    <div className="hola d-flex" style={{marginLeft:'40%'}}>
    <button  className="btn "><i class = "material-icons" onClick={()=>concep(item.id,item.concepto)}>more_horiz</i></button>

    </div>

    </div>
    <Divider/>

    <div className="" style={{marginLeft:'25%'}}>

    { item.sub.map((itemsub, indexsub)=>{
    return (
    <div className={estado18 === item.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub} >
    <div className="d-flex" >
    <p className="d-flex col-3" onClick={()=>setEstado19(estado19 == itemsub.id ? false:itemsub.id)}>{itemsub.nombre}</p>
    <div className=" " style={{marginLeft:'25%'}}>
    <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub.id,itemsub.nombre)}>more_horiz</i></button>

    <Divider/>
    </div>
    <Divider/>
    </div>
    <Divider/>

    <div className="" style={{marginLeft:'20%'}}>
    {itemsub.sub2.map((itemsub2, indexsub2 )=>{
      return(
        <div className={estado19 === itemsub.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub2} >
        <div className="d-flex">
        <p className="d-flex col-3" onClick={()=>setEstado20(estado20 == itemsub2.id ? false:itemsub2.id)}>{itemsub2.nombre}</p>
        <div className="" style={{marginLeft:'4%'}}>
        <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub2.id,itemsub2.nombre)}>more_horiz</i></button>

        <Divider/>
        </div>
        </div>
        <Divider/>

        {itemsub2.sub3.map(itemsub3=>{
                    return(
                      <div style={{marginLeft:'40%'}}  className={estado20 === itemsub2.id ? 'tabla-habilitar':'tabla-deshabilitar '}>
                      <div className="d-flex">
                      <p className="d-flex col-3">{itemsub3.nombre}</p>

                      <div className="" style={{marginLeft:'4%'}}>
                      <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub3.id, itemsub3.nombre)}>more_horiz</i></button>

                      <Divider/>
                      </div>
                      </div>
                      <Divider/>
                      </div>
                    )
                  })
        }
        </div>
      )
    })

    }
    </div>

    </div>

    );
    })

    }

    </div>
    </div>
    )
    })
    }
    </div>
    </div>
    <div className="contenerdor-list mt-2">
    <div className="col-12 d-flex" style={{backgroundColor:'#FFDE59'}}>
    <strong className="d-flex" onClick={()=>setEstado21(!estado21)}><p>Costo de Venta</p></strong>
    <i className="material-icons">expand_more</i>

    </div>
    <div className="col-md-12">
    {
    datos5.map((item, index)=>{
    return(
    <div className={!estado21 ? 'tabla-deshabilitar ':'tabla-habilitar'}>
    <div className="d-flex" key={index}>
    <p className="col-3 d-flex" onClick={()=>setEstado22(estado22 == item.id ? false:item.id)} style={{marginLeft:'5%'}}>{item.concepto}
    </p>
    <div className="hola d-flex" style={{marginLeft:'40%'}}>
    <button  className="btn "><i class = "material-icons" onClick={()=>concep(item.id,item.concepto)}>more_horiz</i></button>

    </div>

    </div>
    <Divider/>

    <div className="" style={{marginLeft:'25%'}}>

    { item.sub.map((itemsub, indexsub)=>{
    return (
    <div className={estado22 === item.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub} >
    <div className="d-flex" >
    <p className="d-flex col-3" onClick={()=>setEstado23(estado23 == itemsub.id ? false:itemsub.id)}>{itemsub.nombre}</p>
    <div className=" " style={{marginLeft:'25%'}}>
    <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub.id, itemsub.nombre)}>more_horiz</i></button>

    <Divider/>
    </div>
    <Divider/>
    </div>
    <Divider/>

    <div className="" style={{marginLeft:'20%'}}>
    {itemsub.sub2.map((itemsub2, indexsub2 )=>{
      return(
        <div className={estado23 === itemsub.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub2} >
        <div className="d-flex">
        <p className="d-flex col-3" onClick={()=>setEstado24(estado24 == itemsub2.id ? false:itemsub2.id)}>{itemsub2.nombre}</p>
        <div className="" style={{marginLeft:'4%'}}>
        <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub2.id, itemsub2.nombre)}>more_horiz</i></button>

        <Divider/>
        </div>
        </div>
        <Divider/>

        {itemsub2.sub3.map(itemsub3=>{
                    return(
                      <div style={{marginLeft:'40%'}}  className={estado24 === itemsub2.id ? 'tabla-habilitar':'tabla-deshabilitar '}>
                      <div className="d-flex">
                      <p className="d-flex col-3">{itemsub3.nombre}</p>

                      <div className="" style={{marginLeft:'4%'}}>
                      <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub3.id, itemsub3.nombre)}>more_horiz</i></button>

                      <Divider/>
                      </div>
                      </div>
                      <Divider/>
                      </div>
                    )
                  })
        }
        </div>
      )
    })

    }
    </div>

    </div>

    );
    })

    }

    </div>
    </div>
    )
    })
    }
    </div>
    </div>
    <div className="contenerdor-list mt-2">
    <div className="col-12 d-flex" style={{backgroundColor:'#FFDE59'}}>
    <strong className="d-flex" onClick={()=>setCosto(!costo)}><p>Costo de produccion o de operaciones</p></strong>
    <i className="material-icons">expand_more</i>

    </div>
    <div className="col-md-12">
    {
    datos6.map((item, index)=>{
    return(
    <div className={!costo ? 'tabla-deshabilitar ':'tabla-habilitar'}>
    <div className="d-flex" key={index}>
    <p className="col-3 d-flex" onClick={()=>setCosto2(costo2 == item.id ? false:item.id)} style={{marginLeft:'5%'}}>{item.concepto}
    </p>
    <div className="hola d-flex" style={{marginLeft:'40%'}}>
        <button  className="btn "><i class = "material-icons" onClick={()=>concep(item.id, item.concepto)}>more_horiz</i></button>

    </div>

    </div>
    <Divider/>

    <div className="" style={{marginLeft:'25%'}}>

    { item.sub.map((itemsub, indexsub)=>{
    return (
      <div className={costo2 === item.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub} >
      <div className="d-flex" >
      <p className="d-flex col-3" onClick={()=>setCosto3(costo3 == itemsub.id ? false:itemsub.id)}>{itemsub.nombre}</p>
      <div className=" " style={{marginLeft:'25%'}}>
      <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub.id, itemsub.nombre)}>more_horiz</i></button>

      <Divider/>
      </div>
      <Divider/>
      </div>
      <Divider/>

      <div className="" style={{marginLeft:'20%'}}>
      {itemsub.sub2.map((itemsub2, indexsub2 )=>{
                  return(
                    <div className={costo3 === itemsub.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub2} >
                    <div className="d-flex">
                    <p className="d-flex col-3" onClick={()=>setCosto4(costo4 == itemsub2.id ? false:itemsub2.id)}>{itemsub2.nombre}</p>
                    <div className="" style={{marginLeft:'4%'}}>
                    <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub2.id, itemsub2.nombre)}>more_horiz</i></button>

                    <Divider/>
                    </div>
                    </div>
                    <Divider/>

                    {itemsub2.sub3.map(itemsub3=>{
                                return(
                                  <div style={{marginLeft:'40%'}}  className={costo4 === itemsub2.id ? 'tabla-habilitar':'tabla-deshabilitar '}>
                                  <div className="d-flex">
                                  <p className="d-flex col-3">{itemsub3.nombre}</p>

                                  <div className="" style={{marginLeft:'4%'}}>
                                  <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub3.id, itemsub3.nombre)}>more_horiz</i></button>

                                  <Divider/>
                                  </div>
                                  </div>
                                  <Divider/>
                                  </div>
                                )
                              })
                    }
                    </div>
                  )
                })

       }
      </div>

      </div>

    );
    })

    }

    </div>
    </div>
    )
    })
    }
    </div>
    </div>
    <div className="contenerdor-list mt-2">
    <div className="col-12 d-flex" style={{backgroundColor:'#FFDE59'}}>
    <strong className="d-flex" onClick={()=>setOrden(!orden)}><p>cuentas de orden deudora </p></strong>
    <i className="material-icons">expand_more</i>

    </div>
    <div className="col-md-12">
    {
    datos7.map((item, index)=>{
    return(
    <div className={!orden ? 'tabla-deshabilitar ':'tabla-habilitar'}>
    <div className="d-flex" key={index}>
    <p className="col-3 d-flex" onClick={()=>setOrden2(orden2 == item.id ? false:item.id)} style={{marginLeft:'5%'}}>{item.concepto}
    </p>
    <div className="hola d-flex" style={{marginLeft:'40%'}}>
        <button  className="btn "><i class = "material-icons" onClick={()=>concep(item.id, item.concepto)}>more_horiz</i></button>

    </div>

    </div>
    <Divider/>

    <div className="" style={{marginLeft:'25%'}}>

    { item.sub.map((itemsub, indexsub)=>{
    return (
      <div className={orden2 === item.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub} >
      <div className="d-flex" >
      <p className="d-flex col-3" onClick={()=>setOrden3(orden3 == itemsub.id ? false:itemsub.id)}>{itemsub.nombre}</p>
      <div className=" " style={{marginLeft:'25%'}}>
      <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub.id, itemsub.nombre)}>more_horiz</i></button>

      <Divider/>
      </div>
      <Divider/>
      </div>
      <Divider/>

      <div className="" style={{marginLeft:'20%'}}>
      {itemsub.sub2.map((itemsub2, indexsub2 )=>{
                  return(
                    <div className={orden3 === itemsub.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub2} >
                    <div className="d-flex">
                    <p className="d-flex col-3" onClick={()=>setOrden4(orden4 == itemsub2.id ? false:itemsub2.id)}>{itemsub2.nombre}</p>
                    <div className="" style={{marginLeft:'4%'}}>
                    <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub2.id, itemsub2.nombre)}>more_horiz</i></button>

                    <Divider/>
                    </div>
                    </div>
                    <Divider/>

                    {itemsub2.sub3.map(itemsub3=>{
                                return(
                                  <div style={{marginLeft:'40%'}}  className={orden4 === itemsub2.id ? 'tabla-habilitar':'tabla-deshabilitar '}>
                                  <div className="d-flex">
                                  <p className="d-flex col-3">{itemsub3.nombre}</p>

                                  <div className="" style={{marginLeft:'4%'}}>
                                  <button  className="btn "><i class = "material-icons" onClick={()=>concep(itemsub3.id, itemsub3.nombre)}>more_horiz</i></button>

                                  <Divider/>
                                  </div>
                                  </div>
                                  <Divider/>
                                  </div>
                                )
                              })
                    }
                    </div>
                  )
                })

       }
      </div>

      </div>

    );
    })

    }

    </div>
    </div>
    )
    })
    }
    </div>
    </div>
    <div className="contenerdor-list mt-2">
    <div className="col-12 d-flex" style={{backgroundColor:'#FFDE59'}}>
    <strong className="d-flex" onClick={()=>setAcreedoras(!acreedoras)}><p>cuentas de orden acreedoras </p></strong>
    <i className="material-icons">expand_more</i>

    </div>
    <div className="col-md-12">
    {
    datos8.map((item, index)=>{
    return(
    <div className={!acreedoras ? 'tabla-deshabilitar ':'tabla-habilitar'}>
    <div className="d-flex" key={index}>
    <p className="col-3 d-flex" onClick={()=>setAcreedoras2(acreedoras2 == item.id ? false:item.id)} style={{marginLeft:'5%'}}>{item.concepto}
    </p>
    <div className="hola d-flex" style={{marginLeft:'40%'}}>
        <button  className="btn "><i class = "material-icons" onClick={()=>setId(item.id, item.concepto)}>more_horiz</i></button>

    </div>

    </div>
    <Divider/>

    <div className="" style={{marginLeft:'25%'}}>

    { item.sub.map((itemsub, indexsub)=>{
    return (
      <div className={acreedoras2 === item.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub} >
      <div className="d-flex" >
      <p className="d-flex col-3" onClick={()=>setAcreedoras3(acreedoras3 == itemsub.id ? false:itemsub.id)}>{itemsub.nombre}</p>
      <div className=" " style={{marginLeft:'25%'}}>
      <button  className="btn "><i class = "material-icons" onClick={()=>setId(itemsub.id, itemsub.nombre)}>more_horiz</i></button>

      <Divider/>
      </div>
      <Divider/>
      </div>
      <Divider/>

      <div className="" style={{marginLeft:'20%'}}>
      {itemsub.sub2.map((itemsub2, indexsub2 )=>{
                  return(
                    <div className={acreedoras3 === itemsub.id ? 'tabla-deshabilitar ':'tabla-habilitar'} key={indexsub2} >
                    <div className="d-flex">
                    <p className="d-flex col-3" onClick={()=>setAcreedoras4(acreedoras4 == itemsub2.id ? false:itemsub2.id)}>{itemsub2.nombre}</p>
                    <div className="" style={{marginLeft:'4%'}}>
                    <button  className="btn "><i class = "material-icons" onClick={()=>setId(itemsub2.id, itemsub2.nombre)}>more_horiz</i></button>

                    <Divider/>
                    </div>
                    </div>
                    <Divider/>

                    {itemsub2.sub3.map(itemsub3=>{
                                return(
                                  <div style={{marginLeft:'40%'}}  className={acreedoras4 === itemsub2.id ? 'tabla-habilitar':'tabla-deshabilitar '}>
                                  <div className="d-flex">
                                  <p className="d-flex col-3">{itemsub3.nombre}</p>

                                  <div className="" style={{marginLeft:'4%'}}>
                                  <button  className="btn "><i class = "material-icons" onClick={()=>setId(itemsub3.id, itemsub3.nombre)}>more_horiz</i></button>

                                  <Divider/>
                                  </div>
                                  </div>
                                  <Divider/>
                                  </div>
                                )
                              })
                    }
                    </div>
                  )
                })

       }
      </div>

      </div>

    );
    })

    }

    </div>
    </div>
    )
    })
    }
    </div>
    </div>
    </div>

    :null
    }
    </div>
    </div>
  )

}

export default Conceptos;
if(document.getElementById('Conceptos')) {
    ReactDOM.render(<Conceptos />, document.getElementById('Conceptos'));
}
