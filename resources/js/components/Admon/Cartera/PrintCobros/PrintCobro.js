import React from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Header from '../../../Ui/Header';
import '../../../../../css/app.scss';




function PrintCobro (){

  return(

    <div className="content-wrapper" style={{backgroundColor:'#ffffff', marginLeft:74}} >
    <Header/>
    <div className="verticar-footer">
          <div className="deshabilitar" onClick = {()=>setShowForm(2)} style={{marginTop:50}}>
              <h6 className="form-section d-flex align-items-center  my-3" style={{backgroundColor:'#FFDE59', borderRadius:15, marginTop:50}}><i className="material-icons" style= {{fontSize:'170%'}}>control_point</i> Agregar Pago</h6>
          </div>
          <h5 className="form-section habilitar" onClick = {()=>setShowForm(2)}><i className="material-icons" style= {{fontSize:'170%', marginLeft:14, marginTop:44, backgroundColor:'#FFDE59', borderRadius:15, width:66}}> control_point</i></h5>
          <img className="imagen" src={ip+'images/footer.png'}/>
    </div>
      

    </div>
  )

}

export default PrintCobro;

if(document.getElementById('PrintCobro')){
  ReactDOM.render(<PrintCobro/>, document.getElementById('PrintCobro'))
}
