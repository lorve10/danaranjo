import React,{useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Header from '../../../Ui/Header';
import Newegresos from './Newegresos';
import Swal from 'sweetalert2'
import Select from 'react-select'
import * as moment from 'moment';
import '../../../../../css/app.scss';
import Pagination from '../../../pagination/Pagination'
import PaginationButton from '../../../pagination/Pagination-button'

import useful from "../../util";

function Egresos () {
  const [loading, setLoading] = useState(false)
  const [showForm, setShowForm] = useState(1)
  const [id, setId] = useState(null)
  const [data, setData] = useState({})
  const [text, setText] = useState('')
  const [pagos, setPagos ] = useState([]);
  const [backPago, setBackPago] = useState([]);
  const [backPago2, setBackPago2] = useState([]);
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [marginval, setMarginVal] = useState(false)
  const [estado, setEstado] = useState(1);
  const [fecha, setFecha] = useState([]);
  const [fecha1, setFecha1] = useState('');
  const [fecha2, setFecha2] = useState('');
  const [newData, setNewData] = useState([]);

  useEffect(()=>{
    obtain_egresos();
    dataFecha();
  }, [])


  const obtain_egresos = async() =>{
    axios.get(ip+'admon/obtain_egresos').then(response=>{
      var res = response.data;
      setBackPago(response.data)
      setBackPago2(response.data)
      const dataNew = Pagination.paginate(response.data,currentPage,postsPerPage)
      setPagos(dataNew)
      console.log(response.data);

    })
  }

  const updateCurrentPage = async (number) => {
    await setPagos([])
    await setCurrentPage(number)
    const dataNew = Pagination.paginate(backPago,number,postsPerPage)
    await setPagos(dataNew)
  }

  const searchInput = async(value) => {
    await setText(value)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData = backPago2.filter(function(item2){
      var name = ((item2.egresos.persona.name).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")

      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var reslocal = local.indexOf(inputSearch) > -1
      var reslocalLasT = local.indexOf(inputSearch) > -1
      var res = false
      if(resNameLasT||resNameLasT){
        res = true
      }
      if(reslocal||reslocalLasT){
        res = true
      }

      return res;
    })

    /*var newDataFecha = backPago2.filter(e=>e.fecha == fecha1);
    console.log(newDataFecha);

    var newDataFecha2 = backPago2.filter(e=>e.fecha == fecha2);
    console.log(newDataFecha2);*/

    await setCurrentPage(1)
    await setBackPago(newData)
    const dataNew = Pagination.paginate(newData,1,postsPerPage)
    await setPagos(dataNew)
    await setFecha(dataNew)
    await setLoading(false)
  }

const dataFecha = async () => {
    console.log(fecha1);
    console.log(fecha2);
    var fecha1back = moment(fecha1, 'YYYY-MM-DD')
    var fecha2back = moment(fecha2, 'YYYY-MM-DD')
    console.log(fecha1back);
    console.log(fecha2back);
    var arrayTopush = []

    backPago2.map(item=>{
    var fecha_creacion2 = moment(item.egresos.fecha, 'YYYY-MM-DD')
    console.log(fecha_creacion2);
    var fecha_1 = fecha1back.diff(fecha_creacion2, 'minutes');
    var fecha_2 = fecha2back.diff(fecha_creacion2, 'minutes');
    if (fecha_1 <= 0 && fecha_2 >= 0) {
      arrayTopush.push(item)
    }

    })
    console.log(arrayTopush);
    await setCurrentPage(1)
    await setBackPago(arrayTopush)
    const dataNew = Pagination.paginate(arrayTopush,1,postsPerPage)
    await setPagos(dataNew)
    await setFecha(dataNew)
    await setLoading(false)
  }

  const goback = async () => {
  await setShowForm(1)
  setId(null)
  obtain_pago()
  }

  const gobackSave = () => {
  setShowForm(1)
  obtain_pago()
  setId(null)
  setData({})
  }

  const dataUpdate = async (data) => {
    console.log(data);

    var fecha = data.fecha
    var n_document = data.n_document
    var mes = data.mes
    var inmueble = data.inmueble
    var tip_pago = data.tip_pago
    var valor = data.valor
    var concepto = data.concepto
    var interes = data.interes
    var observa = data.observa


    const dataForAll = {

      fecha,
      n_document,
      mes,
      inmueble,
      tip_pago,
      valor,
      concepto,
      interes,
      observa
    }
    await setId(data.id)
    await setData(dataForAll)
    await setShowForm(2)
    console.log(dataForAll);
  }

  const deshabilitar = (value) =>{
    var message = 'Quieres eliminar esta Pago'
    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,

    }).then((result) => {
    /* Read more about isConfirmed, isDenied below */
    if (result.isConfirmed) {
      const data = new FormData()
      data.append('id', value.id)
      axios.post(ip+'admon/delete_purse',data).then(response=>{

        Swal.fire('Eliminado correctamente!', '', 'success')
        obtain_pago()

      })

    } else if (result.isDenied) {
      Swal.fire('Acción cancelada', '', 'info')
    }
  })

}

const propHeader = (value) => {
  console.log("entro",value);
  setMarginVal(value)
}


  return(
    <div className="content-wrapper" style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}}>
    <Header section ={6} marginval = {(value)=>propHeader(value)}/>

    <div className="verticar-footer">
          <div className="deshabilitar" onClick = {()=>setShowForm(2)} style={{marginTop:50, cursor: 'pointer'}}>
              <h6 className="form-section d-flex align-items-center  my-3" style={{backgroundColor:'#FFDE59', borderRadius:15, marginTop:50}}><i className="material-icons" style= {{fontSize:'170%'}}>control_point</i> Añadir Egresos</h6>
          </div>
          <h5 className="form-section habilitar" onClick = {()=>setShowForm(2)}><i className="material-icons" style= {{fontSize:'170%', marginLeft:14, marginTop:44, backgroundColor:'#FFDE59', borderRadius:15, width:66}}> control_point</i></h5>
          <img className="imagen" src={ip+'images/footer.png'}/>
    </div>
    {
      showForm == 1 ?
      <div >
      <div className="mt-3">
      <h4 className="text-center" style={{marginRight:"68%"}}>Realizar Pagos de Egresos</h4>
      </div>
        <div className="d-flex col-8">

          <div className="col-md-6 mt-5 d-flex "  style={{marginLeft:20}} >
            <div className="col-md-12 d-flex ">
            <label className="d-flex mr-3 col-4 align-items-center color">Fecha de: </label>
            <input className="form-control mr-3 col-6 d-flex" type="date" onChange = {(e)=>setFecha1(e.target.value)}/*value={fecha} onChange={(event)=>setFecha(event.target.value)}*/ />

            <label className="d-flex mr-3 col-5 align-items-center color">Fecha hasta: </label>
            <input className="form-control col-6 d-flex" type="date" onChange = {(e)=>setFecha2(e.target.value)} /*value={fecha} onChange={(event)=>setFecha(event.target.value)}*/ />

            <div className="col-4 mb-3">
              <button className="btn" style={{backgroundColor:'#FFDE59', color:'black', width: '100px', fontWeight: "bold"}} onClick={()=>dataFecha()}><span className = "material-icons align-items-center justify-content-between" >filter_alt</span>Filtrar</button>
            </div>
            </div>

            <div className="col-12 mt-5 mb-3 d-flex justify-content-between align-items-center" style={{marginLeft: '-98%' ,height: 40, border:'1px solid ', backgroundColor:'white',borderRadius:12}}>
            <input  onChange = {(e)=>searchInput(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Consultar egresos"/>
            {
              text.length == 0 ?
              <span style = {{color:'black', cursor:'pointer'}}  className= "material-icons">search</span>
              :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
            }
            </div>
          </div>

        </div>
      </div>
      :null
        }
        {
          showForm == 1?

          <div className="col-md-10" style={{marginLeft:35}}>
          <table className="table table-striped">
              <thead style={{backgroundColor: '#FFDE59' }}>
                <tr>
                    <th>id</th>
                    <th>Propietario</th>
                    <th>Concepto</th>
                    <th>Forma</th>
                    <th>Fecha</th>
                    <th>Observaciones</th>
                    <th>Valor</th>
                </tr>
              </thead>
              <tbody>
                {
                  pagos.map((item,index)=>{
                    console.log("aqui los item");
                      if(item.mt_pago == 1){
                        var pagar = "Efectivo"
                      }
                      if(item.mt_pago == 2){
                        var pagar = "Banco"
                      }
                      console.log(item);
                    return (
                      <tr key={index}>
                        <td>{item.id}</td>
                        <td>{item.propietario.name }</td>
                        <td>{item.concep.nombre_servic}</td>
                        <td>{pagar}</td>
                        <td>{item.fecha}</td>
                        <td>{item.observaciones}</td>
                        <td> ${useful.convertMoney(item.valor)}</td>
                      </tr>
                    );
                  })
                }
              </tbody>

            </table>
            <div className="d-flex col-md-12 col-12 justify-content-end">
              <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backPago.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
            </div>
          </div>
            :null
          }
        {

          showForm == 2?
          <div >
              <Newegresos  goback={()=>goback()} gobackSave={()=>gobackSave()} data={data} id={id} />
          </div>

          :null

        }





  </div>


);

}
export default Egresos;
if (document.getElementById('Egresos')) {
  ReactDOM.render(<Egresos />, document.getElementById('Egresos'));
}
