import React, {useState, useEffect} from 'react';
import {ip} from '../../../ApiRest';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import axios from 'axios';
import Swal from 'sweetalert2'
import '../../../../../css/app.scss';
import makeAnimated from 'react-select/animated';
import { data } from 'jquery';
import Divider from '@material-ui/core/Divider';
import List from '../Conceptos';


function Newegresos (props) {
    const [id, setId] = useState(null);
    const [loading, setLoading] = useState(false);
    const [text, setText] = useState('');
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);
    const [fecha, setFecha] = useState('');
    const [mes, setMes] = useState('');
    const [documento, setDocumento] = useState('');
    const [valor, setValor] = useState('');
    const [interes, setInteres] = useState('');
    const [observa, setObserva] = useState('')
    const [inmueble, setInmueble] = useState(null);
    const [inmuebleSelect, setInmuebleSelect] = useState([]);
    const [impuesto, setImpuesto] = useState(null);
    const [impuestoSelect, setImpuestoSelect] = useState([]);
    const [concepto, setConcepto] = useState(null);
    const [typeConcepSelect, setTypeConcepSelect] = useState([]);
    const [pago, setPago] = useState(null);

    const [propietarioSelect, setPropietarioSelect] = useState([]);
    const [propietario, setPropietario] = useState(null);

    const [typoPago, setTypoPago] = useState([
      {
        value:'1',
        label:'Efectivo'
      },
      {
        value:'2',
        label:'Banco'
      }
    ])

    const [ formContable , setFormContable ] = useState({
      concepto:null,
      valor:'',
      impuesto:'',
      observacion:''
    })

    const [ listContable, setListContable ] = useState([]);

useEffect(()=>{
  mostrarImpu(),
  cargar_pro()
  validar(),
  conceptoPago()
},[])
const conceptoPago = () =>{
    axios.get(ip+'admon/list_Servicios').then(response=>{
        var res = response.data
        var concep = []
        var filtrado = res.filter(e=>e.cat_id == 2)
        console.log(filtrado);
        filtrado.map(item=>{
          data = {
            value:item.id_servicios,
            label:item.nombre_servic
          }
          concep.push(data)
        })
        setTypeConcepSelect(concep)
      })
}


const cargar_pro = async() => {
    axios.get(ip+'admon/obtain_terce').then(response=>{
        console.log(response);
        var res = response.data;
        var propietario = []
        res.map(item=>{
            if (item.tip_per == 1) {
              var primer = []
              primer = item.name;
            }
            else  {
              var primer = []
              primer = item.name + " " + item.secname + " " + item.lastname + " " + item.seclastname
            }
            const data = {
                value:item.id,
                label:item.N_documet  + " - " + item.name + " " + item.secname + " " + item.lastname + " " + item.seclastname
            }
            propietario.push(data)
        })
        setPropietarioSelect(propietario)
        if(props.id>0){
            var filterpropietario = res.filter(e=>e.id == props.data.propietario.id)
            console.log("esta es la props terceros");
            console.log(filterpropietario.name);
            const data = {
                value:filterpropietario[0].id,
                label:filterpropietario[0].name
            }
            setPropietario(data)
        }
    })
}

    const mostrarImpu = () => {
      axios.get(ip+'admon/obtain_Impu').then(response=>{
        var res = response.data
        var impuesto = []
        res.map(item=>{
          data = {
            value:item.id,
            label:item.impuesto+"%"
          }
          impuesto.push(data)
        })
        setImpuestoSelect(impuesto)
      })
    }

    const obtain_data = async (data, nombre) =>{
    var  conp = {
        value: data,
        label: nombre
      };
      if(data != null ){
        setFormContable({...formContable, concepto: conp })
        MessageSuccess("se ha seleccionado el dato");
        $('#Concepto').modal('hide');
      }
    }

    const validar = async () =>{
      if(props.id > 0 ){
        console.log("entro al if");
        setFecha(props.data.fecha);
        var cadena1 = props.data.mes;
        var cadena2 = cadena1.slice(0, -3);
        setMes(cadena2);

        if(props.data.tip_pago == 1){
          setPago({
                value:'1',
                label:'Efectivo',
              })
        }
        if(props.data.tip_pago == 2){
          setPago({
                value:'2',
                label:'Banco',
              })
        }
        setValor(props.data.valor)
        setConcepto(props.data.concepto)
        setInteres(props.data.interes)
        setObserva(props.data.observa)

      }
    }

    console.log(inmueble);

    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }
    console.log(formContable);
    const save_pago = async () => {

      console.log("entrooooo");

      var message = ''
      var error = false

      var mesdata = mes + "-01"
    if(fecha == ''){
      error = true
      message = "Selecciona una fecha limite "
    }
    else if(mesdata == '-01' && mes == ''){
      error = true
      message = "Por favor escoje un mes "
    }
    else if(propietario == null){
        error = true
        message = "Selecciona un inmueble "
    }
   else if(concepto == null){
      error = true
      message = "Selecciona tipo de concepto "
    }
    else if(pago == null){
       error = true
       message = "Selecciona tipo de pago "
   }
   else if(valor == ''){
      error = true
      message = "Escribe el valor "
    }
    else if(interes == ''){
       error = true
       message = "Escribe el interes "
     }
     else if(observa == ''){
        error = true
        message = "Escribe alguna observacion "
      }

    if (error) {
    MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('id',id)
      data.append('fecha', fecha)
      data.append('mes', mesdata)
      data.append('n_documento', documento)
      data.append('tercero', propietario.value)
      data.append('concepto', concepto.value)
      data.append('tip_pago', pago.value)
      data.append('valor', valor)
      data.append('interes', interes)
      data.append('observaciones', observa)
      axios.post(ip+'admon/guard_egreso',data).then(response=>{
        if (id == null) {
          MessageSuccess("Egreso creado correctamente");
          props.gobackSave()

        }
        else{
          MessageSuccess("Egresos editado correctamente");
          props.gobackSave()

        }
      })
    }

  }

    return(
      <div className="mt-5 ">

      <div className="btn container col-1" style={{marginLeft:"120px"}} onClick = {()=>props.goback()}>
      <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
      </div>

        <div  className="container col-10 mt-4">

        <h5 className="col-2"><strong>Añadir Egresos:</strong> </h5>
          <div className="row col-12">
            <div className="col-md-5 d-flex ">
            <label className="d-flex col-4 align-items-center mr-3 color">Fecha: </label>
            <input className="form-control d-flex"  value={fecha}  type="date" onChange={(event)=>setFecha(event.target.value)}/>
            </div>

            <div className="col-md-5 d-flex">
            <label className="d-flex col-4 mr-3 color"> Mes:</label>
            <input className="form-control d-flex"  value={mes} type="month" onChange={(event)=>setMes(event.target.value)}/>
            </div>

            <div className="col-md-10 d-flex"  >
            <label className="d-flex col-4 mr-3 color" style={{width: '180px'}} >Número Documento:</label>
            <input className="form-control d-flex " placeholder="Ingrese número de documento" value={documento} type="number" onChange={(event)=>setDocumento(event.target.value)} />
            </div>

            <div className="col-md-10 d-flex">
            <label className="d-flex col-4 mr-2 align-items-center color" style={{width: '180px'}}>Terceros: </label>
            <Select
            className="col-8"
            style={{ backgroundColor:'#FDA71A'}}
            value={propietario}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={propietarioSelect}
            onChange={(e)=>setPropietario(e)}
            placeholder = "Seleccionar Tercero"
            name="colors"
            />
            </div>
            <div className="col-md-10 d-flex">
            <label className="d-flex col-4 mr-2 align-items-center color" style={{width: '180px'}} >Concepto: </label>
            <Select
            className="col-8"
            style={{ backgroundColor:'#FDA71A'}}
            value={concepto}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={typeConcepSelect}
            onChange={(e)=>setConcepto(e)}
            placeholder = "Seleccionar Tipo Concepto"
            name="colors"
            />
            </div>
            <div className="col-md-10 d-flex">
            <label className="d-flex col-4 color mr-2 " style={{width: '180px'}}> Metodo de pago:</label>
            <Select
            className="col-8"
            style={{ backgroundColor:'#FDA71A'}}
            value={pago}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={typoPago}
            onChange={(e)=>setPago(e)}
            placeholder = "Seleccionar Tipo Pago"
            name="colors"
            />
            </div>
            <div className="col-md-5 d-flex"  >
            <label className="d-flex col-4 mr-3 color" style={{width: '180px'}}>Interés:</label>
            <input className="form-control  d-flex" placeholder="Ingrese el valor del interés" value={interes}  type="number" onChange={(event)=>setInteres(event.target.value)} />
            </div>
            <div className="col-md-5 d-flex"  >
            <label className="d-flex col-4 mr-3 color" >Valor pago :</label>
            <input className="form-control  d-flex" placeholder="Ingrese el valor del interés" value={valor}  type="number" onChange={(event)=>setValor(event.target.value)} />
            </div>
            <div className="col-md-10 d-flex"  >
            <textarea className="form-control  d-flex" placeholder="Observaciones" value={observa} type="text" onChange={(event)=>setObserva(event.target.value)} />
            </div>


        </div>

        <div className="my-2 d-flex col-md-9 justify-content-end px-0" style = {{left:'5%'}}>
          <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}} onClick = {()=>save_pago()}>Guardar Pago</button>
        </div>
      </div>






    </div>

    );
}

export default Newegresos;
if(document.getElementById('Newegresos')) {
    ReactDOM.render(<Newegresos />, document.getElementById('Newegresos'));
}
