import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Select from 'react-select';
import axios from 'axios';
import Swal from 'sweetalert2'
import '../../../../../css/app.scss';
import makeAnimated from 'react-select/animated';
import Divider from '@material-ui/core/Divider';
import { data } from 'jquery';
import List from '../Conceptos';


function Newpurse (props) {
    const [id, setId] = useState(null);
    const [loading, setLoading] = useState(false);
    const [text, setText] = useState('');
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);
    const [fecha, setFecha] = useState('');
    const [mes, setMes] = useState('');
    const [documento, setDocumento] = useState('');
    const [valor, setValor] = useState('');
    const [interes, setInteres] = useState('');
    const [observa, setObserva] = useState('')
    const [inmueble, setInmueble] = useState(null);
    const [inmuebleSelect, setInmuebleSelect] = useState([]);
    const [concepto, setConcepto] = useState(null);
    const [concepto2, setConcepto2] = useState(null);

    const [typeConcepSelect, setTypeConcepSelect] = useState([]);
    const [pago, setPago] = useState(null);
    const [impuesto, setImpuesto] = useState(null);
    const [impuestoSelect, setImpuestoSelect] = useState([]);
    const [typoPago, setTypoPago] = useState([
      {
        value:'1',
        label:'Efectivo'
      },
      {
        value:'2',
        label:'Banco'
      }
    ])
    const [ formContable , setFormContable ] = useState({
      concepto:null,
      valor:'',
      impuesto:'',
      observacion:''
    })

    const [ listContable, setListContable ] = useState([]);

useEffect(()=>{
  mostrarInmu()
  mostrarImpu()
  //validar()
  conceptoPago()
  //obtain_data()
},[])

const conceptoPago = () =>{
    axios.get(ip+'admon/list_Servicios').then(response=>{
        var res = response.data
        var concep = []
        var filtrado = res.filter(e=>e.cat_id == 2)
        console.log(filtrado);
        filtrado.map(item=>{
          data = {
            value:item.id_servicios,
            label:item.nombre_servic
          }
          concep.push(data)
        })
        setTypeConcepSelect(concep)
      })
}


const mostrarInmu = () =>{
      axios.get(ip+'admon/obtain_Inmu').then(response=>{
          var res = response.data
          var inmueble = []
          res.map(item=>{
            data = {
              value:item.id,
              label:item.tipo_inmueble.nombre+"-"+item.numero
            }
            inmueble.push(data)
          })
          setInmuebleSelect(inmueble)

          if(props.id>0){
              var filterInmueble = res.filter(e=>e.id == props.data.inmueble.id)
              console.log("esta es la props terceros");
              console.log(filterInmueble);
              const data = {
                  value:filterInmueble[0].id,
                  label:filterInmueble[0].tipo_inmueble.nombre+"-"+filterInmueble[0].numero
              }
              setInmueble(data)
          }
      })
    }

    const mostrarImpu = () => {
      axios.get(ip+'admon/obtain_Impu').then(response=>{
        var res = response.data
        var impuesto = []
        res.map(item=>{
          data = {
            value:item.id,
            label:item.impuesto+"%"
          }
          impuesto.push(data)
        })
        setImpuestoSelect(impuesto)
      })
    }

    const validar = async () =>{
      if(props.id > 0 ){
        console.log("entro al if");
        setFecha(props.data.fecha);
        var cadena1 = props.data.mes;
        var cadena2 = cadena1.slice(0, -3);
        setMes(cadena2);

        if(props.data.tip_pago == 1){
          setPago({
                value:'1',
                label:'Efectivo',
              })
        }
        if(props.data.tip_pago == 2){
          setPago({
                value:'2',
                label:'Banco',
              })
        }
        setValor(props.data.valor)
        setConcepto(props.data.concepto)
        setInteres(props.data.interes)
        setObserva(props.data.observa)

      }
    }

    const obtain_data = async (data, nombre) =>{
    var  conp = {
        value: data,
        label: nombre
      };
      if(data != null ){
        setFormContable({...formContable, concepto: conp })
        MessageSuccess("se ha seleccionado el dato");
        $('#Concepto').modal('hide');
      }
    }



    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

    const save_pago = async () => {
      console.log("entrooooo");

      var message = ''
      var error = false

      var mesdata = mes + "-01"
    if(fecha == ''){
      error = true
      message = "Selecciona una fecha limite "
    }
    else if(mesdata == '-01' && mes == ''){
      error = true
      message = "Por favor escoje un mes "
    }
    else if(inmueble == null){
        error = true
        message = "Selecciona un inmueble "
    }
   else if(concepto == null){
      error = true
      message = "Selecciona tipo de concepto "
    }
    else if(pago == null){
       error = true
       message = "Selecciona tipo de pago "
   }
     else if(observa == ''){
        error = true
        message = "Escribe alguna observacion "
      }

    if (error) {
    MessageError(message)
    }
    else{
      var fechaini = mes+'-1'
      var fechaend = mes+'-31'
      const data = new FormData()
      data.append('id',id)
      data.append('fecha', fecha)
      data.append('mes', mesdata)
      data.append('fechaini',fechaini)
      data.append('fechaend',fechaend)
      data.append('inmueble', inmueble.value)
      data.append('tip_pago', pago.value)
      data.append('concepto', concepto.value)
      data.append('valor', valor)
      data.append('interes', interes)
      data.append('observa', observa)
      axios.post(ip+'admon/guard_purse',data).then(response=>{
        if(response.data){
            if (id == null) {
              MessageSuccess("pago creado correctamente");
              props.gobackSave()
            }
        }
        else{
          MessageError("Este inmueble " + inmueble.label + " no tiene cobro generado para este mes " + mesdata);
          props.gobackSave()

        }

      })
    }
  }
  const onClickAddContable = () =>{

    const list = listContable
    list.push(formContable)
    setListContable(list)

    setFormContable({
      concepto:null,
      valor:'',
      impuesto:null,
      observacion:''

    })
  }

    return(
      <div className="mt-5 ">

      <div className="btn container col-1" style={{marginLeft:"120px"}} onClick = {()=>props.goback()}>
      <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
      </div>

        <div  className="container col-10 mt-4">

        <h5 className="col-2"><strong>Añadir Pago:</strong> </h5>
          <div className="row col-12">
            <div className="col-md-5 d-flex ">
            <label className="d-flex col-4 mr-3 align-items-center color">Fecha: </label>
            <input className="form-control d-flex"  value={fecha}  type="date" onChange={(event)=>setFecha(event.target.value)}/>
            </div>

            <div className="col-md-5 d-flex">
            <label className="d-flex col-4 mr-3 color">Mes:</label>
            <input className="form-control d-flex"  value={mes} type="month" onChange={(event)=>setMes(event.target.value)}/>
            </div>

            <div className="col-md-10 d-flex">
            <label className="d-flex col-4 mr-2 align-items-center color" >Inmueble: </label>
            <Select
            className="col-8"
            style={{ backgroundColor:'#FDA71A'}}
            value={inmueble}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={inmuebleSelect}
            onChange={(e)=>setInmueble(e)}
            placeholder = "Seleccionar Tipo Inmueble"
            name="colors"
            />
            </div>
            <div className="col-md-10 d-flex">
            <label className="d-flex col-4 mr-2 align-items-center color" >Concepto: </label>
            <Select
            className="col-8"
            style={{ backgroundColor:'#FDA71A'}}
            value={concepto}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={typeConcepSelect}
            onChange={(e)=>setConcepto(e)}
            placeholder = "Seleccionar Tipo Concepto"
            name="colors"
            />
            </div>
            <div className="col-md-10 d-flex">
            <label className="d-flex col-4 mr-2 color"> Tipo Pago:</label>
            <Select
            className="col-8"
            style={{ backgroundColor:'#FDA71A'}}
            value={pago}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={typoPago}
            onChange={(e)=>setPago(e)}
            placeholder = "Seleccionar Tipo Pago"
            name="colors"
            />
            </div>

            <div className="col-md-10 d-flex"  >
            <label className="d-flex col-4 mr-3 color" >Valor Pago:</label>
            <input className="form-control d-flex" style={{width: '533px'}} value={valor}  type="number" onChange={(event)=>setValor(event.target.value)} />
            </div>
            <div className="col-md-10 d-flex"  >
            <textarea className="form-control  d-flex" placeholder="Observaciones" value={observa} type="text" onChange={(event)=>setObserva(event.target.value)} />
            </div>

        </div>
        <div className="my-2 d-flex col-md-9 justify-content-end px-0" style = {{left:'5%'}}>
          <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}} onClick = {()=>save_pago()}>Guardar Pago</button>
        </div>
      </div>








    </div>

    );
}

export default Newpurse;
if(document.getElementById('Newpurse')) {
    ReactDOM.render(<Newpurse />, document.getElementById('Newpurse'));
}
