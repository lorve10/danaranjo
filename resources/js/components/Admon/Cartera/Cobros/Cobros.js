import React,{useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Select from 'react-select';
import axios from 'axios';
import Swal from 'sweetalert2';
import useful from '../../util';
import EstadoCuent from './EstadoCuent';
import PrintCobroLocal from './PrintCobroLocal';
import PrintCobroMassive from './PrintCobroMassive'
import Header from '../../../Ui/Header';
import '../../../../../css/app.scss';
import { data } from 'jquery';
import { Divider } from '@material-ui/core';
import makeAnimated from 'react-select/animated';
import Pagination from '../../../pagination/Pagination'
import PaginationButton from '../../../pagination/Pagination-button'


function Cobros () {
  const [listInmueble, setListInmueble ] = useState([]);
  const [id, setId] = useState(null);
  const [fecha_gene, setFecha_gene] = useState('')
  const [marginval, setMarginVal] = useState(false)
  const [data, setData] = useState({})
  const [mesCobro, setMesCobro] = useState('')
  const [showForm, setShowForm] = useState(1)
  const [mostrar, setMostrar] = useState(1)
  const [ver, setVer] = useState(1)
  const [concepto, setConcepto] = useState(null)
  const [concepSelect, setConcepSelect] = useState([]);
  const [valor, setValor] = useState(null)
  const [total, setTotal] =useState(null)
  const [todos, setTodos] = useState(false)
  const [arraySelect, setArraySelect] = useState([])
  const [ showList, setShowList ] = useState(true)
  const [ caja, setCaja ] = useState(false);
  const [ descuent, setDescuent ] = useState(null);
  const [text, setText] = useState('')

  const [caso, setCaso] = useState([])

  const [backInmu, setBackInmu] = useState([]);
  const [backInmu2, setBackInmu2] = useState([]);
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);

  useEffect(()=>{
    mostrarInmueble()
    conceptoCobro()
  },[])

/// funcion para traer lista de servicios
  const conceptoCobro = () =>{
      axios.get(ip+'admon/list_Servicios').then(response=>{
          var res = response.data
          var concep = []
          var  filtro = res.filter(e=>e.cat_id == 2)
          filtro.map(item=>{
          const  data = {
              value:item.id_servicios,
              label:item.nombre_servic
            }
            concep.push(data)
          })
          setConcepSelect(concep)
        })
  }

//// funcion para traer cuotas y se filtra
  const valor_pagar =(data) =>{
    axios.get(ip+'admon/obtain_Cuotas').then(response =>{
      var res = response.data
      var filtro = res.filter(e=>e.id_concepto == data)
      console.log("aqui resolver");
      console.log(filtro);
      setValor(filtro[0].tarifa)
    })
  }

  //// funcion para traer inmuebles
  const mostrarInmueble = async() =>{
    axios.get(ip+'admon/obtain_Inmu').then(response=>{
      setBackInmu(response.data)
      setBackInmu2(response.data)
      const dataNew = Pagination.paginate(response.data,currentPage,postsPerPage)
      setListInmueble(dataNew)
      listInmueble.map(item=>{
        var caso = item.tarifa_inmueble.tarifa;
        setCaso(caso)
      })
    })
  }
////paginador
const updateCurrentPage = async (number) => {
  await setListInmueble([])
  await setCurrentPage(number)
  const dataNew = Pagination.paginate(backInmu,number,postsPerPage)
  await setListInmueble(dataNew)
}

////Buscador para los cobros
  const searchInput = async(value) => {
    await setText(value)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData = backInmu2.filter(function(item2){
      var name = ((item2.tipo_inmueble.nombre + " - " + item2.numero ).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var res = false
      if(resNameLasT||resNameLasT){
        res = true
      }
      return res;
    })
      await setCurrentPage(1)
      await setBackInmu(newData)
      const dataNew = Pagination.paginate(newData,1,postsPerPage)
      await setListInmueble(dataNew)

  }

const saveCobro = async () => {
  var message = ''
  var error = false
if(mesCobro == null){
  error = true
  message = "Seleccione mes de cobro "
}
else if ( fecha_gene == ''){
  error = true
  message = "Selecciona fecha de generación del cobro"
}
if(error){
    MessageError(message)
}
else{
  var fechaini = mesCobro+'-01'
  var fechaend = mesCobro+'-31'
  const data = new FormData()
  data.append('id',id)
  data.append('concepto', concepto.value)
  data.append('fecha_gene', fecha_gene)
  data.append('mes_admin', mesCobro+'-01')
  data.append('fechaini',fechaini)
  data.append('fechaend',fechaend)
  if(caja == false){
    var total = 0
    data.append('descuento', total)
  }
  else {
  var total = descuent
  data.append('descuento', total)
  }
  if(concepto.value == 3){
    data.append('cobro', caso)
  }
  else {
    data.append('cobro', valor)

  }
  if(todos){
    data.append('data', JSON.stringify(listInmueble))
  }else{
    data.append('data', JSON.stringify(arraySelect))
  }
  axios.post(ip+'admon/guard_Cobro',data).then(response=>{
    if ( response.data == 1) {
      MessageSuccess("Cobro creado correctamente");
      setMostrar(2)
      setFecha_gene("")
      setMesCobro("")

    }
    else{
      MessageError("Ya existe un cobro creado con esta fecha o concepto");
      setMostrar(2)
      setFecha_gene("")
      setMesCobro("")
    }
  })
}

}

const MessageError = async (data) => {
  Swal.fire({
    title: 'Error',
    text: data,
    icon: 'warning',
  })
}
const MessageSuccess = async (data) => {
  Swal.fire({
    text: data,
    icon: 'success',
  })
}

  const goback = async () => {
    await setShowForm(1)
  }

const propHeader = (value) => {
  console.log("entro",value);
  setMarginVal(value)
}

const validarShowForm = async (valor) => {
  await setShowForm(valor)
  await setMostrar(1)
}


  const valida = (e) =>{
    if(e.value){
      setConcepto(e)
      valor_pagar(e.value)
      setVer(3)
    }
  }
  const generar = () =>{
    var message = ''
    var error = false
    if(mesCobro == null){
      error = true
      message = "Seleccione mes de cobro "
    }
    else if ( fecha_gene == ''){
      error = true
      message = "Selecciona fecha de generación del cobro"
    }if(error){
        MessageError(message)
    }
    else{
      setMostrar(2)
    }
  }

  const checkedItem = async(event, value) =>{
    await setShowList(false)
    var array = arraySelect
    if (!event.target.checked) {
      array.push(value)
    }
    else{
      let index = array.findIndex(e=>e.id == value.id)
      array.splice(index,1)
    }
    await setArraySelect(array)
    await setShowList(true)
  }

  const descuento =(event)=>{
    console.log(event.target.checked);
    if(event.target.checked){
      setCaja(true)
      console.log("verdaderos");
    }
    if(event.target.checked == false){
      setCaja(false)
      console.log("falso");


    }


  }

  return(
    <div className="content-wrapper" style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:74}} >
    <Header section ={5} marginval = {(value)=>propHeader(value)}/>
    <div className="verticar-footer">
          <div className="deshabilitar"  style={{marginTop:50, cursor:'pointer'}} onClick={()=>validarShowForm(4)}>
              <h6 className="form-section d-flex align-items-center  my-3" style={{backgroundColor:'#FFDE59', borderRadius:15, marginTop:50}}><i className="material-icons" style= {{fontSize:'170%'}}>email</i>Enviar masivo</h6>
          </div>
          <div className="deshabilitar" onClick = {()=>validarShowForm(3)} style={{marginTop:20, cursor:'pointer'}}>
              <h6 className="form-section d-flex align-items-center  my-3" style={{backgroundColor:'#FFDE59', borderRadius:15, marginTop:50}}><i className="material-icons" style= {{fontSize:'170%'}}>print</i> Imprimir por local</h6>
          </div>
          <div className="deshabilitar" onClick = {()=>validarShowForm(5)} style={{marginTop:15, cursor:'pointer'}}>
              <h6 className="form-section d-flex align-items-center  my-3" style={{backgroundColor:'#FFDE59', borderRadius:15, marginTop:50}}><i className="material-icons" style= {{fontSize:'170%'}}>article</i>Estado de cuenta</h6>
          </div>
          <div className=""  style={{marginTop:20}}>
              <h5 className="form-section habilitar" ><i className="material-icons" style= {{fontSize:'137%', marginLeft:14, marginTop:15, backgroundColor:'#FFDE59', borderRadius:15, width:66}}>email</i></h5>
          </div>
          <div className=""  style={{marginTop:11}}>
              <h5 className="form-section habilitar" ><i className="material-icons" style= {{fontSize:'135%', marginLeft:14, marginTop:15, backgroundColor:'#FFDE59', borderRadius:15, width:66}}> print</i></h5>
          </div>
          <div className=""  style={{marginTop:11}}>
              <h5 className="form-section habilitar" ><i className="material-icons" style= {{fontSize:'135%', marginLeft:14, marginTop:15, backgroundColor:'#FFDE59', borderRadius:15, width:66}}> article</i></h5>
          </div>
          <img className="imagen" src={ip+'images/footer.png'} style={{marginTop:250}}/>
    </div>
      {
        showForm == 1?
        <div >
        <div className="col-9 d-flex">
        <div className="col-md-6 mt-5"  style={{marginLeft:120}} >
          <h6>/<a href = {ip+'admon/cartera'}>Cartera</a>/Cobros</h6>

          <h4>Cobros</h4>
            <div className="col-12 mt-3 d-flex">
            <label className="d-flex col-7  color" >Concepto: </label>
            <Select
            className="col-12"
            value={concepto}
            components={animatedComponents}
            options={concepSelect}
            onChange={(e)=>valida(e)}
            placeholder = "Seleccionar Tipo Concepto"
            name="colors"
            />
            </div>
            {
              ver == 3 ?
              <div>
                <div className="col-12 mt-3 d-flex">
                    <label className="d-flex col-7">Mes de Cobro</label>
                    <input className="form-control d-flex col-7" type="month" value={mesCobro} onChange={(event)=>setMesCobro(event.target.value)} />
                </div>
                <div className="col-12 mt-3 d-flex mt-3">
                    <label className="d-flex col-7">Fecha de Generación</label>
                    <input className="form-control d-flex col-7" type="date" value={fecha_gene} onChange={(event)=>setFecha_gene(event.target.value)} />
                </div>

                <div className="col-md-6 justify-content-end " style = {{marginLeft:'83%', marginTop: '15px'}}>
                  <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}} onClick={()=>generar()} >Generar Cobros</button>
                </div>
              </div>
              :null
            }

          </div>

        </div>
        <Divider className="col-md-5" style={{backgroundColor:'black', marginLeft: '148px', marginTop: '25px'}} />
      </div>

      :null
      }


        {
          mostrar  == 2 && showList?
          <div className="col-md-5 mt-2" style={{marginLeft:'25%'}}>
          <div className="row mt-2 d-flex "  style={{marginLeft:120}} >
              <div className="col-9 mt-2 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid ', backgroundColor:'white', borderRadius:12, marginLeft:'-295px'}}>
              <input  onChange = {(e)=>searchInput(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Consultar Cuotas...  "/>
              {
                text.length == 0 ?
                <span style = {{color:'black', cursor:'pointer'}}  className= "material-icons">search</span>
                :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
              }
              </div>
          </div>
          <table className="table table-striped" style={{marginLeft: '-180px'}}>
            <thead style={{backgroundColor: '#FFDE59'}}>
              <tr>
                <th scope="col">Tipo Inmueble / Numero</th>
                <th scope="col">Valor cuota</th>
                <th>Todos <input type="checkbox"  id = "holaMundo" onChange = {()=>setTodos(!todos)}/></th>
              </tr>
            </thead>
            <tbody>
            {
              listInmueble.map((item, index)=>{
                if(concepto.value == 3){
                  var pagar = item.tarifa_inmueble.tarifa;
                }
                else {
                  var pagar = valor
                }
                return(
                  <tr key={index}>
                    <td>{item.tipo_inmueble.nombre + " - " + item.numero }</td>
                    <td>$ {useful.convertMoney(pagar)}</td>
                    <td id = "listado" ><input type="checkbox" onChange={(e)=>checkedItem(e,item)} checked = { todos || arraySelect.findIndex(e=>e.id == item.id)>=0}/></td>
                  </tr>
                )
              })
            }

            </tbody>
            <div className="d-flex mt-4">
            <strong className="mr-3">Agregar descuento   <input type="checkbox" onChange={(e)=>descuento(e)} /> </strong>
            {
              caja == true ?
              <div className="d-flex">
                <input type = "number" className="form-control col-6"   value = {descuent} onChange={(e)=>setDescuent(e.target.value)}/>
              </div>
              :null
            }
            </div>
            <div className="d-flex col-md-12 col-12 justify-content-end">
              <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backInmu.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
            </div>
          </table>

          <div className="col-8 aceptar-denegar mt-1" style={{marginLeft: '-75px', marginBottom: '50px', backgroundColor:'#FFEDA6'}} >
              <p className="justify-content-center">
                <strong className="d-flex justify-content-center" >¿Estas seguro de generar las cuentas cobros con estos valores?</strong >
              </p>
              <div className="justify-content-center" style={{marginLeft:65, marginTop: '20px'}}>
                <button className="btn mr-2 col-4"  style={{border:'2px ', backgroundColor:'#FFDE59', borderRadius:10}} onClick={()=>saveCobro()} >Si</button>
                <button className="btn col-4"  style={{border:'2px', backgroundColor:'#FFDE59', borderRadius:10}} onClick={()=>setMostrar(1)} >No</button>
              </div>
          </div>
        </div>

        :null

        }

        {

        showForm == 3?
        <div >
            <PrintCobroLocal  goback={()=>goback()} />
        </div>

        :showForm == 4?
        <div >
            <PrintCobroMassive  goback={()=>goback()}  />
        </div>

        :showForm == 5?
        <div >
            <EstadoCuent  goback={()=>goback()}  />
        </div>


        :null

        }
  </div>


);

}
export default Cobros;
if (document.getElementById('Cobros')) {
  ReactDOM.render(<Cobros />, document.getElementById('Cobros'));
}
