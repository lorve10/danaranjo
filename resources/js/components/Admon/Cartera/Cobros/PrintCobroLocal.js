import React, {useState, useEffect} from 'react';
import {ip} from '../../../ApiRest';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import axios from 'axios';
import Swal from 'sweetalert2'
import '../../../../../css/app.scss';
import makeAnimated from 'react-select/animated';
import * as moment from 'moment';
import { data } from 'jquery';
import downloadjs from 'downloadjs';
import useful from '../../util';

function PrintCobroLocal (props) {
    const [id, setId] = useState(null);
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);
    const [inmuebleSelect, setInmuebleSelect] = useState([]);
    const [mostrar, setMostrar] = useState(1);
    const [inmueble, setInmueble ] = useState(null);
    const [fecha1, setFecha1] = useState('');
    const [listInmueble, setListInmueble] = useState([])
    const [ token, setToken ] = useState(null)
    const [ token2, setToken2 ] = useState(null)

    const [concepto, setConcepto] = useState(null)
    const [typeConcepSelect, setTypeConcepSelect] = useState([]);



useEffect(()=>{
  mostrarInmu()
  conceptoPago()
  var csrf = document.querySelector('meta[name="csrf-token"]').content;
  var csrf2 = document.querySelector('meta[name="csrf-token"]').content;

  console.log("algoooo");
  console.log(csrf);
  setToken(csrf);
  setToken2(csrf2)
},[])

const goback = async () => {
    await setEstado(1)
    setId(null)
}
const conceptoPago = () =>{
    axios.get(ip+'admon/list_Servicios').then(response=>{
        var res = response.data
        var concep = []
        var filtrado = res.filter(e=>e.cat_id == 2)
        console.log(filtrado);
        filtrado.map(item=>{
        const  data = {
            value:item.id_servicios,
            label:item.nombre_servic
          }
          concep.push(data)
        })
        setTypeConcepSelect(concep)
      })
}

const search_data = () => {
    console.log(fecha1)

    var fechaini = fecha1+'-1'
    var fechaend = fecha1+'-31'
    var dataform = new FormData()
    console.log(inmueble);
    dataform.append('concepto',concepto.value)
    dataform.append('fechaini',fechaini)
    dataform.append('fechaend',fechaend)
    dataform.append('id',inmueble.value)
    axios.post(ip+'admon/obtain_cobros_mes',dataform).then(response=>{
        var res = response.data
        console.log(res);
        setListInmueble(response.data.data);

        setMostrar(2)
        var inmueble = []
        //var filter = res.filter(e=>e.tipo_inmueble.nombre +"-"+ e.numero  == value.label);
        //console.log(filter);

    })
}


 //const imprime  = () => {
   ///console.log(listInmueble);
    ///const data = {data:listInmueble}
    //axios.post(ip+'admon/descargar/cobros',data).then(response=>{
    //  console.log(response);
    //})
 //}

const dataMes = (fecha1) => {
    console.log(fecha1);
}

const mostrarInmu = () =>{
      axios.get(ip+'admon/obtain_Inmu').then(response=>{
          var res = response.data
          var inmueble = []
          res.map(item=>{
            data = {
              value:item.id,
              label:item.tipo_inmueble.nombre+"-"+item.numero
            }
            inmueble.push(data)
          })
          setInmuebleSelect(inmueble)
      })
    }

    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }

    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

const correo = () =>{
  const data = new FormData()
  data.append('data', JSON.stringify(listInmueble))
  data.append('token', token)
  axios.post(ip+'admon/enviar',data).then(response=>{
    console.log("se responde");
    console.log(response.data);
  })

}

console.log(fecha1+'-1')
    return(
      <div className="mt-5 col-10" style={{marginLeft: '45px'}}>

      <div className="btn container col-1" onClick = {()=>props.goback()}>
      <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
      </div>

        <div  className="container col-10 mt-4" style={{marginLeft:50}}>

        <h5 className="col-4"><strong>Imprimir cobros por local :</strong> </h5>
          <div className="row col-12">
          <div className="col-md-10 d-flex">
          <label className="d-flex col-4 align-items-center color" >Concepto: </label>
          <Select
          className="col-10"
          style={{ backgroundColor:'#FDA71A'}}
          value={concepto}
          closeMenuOnSelect={true}
          components={animatedComponents}
          options={typeConcepSelect}
          onChange={(e)=>setConcepto(e)}
          placeholder = "Seleccionar Tipo Concepto"
          name="colors"
          />
          </div>
            <div className="col-md-5 d-flex">
            <label className="d-flex col-5 align-items-center color" >Local : </label>
            <Select
            className="col-10"
            style={{ backgroundColor:'#FDA71A'}}
            closeMenuOnSelect={true}
            value = {inmueble}
            components={animatedComponents}
            options={inmuebleSelect}
            onChange={(e)=>setInmueble(e)}
            placeholder = "Seleccionar Local"
            name="colors"
            />
            </div>

            <div className="col-md-12 d-flex">
            <label className="d-flex col-2 mr-3 color">Mes :</label>
            <input className="form-control col-3 d-flex" type="month" onChange = {(event)=>setFecha1(event.target.value)}/>
            </div>

        </div>
        <div className="my-2 d-flex col-md-9 justify-content-end px-0" style = {{float:'left'}}>
          <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black', marginRight: '33%'}} onClick={()=>search_data()}>Buscar</button>
        </div>
      </div>



      {
          mostrar  == 2 && listInmueble.length>0?
          <div className="col-md-5 mt-3" style={{marginLeft:'6%'}}>
          <table className="table table-striped" style={{backgroundColor: '#FFDE59'}}>
            <thead>
              <tr>
                <th scope="col">Tipo Inmueble / Numero</th>
                <th scope="col">Valor cuota</th>
                <th scope="col">Mes</th>
              </tr>
            </thead>
            <tbody style={{backgroundColor: 'white'}}>

            {
              listInmueble.map((item, index)=>{
                return(
                  <tr key={index}>
                    <td>{item.cobros.tipo_inmueble.nombre + " - " + item.cobros.numero }</td>
                    <td>$ {useful.convertMoney(item.cobros.tarifa_inmueble.tarifa)}</td>
                    <td>{item.mes_admin}</td>
                  </tr>
                )
              })
            }

            </tbody>
          </table>
        <div className="d-flex">
        <form className="d-flex" action={ip+"admon/enviar"} method="POST">
        <input type= "hidden"  name= "data"  value={JSON.stringify(listInmueble)} />
        <input type="hidden" name="_token" value={token2}/>
        <button type="submit"  className="btn  " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}}  >Enviar</button>
        </form>
        <form className="d-flex" action={ip+"admon/descargar/cobros"} method="POST" target="_blank">
            <input type= "hidden"  name= "data"  value={JSON.stringify(listInmueble)} />
            <input type="hidden" name="_token" value={token}/>
            <button type="submit"  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}}>Imprimir</button>
        </form>
        </div>



        </div>

        :null

        }
    </div>

    );
}

export default PrintCobroLocal;
if(document.getElementById('PrintCobroLocal')) {
    ReactDOM.render(<PrintCobroLocal />, document.getElementById('PrintCobroLocal'));
}
