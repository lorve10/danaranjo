import React, {useState, useEffect} from 'react';
import {ip} from '../../../ApiRest';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import axios from 'axios';
import Swal from 'sweetalert2'
import * as moment from 'moment';
import { data } from 'jquery';
import downloadjs from 'downloadjs';
import useful from '../../util';
import '../../../../../css/app.scss';
import makeAnimated from 'react-select/animated';
import Pagination from '../../../pagination/Pagination'
import PaginationButton from '../../../pagination/Pagination-button'

function PrintCobroMassive (props) {
    const [id, setId] = useState(null);
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);
    const [inmuebleSelect, setInmuebleSelect] = useState([]);
    const [mostrar2, setMostrar2] = useState(1);
    const [inmueble, setInmueble ] = useState(null);
    const [fecha1, setFecha1] = useState('');
    const [listCobro, setListCobro,] = useState([])
    const [ token, setToken ] = useState(null)
    const [ token2, setToken2 ] = useState(null)

    const [concepto, setConcepto] = useState(null)
    const [typeConcepSelect, setTypeConcepSelect] = useState([]);



useEffect(()=>{
  mostrarInmu()
  conceptoPago()
  var csrf = document.querySelector('meta[name="csrf-token"]').content;
  var csrf2 = document.querySelector('meta[name="csrf-token"]').content;

  console.log("algoooo");
  console.log(csrf);
  setToken(csrf);
  setToken2(csrf2)
},[])

const goback = async () => {
    await setEstado(1)
    setId(null)
}
const conceptoPago = () =>{
    axios.get(ip+'admon/list_Servicios').then(response=>{
        var res = response.data
        var concep = []
        var filtrado = res.filter(e=>e.cat_id == 2)
        console.log(filtrado);
        filtrado.map(item=>{
        const  data = {
            value:item.id_servicios,
            label:item.nombre_servic
          }
          concep.push(data)
        })
        setTypeConcepSelect(concep)
      })
}

const search_data = async () => {
    var fechaini = fecha1+'-01'
    var fechaend = fecha1+'-31'
    var dataform = new FormData()
    dataform.append('concepto',concepto.value)
    dataform.append('fechaini',fechaini)
    dataform.append('fechaend',fechaend)
    axios.post(ip+'admon/obtain_cobros_print',dataform).then(response=>{
        setListCobro(response.data.data);
        console.log(response.data);
        setMostrar2(2)
    })
}




const dataMes = (fecha1) => {
    console.log("");
}

const mostrarInmu = () =>{
      axios.get(ip+'admon/obtain_Inmu').then(response=>{
          var res = response.data
          var inmueble = []
          res.map(item=>{
            data = {
              value:item.id,
              label:item.tipo_inmueble.nombre+"-"+item.numero
            }
            inmueble.push(data)
          })
          setInmuebleSelect(inmueble)
      })
    }

    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }

    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

const masivo = () =>{

  const data = new FormData()
  var fechaini = fecha1+'-01'
  var fechaend = fecha1+'-31'
  data.append('concepto', concepto.value)
  data.append('fechaini', fechaini)
  data.append('fechaend', fechaend)
  axios.post(ip+'admon/robot',data).then(response=>{
    console.log("se responde");
    console.log(response.data);
  })

}

console.log(fecha1+'-1')
    return(
      <div className="mt-5 col-10" style={{marginLeft: '45px'}}>

      <div className="btn container col-1" onClick = {()=>props.goback()}>
      <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
      </div>

        <div  className="container col-10 mt-4" style={{marginLeft:50}}>

        <h5 className="col-4"><strong>Enviar cobros masivamente:</strong> </h5>
          <div className="row col-12">
            <div className="col-md-10 d-flex">
              <label className="d-flex col-4 align-items-center color mr-1" >Concepto: </label>
              <Select
              className="col-4"
              style={{ backgroundColor:'#FDA71A'}}
              value={concepto}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={typeConcepSelect}
              onChange={(e)=>setConcepto(e)}
              placeholder = "Seleccionar Tipo Concepto"
              name="colors"
              />
            </div>
            <div className="col-md-12 d-flex">
            <label className="d-flex col-2 mr-3 color">Mes :</label>
            <input className="form-control col-3 d-flex" type="month" onChange = {(event)=>setFecha1(event.target.value)}/>
            </div>
            <div className="my-2 d-flex col-md-9 justify-content-end px-0" style = {{float:'left'}}>
              <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black', marginRight: '33%'}} onClick={()=>search_data()}>Buscar</button>
            </div>
        </div>

      </div>
      { mostrar2 == 2 ?
          <div className="col-md-8 mt-5" style={{marginLeft:'6%'}}>
          <table className="table table-striped" style={{backgroundColor: '#FFDE59'}}>
            <thead>
              <tr>
                <th >Tipo Inmueble / Numero</th>
                <th >Concepto</th>
                <th >Valor cuota</th>

              </tr>
            </thead>
            <tbody style={{backgroundColor: 'white'}}>
            {
              listCobro.map((item, index)=>{
                return(
                  <tr>
                    <td>{item.cobros.tipo_inmueble.nombre+ " "+ item.cobros.numero}</td>
                    <td>{item.cobros.propietario.name}</td>
                    <td>${useful.convertMoney(item.cobro)}</td>
                  </tr>
                )
              })
            }
            </tbody>
          </table>


          <button type="btn"  className="btn  " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black', marginLeft: '90%'}} onClick={()=>masivo()} >Enviar</button>


        </div>
        : null
      }

    </div>

    );
}

export default PrintCobroMassive;
if(document.getElementById('PrintCobroMassive')) {
    ReactDOM.render(<PrintCobroMassive />, document.getElementById('PrintCobroMassive'));
}
