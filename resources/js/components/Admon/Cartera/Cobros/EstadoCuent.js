import React, {useState, useEffect} from 'react';
import {ip} from '../../../ApiRest';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import axios from 'axios';
import Swal from 'sweetalert2'
import '../../../../../css/app.scss';
import makeAnimated from 'react-select/animated';
import * as moment from 'moment';
import { data } from 'jquery';
import downloadjs from 'downloadjs';
import useful from '../../util';
import Pagination from '../../../pagination/Pagination'
import PaginationButton from '../../../pagination/Pagination-button'

function EstadoCuent (props) {
    const [id, setId] = useState(null);
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);
    const [inmuebleSelect, setInmuebleSelect] = useState([]);
    const [mostrar2, setMostrar2] = useState(1);
    const [inmueble, setInmueble ] = useState(null);
    const [fecha1, setFecha1] = useState('');
    const [listCobro, setListCobro,] = useState([])
    const [ token, setToken ] = useState(null)
    const [ token2, setToken2 ] = useState(null)
    const [text, setText] = useState('')
    const [concepto, setConcepto] = useState(null)
    const [typeConcepSelect, setTypeConcepSelect] = useState([]);

    const [currentPage, setCurrentPage] = useState(1)
    const [postsPerPage, setPostsPerPage] = useState(6)
    const [backCobro, setBackCobro] = useState(listCobro);
    const [backCobro2, setBackCobro2] = useState(listCobro);
    const [total, setTotal] = useState(0);



useEffect(()=>{
  cobros()
  ////  generar token para laravel sin esto no se puede ir al pdf
  var csrf = document.querySelector('meta[name="csrf-token"]').content;
  var csrf2 = document.querySelector('meta[name="csrf-token"]').content;
  setToken(csrf);
  setToken2(csrf2)
  ///
},[])

////obteniendo cobro
const cobros = () =>{
  axios.get(ip+'admon/obtain_cobro',data).then(response=>{
    console.log("se responde");
    console.log(response.data);
    setBackCobro(response.data)
    setBackCobro2(response.data)
    const dataNew = Pagination.paginate(response.data,currentPage,postsPerPage)
    setListCobro(dataNew)
  })
}
///buscadro
const searchInput = async(value) => {
  await setText(value)
  const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
  var newData = backCobro2.filter(function(item2){
    var name = ((item2.cobros.tipo_inmueble.nombre + " " + item2.cobros.numero ).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var concep = ((item2.servi.nombre_servic).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")

    var resName = name.indexOf(inputSearch) > -1
    var resNameLasT = name.indexOf(inputSearch) > -1
    var resConcep = concep.indexOf(inputSearch) > -1
    var resConcepLast = concep.indexOf(inputSearch) > -1

    var res = false
    if(resNameLasT||resNameLasT){
      res = true
    }
    if(resConcepLast||resConcepLast){
      res = true
    }
    return res;
  })
    await setCurrentPage(1)
    await setBackCobro(newData)
    const dataNew = Pagination.paginate(newData,1,postsPerPage)
    await setListCobro(dataNew)

}
////paginador
const updateCurrentPage = async (number) => {
  await setListCobro([])
  await setCurrentPage(number)
  const dataNew = Pagination.paginate(backCobro,number,postsPerPage)
  await setListCobro(dataNew)
}

const goback = async () => {
    await setEstado(1)
    setId(null)
}
    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }

    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

    const totalCobro = () => {
      var total = 0
      listCobro.map(item=>{
        total = total + item.cobro
      })
      return total
    }



    return(
      <div className="mt-5 col-10" style={{marginLeft: '45px'}}>

      <div className="btn container col-1" onClick = {()=>props.goback()}>
      <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
      </div>

        <div  className="container col-10 mt-4" style={{marginLeft:50}}>

        <h5 className="col-4"><strong>Estado de cuenta:</strong> </h5>
          <div className="row col-12">
        </div>
      </div>
        <div className="row mt-5 d-flex "  style={{marginLeft:120}} >
            <div className="col-6 mt-3 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid ', backgroundColor:'white',borderRadius:12, marginLeft:-52}}>
            <input  onChange = {(e)=>searchInput(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Consultar Cuotas...  "/>
            {
              text.length == 0 ?
              <span style = {{color:'black', cursor:'pointer'}}  className= "material-icons">search</span>
              :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
            }
            </div>
        </div>
          <div className="col-md-10 mt-5" style={{marginLeft:'6%'}}>
          <table className="table table-striped" style={{backgroundColor: '#FFDE59'}}>
            <thead>
              <tr>
                <th >Tipo Inmueble / Numero</th>
                <th >Concepto</th>
                <th >Mes cobro</th>
                <th >Estado de pago</th>
                <th >Fecha de pago</th>
                <th >Valor</th>
              </tr>
            </thead>
            <tbody style={{backgroundColor: 'white'}}>
            {
              listCobro.map((item, index)=>{



                 console.log("aca total " + total);
                if(item.estado_pago == 0){
                  var estado = "No ha pagado"
                }
                else if (item.estado_pago == 1) {
                  var estado = "Pagado"
                }
                else if(item.estado_pago == 2){
                  var estado = "Pago la mitad"
                }

                return(
                  <tr>
                    <td>{item.cobros.tipo_inmueble.nombre+ " - "+ item.cobros.numero}</td>
                    <td>{item.servi.nombre_servic}</td>
                    <td>{item.mes_admin}</td>
                    <td>{estado}</td>
                    <td>{item.fecha_pago}</td>
                    <td>${useful.convertMoney(item.cobro)}</td>
                  </tr>
                )
              })
            }
            </tbody>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Total: ${useful.convertMoney(totalCobro())}</td>


          </table>
        </div>
        <div className="d-flex col-md-12 col-12 justify-content-end">
          <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backCobro.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
        </div>
    </div>

    );
}

export default EstadoCuent;
if(document.getElementById('EstadoCuent')) {
    ReactDOM.render(<EstadoCuent />, document.getElementById('EstadoCuent'));
}
