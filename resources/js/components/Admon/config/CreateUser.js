import Reacct,{useState} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2';
import Header from '../../Ui/Header';
import Recovery from './Recovery';


function CreateUser(props) {
  const [loading, setLoading] = useState(1)
  const [marginval, setMarginVal] = useState(false)
  const [email, setEmail] = useState('')
  const [pass, setPass] = useState('')
  const [password, setPassword] = useState('')
  const [password2, setPassword2] = useState('')


  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
      })
  }
  const MessageSuccess = async (data) => {
  Swal.fire({
    text: data,
    icon: 'success',
    })
  }
  const limpiar = async() =>{
    setEmail('')
    setPass('')
    setPassword('')
    setPassword2('')
  }
  const validateEmail = async (data) => {
    var regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    console.log("validacion email");
    console.log(regex.test(data));
    return regex.test(data);

  }

  const editpass = async () => {
      var res = await validateEmail(email)
      var message = ''
      var error = false
      console.log(res);
      if (res == false) {
        MessageError("El correo no es correcto o está vacío")
      }
      else if (pass == '' && password == '') {
        MessageError("El campo de contraseña  no debe ir vacío")
      }
      else if (password.length > 6) {
        MessageError("La contraseña no es correcta debe tener mínimo 6 caracteres")
      }
      else if (pass != password){
        MessageError("las contraseñas no coinciden")

      }
    else{
      const data = new FormData()
      data.append('email',email)
      data.append('pass',pass)
      data.append('password', password)
      axios.post(ip+'admon/guard_newUser',data).then(response=>{
        console.log(response);
      if (response.data) {
        MessageSuccess("Usuario Creado correctamente")
        limpiar()
        props.gobackSave();
     }
      else{
       MessageError(" No se logro Crear el usuario")
       limpiar()
     }
  })
  }
  }


  const propHeader = (value) => {
    console.log("entro",value);
    setMarginVal(value)
  }
  return(

  <div className="content-wrapper" style={marginval ? {marginLeft:218,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:218}}>
      <div className="btn container col-1" style={{right:"70px"}} onClick = {()=>props.goback()}>
          <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
      </div>

    { loading == 1?
      <div className = "container">
        <p className="col-7 mt-5">Crear Nuevo Usuario</p>
      <div className = "col-md-6">
          <input value={email} className = "form-control"  onChange = {(e)=>setEmail(e.target.value)} type="email" placeholder = " Tu usuario o email " />
      </div>
        <div>
          <div className="col-md-6">
              <h5></h5>
              <input value={pass} className = "form-control" onChange = {(e)=>setPass(e.target.value)} type = "password" placeholder="escribe una contraseña"/>
          </div>
        </div>
        <div className="mt-3">
            <div className = "col-md-6">
                <input value={password} className = "form-control" onChange = {(e)=>setPassword(e.target.value)} type = "password" placeholder="repite contraseña"/>
            </div>
        </div>
            <div className=" col-md-6 justify-content-end mt-2" style = {{float:'right'}}>
              <button v-if="id" className="btn" style={{backgroundColor:'#FFDE59', borderRadius:10}} onClick = {()=>editpass()}>Crear Usuario</button>
            </div>
      </div>
      :null
    }

  </div>


  )


}

export default CreateUser;
if(document.getElementById('CreateUser')){
  ReactDOM.render(<CreateUser/>, document.getElementById('CreateUser'))
}
