import React,{useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Header from '../../Ui/Header';
import Swal from 'sweetalert2';
import CreateUser from './CreateUser';
import '../../../../css/app.scss';
import Pagination from '../../pagination/Pagination'
//import Image from '../../../../../public/images/footer.png';
import PaginationButton from '../../pagination/Pagination-button'
import Recovery from './Recovery';


function Configuracion () {
  const [loading, setLoading] = useState(false)
  const [text, setText] = useState('')
  const [listUsers, setListUsers ] = useState([]);
  const [backInmu, setBackInmu] = useState(listUsers);
  const [backInmu2, setBackInmu2] = useState(listUsers);
  const [id, setId] = useState(null);
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [backInm, setBackInm] = useState([])
  const [backInm2, setBackInm2] = useState([])
  const [marginval, setMarginVal] = useState(false)
  const [estado, setEstado] = useState(1);


  const updateCurrentPage = async (number) => {
    await setListUsers([])
    await setCurrentPage(number)
    const dataNew = Pagination.paginate(backInm,number,postsPerPage)
    await setListUsers(dataNew)
  }

  const searchInput = async(value) => {
    await setText(value)
    await setLoading(true)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData = backInm2.filter(function(item2){
      var name = ((item2.propietario.name).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var numero = ((item2.numero).toString()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var Tipo  = ((item2.tipo_inmueble.nombre).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var resNumero = numero.indexOf(inputSearch) > -1
      var resNumeroLasT = numero.indexOf(inputSearch) > -1
      var resTipo = Tipo.indexOf(inputSearch) > -1
      var resTipoLasT = Tipo.indexOf(inputSearch) > -1
      var res = false
      if(resNameLasT||resNameLasT){
        res = true
      }
      if(resTipo||resTipoLasT){
        res = true
      }
      if(resNumero||resNumeroLasT){
        res = true
      }

      return res;
    })
      await setCurrentPage(1)
      await setBackInm(newData)
      const dataNew = Pagination.paginate(newData,1,postsPerPage)
      await setListUsers(dataNew)
      await setLoading(false)

  }

  const mostrarUsers = async() =>{
    axios.get(ip+'admon/mostrarUsers').then(response=>{
      console.log(response);
      var res = response.data;
      setBackInm(response.data)
      setBackInm2(response.data)
      const dataNew = Pagination.paginate(response.data,currentPage,postsPerPage)
      setListUsers(dataNew)
    })
  }

  const dataUpdate = async (data) => {
    console.log(data);

    var email = data.email;

    const dataForAll = {
      email
    }
    await setId(data.id)
    await setData(dataForAll)
    await setEstado(4)
    console.log(dataForAll);
  }

  const deshabilitar = (value) =>{
    var message = 'Quieres eliminar este usuario?'
    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,
      background: '#FFEDA6',
      confirmButtonColor:'#FFDE59',
      denyButtonColor:'#FFDE59',
      borderRadius: '100px',

    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value.id)
        axios.post(ip+'admon/deleted_users',data).then(response=>{

          Swal.fire('Eliminado correctamente!', '', 'success')
          mostrarUsers();
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }

  useEffect(()=>{
    mostrarUsers();
  }, [])

  const goback = async () => {
    await setEstado(1)
    setId(null)
    mostrarUsers();
  }

  const gobackSave = () => {
    setEstado(1)
    setId(null)
    setData({})
    mostrarUsers();
  }

const propHeader = (value) => {
  console.log("entro",value);
  setMarginVal(value)
}

  return(
    <div className="content-wrapper" style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}} >
    <Header section ={9}  marginval = {(value)=>propHeader(value)}/>
    <div className="verticar-footer">
          <div className="deshabilitar" onClick = {()=>setEstado(3)} style={{marginTop:50}}>
              <h6 className="form-section d-flex align-items-center  my-3" style={{backgroundColor:'#FFDE59', borderRadius:15, marginTop:50, cursor:'pointer'}}><i className="material-icons" style= {{fontSize:'170%'}}>control_point</i> Agregar usuario</h6>
          </div>
          <h5 className="form-section habilitar" onClick = {()=>setEstado(3)}><i className="material-icons" style= {{fontSize:'170%', marginLeft:14, marginTop:44, backgroundColor:'#FFDE59', borderRadius:15, width:66}}> control_point</i></h5>
          <img className="imagen" src={ip+'images/footer.png'}/>
    </div>
    {
      estado == 1 ?
      <div >
        <div className="d-flex col-8">
        <div className="col-md-10" style={{marginLeft: '4%', marginTop: '10%'}}>
          <h4>Configuración administrador</h4>
        </div>
        </div>
      </div>
      :null
        }
    {
      estado == 1?
      <div className="col-md-10 mt-5" style={{marginLeft:35}}>
        <table class="table table-striped">
        <thead style={{backgroundColor: '#FFDE59' }}>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Nombre</th>
            <th scope="col">Email</th>
            <th scope="col">Editar</th>
            <th scope="col">Deshabilitar</th>
          </tr>
        </thead>
        <tbody>

        {
          listUsers.map((item)=>{
            return(
              <tr>
                <th scope="row">{item.id}</th>
                <td>{item.name }</td>
                <td>{item.email}</td>
                <td>
                  <div>
                  <button onClick = {()=>dataUpdate(item)} className="btn "><i class = "material-icons">edit</i></button>
                  </div>
                </td>
                <td>
                  <div>
                  <button onClick = {()=>deshabilitar(item)} className="btn "><i class = "material-icons">delete</i></button>
                  </div>
                </td>
              </tr>
            )
          })
        }

        </tbody>
      </table>
      <div className="d-flex col-md-12 col-12 justify-content-end">
        <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backInm.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
      </div>
    </div>
      :null
    }



    {estado == 3 ?
      <div style={{marginLeft:46}}>
            <div className=" col-md-8" style={{marginTop: 50}}>
              <h1>Agregar Usuario</h1>
            </div>
              <CreateUser data={data} gobackSave = {()=>gobackSave()} goback = {()=>goback()}  id = {id}/>
      </div>

      : estado == 4 ?
      <div style={{marginLeft:46}}>
        <div className=" col-md-8" style={{marginTop: 50}}>
        <h1>Editar usuario</h1>
        </div>
        <Recovery data={data} gobackSave = {()=>gobackSave()} goback = {()=>goback()}  id = {id}/>
      </div>

      :null
    }


  </div>


);

}
export default Configuracion;
if (document.getElementById('Configuracion')) {
  ReactDOM.render(<Configuracion />, document.getElementById('Configuracion'));
}
