import React,{useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2';
import Header from '../../Ui/Header';


function Recovery(props) {
  const [loading, setLoading] = useState(false)
  const [marginval, setMarginVal] = useState(false)
  const [email, setEmail] = useState('')
  const [pass, setPass] = useState('')
  const [password, setPassword] = useState('')
  const [password2, setPassword2] = useState('')


  useEffect(()=>{
    validar();
  }, [])

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
      })
  }
  const MessageSuccess = async (data) => {
  Swal.fire({
    text: data,
    icon: 'success',
    })
  }
  const validateEmail = async (data) => {
    var regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    console.log("validacion email");
    console.log(regex.test(data));
    return regex.test(data);

  }
  const limpiar = async() =>{
    setEmail('')
    setPass('')
    setPassword('')
    setPassword2('')
  }

  const validar = async() => {
    if (props.id > 0){
      setEmail(props.data.email)
    }
  }

  const editpass = async () => {
      var res = await validateEmail(email)
      var message = ''
      var error = false
      console.log(res);
      if (res == false) {
        MessageError("El correo no es correcto o está vacío")
      }
      else if(pass == ''){
        MessageError("El campo de antigua contraseña no debe ir vacío")
      }
      if (password == '' && password2 == '') {
        MessageError("El campo de contraseña  no debe ir vacío")
      }
      else if (password.length < 6) {
        MessageError("La contraseña no es correcta debe tener mínimo 6 caracteres")
      }
      else if (password != password2){
        MessageError("las contraseñas no coinciden")

      }
    else{
      const data = new FormData()
      data.append('email',email)
      data.append('pass',pass)
      data.append('password', password)
      axios.post(ip+'admon/recovery',data).then(response=>{
        console.log(response);
      if (response.data) {
        MessageSuccess("Contraseña editada")
        limpiar()
     }
      else{
       MessageError(" No se logro Crear el usuario")
       limpiar()
     }
  })
  }
  }


  const propHeader = (value) => {
    console.log("entro",value);
    setMarginVal(value)
  }
  return(

    <div className="content-wrapper" style={marginval ? {marginLeft:218,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:218}}>
      <div className="btn container col-1" style={{right:"70px"}} onClick = {()=>props.goback()}>
          <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
      </div>
    <div className = "container">
        <p className="col-7 mt-5"><strong>Cambiar contraseña</strong></p>
      <div className = "col-md-6">
          <input value={email} className = "form-control"  onChange = {(e)=>setEmail(e.target.value)} type="email" placeholder = " Tu usuario o email " />
      </div>
        <div>
          <div className="col-md-6">
              <h5></h5>
              <input value={pass} className = "form-control" onChange = {(e)=>setPass(e.target.value)} type = "password" placeholder="escribe tu contraseña"/>
          </div>
        </div>
        <div className="mt-3">
            <div className = "col-md-6">
                <input value={password} className = "form-control" onChange = {(e)=>setPassword(e.target.value)} type = "password" placeholder="escribe nueva contraseña"/>
            </div>
        </div>
        <div className="mt-3">
            <div className = "col-md-6">
                <input value={password2} className = "form-control" onChange = {(e)=>setPassword2(e.target.value)} type = "password" placeholder="Repite tu nueva contraseña"/>
            </div>
        </div>
            <div className=" col-md-6 justify-content-end mt-2" style = {{float:'right'}}>
              <button v-if="id" className="btn" style={{backgroundColor:'#FFDE59', borderRadius:10}} onClick = {()=>editpass()}>Editar Usuario</button>
            </div>
      </div>
  </div>


  )


}

export default Recovery;
if(document.getElementById('Recovery')){
  ReactDOM.render(<Recovery/>, document.getElementById('Recovery'))
}
