import React, {useState, useEffect, Fragment} from 'react';
import {ip} from '../../ApiRest';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import axios from 'axios';
import Swal from 'sweetalert2';
import '../../../../css/app.scss';
import makeAnimated from 'react-select/animated';
import { data } from 'jquery';
import * as XLSX from "xlsx";
import { log } from 'async';
import { Divider } from '@material-ui/core';

import useful from "../util";

function NewProperty (props) {
    const [loading, setLoading] = useState(false);
    const [text, setText] = useState('');
    const [inmueble, setInmueble] = useState(null);
    const [inmuebleSelect, setInmuebleSelect] = useState(null);
    const [tarifa, setTarifa] = useState(null);
    const [tarifaSelect, setTarifaSelect] = useState(null);
    const [propietario, setPropietario] = useState(null);
    const [propietariobackUp, setPropietariobackUp] = useState(null);
    const [propietarioSelect, setPropietarioSelect] = useState([]);
    const [nombreTipInm, setNombreTipInm] = useState(null);
    const [nombreUnidadRes, setNombreUnidadRes] = useState(null);
    const [unidadres, setUnidadres] = useState(null);
    const [unidadResSelect, setUnidadResselect] = useState(null);
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);
    const [estadoPar, setEstadoPar] = useState(1);
    const [estadoDep, setEstadoDep] = useState(1);
    const [estado2, setEstado2] = useState(1);
    const [parq, setParq] = useState(null);
    const [depo, setDepo] = useState(null);

    const [numero, setNumero] = useState(null);
    const [numeroPar, setNumeroPar] = useState(null);
    const [numeroDep, setNumeroDep] = useState(null);
    const [area_mt2, setArea_mt2] = useState(null);

    const [ variable, setVariable ] = useState(false)
    const [ variable2, setVariable2 ] = useState(false)

    ///constante para masivo solo para eso
    const [ resMasivo, setResMasivo ] = useState(null)
    //
    const [ vaTipo, setVaTipo ] = useState(null)
    const [ uni, setUni ] = useState(null)
    ///
    const [ recoje, setRecoje ] = useState(null)

    useEffect(()=>{
        //cargar_pro();
        obtain_tarifa();
        obtain_tipo();
        validar();
        ///cargar datos res para masivo
        obtain_masivo();
    }, [])

    const obtain_tarifa = () => {
        axios.get(ip+'admon/obtain_tarifa').then(response=>{
            console.log("tarifaaaa entro")
            console.log(response);
            var res = response.data;
            var tarifa = []
            var filtrado = res.filter(e=>e.id_concepto == 3);
            console.log(filtrado);
            filtrado.map(item=>{
              data = {
                value:item.id,
                label:item.tarifa
              }
              tarifa.push(data)
            })
            setTarifaSelect(tarifa);
            if(props.id>0){
                var filtertarifa = res.filter(e=>e.id == props.data.tarifa_inmueble.id)
                console.log("llamar parcerooo");
                console.log( props.data.tarifa_inmueble.id);
                console.log("esta es la props tarifa");
                console.log(props.data);
                console.log(filtertarifa);
                const data = {
                    value:filtertarifa[0].id,
                    label:filtertarifa[0].tarifa
                }
                setTarifa(data)
            }
        })
    }

    const obtain_tipo = () => {
        axios.get(ip+'admon/obtain_tipo').then(response=>{
            var res = response.data;
            var filtro = res.filter(e=>e.id != 1)
            setVaTipo(res)
            var inmueble = []
            filtro.map(item=>{
              data = {
                value:item.id,
                label:item.nombre
              }
              inmueble.push(data)
              obtain_unidad(item.id, false)

            })
            setInmuebleSelect(inmueble);
            if(props.id > 0){
                var filterInmueble = res.filter(e=>e.id == props.data.tip_inmueble && e.id != 1)
                console.log("esta es la props inmueble");
                console.log(props.data);
                console.log(filterInmueble);
                const data = {
                    value:filterInmueble[0].id,
                    label:filterInmueble[0].nombre
                }
                setInmueble(data)
            }
        })
    }

    const tiposInm = (value) =>{
      setInmueble(value)
      obtain_unidad(value.value ,true)


    }


    const obtain_unidad = (value, data) => {
      axios.get(ip+'admon/obtain_unidad').then(response=>{
          var res = response.data;
          var unidad = []
          setUni(res)
          var filtro = res.filter(e=>e.tip_inm == value)
          filtro.map(item=>{
            const data3 = {
              value:item.id_res,
              label:item.nombre_res
            }
            unidad.push(data3)
          })
          setUnidadResselect(unidad);

          if(data === true){
            console.log("entrooo a ser verdadero");
              setUnidadres("")
          }

          if(props.id > 0){
              var filterUnidad = res.filter(e=>e.id_res == props.data.torre)
              console.log("entro a validar la id listo perrito");
                const data2 = {
                  value:filterUnidad[0].id_res,
                  label:filterUnidad[0].nombre_res
              }

              if(data === true){
                console.log("entrooo a ser verdadero");
                  setUnidadres("")
              }
              else {
                setUnidadres(data2)
              }

          }
      })
  }

  /// este obtener es para la parte de Masivamente

      const obtain_masivo = () => {
        axios.get(ip+'admon/obtain_unidad').then(response=>{
            var res = response.data;
            var unidad = []
            res.map(item=>{
              const data3 = {
                value:item.id_res,
                label:item.nombre_res
              }
              unidad.push(data3)
            })
          setResMasivo(unidad);
        })
    }

    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }

    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

    const saveInmueble = async () => {
      console.log("Aqui variable2")
      console.log(variable2)
      var message = ''
      var error = false

    if(variable == false){
      var otra = 0
    }
    if (variable2 == false) {
      var depositoF = 0
    }
     if (variable == true) {
      var otra = parq
    }
     if (variable2 == true) {
      var depositoF = depo
    }

    if(variable2 == true){
      error = true
      message = "El número del depósito no puede ser vacío "
    }

    else if(inmueble == null){
      error = true
      message = "Seleccione el tipo de inmueble "
    }
    else if (unidadres == ''){
      error = true
      message = "Selecciona la unidad residencial "
    }
    else if (numero == null){
      error = true
      message = "El número del inmueble no puede ser vacío "
    }
    else if(variable == true){
      error = true
      message = "El número del parqueadero no puede ser vacío "
    }
    else if (area_mt2 == null){
      error = true
      message = "El área mt2 no puede ser vacío "
    }
    else if (tarifa == null){
        error = true
        message = "Selecciona la tarifa "
    }
    if(error){
        MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('id',props.id)
      data.append('tip_inmueble', inmueble.value)
      data.append('torre', unidadres.value)
      data.append('parqueadero', otra)
      data.append('numero', numero)
      data.append('num_parq', numeroPar == null ? 0 : numeroPar)
      data.append('deposito', depositoF)
      data.append('num_depo', numeroDep == null ? 0 :numeroDep)
      data.append('area_mt2', area_mt2)
      data.append('tarifa', tarifa.value)
      axios.post(ip+'admon/guard_inmu',data).then(response=>{
        console.log("aqui data");
        console.log(response.data);

        if(response.data == 2){
          MessageError("Este número del inmueble ya existe");
        }
        else if (response.data == 3) {
          MessageError("Este número del parqueadero ya existe");
        }
        else if (response.data == 4) {
          MessageError("Este número del depósito ya existe");
        }
        else{
          if (props.id == null) {
            crearOtro();
          }
          else{
            MessageSuccess("Inmueble editado correctamente");
            props.gobackSave();
            obtain_unidad()
            obtain_tipo()
          }

        }

      })
    }

  }

  const crearOtro = () =>{
    var message = '¿Quieres crear otro inmueble?';
    Swal.fire({
      text: message,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,
      background: '#FFEDA6',
      confirmButtonColor:'#FFDE59',
      denyButtonColor:'#FFDE59',
      borderRadius: '100px',
      width: 300,
      height: 300
    }).then((result) => {
      if (result.isConfirmed) {

        setInmueble(null);
        setUnidadres(null);
        setParq(null);
        setDepo(null);
        setNumero("");
        setArea_mt2("");
        setTarifa(null);
        setNumeroPar(null);
        setNumeroDep(null)
        setEstadoPar(1)
        setEstadoDep(1)
        setVariable(false)
        setVariable2(false)
        MessageSuccess("Inmueble creado correctamente");

      } else if (result.isDenied) {
        MessageSuccess("Inmueble creado correctamente");
        props.gobackSave();
        obtain_unidad()
        obtain_tipo()
      }
    })
  }

  const saveTipInmue = async () => {
    var message = ''
    var error = false
    if (nombreTipInm == null){
    error = true
    message = "Escribe el nombre del tipo de inmueble "
  }
  if(error){
      MessageError(message)
  }
  else{
    const data = new FormData()
    data.append('nombre', nombreTipInm)
    axios.post(ip+'admon/guard_tipInmueble',data).then(response=>{
      if (response.data == 1) {
        MessageSuccess("Tipo de inmueble creado correctamente");
        setNombreTipInm("");
        obtain_unidad()
        obtain_tipo()
        //props.gobackSave();
      }
      else {
        MessageError("Ya hay un tipo de inmueble creado con ese nombre");
        setNombreTipInm("");
        obtain_unidad()
        obtain_tipo()

      }
    })
  }

}

    const saveUnidadRes = async () => {
      var message = ''
      var error = false
      if(inmueble == null){
        error = true
        message = "Elige tipo de inmueble "
      }
      else if (nombreUnidadRes == null){
      error = true
      message = "Escribe el nombre de la unidad residencial "
    }
    if(error){
        MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('tip_inm', inmueble.value)
      data.append('nombre_res', nombreUnidadRes)
      axios.post(ip+'admon/guard_unidadRes',data).then(response=>{
        if (response.data == 1) {
          MessageSuccess("Tipo de unidad residencial creada correctamente");
          setInmueble("");
          setNombreUnidadRes("");
          obtain_unidad()
          obtain_tipo()
          //props.gobackSave();
        }else {
          MessageError("Ya hay un tipo de unidad creada con ese nombre o con ese tipo de inmueble");
          setInmueble("");
          setNombreUnidadRes("");
          obtain_unidad()
          obtain_tipo()
        }
      })
    }

  }

  const validar = async () =>{
    console.log("entro aquiii");
    if(props.id > 0 ){
      if(props.data.parqueadero == 1){
        setVariable(true)
        setNumeroPar(props.data.num_parq);
        setEstadoPar(2)
      }
      if(props.data.deposito == 1){
        setVariable2(true)
        setNumeroDep(props.data.num_depo);
        setEstadoDep(2)
      }
      setInmueble(props.data.tip_inmueble);
      setUnidadres(props.data.unidad_res);
      setParq(props.data.parqueadero);
      setDepo(props.data.deposito);
      setNumero(props.data.numero);
      setArea_mt2(props.data.area_mt2);
      setTarifa(props.data.tarifa);

    }
  }

  const descPlantilla = async ()=>{
    $("#site").on('click', function(){
      window.open(ip+"plantilla/Plantilla.xlsx","_blank");
    });
  }

  const validateDocument = async(oEvent) => {
    const oFile = oEvent.target.files[0];
    console.log(oEvent.target.files[0])
      if(oFile.name.includes('xlsx') || oFile.name.includes('xls')){
        chargeMassive(oEvent);
      }else{
      MessageError("Solo se permiten archivos excel");
    }
  }

  {/*Aca se realiza para poder cargar un archivo en excel*/}

  const chargeMassive = async (oEvent) => {
  // Get The File From The Input
  var oFile = oEvent.target.files[0];
  var sFilename = oFile.name;
  // Create A File Reader HTML5
  var reader = new FileReader();

  // Ready The Event For When A File Gets Selected
  reader.onload = function(evt) {
    const bstr = evt.target.result;
    const wb = XLSX.read(bstr, { type: "binary" });
    /* Get first worksheet */
    const wsname = wb.SheetNames[0];
    const ws = wb.Sheets[wsname];
    /* Convert array of arrays */
    const data = XLSX.utils.sheet_to_csv(ws, { header: 1 });
    /* Update state */
    console.log("Data>>>" + data);// shows that excel data is read
    console.log(useful.convertToJson(data)); // shows data in json format
    validateJsonData(useful.convertToJson(data))
  };

  // Tell JS To Start Reading The File.. You could delay this if desired
  reader.readAsBinaryString(oFile);
  }

  const validateJsonData = (data) => {
    var array = data
    array.splice(data.length-1,1)
    console.log(array);
    console.log("estaaa es los datos");
    console.log(resMasivo);
    console.log(tarifaSelect);
    console.log(inmuebleSelect);
    //console.log(propietariobackUp);
    var newArray = []
    for (let index = 0; index < array.length; index++) {

      var da = inmuebleSelect.filter(e=>e.label.toUpperCase() == array[index].tipo_inmueble.toUpperCase())[0].value;
      var data = vaTipo.filter(e=>e.id == da);
      ////
      console.log("aqui id de tipoooo");
      console.log(da);

      var data2 =  resMasivo.filter(e=>e.label.toUpperCase() == array[index].torre.toUpperCase())[0].value;
      var data3 = uni.filter(e=>e.id_res == data2);
      console.log("aqui data 3333");
      console.log(data3);
      var respo = [];

      var result = data3.map(item=>{
        return item.tip_inm;
      })

      console.log("aqui ide de torre");
      console.log(result);

      if(inmuebleSelect.filter(e=>e.label.toUpperCase() == array[index].tipo_inmueble.toUpperCase()).length ==0){
        MessageError("Hay un valor de tipo de inmueble que no corresponde en la fila "+parseInt(index+1));
        window.document.getElementById("ChargeMasive").value = "";
        newArray = []
        break;


      }

      else if(resMasivo.filter(e=>e.label.toUpperCase() == array[index].torre.toUpperCase()).length == 0){
        console.log("aqui dentra al filtrooo resss");
        console.log(resMasivo);
        MessageError("Hay un valor de torre que no corresponde en la fila "+parseInt(index+1));
        window.document.getElementById("ChargeMasive").value = "";
        newArray = []
        break;
      }
      else if(result != da){
        MessageError("El tipo inmueble no coincide con la unidad residencialen la fila "+parseInt(index+1));
        window.document.getElementById("ChargeMasive").value = "";
        newArray = []
        break;
      }


      /*else if(propietariobackUp.filter(e=>e.N_documet ==  array[index].terceros).length == 0){
        MessageError("Hay un valor de propietario que no corresponde en la fila "+parseInt(index+1));
        window.document.getElementById("ChargeMasive").value = "";
        newArray = []
        break;
      }*/

     else if(tarifaSelect.filter(e=>e.label ==  array[index].tarifa).length == 0){
      MessageError("Hay un valor de tarifa que no corresponde en la fila "+parseInt(index+1));
      window.document.getElementById("ChargeMasive").value = "";
      newArray=[]
      break;
     }
     else{
       console.log("entro else")
       console.log(array[index])
       var dataToPush = {
        tipo_inmueble : inmuebleSelect.filter(e=>e.label.toUpperCase() == array[index].tipo_inmueble.toUpperCase())[0].value,
        torre : resMasivo.filter(e=>e.label.toUpperCase() == array[index].torre.toUpperCase())[0].value,
        parqueadero: array[index].parqueadero,
        numero_parqueadero: array[index].numero_parqueadero,
        deposito: array[index].deposito,
        numero_deposito: array[index].numero_deposito,
        numero_inmueble: array[index].numero_inmueble,
        area: array[index].area_mt2,
        //propietario: propietariobackUp.filter(e=>e.N_documet ==  array[index].terceros)[0].id,
        tarifa:tarifaSelect.filter(e=>e.label ==  array[index].tarifa)[0].value
       }
      newArray.push(dataToPush)
     }

    }
    if (newArray.length>0) {
      console.log("esta es toda la data:",newArray);
      const dataform = new FormData()
      dataform.append('data',JSON.stringify(newArray))
      axios.post(ip+'admon/guard_inmu_massive',dataform).then(response=>{

        if(response.data == 2){
          MessageError("No es posible crear: verifique en su excel si ya existe otro inmueble con algún número asignado");
          props.gobackSave();
        } else {
          if(props.id == null){
          MessageSuccess("Tipos de inmueble creado correctamente");
          }
        }
      }
    )}
  }
    {/*Aca termina*/}

    const parqueaderoData = async (data) => {
      console.log(data)
      if(data.target.checked == true){
        setEstadoPar(2)
        setParq(1)
        setVariable(true)
      }
      else if (data.target.checked == false){
        setEstadoPar(1)
        setVariable(false)
        setNumeroPar(null);
      }
    }

    const depositoData = async (data) => {
      console.log(data)
      if(data.target.checked == true){
        setEstadoDep(2)
        setDepo(1)
        setVariable2(true)

      }
      else if (data.target.checked == false){
        setEstadoDep(1)
        setVariable2(false)
        setNumeroDep(null);

      }
    }

    const traductor = (elemento) =>{
      console.log(elemento);
          if (!/^([0-9.])*$/.test(elemento)){
            MessageError("Verifique que digitó números y escribió el decimal con punto (.)");

          var str = elemento.replace(/ [1234567890] /g, "");
          console.log("se limpiaron los datos");
          console.log(str);

           setArea_mt2("")
         }
          else {
            setArea_mt2(elemento)
          }
    }

    return(
      <div className="mt-5 col-11">
        <div className="btn container col-1 " onClick = {()=>props.goback()}>
          <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
        </div>

        { props.estado_formulario == 3 ?
           <div>
             <div className="row col-12">
            <div className="my-2 d-flex col-md-9">
              <h4 className="mr-3">{props.id > 0 ? 'Editar Inmueble':'Agregar inmueble'}</h4>
            </div>

            {
              props.id > 0 ?


              <div className="col-md-12 d-flex ">
              <label className="d-flex col-12 align-items-center color" style = {{width:'205px'}}>Tipos de inmuebles: </label>
              <Select
              isDisabled
              className="col-9"
              value={inmueble}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={inmuebleSelect}
              onChange={(e)=>tiposInm(e)}
              placeholder = "Seleccionar tipo inmueble"
              name="colors"
              />
              </div>
              :
              <div className="col-md-12 d-flex ">
              <label className="d-flex col-12 align-items-center color" style = {{width:'205px'}}>Tipos de inmuebles: </label>
              <Select
              className="col-9"
              value={inmueble}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={inmuebleSelect}
              onChange={(e)=>tiposInm(e)}
              placeholder = "Seleccionar tipo inmueble"
              name="colors"
              />
              </div>
            }

            {
              props.id > 0 ?

              <div className="col-md-12 d-flex ">
              <label className="d-flex col-12 align-items-center color" style = {{width:'205px'}}>Interior: </label>
              <Select
              isDisabled
              className="col-9"
              value={unidadres}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={unidadResSelect}
              onChange={(e)=>setUnidadres(e)}
              placeholder = "Seleccionar el interior"
              name="colors"
              />
              </div>

              :

              <div className="col-md-12 d-flex ">
              <label className="d-flex col-12 align-items-center color" style = {{width:'205px'}}>Interior: </label>
              <Select
              className="col-9"
              value={unidadres}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={unidadResSelect}
              onChange={(e)=>setUnidadres(e)}
              placeholder = "Seleccionar el interior"
              name="colors"
              />
              </div>
            }

            {
              props.id > 0 ?
              <div className="col-md-12 d-flex">
              <label className="d-flex col-12 mr-2 color" style={{width:'205px'}}>Número del inmueble:</label>
              <input className="form-control col-md-9 d-flex" disabled style={{width:'771px'}} placeholder="Ingrese el número del inmueble" value={numero} type="number" onChange={(event)=>setNumero(event.target.value)}/>
              </div>

              :

              <div className="col-md-12 d-flex">
              <label className="d-flex col-12 mr-2 color" style={{width:'205px'}}>Número del inmueble:</label>
              <input className="form-control col-md-9 d-flex" style={{width:'771px'}} placeholder="Ingrese el número del inmueble" value={numero} type="number" onChange={(event)=>setNumero(event.target.value)}/>
              </div>


            }


            <div className = "col-md-10 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '205px'}}>Asignar parqueadero</label>
                <div className="form-check form-switch" >
                    <input className="form-check-input" onClick={(parq)=>parqueaderoData(parq)} checked={variable} type="checkbox" id="flexSwitchCheckDefault" style={{width: '50px' , height: '25px'}}/>
                    <label className="form-check-label" for="flexSwitchCheckDefault"></label>
                </div>
            </div>
            {
              estadoPar == 2 ?

              <div className="col-md-12 d-flex">
              <label className="d-flex col-12 mr-2 color" style={{width:'205px'}}>Número de parqueadero:</label>
              <input className="form-control col-md-9 d-flex" style={{width:'771px'}} placeholder="Ingrese el número del parqueadero" value={numeroPar} type="number" onChange={(event)=>setNumeroPar(event.target.value)}/>
              </div>

              :null

            }

            <div className = "col-md-10 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '205px'}}>Asignar depósito</label>
                <div className="form-check form-switch" >
                    <input className="form-check-input" onClick={(depo)=>depositoData(depo)} checked={variable2} type="checkbox" id="flexSwitchCheckDefault" style={{width: '50px' , height: '25px'}}/>
                    <label className="form-check-label" for="flexSwitchCheckDefault"></label>
                </div>
            </div>

            {
              estadoDep == 2 ?

              <div className="col-md-12 d-flex">
              <label className="d-flex col-12 mr-2 color" style={{width:'205px'}}>Número de depósito:</label>
              <input className="form-control col-md-9 d-flex" style={{width:'771px'}} placeholder="Ingrese el número del depósito" value={numeroDep} type="number" onChange={(event)=>setNumeroDep(event.target.value)}/>
              </div>

              : null
            }

            <div className="col-md-12 d-flex" >
            <label className="d-flex col-12 mr-2 color" style={{width: '205px'}} >Área mt2:</label>
            <input className="form-control col-md-9 d-flex" style={{width: '771px'}} placeholder="Ingrese el área mt2" value={area_mt2} type="text" onChange={(event)=>traductor(event.target.value)} />
            </div>

            {/*<div className="col-md-12 d-flex">
            <label className="d-flex col-4 align-items-center color" style={{width: '165px'}}>Propietario: </label>
            <Select
            className="col-9"
            style={{ backgroundColor:'#FDA71A'}}
            value={propietario}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={propietarioSelect}
            onChange={(e)=>setPropietario(e)}
            placeholder = "Seleccionar propietario"
            name="colors"
            />
            </div>*/}

            <div className="col-md-12 d-flex">
            <label className="d-flex col-4 align-items-center color" style={{width: '205px'}}>Tarifa: </label>
            <Select
            className="col-9"
            value={tarifa}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={tarifaSelect}
            onChange={(e)=>setTarifa(e)}
            placeholder = "Seleccionar tarifa"
            name="colors"
            />
            </div>
          </div>
          <div className="my-2 d-flex col-md-9 justify-content-end px-0" style = {{marginLeft: '174px'}}>
            <button className="btn btn-success" style={{border:'1px solid #FFDE59', backgroundColor:'#FFDE59', color:'black', fontWeight: "bold"}} onClick = {()=>saveInmueble()}>{props.id > 0 ? 'Guardar Cambios':'Agregar'}</button>
          </div>

            </div>

          :null
        }

        <Divider className="col-md-12 my-2" style={{backgroundColor:'black', marginLeft: '13px', width: '965px'}} />

        <div className="form-group">

          {
            props.estado_formulario == 4 ?

            <div>
              <div className="my-2 d-flex col-md-9">
            {props.id == null&&<div className="my-2 d-flex col-md-9">
            <h4 className="my-2 mr-3">Agregar inmuebles masivamente</h4>
            </div>}
            {/*<button className="btn btn-success" id="site" style={{border:'1px solid #FFDE59', backgroundColor:'#FFDE59', color:'black', fontWeight: "bold"}} onClick = {()=>chargeMassive()}>Agregar Masivamente</button>*/}

          </div>

          <div className="my-1 d-flex col-md-9">
            {props.id == null&&<button className="btn btn-success" id="site" style={{border:'1px solid #FFDE59', backgroundColor:'#FFDE59', color:'black', fontWeight: "bold"}} onClick = {()=>descPlantilla()}>Descargar plantilla</button>}
          </div>



          <div className="my-1 d-flex col-md-12" style={{fontWeight: "bold"}}>
          {props.id == null&&<label className="mr-3 d-flex align-items-center justify-content-center caja-subir-archivo cursor-pointer" style = {{height: '110px', width: '350px', cursor: 'pointer', marginTop:20}}>
          <div className="text-center">
            <i className="material-icons color-degradado-danaranjo" style={{fontSize: '60px'}}>cloud_upload</i>
            <p>Seleccionar archivo</p>
          </div>
          <input className="ocultar" type = "file" onChange = {(e)=>validateDocument(e)} id = "ChargeMasive"/>
          </label>}
          </div>



          {props.id == null&&<Divider className="col-md-10 my-3" style={{backgroundColor:'black', marginLeft: '13px'}} />}
            </div>

            :null
          }


      </div>
      {
        props.estado_formulario == 5 ?
        <div>

      { props.id == null ?
        <div  className="form-group">
          <div className="col-6">
          <h4 className="form-section d-flex align-items-center">Agregar unidades residenciales</h4>
          <p>Ingrese el nombre de la torre o el interior y asignele el número</p>
          </div>
            <div className="row col-12">
              <div className="col-md-12 d-flex ">
              <label className="d-flex col-12 align-items-center color" style = {{width:'227px'}}>Tipos de inmuebles: </label>
              <Select
              className="col-6"
              value={inmueble}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={inmuebleSelect}
              onChange={(e)=>setInmueble(e)}
              placeholder = "Seleccionar tipo inmueble"
              name="colors"
              />
              </div>
              <div className="col-md-12 d-flex" >
              <label className="d-flex col-3 mr-2 align-items-center color" style = {{width:'max-content'}}>Nombre de la torre/interior:</label>
              <input className="form-control col-md-6 mr-3 d-flex" placeholder="Ingrese el nombre de la torre o el interior (Torre 1, Torre 2)" style={{ border:'1px solid colors', width:'516px'}} value={nombreUnidadRes} onChange={(event)=>setNombreUnidadRes(event.target.value.toUpperCase())} type="text" />
              </div>
              <div className="col-md-8 d-flex" style = {{float:'right', marginTop: '96'}}>
              <button className="btn btn-success col-md-4" style={{border:'1px solid #FFDE59', backgroundColor:'#FFDE59', color:'black', height:'37px', fontWeight: "bold", width: '230px'}} onClick = {()=>saveUnidadRes()}>Guardar unidad residencial</button>
              </div>
          </div>

          {props.id == null&&<Divider className="col-md-10 my-3" style={{backgroundColor:'black', marginLeft: '13px'}} />}

        </div>

        : null
      }

        </div>
        :null
      }

      {
        props.estado_formulario == 6 ?
        <div>
          <div className="col-6">
          <h4 className="form-section d-flex align-items-center">Agregar tipos de inmuebles</h4>
          </div>
            <div className="row col-12">
              <div className="col-md-12 d-flex" >
              <label className="d-flex col-3 mr-3 align-items-center color" style = {{width:'max-content'}}>Nombre del tipo de inmueble:</label>
              <input className="form-control col-md-4 mr-3 d-flex" placeholder="Digíte el tipo de inmueble" style={{ border:'1px solid colors'}} value={nombreTipInm} onChange={(event)=>setNombreTipInm(event.target.value.toUpperCase())} type="text" />
              <button className="btn btn-success col-md-4" style={{border:'1px solid #FFDE59', backgroundColor:'#FFDE59', color:'black', height:'37px', fontWeight: "bold", width: '230px'}} onClick = {()=>saveTipInmue()}>Guardar tipo de inmueble</button>
              </div>
              <div className="col-md-8 d-flex" style = {{float:'right', marginTop: '96'}}>

              </div>
          </div>

          {props.id == null&&<Divider className="col-md-10 my-3" style={{backgroundColor:'black', marginLeft: '13px'}} />}
        </div>
        :null
      }

    </div>

    );
}

export default NewProperty;
if(document.getElementById('NewProperty')) {
    ReactDOM.render(<NewProperty />, document.getElementById('NewProperty'));
}
