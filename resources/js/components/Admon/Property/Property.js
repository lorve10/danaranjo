import React,{useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Header from '../../Ui/Header';
import Swal from 'sweetalert2';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import NewProperty from './NewProperty';
import '../../../../css/app.scss';
import { Tooltip, Button, Grid  } from '@material-ui/core';

import util from "../util";
import Pagination from '../componentes/paginate';

function Property () {
  const [loading, setLoading] = useState(false)
  const [text, setText] = useState('')
  const [listInmueble, setListInmueble ] = useState([]);
  const [backInmu, setBackInmu] = useState(listInmueble);
  const [backInmu2, setBackInmu2] = useState(listInmueble);
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);
  const [type, setType] = useState(null);
  const [id, setId] = useState(null);
  const [data, setData] = useState({})
  const [showForm, setShowForm] = useState(1)
  const [currentPage, setCurrentPage] = useState(1)
  const [perPage, setPerPage] = useState(6)
  const [backInm, setBackInm] = useState([])
  const [backInm2, setBackInm2] = useState([])
  const [marginval, setMarginVal] = useState(false)
  const [estado, setEstado] = useState(1);
  const [estado_formulario, setEstado_formulario] = useState(3);

  const [ disable, setDisable ] = useState(false);
  const [ todos, setTodos ] = useState([
    {
      value: '1',
      label: 'Todos',
    }
  ]);
  const [ tipo, setTipo ] = useState([]);
  const [ selectTipo, setSelectTipo ] = useState(null);

  useEffect(()=>{
    mostrarInmueble();
    active();
    selectoTipo();
  }, [])

  const selectoTipo = async () => {
    axios.get(ip+'admon/obtain_tipo').then(response=>{
      var res = response.data;
      var tip_inmueble = []
      res.map(item=>{
       const data = {
          value:item.id,
          label:item.nombre
        }
        tip_inmueble.push(data)

      })
      setTipo(tip_inmueble);
    });
  }


  const updateCurrentPage = async (number) => {
    await setListInmueble([])
    await setCurrentPage(number)
    const dataNew = util.paginate(backInm2,number,perPage)
    await setListInmueble(dataNew)
  }

  const searchInput = async(value, otro) => {
    await setText(value)
    await setLoading(true)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData = backInm2.filter(function(item2){
      //var name = ((item2.propietario.name).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var numero = ((item2.numero).toString()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var Tipo  = ((item2.tipo_inmueble.nombre).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var Torre  = ((item2.tipo_unidad.nombre_res).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      /*var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1*/
      var resNumero = numero.indexOf(inputSearch) > -1
      var resNumeroLasT = numero.indexOf(inputSearch) > -1
      var resTipo = Tipo.indexOf(inputSearch) > -1
      var resTipoLasT = Tipo.indexOf(inputSearch) > -1
      var resTorre = Torre.indexOf(inputSearch) > -1
      var resTorreLasT= Torre.indexOf(inputSearch) > -1
      var res = false
      /*if(resNameLasT||resNameLasT){
        res = true
      }*/
      if(resTipo||resTipoLasT){
        res = true
      }
      if(resNumero||resNumeroLasT){
        res = true
      }
      if(resTorre||resTorreLasT){
        res = true
      }

      return res;
    })

    if(otro){
      console.log("entrooo aquiii");
      console.log(otro);
      await setCurrentPage(1)
      await setBackInmu(otro)
      const dataNew = util.paginate(otro,1,perPage)
      await setListInmueble(otro)
      await setLoading(false)
    }
    else {
      await setCurrentPage(1)
      await setBackInmu(newData)
      const dataNew = util.paginate(newData,1,perPage)
      await setListInmueble(dataNew)
      await setLoading(false)
    }


  }

  const tipo_inm = (e) =>{

    const data2 ={
      value:e.value,
      label:e.label
    }
    setSelectTipo(data2)
    setCurrentPage(1)
    if (e.value == 1){
      const dataNew = util.paginate(backInm2,1,perPage)
      // console.log(dataNew);
      // var filtro = dataNew.filter((data,index) => index.length > 6 );
      // console.log("entrooo");
      // console.log(filtro);
      setListInmueble(dataNew)
      searchInput("", dataNew)

    }
    else{
      var data = []
      console.log("antes de filtrar => "+backInm2.length)
      data =  backInm2.filter((item, i)  => item.tip_inmueble == e.value);
      setListInmueble(data)
      console.log("Despues de filtrar =>"+data.length)
      const dataNew = util.paginate(data,1,perPage)
      setListInmueble(dataNew);
      searchInput("", dataNew)

      setLoading(false)
    }

  }

  const mostrarInmueble = async() =>{
    axios.get(ip+'admon/obtain_Inmu').then(response=>{
      console.log(response);
      var res = response.data;
      setBackInm(response.data)
      setBackInm2(response.data)
      const dataNew = util.paginate(response.data,currentPage,perPage)
      setListInmueble(dataNew)
    })
  }

  const dataUpdate = async (data) => {
    console.log("Estaaa es la daa")
    console.log(data);

    var tip_inmueble = data.tip_inmueble
    var torre = data.torre
    var parqueadero = data.parqueadero
    var num_parq = data.num_parq
    var deposito = data.deposito
    var num_depo = data.num_depo
    var numero = data.numero
    var area_mt2 = data.area_mt2
    //var propietario = data.propietario
    var tarifa_inmueble = data.tarifa_inmueble

    const dataForAll = {

      tip_inmueble,
      torre,
      parqueadero,
      num_parq,
      deposito,
      num_depo,
      numero,
      area_mt2,
      //propietario,
      tarifa_inmueble
    }
    await setId(data.id)
    await setData(dataForAll)
    await active(3)
    console.log(dataForAll);
  }

  const deshabilitar = (valor) =>{

    if(valor.deleted == 0){
      var message = '¿Quieres deshabilitar este inmueble?';
      Swal.fire({
        title: message,
        icon: 'warning',
        showDenyButton: true,
        confirmButtonColor: '#3085d6',
        denyButtonColor: '#d33',
        confirmButtonText: 'Si',
        denyButtonText: 'No'
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          const data = new FormData()
          data.append('id', valor.id)
          axios.post(ip+'admon/deleted_inmueble',data).then(response=>{

            if(response.data == 1){
              Swal.fire('Deshabilitado correctamente!', '', 'success')
              mostrarInmueble();
            }
            else{
              Swal.fire('No se puede eliminar por que ya tiene una cuenta de cobro.', '', 'info')
              mostrarInmueble();
            }

          })

        } else if (result.isDenied) {
          Swal.fire('Acción cancelada', '', 'info')
        }
      })

    }else{
      var message = '¿Quieres habilitar este inmueble?';
      Swal.fire({
        title: message,
        icon: 'warning',
        showDenyButton: true,
        confirmButtonColor: '#3085d6',
        denyButtonColor: '#d33',
        confirmButtonText: 'Si',
        denyButtonText: 'No'
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          const data = new FormData()
          data.append('id', valor.id)
          axios.post(ip+'admon/habilited_inmueble',data).then(response=>{
            if(response.data == 1){
              Swal.fire('Habilitado correctamente!', '', 'success')
              mostrarInmueble();
            }
          })

        } else if (result.isDenied) {
          Swal.fire('Acción cancelada', '', 'info')
        }
      })
    }

  }

  const goback = async () => {
    await setEstado(1)
    setId(null)
  }

  const gobackSave = () => {
    setEstado(1)
    setId(null)
    setData({})
    mostrarInmueble();
    selectoTipo();
  }

const propHeader = (value) => {
  console.log("entro",value);
  setMarginVal(value)
}

  const active = async (valor) => {
    if (valor == 3) {
      setEstado(3)
      console.log("entrooo");
      await setEstado_formulario(valor)

    }
    await setEstado_formulario(valor)

  }
  console.log(disable);
  return(
    <div className="content-wrapper" style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}} >
    <Header section ={2}  marginval = {(value)=>propHeader(value)}/>
    <div className="verticar-footer-2">


        {
          estado == 3 ?

          <div>
                <Tooltip title="Agregar tipos de inmuebles" placement="left">
                 <Button>
                   <h5 onClick = {()=>active(6)}><img src = {ip+'images/Asambleas.png'} style= {{width:'56px', marginLeft: '0px', marginTop: '35px'}} /> </h5>
                 </Button>
                </Tooltip>

                <Tooltip title="Agregar unidades recidenciales" placement="left">
                  <Button>
                    <h5 onClick = {()=>active(5)}><img src = {ip+'images/unidadesR.png'} style= {{width:'56px', marginLeft: '0px', marginTop: '35px'}} /> </h5>
                  </Button>
                </Tooltip>

                <Tooltip title="Agregar inmueble" placement="left">
                  <Button>
                    <h5 onClick = {()=>active(3)}><img src = {ip+'images/agregar_uni.png'} style= {{width:'56px', marginLeft: '0px', marginTop: '35px'}} /> </h5>
                  </Button>
                </Tooltip>

                <Tooltip title="Agregar inmueble masivamente" placement="left">
                  <Button>
                    <h5 onClick = {()=>active(4)}><img src = {ip+'images/masivo.png'} style= {{width:'56px', marginLeft: '0px', marginTop: '35px'}} /> </h5>
                  </Button>
                </Tooltip>




          </div>

          : null
        }

        {
          estado == 1?
          <Tooltip title="Agregar inmueble" placement="left">
             <Button>
               <h5 onClick = {()=>active(3)}><img src = {ip+'images/agregar_uni.png'} style= {{width:'56px', marginLeft: '0px', marginTop: '35px'}} /> </h5>
             </Button>
           </Tooltip>

           :null
        }

          {/*<div className="deshabilitar" onClick = {()=>setEstado(4)} style={{marginTop:30}}>
              <h6 className="form-section d-flex align-items-center  my-3" style={{backgroundColor:'#FFDE59', borderRadius:15, marginTop:50, cursor:'pointer'}}><i className="material-icons" style= {{fontSize:'170%'}}>control_point</i> Agregar inmuebles masivamente</h6>
      </div>*/}


          {/*<h5 className="form-section habilitar" onClick = {()=>setEstado(4)}><i className="material-icons" style= {{fontSize:'170%', marginLeft:14, marginTop:20, backgroundColor:'#FFDE59', borderRadius:15, width:66}}> control_point</i></h5>*/}

          <img className="imagen" src={ip+'images/footer.png'}/>
    </div>
    {
      estado == 1 ?
      <div>
      <div className="row">
        <div className="col-md-9 " style={{marginLeft:47}}>
        <div className="btn container mt-3 d-flex" style={{marginLeft:'-135px'}}>
          <div className="form-section col-md-8 "><strong><h6 className="mt-2 ruta" style={{marginLeft:'-50px'}}>/ <a href = {ip+'admon/conjunto'}>Módulo conjunto </a>/ Unidades residenciales</h6></strong></div>
        </div>
        <div className="mt-3">
          <h4 className="text-center" style={{marginLeft:"-75%"}}>Unidades residenciales</h4>
        </div>
          <div className="d-flex mt-2" style={{marginTop: 9}}>
              <label className="mr-3 d-flex mt-2">Tipo inmueble: </label>
              <Select
              className="col-3"
              defaultValue={todos}
              value={selectTipo}
              closeMenuOnSelect={false}
              components={animatedComponents}
              options={tipo}
              onChange={(e)=>tipo_inm(e)}
              placeholder="Seleccione..."
              name="colors"
              />
              <div className="col-7  mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #D9D9D9 ', backgroundColor:'white',borderRadius:12}}>
              <input  onChange = {(e)=>searchInput(e.target.value)} style = {{width:'490px',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Consultar número de residencia, torre, propietario... "/>
              {
                text.length == 0 ?
                <span style = {{color:'black', cursor:'pointer'}}  className= "material-icons">search</span>
                :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
              }
              </div>
          </div>

        </div>
      </div>
      </div>
      :null
    }
    {
      estado == 1?
      <div className="col-md-10" style={{marginLeft:35}}>
        <table class="table table-striped">
        <thead style={{backgroundColor: '#FFDE59' }}>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Tipo Inmueble</th>
            <th scope="col">Torre - Numero</th>
            <th scope="col">Área Mt2</th>
            <th scope="col">Tarifa</th>
            <th scope="col">Editar</th>
            <th scope="col">Deshabilitar</th>
          </tr>
        </thead>
        <tbody>

        {
          listInmueble.map((item)=>{
            return(
              <tr>
                <th scope="row">{item.id}</th>
                <td>{item.tipo_inmueble.nombre}</td>
                <td>{item.tipo_unidad.nombre_res + " - " + item.numero }</td>
                <td>{item.area_mt2}</td>
                {/*{
                  item.propietario.tip_per == 1 ?

                  <td>{item.propietario.name}</td>
                  :
                  <td>{item.propietario.name} {item.propietario.secname == null ? '': item.propietario.secname} {item.propietario.lastname} {item.propietario.seclastname}</td>
                }*/}
                <td>$ {util.convertMoney(item.tarifa_inmueble.tarifa)}</td>
                <td>
                  <div>
                  <button onClick = {()=>dataUpdate(item)} className="btn "><i class = "material-icons">edit</i></button>
                  </div>
                </td>
                <td>
                  <div className="form-check form-switch">
                      <input className="form-check-input cursor-pointer" onClick={()=>deshabilitar(item)} checked={item.deleted == 0 ? true : false} type="checkbox" id="flexSwitchCheckDefault"/>
                      <label className="form-check-label" for="flexSwitchCheckDefault"></label>
                  </div>
                </td>
              </tr>
            )
          })
        }

        </tbody>
      </table>
      <div className="d-flex col-md-12 col-12 justify-content-end">
        <Pagination currentPage={currentPage} perPage={perPage} countdata={backInm.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
      </div>
    </div>
      :null
    }



    {estado == 3 ?
      <div style={{marginLeft:46}}>

              <NewProperty estado_formulario={estado_formulario} data={data} gobackSave = {()=>gobackSave()} goback = {()=>goback()}  id = {id}/>
        </div>

      :null
    }


  </div>


);

}
export default Property;
if (document.getElementById('Property')) {
  ReactDOM.render(<Property />, document.getElementById('Property'));
}
