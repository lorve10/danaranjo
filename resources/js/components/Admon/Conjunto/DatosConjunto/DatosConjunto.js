import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Swal from 'sweetalert2';
import Select from 'react-select'
import Header from '../../../Ui/Header';
import Data from './Data';
import DataConsejo from './DataConsejo';
import axios from 'axios';
import '../../../../../css/app.scss';
import makeAnimated from 'react-select/animated'
import Pagination from '../../../pagination/Pagination'
import PaginationButton from '../../../pagination/Pagination-button'
import { data } from 'jquery';
import Compressor from 'compressorjs';

function DatosConjunto() {
  const [ loading, setLoading ] = useState(false)
  const [ showForm, setShowForm ] = useState(1)
  const [ marginval, setMarginVal ] = useState(false)
  const [ id, setId ] = useState(null)
  const [ data, setData ] = useState({})
  const [ urlImg, setUrlImg ] = useState(null)
  const [ img, setImg ] = useState({})

  const [ nameAdmin, setNameAdmin ] = useState("")
  const [ firma, setFirma ] = useState("")
  const [ phone, setPhone ] = useState("")
  const [ correo, setCorreo ] = useState("")
  const [ animatedComponents, setAnimatedComponents ] = useState(makeAnimated)

  ///
  const [ dataCon, setDataCon ] = useState("")
  const [config, setConfig] = useState([
    {
    value:'1',
    label:'Datos conjunto',
    },
    {
    value:'2',
    label:'Datos administrador',
    },
    {
    value:'3',
    label:'Datos consejo administrativo ',
    },
]);


  useEffect(()=>{
    obtainInfo();
  },[])

  const obtainInfo = async () => {
    axios.get(ip+'admon/obtain_admin').then(response=>{
      response.data.map(item=>{
        setId(item.id)
        setNameAdmin(item.nombre)
        setUrlImg(ip+item.firma);
        setPhone(item.telefono);
        setCorreo(item.correo)

      })

  })
}

const loadImage = (event) => {
  const file = event.target.files[0];
  if (file.type == "image/png" || file.type == "image/jpg" || file.type == "image/jpeg"){
    if (file.size <= 10000000) {
      new Compressor(file, {
        quality: 0.6,

        success(result) {
          var file = new File ([result], result.name, {type:result.type});
          setImg(file)
          let reader = new FileReader();
          reader.onload = e => {
            setUrlImg(e.target.result)
          };
          reader.readAsDataURL(file);
        }
      })
    }
    else{
      MessageError("Imagen muy pesada");
    }
  }
  else{
    MessageError("Formato no válido: Recuerda que debe ser un formato PNG, JPG O JPEG");
  }

}

const validateEmail = async (data) => {
  var regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  console.log("validacion email");
  console.log(regex.test(data));
  return regex.test(data);
}

const MessageError = async (data) => {
  Swal.fire({
    title: 'Error',
    text: data,
    icon: 'warning',
  })
}
const MessageSuccess = async (data) => {
  Swal.fire({
    text: data,
    icon: 'success',
  })
}

const save_data = async () => {

  var email = await validateEmail(correo);
  if (nameAdmin == "") {
    MessageError("El Nombre del administrador no puede estar vacío")
  }

  else if (urlImg == "") {
    MessageError("Debes ajuntar una imagen de firma")
  }
  else if (phone.length >= 11 ){
    MessageError("El telefono no puede tener mas de 11 digitos")
  }
  else if (email == false ){
    MessageError("el correo es incorrecto o esta vacío")
  }
  else{
    const data = new FormData()
    data.append('id', id)
    data.append('nombre', nameAdmin)
    data.append('firma', img)
    data.append('telefono', phone)
    data.append('correo', correo)
    axios.post(ip+'admon/guard_datadmin',data).then(response=>{

      if (id == null) {
        MessageSuccess("Datos del administrador creados correctamente")
        obtainInfo()
      }
      else{
        MessageSuccess("Editado correctamente")
        obtainInfo()
      }
      })
    }
  }
  const propHeader = (value) => {
    console.log("entro",value);
    setMarginVal(value)
  }

  const Configuracion = (data) =>{


    if(data.value == 1){
      const data ={
        value:'1',
        label:'Datos conjunto'
      }
      setDataCon(data)
      setShowForm(2)
    }
    else if (data.value == 2) {
      const data ={
        value:'2',
        label:'Datos administrador'
      }
      setDataCon(data)
      setShowForm(3)

    }
    else if (data.value == 3) {
      const data ={
        value:'3',
        label:'Datos consejo administrativo'
      }
      setDataCon(data)
      setShowForm(4)

    }

  }

  return(
  <div className="content-wrapper " style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}}>
    <Header section={2}  marginval = {(value)=>propHeader(value)}/>
    <div className="verticar-footer">
      {
        showForm == 2 ?
        <img className="imagen" src={ip+'images/footer.png'}/>
        :
        <div>
        </div>
      }

    </div>
    <div>
      <div className="col-12">
        <div className="col-12 " style={{marginLeft: '60px'}}>
          <strong><h6 className="mt-4 ruta" style={{marginLeft: '-25px'}}>/ <a href = {ip+'admon/conjunto'}>Módulo conjunto </a>/ Datos del Conjunto</h6></strong>
          <div style={{marginTop:'40px', marginLeft: '-36px'}}>
            <p className="m-2">Seleccione el dato del conjunto que desea configurar </p>
            <Select
            className="col-md-12 select3"
            value={dataCon}
            components={animatedComponents}
            options={config}
            onChange={(e)=>Configuracion(e)}
            placeholder = "Seleccione los datos a elegir"
            />
          </div>
        {
          showForm == 1 ?
          <div>

          </div>

        :showForm == 2 ?
          <div className="" style={{marginLeft: '5px'}}>
            <Data id={id}  data={data} />
          </div>

        : showForm == 3 ?

        <div className="hola " style={{marginLeft:'70px'}}>
        <div className="my-2 d-flex col-md-9" style={{marginLeft:'-101px'}}>
          <p className="my-2 mr-3">Datos del administrador</p>
        </div>
        <div className="d-flex"  style={{marginLeft:'-94px'}}>
        <div className="col-md-6">
                <div className=" d-flex justify-content-center align-items-center mt-2 " style={{marginRight:'73%'}}>
                  <label htmlFor="activa">
                    {
                      urlImg == null ?
                      <div className="material-icons border radius-10 muestra">
                        <div className="img-foto">camera_alt</div>
                        </div>
                        :<img style = {{
                          maxWidth: 100,
                          maxHeight: 100
                        }} src = {urlImg}/>
                      }


                    </label>

                  </div>
                  <div className="justify-Content-center flie">
                    <input onChange = {(e)=>loadImage(e)} className="file" id = "activa" type="file"/>
                  </div>
                  <label className="text-start">Firma administrador</label>

                </div>
              </div>
            <div className = "col-md-6 d-flex">
                <label className = "d-flex col-6 color mr-3" style={{width: '200px', marginLeft: '-100px'}}>Administrador:</label>
                <input className="form-control col-9" type="text" style={{width: '388px'}} value = {nameAdmin} placeholder="Nombre administrador" onChange = {(e)=>setNameAdmin(e.target.value)} />
            </div>
            <div className = "col-md-6 d-flex">
                <label className = "d-flex col-6 color mr-3" style={{width: '200px', marginLeft: '-100px'}}>Teléfono:</label>
                <input className="form-control col-9" type="text" style={{width: '388px'}} value = {phone} placeholder="Telefono admin" onChange = {(e)=>setPhone(e.target.value)} />
            </div>


            <div className = "col-md-6 d-flex">
                <label className = "d-flex col-6 color mr-3" style={{width: '200px', marginLeft: '-100px'}}>Correo administrador:</label>
                <input className="form-control col-9" type="text" style={{width: '388px'}} value = {correo} placeholder="correo admin" onChange = {(e)=>setCorreo(e.target.value)} />
            </div>

            <div className="col-md-4 justify-content-end mt-1" style = {{marginLeft:'36%'}}>
              <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}} onClick = {()=>save_data()}>Guardar</button>
            </div>

        </div>

        : showForm == 4 ?
        <div className="" style={{marginLeft: '5px'}}>
          <DataConsejo />
        </div>

        :null

        }
        </div>
      </div>
    </div>

</div>
);

}
export default DatosConjunto;
if (document.getElementById('DatosConjunto')) {
  ReactDOM.render(<DatosConjunto />, document.getElementById('DatosConjunto'))
}
