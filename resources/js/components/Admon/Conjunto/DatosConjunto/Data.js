import React, { useState, useEffect } from 'react';
import Select from 'react-select'
import makeAnimated from 'react-select/animated'
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import axios from 'axios'
import Swal from 'sweetalert2';
import Compressor from 'compressorjs';



function Data (props) {
  const [ id, setId ] = useState(null)
  const [ name, setName ] = useState("")
  const [ urlImg, setUrlImg ] = useState(null)
  const [ img, setImg ] = useState({})
  const [ direccion, setDireccion ] = useState("")
  const [ telefono, setTelefono ] = useState("")

  const [ departamentlist, setDepartamentlist ] = useState(null)
  const [ departamentSelect,  setDepartamentSelect ] = useState([])
  const [ municipiolist, setMunicipiolist ] = useState(null)
  const [ municipioSelect,  setMunicipioSelect ] = useState([])
  const [ animatedComponents, setAnimatedComponents ] = useState(makeAnimated)
  const [ loadingMuncipio, setLoadingMuncipio ] = useState(false)

  const [ muni, setMuni ] = useState(null)
  const [ depa, setDepa ] = useState(null)

  const [ pais, setPais ] = useState("")
  const [ moneda, setMoneda ] = useState(null)
  const [ monedaSelect, setMonedaSelect ] = useState([]);


  useEffect(() => {

    obtainInfo()
    console.log("compila");
    obtain_departamento();
    obtain_moneda();
  }, []);

  const obtainInfo = async () => {
    axios.get(ip+'admon/obtain_conjunto').then(response=>{
      var res = response.data
      console.log(res);

      res.map(item=>{
        setId(item.id)
        setName(item.nombre)
        setDireccion(item.direccion)
        setTelefono(item.telefono)
        //setMoneda(item.moneda)
        setPais(item.pais)
        setUrlImg(ip+item.logo)
        obtain_departamento(item.departamento, item.municipio)
        obtain_moneda(item.moneda)
      })

  })
}
console.log(moneda);

  const obtain_moneda = async (data) => {
    axios.get(ip+'admon/obtain_moneda').then(response=>{
      console.log("moneda entro")
      console.log(response);
      var res = response.data;
      var moneda = []
      var filtrado = res.filter(e=>e.id);
      console.log("filtrado moneda")
      console.log(filtrado);
      filtrado.map(item=>{
        const data = {
          value:item.id,
          label:item.codigo + " - "  + item.nombre
        }
        moneda.push(data)
      })
      setMonedaSelect(moneda);

        var filterMoneda = res.filter(e=>e.id == data)
        console.log("aqui filterMoneda")
        console.log(filterMoneda)
        filterMoneda.map(item=>{
          const datamon = {
            value:item.id,
            label:item.codigo + " - "  + item.nombre
          }
          setMoneda(datamon)
        })


    })
  }



  const obtain_departamento = async (valor, otro) => {
    axios.get(ip+'admon/departament').then(response=>{
      var res = response.data
      var departamento = [];
      res.map(item=>{
        const data = {
          value:item.id_departamento,
          label:item.departamento
        }
        departamento.push(data)
        setDepartamentlist(departamento)
      })
      console.log(depa);
      var filtro = res.filter(e=>e.id_departamento == valor );
      var depar = []
      filtro.map(item=>{
        const data = {
          value:item.id_departamento,
          label:item.departamento
        }
        obtain_municipio(item.id_departamento, false, otro)
        depar.push(data)
        setDepartamentSelect(data)
      })

    })
  }
  const obtain_municipio = async (data, value, otro) => {
        setMunicipioSelect([])
        axios.get(ip+'admon/municipio').then(response=>{
          var res = response.data
          var municipio =[]
          var filtro = response.data.filter(e=>e.departamento_id == data);
          var filtro2 = filtro.filter(e=>e.id_municipio == otro);
          filtro.map(item=>{
            const data = {
              value:item.id_municipio,
              label:item.municipio
            }
            municipio.push(data)
          })
          setMunicipiolist(municipio)

          filtro2.map(item=>{
            const data = {
              value:item.id_municipio,
              label:item.municipio
            }
            setMunicipioSelect(data)
          })

        })
  }

  const departame = (value)=>{
    console.log(value.value);
    setDepartamentSelect(value);
    obtain_municipio(value.value, true)
  }

  const loadImage = (event) => {
    const file = event.target.files[0];
    if (file.type == "image/png" || file.type == "image/jpg" || file.type == "image/jpeg"){
      if (file.size <= 10000000) {
        new Compressor(file, {
          quality: 0.6,

          success(result) {
            var file = new File ([result], result.name, {type:result.type});
            setImg(file)
            let reader = new FileReader();
            reader.onload = e => {
              setUrlImg(e.target.result)
            };
            reader.readAsDataURL(file);
          }
        })
      }
      else{
        MessageError("Imagen muy pesada");
      }
    }
    else{
      MessageError("Formato no válido: Recuerda que debe ser un formato PNG, JPG O JPEG");
    }

  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }

  const save_data = async () => {

    if (name == "") {
      MessageError("El nombre del conjunto no puede estar vacío")
    }
    else if (pais == "") {
      MessageError("Debes ingresar país")
    }
    else if (urlImg == null) {
      MessageError("El del logo no puede estar vacío")
    }
    else if (direccion == "") {
      MessageError("El campo de direccion esta vacío")
    }
    else if (departamentlist == null ){
      MessageError("El campo departamento esta vacío")
    }
    else if (municipiolist == null ){
      MessageError("El campo municipio esta vacío")
    }
    else{
      const data = new FormData()
      data.append('id', id)
      data.append('nombre', name)
      data.append('pais', pais)
      data.append('moneda', moneda.value == null ? 0 : moneda.value)
      data.append('file', img)
      data.append('direccion', direccion)
      data.append('telefono', telefono)
      data.append('departamento', departamentSelect.value)
      data.append('municipio', municipioSelect.value)
      axios.post(ip+'admon/guard_conjunto',data).then(response=>{

        if (id == null) {
          MessageSuccess("Conjunto creado correctamente")
          obtainInfo()
        }
        else{
          MessageSuccess("Editado correctamente")
          obtainInfo()
        }
        })
      }
    }
return(
    <div className="hola  " style={{marginLeft:'63px'}}>
    <div className="my-2 d-flex col-md-9" style={{marginLeft:'-101px'}}>
      <p className="my-2 mr-3">Digite los datos del conjunto: </p>
    </div>
    <div className="d-flex" style={{marginLeft:'-94px'}}>
    <div className="col-md-6">
      <div className=" d-flex justify-content-center align-items-center mt-2 " style={{marginRight:'73%'}}>
        <label htmlFor="activa">
          {
            urlImg == null ?
            <div className="material-icons border radius-10 muestra">
              <div className="img-foto">camera_alt</div>
              </div>
              :<img style = {{
                maxWidth: 100,
                maxHeight: 100
              }} src = {urlImg}/>
            }
          </label>
        </div>
        <div className="justify-Content-center flie">
          <input onChange = {(e)=>loadImage(e)}className="file" id = "activa" type="file"/>
        </div>
        <label className="text-center">Logo conjunto</label>
      </div>
    </div>
        <div className = "col-md-6 d-flex ">
            <label className = "d-flex col-7 color mr-3" style={{width: 'max-content', marginLeft: '-100px'}}>Nombre del conjunto:</label>
            <input className="form-control col-9" type="text" style={{width: '387px'}} value = {name} placeholder="Nombre del conjunto" onChange = {(e)=>setName(e.target.value)} />
        </div>
        {
          id > 0 ?

          <div>
            <div className = "col-md-6 d-flex">
              <label className = "d-flex col-6 color mr-3" style={{width: '180px', marginLeft: '-100px'}}>País:</label>
              <input className="form-control col-9" disabled type="text" style={{width: '387px'}} value = {pais} placeholder="Escribe el país" onChange = {(e)=>setPais(e.target.value)} />
            </div>
          </div>
          :
          <div>
            <div className = "col-md-6 d-flex">
              <label className = "d-flex col-6 color mr-3" style={{width: '180px', marginLeft: '-100px'}}>País:</label>
              <input className="form-control col-9" type="text" style={{width: '387px'}} value = {pais} placeholder="Escribe el país" onChange = {(e)=>setPais(e.target.value)} />
            </div>
          </div>
        }

          <div className="col-md-7 d-flex">
            <label className="d-flex col-5 mr-2 align-items-center color" style={{width: '181px', marginLeft: '-100px'}}>Departamento: </label>
            <Select
            className="col-md-8 select2"
            value={departamentSelect}
            components={animatedComponents}
            options={departamentlist}
            onChange={(e)=>departame(e)}
            placeholder = "Seleccione el departamento"
            />
          </div>

          <div className="col-md-7 d-flex">
            <label className="d-flex col-5 mr-2 align-items-center color" style={{width: '181px', marginLeft: '-100px'}}>Ciudad: </label>
            <Select
              className="col-md-8 select"
              value={municipioSelect}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={municipiolist}
              onChange={(e)=>setMunicipioSelect(e)}
              placeholder = "Seleccione el municipio"
              name="colors"
              />
          </div>
          <div className = "col-md-6 d-flex">
              <label className = "d-flex col-6 color mr-3" style={{width: '180px', marginLeft: '-100px'}}>Dirección:</label>
              <input className="form-control col-9" type="text" style={{width: '387px'}} value = {direccion} placeholder="Dirección" onChange = {(e)=>setDireccion(e.target.value)} />
          </div>
          <div className = "col-md-6 d-flex">
              <label className = "d-flex col-6 color mr-3" style={{width: '180px', marginLeft: '-100px'}}>Teléfono:</label>
              <input className="form-control col-9" type="number" style={{width: '387px'}} value = {telefono} placeholder="Numero de telefono" onChange = {(e)=>setTelefono(e.target.value)} />
          </div>
          <div className="col-md-7 d-flex">
            <label className="d-flex col-5 mr-2 align-items-center color" style={{width: '181px', marginLeft: '-100px'}}>Moneda: </label>
            <Select
            className="col-md-8 select2"
            value={moneda}
            components={animatedComponents}
            options={monedaSelect}
            onChange={(e)=>setMoneda(e)}
            placeholder = "Seleccione la moneda"
            />
          </div>
            <div className="col-md-4 justify-content-end " style = {{marginLeft:'35%'}}>
              <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}} onClick = {()=>save_data()}>Guardar</button>
            </div>
      </div>
  )
}
export default Data;
if (document.getElementById('Data')) {
  ReactDOM.render(<Data />, document.getElementById('Data'))
}
