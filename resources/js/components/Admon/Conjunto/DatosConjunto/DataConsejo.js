
import React, { useState, useEffect } from 'react';
import Select from 'react-select'
import makeAnimated from 'react-select/animated'
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import axios from 'axios'
import Swal from 'sweetalert2';
import * as moment from 'moment';
import Compressor from 'compressorjs';
import { Tooltip, Button, Grid  } from '@material-ui/core';
import Pagination from '../../../pagination/Pagination'
import PaginationButton from '../../../pagination/Pagination-button'



function DataConsejo (props) {
  const [ id, setId ] = useState(null)
  const [ fechaIni, setFechaIni ] = useState("")
  const [ fechaFinal, setFechaFinal ] = useState("")
  const [ lider, setLider ] = useState("")
  const [ telefono, setTelefono ] = useState("")
  const [ cargo, setCargo ] = useState([])
  const [ estado, setEstado ] = useState(1)
  const [ showForm, setShowForm ] = useState(1)
  const [ valor, setValor ] = useState({
    cargo: '',
    nombre:'',
    telefono:'',
    correo:''
  })
  const [ lista, setLista ] = useState([])

  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [backCargo, setBackCargo] = useState([])
  const [backCargo2, setBackCargo2] = useState([])

  const [fecha1, setFecha1] = useState('');
  const [fecha2, setFecha2] = useState('');


  useEffect(() => {
    datos()
  }, []);




const datos = async () => {
  axios.get(ip+'admon/obtain_cargos').then(response=>{
  setCargo(response.data)
  setBackCargo(response.data)
  setBackCargo2(response.data)
  const dataNew = Pagination.paginate(response.data,currentPage,postsPerPage)
  setCargo(dataNew)

})
}


  const updateCurrentPage = async (number) => {
  await setPerson([])
  await setCurrentPage(number)
  const dataNew = Pagination.paginate(backTerce,number,postsPerPage)
  await setPerson(dataNew)
  await setFiltro(dataNew)
}



  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }

  const save_data = async () => {
    var message = ''
    var error = false

    if (fechaIni == null || fechaIni == "") {
      error = true
      message = "Debes agregar la fecha inicial"
    }
    else if (fechaFinal == null || fechaFinal == "") {
      error = true
      message = "Debes agregar la fecha final"
    }
    else if (lista.length < 0  || lista == null || lista == ''){
      error = true
      message = "Debes agregar un cargo"
    }
    else if (valor == null || valor == ''){
      error = true
      message = "Debes agregar un cargo"
    }

    if (error) {
      MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('id', id)
      data.append('fechaini', fechaIni)
      data.append('fechafin', fechaFinal)
      data.append('lider', lider)
      data.append('telefono', telefono)
      data.append('cargo', JSON.stringify(lista))
      axios.post(ip+'admon/guard_consejo',data).then(response=>{
        if (id == null) {
          MessageSuccess("Cargos creados correctamente")
            setShowForm(1)
            datos();
            setValor({
              cargo: '',
              nombre:'',
              telefono:'',
              correo: ''
            })
            setLista([])
            setFechaIni("")
            setFechaFinal("")
        }
        else{
          MessageSuccess("Editado correctamente")
            datos();
            setValor({
              cargo: '',
              nombre:'',
              telefono:'',
              correo: ''
            })
            setLista([])
            setFechaIni("")
            setFechaFinal("")

        }
        })
      }
    }

    const validateEmail = async (data) => {
      var regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      console.log("validacion email");
      console.log(regex.test(data));
      return regex.test(data);
    }

    const onClickAddContable = async () =>{
      var email = await validateEmail(valor.correo);
      console.log(email);
      console.log("entrooo");
      if(valor.cargo == ""){
        MessageError("Debes llenar campo de cargo")
      }
      else if (valor.nombre == "") {
        MessageError("Debes llenar campo de nombre")
      }
      else if (valor.telefono == "") {
        MessageError("Debes llenar campo telefono")
      }
      else if (email == false) {
        MessageError("El correo no es valido o esta vacio")
      }
      else {
        const list = lista
        list.push(valor)
        setLista(list)
        setValor({
          cargo: '',
          nombre:'',
          telefono:'',
          correo: ''
        })

      }

    }

    const deshabilitar = (value) =>{

          var message = '¿Quieres eliminar este cargo?'
          Swal.fire({
          title: message,
          background:'#FFEDA6',
          showDenyButton: true,
          confirmButtonColor:'#FFDE59',
          denyButtonColor:'#FFDE59',
          confirmButtonText: `Sí`,
          denyButtonText: `No`,
          customClass: {
          popup: 'border-radius-0'
      }
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            const data = new FormData()
            data.append('id', value.id)
            axios.post(ip+'admon/deleted_cargos',data).then(response=>{
              if(response.data == 1){
                Swal.fire('Eliminado correctamente!', '', 'success')
                datos();
              }

            })

          } else if (result.isDenied) {
              datos();
            Swal.fire({
              background:'#FFEDA6',
              title:'Accion cancelada',
            })
          }
        })

    }


    const dataFecha = async () => {
      console.log(fecha1);
      console.log(fecha2);
      var fecha1back = moment(fecha1, 'YYYY-MM-DD')
      var fecha2back = moment(fecha2, 'YYYY-MM-DD')
      console.log(fecha1back);
      console.log(fecha2back);
      var arrayTopush = []

      backCargo2.map(item=>{
      var fecha_creacion2 = moment(item.fechaini, 'YYYY-MM-DD')
      var fecha_creacion = moment(item.fechafin, 'YYYY-MM-DD')

      console.log(fecha_creacion2);
      var fecha_1 = fecha1back.diff(fecha_creacion2, 'minutes');
      var fecha_2 = fecha2back.diff(fecha_creacion, 'minutes');
      if (fecha_1 <= 0 && fecha_2 >= 0) {
        arrayTopush.push(item)
      }

      })
      console.log(arrayTopush);
      await setCurrentPage(1)
      await setBackCargo(arrayTopush)
      const dataNew = Pagination.paginate(arrayTopush,1,postsPerPage)
      await setCargo(dataNew)

    }

  return(
    <div className="hola  " style={{marginLeft:'63px'}}>

{
  showForm == 1 ?
      <div className="col-6" style={{marginLeft:'-105px'}}>
        <div className="col-md-8 mt-4 d-flex ">
        <label className="d-flex mr-3 col-4 align-items-center color2">De: </label>
        <input className="form-control mr-3 col-6 d-flex" type="date" onChange = {(e)=>setFecha1(e.target.value)}/*value={fecha} onChange={(event)=>setFecha(event.target.value)}*/ />

        <label className="d-flex mr-3 col-5 align-items-center color2">Hasta: </label>
        <input className="form-control col-6 d-flex" type="date" onChange = {(e)=>setFecha2(e.target.value)} /*value={fecha} onChange={(event)=>setFecha(event.target.value)}*/ />

        <div className="col-4 mb-3">
          <button className="btn" style={{backgroundColor:'#FFDE59', color:'black', width: '46px', fontWeight: "bold"}} onClick={()=>dataFecha()}><span className = "material-icons align-items-center justify-content-between" >filter_alt</span></button>
        </div>
        <div className="col-4 mb-3" style={{ marginLeft: '-65px', marginTop:' -1px'}}>
          <Tooltip title="Agregar consejo" placement="right">
             <Button>
               <span class="material-icons align-items-center" onClick={()=>setShowForm(2)} >
                 add_circle
               </span>
             </Button>
           </Tooltip>

        </div>
        </div>
      <table className="table table-striped" style={{marginLeft: '9px'}}>
          <thead style={{backgroundColor: '#FFDE59' }}>
            <tr>
                <th>Cargo</th>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Correo</th>
                <th></th>

            </tr>
          </thead>
          <tbody>
            {
              cargo.map(item=>{
                return(
                  <tr>
                    <td>{item.cargo}</td>
                    <td>{item.nombre}</td>
                    <td>{item.telefono}</td>
                    <td>{item.correo}</td>
                    <td>
                      <span class="material-icons align-items-center" onClick={()=>deshabilitar(item)} style={{cursor:'pointer'}} >
                      remove_circle
                      </span>
                    </td>

                  </tr>
                )

              })
              }
          </tbody>
        </table>
        <div className="d-flex col-md-12 col-12 justify-content-end">
          <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backCargo.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
        </div>
      </div>
      :showForm == 2 ?
      <div>
        <p className="mt-3" style={{marginLeft: '-88px'}}>Seleccione el periodo de consejo</p>
        <div className = "col-md-6 d-flex">
            <label className = "d-flex col-6 color mr-3" style={{width: '180px', marginLeft: '-100px'}}>Fecha inicial: </label>
            <input className="form-control col-12" type="date" style={{width: '261px'}} value = {fechaIni} placeholder="Fecha final" onChange = {(e)=>setFechaIni(e.target.value)} />
        </div>
          <div className = "col-md-6 d-flex">
              <label className = "d-flex col-6 color mr-3" style={{width: '180px', marginLeft: '-100px'}}>Fecha final:</label>
              <input className="form-control col-12" type="date" style={{width: '261px'}} value = {fechaFinal} placeholder="Fecha inicial" onChange = {(e)=>setFechaFinal(e.target.value)} />
          </div>




  <div>
    <p className="mt-3" style={{marginLeft: '-88px'}}>Miembros del consejo administrativo</p>

    <table style={{marginLeft: '-92px'}}>
        <thead style={{backgroundColor: '#d9d9d9', }}>
          <tr>
              <th>Cargo</th>
              <th>Nombre</th>
              <th>Teléfono</th>
              <th>Correo</th>
          </tr>
        </thead>
        <tbody>

                <tr>
                  <td>
                    <input className="form-control col-11" type="text" style={{width: '148px'}} value={valor.cargo}  name = "cargo" placeholder="Cargo"  onChange={(e)=>setValor({ ...valor, [e.target.name]: e.target.value })}/>
                  </td>
                  <td>
                    <input className="form-control col-11" type="text" style={{width: '134px'}} value={valor.nombre}  name = "nombre" placeholder="Nombre"  onChange={(e)=>setValor({ ...valor, [e.target.name]: e.target.value })}/>

                  </td>
                  <td>
                    <input className="form-control col-11" type="number" style={{width: '164px'}} value={valor.telefono}  name = "telefono"  placeholder="Número de teléfono" onChange={(e)=>setValor({ ...valor, [e.target.name]: e.target.value })} />

                  </td>
                  <td>
                    <input className="form-control col-11" type="text" style={{width: '164px'}} value={valor.correo}  name = "correo"  placeholder="Correo" onChange={(e)=>setValor({ ...valor, [e.target.name]: e.target.value })} />
                  </td>
                  <td>
                    <img onClick={()=>onClickAddContable()} src = {ip+'images/maxx.png'} style={{width:'26px'}} />
                  </td>
                </tr>

            {
              lista.map(item=>{
                return(
                  <tr>
                    <td>
                      <input className="form-control col-11" disabled type="text" style={{width: '148px'}} value={item.cargo}  name = "cargo" placeholder="Cargo"  />
                    </td>
                    <td>
                      <input className="form-control col-11" disabled type="text" style={{width: '134px'}} value={item.nombre}  name = "nombre" placeholder="Nombre"  />

                    </td>
                    <td>
                      <input className="form-control col-11" disabled type="number" style={{width: '164px'}} value={item.telefono}  name = "telefono"  placeholder="Número de teléfono" />

                    </td>
                    <td>
                      <input className="form-control col-11" disabled type="text" style={{width: '164px'}} value={item.correo}  name = "Correo"  placeholder="Correo"  />
                    </td>
                  </tr>
                )
              })
            }





        </tbody>
      </table>

    </div>
  </div>
    :null
}

        <div className = "contenedor d-flex">

        <div className="col-5 mr-4 mt-4" style={{marginLeft: '-100px'}}>

        </div>

        {

          estado == 3 ?
          <div className="col-12 mr-3" style={{marginLeft: '-9px'}}>

                  {
                    cargo.map(item=>{
                      return(
                        <div className="d-flex">
                          <div className = "col-md-6 d-flex">
                              <label className = "d-flex col-4 color mr-3" style={{width: '145px', marginLeft: '-100px'}}>Cargo: </label>
                              <input className="form-control col-7" disabled value = {item.cargo} type="text" style={{width: '148px'}} name = "nombre" placeholder="Asignar cargo"  />
                          </div>
                          <div className = "col-md-4 d-flex">
                              <label className = "d-flex col-4 color mr-3" style={{width: '152px', marginLeft: '-344px'}}>Nombre: </label>
                              <input className="form-control col-7" disabled type="text" value = {item.nombre} style={{width: '134px'}} name = "nombre" placeholder="Nombre" />
                          </div>
                          <div className = "col-md-6 d-flex">
                              <label className = "d-flex col-6 color mr-3" style={{width: '100px', marginLeft: '-433px'}}>Teléfono:</label>
                              <input className="form-control col-5" disabled type="number" value = {item.telefono} style={{width: '164px'}} name = "telefono"  placeholder="Numero de telefono"  />
                          </div>
                            <div style={{marginLeft:'71%',marginTop:'4px', position: 'absolute'}}>
                              <img onClick={()=>deshabilitar(item)}  src = {ip+'images/Quitar.png'} style={{width:'26px'}} />
                            </div>
                          </div>
                      )

                    })
                    }

          </div>


          :null
        }
        {
          showForm == 2 ?
          <div>
          <div className="col-md-4 mt-5 justify-content-end " style = {{marginLeft:'4%'}}>
            <button className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black', marginLeft:'-65px', marginBottom: '-56px'}} onClick = {()=>setShowForm(1)}><i className="material-icons icon-btn"> arrow_back_ios</i></button>
            <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}} onClick = {()=>save_data()}>Guardar</button>
          </div>
          </div>

          :null

        }


      </div>

          </div>
  )

}

export default DataConsejo;
if (document.getElementById('DataConsejo')) {
  ReactDOM.render(<Data />, document.getElementById('DataConsejo'))
}
