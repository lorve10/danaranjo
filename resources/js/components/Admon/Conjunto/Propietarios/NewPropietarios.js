import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import Select from 'react-select'
import {ip} from '../../../ApiRest';
import Swal from 'sweetalert2'
import '../../../../../css/app.scss';
import makeAnimated from 'react-select/animated'
import { InsertEmoticonSharp } from '@material-ui/icons';
import Compressor from 'compressorjs';
import { Tooltip } from '@material-ui/core';


function NewPropietarios (props) {
    const [showForm, setShowForm] = useState(0)
    const [loading, setLoading] = useState(false);
    const [text, setText] = useState('');
    const [numeroDoc, setNumeroDoc] = useState('');///
    const [expedition, setExpedition] = useState('');///
    const [name, setName] = useState('');//
    const [seconame, setSecoName] = useState('');//
    const [lastname, setLastname] = useState('');//
    const [seclastname, setSeclastname] = useState('');//
    const [addres, setAddres] = useState('')//
    const [city, setCity] = useState('')//
    const [phone, setPhone] = useState('')//telefono
    const [phone2, setPhone2] = useState('')//celular
    const [file, setFile] = useState({})//Documento certificado
    const [email, setEmail] = useState('')//correo
    const [sigla, setSigla] = useState('')
    const [type, setType] = useState(null);//tipo persona juridica o Natural
    const [propietario, setPropietario] = useState(null)//tipo de Arrendatario
    const [documento, setDocumento] = useState(null);//
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
    const [vehiculo, setVehiculo] = useState(0);
    const [marca, setMarca] = useState(null);
    const [placa, setPlaca] = useState(null);
    const [color, setColor] = useState(null);
    const [profesion, setProfesion] = useState('');
    const [estadoCivil, setEstadoCivil] = useState(null);
    const [numeroRes, setNumeroRes] = useState('');
    const [numeroPer, setNumeroPer] = useState('');
    const [img, setImg] = useState({})//Foto de la persona
    const [urlImg, setUrlImg] = useState(null)
    const [arrend, setArrend] = useState(0);
    const [ variable, setVariable ] = useState(false)
    const [estado, setEstado] = useState(1);
    const [arrendatario, setArrendatario] = useState('');
    const [propietarios, setPropietarios] = useState(null);
    const [propietariosList, setPropietariosList] = useState([]);

    const [docuList, setDocuList] = useState([
      {
    value:'1',
    label:'Cédula',
    },
    {
    value:'2',
    label:'Cédula Extranjera',
    },
    {
    value:'3',
    label:'Pasaporte ',
    },
    {
    value:'4',
    label:'otros ',
    }

  ]);
    const [propietarioList, setPropietarioList] = useState([
    {
    value:'1',
    label:'Propietario',
    },
    {
    value:'2',
    label:'Arrendatario',
    },
  ]);

    const [vehiculoList, setVehiculoList] = useState([
    {
    value:'3',
    label:'No tiene',
    },
    {
    value:'1',
    label:'Carro',
    },
    {
    value:'2',
    label:'Moto',
    }

  ]);

  const [estadoCivilList, setEstadoCivilList] = useState([
    {
    value:'1',
    label:'Soltero/a',
    },
    {
    value:'2',
    label:'Casado/a',
    },
    {
    value:'3',
    label:'Unión libre o unión de hecho',
    },
    {
    value:'4',
    label:'Separado/a',
    },
    {
    value:'5',
    label:'Divorciado/a',
    },
    {
    value:'6',
    label:'Viudo/a',
    }
  ]);

  const [departamentlist, setDepartamentlist] = useState(null);
  const [departamentSelect,  setDepartamentSelect] = useState([]);
  const [municipiolist, setMunicipiolist] = useState(null);
  const [municipioSelect,  setMunicipioSelect] = useState([]);
  const [unidadres, setUnidadres] = useState(null);
  const [unidadVive, setUnidadVive]  = useState(null);
  const [unidadResSelect, setUnidadResselect] = useState([]);
  const [inmueble, setInmueble] = useState(null);
  const [inmuebleSelect, setInmuebleSelect] = useState([]);
  const [mascota, setMascota] = useState(null);


  const [ uniArrend, setUniArrend ] = useState(null)///variable para el showForm 2
  const [ uniArrendSelect, setUniArrendSelect ] = useState([])///variable para el showForm 2

  const [ otraUni, setOtraUni ] = useState(null)///variable para el showForm 2
  const [ otraUniSelect, setOtraUniSelect ] = useState([])///variable para el showForm 2

  const [ proy, setProy ] = useState(null)
  const [ proy2, setProy2 ] = useState([])


  useEffect(()=>{
    obtain_departamento();
    obtain_tipo();
    obtain_pro();
    validar();
    obtain_unidad();

  },[]);



//// este al momento de editar se ejecuta
console.log("aqui props tipo propietario");
console.log(props.data.tip_prop);
  const validar = async () =>{
    if(props.id > 0 ){
    if(props.data.tip_prop == 1){
      setShowForm(1)
      setPropietario({
          value:'1',
          label:'Propietario'
        })
    }
    if(props.data.tip_prop == 2){
      setShowForm(2)
      setPropietario({
          value:'2',
          label:'Arrendatario'
        })
    }

    if(props.data.tip_ident == 1){
      setDocumento({
          value:'1',
          label:'Cedula'
        })
    }
    if(props.data.tip_ident == 2){
      setDocumento({
          value:'2',
          label:'Cedula Extranjera'
        })
    }
    if(props.data.tip_ident == 3){
      setDocumento({
          value:'3',
          label:'Pasaporte '
        })
    }
    if(props.data.tip_ident == 4){
      setDocumento({
          value:'4',
          label:'otros'
        })
    }
    if(props.data.vehiculo == 1){
      setVehiculo({
          value:'1',
          label:'Carro'
        })
    }
    if(props.data.vehiculo == 2){
      setVehiculo({
          value:'2',
          label:'Moto'
        })
    }
    if(props.data.vehiculo == 3){
      setVehiculo({
          value:'3',
          label:'No tiene'
        })
    }
    if(props.data.estado_civil == 1){
      setEstadoCivil({
          value:'1',
          label:'Soltero/a',
        })
    }
    else if(props.data.estado_civil == 2){
      setEstadoCivil({
          value:'2',
          label:'Casado/a',
        })
    }
    else if(props.data.estado_civil == 3){
      setEstadoCivil({
        value:'3',
        label:'Unión libre o unión de hecho',
        })
    }
    else if(props.data.estado_civil== 4){
      setEstadoCivil({
        value:'4',
        label:'Separado/a',
        })
    }
    else if(props.data.estado_civil == 5){
        setEstadoCivil({
          value:'5',
          label:'Divorciado/a',
          })
    }
    else if(props.data.estado_civil == 6){
        setEstadoCivil({
          value:'6',
          label:'Viudo/a',
        })
    }

    if(props.data.arrendatario == 1){
      console.log("entro a validar arrendatario")
      setVariable(true)
      setArrend(props.data.arrendatario)
      setEstado(2)
    }

      setExpedition(props.data.expedido)
      setName(props.data.name)
      setSecoName(props.data.secname == null ? '' : props.data.secname)
      setLastname(props.data.lastname)
      setSeclastname(props.data.seclastname)
      setAddres(props.data.addres)
      setCity(props.data.city)
      setEmail(props.data.correo)
      setPhone(props.data.phone)
      setPhone2(props.data.mobile)
      setNumeroDoc(props.data.n_document)
      setProfesion(props.data.profesion)
      setUnidadres(props.data.torre)
      setNumeroRes(props.data.numero_res)
      setMarca(props.data.marca)
      setPlaca(props.data.placa)
      setColor(props.data.color)
      setNumeroPer(props.data.numero_per)
      setUrlImg(ip+"storage/"+props.data.foto)
      //setFile("/storage/"+props.data.documento)
      //setFile("/storage/"+props.data.name_file)
      //setUrlImg(ip+props.data.foto)
      setUniArrend(props.data.torre)
      setArrendatario(props.data.arrendatario)
      setUnidadVive(props.data.vive)
    }
  }


/// obtener departamentos este aplica solo para formulario propietario
  const obtain_departamento = async () => {
    axios.get(ip+'admon/departament').then(response=>{
      var res = response.data
      var departamento = [];
      res.map(item=>{
        const data = {
          value:item.id_departamento,
          label:item.departamento
        }
        departamento.push(data)
        setDepartamentlist(departamento)
      })
      var filtro = res.filter(e=>e.id_departamento == props.data.departamento);
      var depar = []
      filtro.map(item=>{
        const data = {
          value:item.id_departamento,
          label:item.departamento
        }
        obtain_municipio(item.id_departamento, false)
        depar.push(data)
        setDepartamentSelect(data)
      })

    })
  }

//// obtener municipio dependiendo el departamento tenemos una funcion para esta ---->
  const obtain_municipio = async (data, value) => {
        setMunicipioSelect([])
        axios.get(ip+'admon/municipio').then(response=>{
          var res = response.data
          var municipio =[]
          var filtro = response.data.filter(e=>e.departamento_id == data);
          var filtro2 = filtro.filter(e=>e.id_municipio == props.data.municipio);
          filtro.map(item=>{
            const data = {
              value:item.id_municipio,
              label:item.municipio
            }
            municipio.push(data)
          })
          setMunicipiolist(municipio)

          filtro2.map(item=>{
            const data = {
              value:item.id_municipio,
              label:item.municipio
            }
            setMunicipioSelect(data)
          })

        })
  }


//tipo de unidad recidencia solo aplica para formulario propietario
  const obtain_tipo = () => {
    axios.get(ip+'admon/obtain_tipo').then(response=>{
        var res = response.data;
        var filtro = res.filter(e=>e.id != 1)
        var inmuebles = []
        filtro.map(item=>{
          const datapush = {
            value:item.id,
            label:item.nombre
          }
          inmuebles.push(datapush)
          obtain_unidad(item.id, false)
          setInmueble(inmuebles);

        })
        if(props.id>0){
            var filterInmueble = res.filter(e=>e.id == props.data.tip_inmueble && e.id != 1)
            filterInmueble.map(item=>{
              const datainmu = {
                value:item.id,
                label:item.nombre
              }
              setInmuebleSelect(datainmu)
            })

        }
    })
}

//// Este solo aplica para formulario propietario
  const obtain_unidad = (data, value) => {
    axios.get(ip+'admon/obtain_Inmu').then(response=>{
        var res = response.data;
        var unidad = []
        var otro = []
        var result = []
        console.log(res);
        var filtro = res.filter(e=>e.tipo_inmueble.id == data && e.deleted == 0)
        var filtro2 = filtro.filter(e=>e.estado == 0);
        filtro2.map(item=>{
          const data = {
            value:item.id,
            label:item.tipo_unidad.nombre_res + " - " + item.tipo_inmueble.nombre + " " + item.numero
          }
          unidad.push(data)
        })
          setUnidadResselect(unidad);

          if(value === true){
            console.log("entrooo a ser verdadero");
              setUnidadres("")
              setUnidadVive("")
          }

          if(props.id > 0){
            var filterUnidad = res.filter(e=>e.id == props.data.vive)
            console.log("aquinoooooo");
            console.log(filterUnidad);
              const data = {
                 value:filterUnidad[0].id,
                 label:filterUnidad[0].tipo_unidad.nombre_res+ " - " + filterUnidad[0].tipo_inmueble.nombre + " " + filterUnidad[0].numero
             }

             if(value === true){
              console.log("entrooo a ser verdadero");
              setUnidadVive("")
            }
            else {
              setUnidadVive(data)
            }

           }
          })
   }

///desde aqui empieza para validar en arrendatario  propietario y filtra
  const obtain_pro = async() => {
    axios.get(ip+'admon/obtain_propietarios').then(response=>{
        var res = response.data;
        var propietarios = []
        var filtrado = res.filter(e=>e.tip_prop == 1);
        var filtra2 = filtrado.filter(e=>e.deleted == 0);
        console.log("aqui los propietarios");
        console.log(filtra2);
        filtra2.map(item=>{
            const data = {
                value:item.id,
                label:item.n_document  + " - " + item.name + " " + (item.secname == null ? '':item.secname)+ " " + (item.lastname == null ? '':item.lastname) + " " + (item.seclastname == null ? '':item.seclastname)
            }
            propietarios.push(data)
            obtain_provive(item.id, false)
            obtain_unidad(item.id, false)

        })
        setPropietariosList(propietarios)
        if(props.id > 0){
            var filterpropietarios = res.filter(e=>e.id == props.data.prop_inmu)
            const data = {
                value:filterpropietarios[0].id,
                label:filterpropietarios[0].n_document +" "+filterpropietarios[0].name + " " + (filterpropietarios[0].secname == null ? '':filterpropietarios[0].secname) + " " + (filterpropietarios[0].lastname == null ? '':filterpropietarios[0].lastname) + " " +  (filterpropietarios[0].seclastname == null ? '':filterpropietarios[0].seclastname)
            }
            setPropietarios(data)
        }
    })
  }

// Este filtro muestra la casa o apartamento del propietario Seleccionado
  const obtain_provive = (data, value) => {
    axios.get(ip+'admon/obtain_propietarios').then(response=>{

        var res = response.data;
        var inmuebles = []
        var filtro = res.filter(e=>e.id == data)
        filtro.map(item=>{
          const datapush = {
            value: item.tipo_inmueble.id,
            label: item.tipo_inmueble.nombre
          }
          inmuebles.push(datapush)
          setOtraUniSelect(inmuebles);
          console.log("holaaaaa aqui filtro");
          console.log(filtro);
        })
        if(props.id > 0){
            filtro.map(item=>{
              const datainmu = {
                value:item.tipo_inmueble.id,
                label:item.tipo_inmueble.nombre
              }
              setOtraUni(datainmu)
            })

        }
    })
}

///este muestra las casas es decir numero y torre del propietario Seleccionado ARRENDATARIO
const obtain_unidadVive = (data, value) => {
    axios.get(ip+'admon/obtain_propietarios').then(response=>{
      var res = response.data;
      var unidad = []
      var filtro2 = []
      console.log("aquiiiiii");
      console.log(data);
      var filtro = res.filter(e=>e.id == data)
      console.log("aqui usuario ");
      console.log(filtro);
      unidad = filtro.filter(e=>e.vive !== 0)
      console.log("aqui unidad");
      console.log(unidad);
      unidad.map(item=>{
      var  data = JSON.parse(item.torre)
      var inmu =  data.filter((e,i)=>e.value != i)
      console.log("bacanooooooo");
      console.log(inmu);
      setUniArrendSelect(inmu);
      })

      if(value === true){
        console.log("entrooo a ser verdadero");
          setUniArrend("")
          setOtraUni("")
      }


  })
}



//// funciones para recojer dato de departamento y hacer filtro
  const departame = (value)=>{
      setDepartamentSelect(value);
      obtain_municipio(value.value, true)
  }

// funcion para filtra por persona y unidad que tenga
  const residencias = (value)=>{
      setInmuebleSelect(value);
      obtain_unidad(value.value, true)
  }

///este filtro es de formualario arrendatario
  const propvive = (value)=>{
      setPropietarios(value);
      obtain_provive(value.value, true)
      obtain_unidadVive(value.value, true)
  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }

  ///validacion de correo electronico
  const validateEmail = async (data) => {
    var regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    console.log("validacion email");
    console.log(regex.test(data));
    return regex.test(data);

  }

//// valida tipo de documento solo admite pdf
  const validateDocument = async(oEvent) => {
    const oFile = oEvent.target.files[0];
    console.log(oEvent.target.files[0])
      if(oFile.name.includes('.pdf') || oFile.name.includes('.PDF')){
        //chargeMassive(oEvent);
        setFile(oFile);
      }else{
      MessageError("Solo se permiten archivos PDF");
      setFile({});
    }
  }

//// esto es de una biblioteca esta resive la img o png solo permite un tamaño espesifico
  const loadImage = (event) => {
    const file = event.target.files[0];
    if (file.type == "image/png" || file.type == "image/jpg" || file.type == "image/jpeg"){
      if (file.size <= 10000000) {
        new Compressor(file, {
          quality: 0.6,

          success(result) {
            var file = new File ([result], result.name, {type:result.type});
            setImg(file)
            let reader = new FileReader();
            reader.onload = e => {
              setUrlImg(e.target.result)
            };
            reader.readAsDataURL(file);
          }
        })
      }
      else{
        MessageError("Imagen muy pesada")
        setImg({});
      }
    }
    else{
      MessageError("Formato no válido: Recuerda que debe ser un formato PNG, JPG O JPEG");
      setImg({});
    }

  }

/// función guardar
 const save_per = async () => {
   var message = ''
   var error = false
   var res = await validateEmail(email)
    /// este es validación de propietario
   if(showForm == 1){
     /// estas son variable para no interferir con arrrendatario
     var depart = departamentSelect.value
     var munici = municipioSelect.value
     var tipo_inmueblee = inmuebleSelect.value
     var prop = 0
     console.log("aqui guardando");
     console.log(propietario.value);

     var torreArrende = JSON.stringify(unidadres);


      if(unidadVive == null){
          var vivir = 0
      }
      else {
          var vivir = unidadVive.value
      }

      if(vehiculo == 0){
        var vehicule = 0
      }else {
        var vehicule = vehiculo.value
      }

      if(documento == null) {
        error = true
        message = "Escoge tipo de documento"
      }
      else if(numeroDoc == '') {
        error = true
        message = "Escribe el numero de documento de la persona"
      }
      else if(expedition == '') {
        error = true
        message = "Escribe el lugar de expedición del documento"
      }
      else if(img == null) {
        error = true
        message = "Debe subir una foto"
      }
      else if(name == '') {
        error = true
        message = "Escribe tu nombre"
      }
      else if(lastname == '') {
        error = true
        message = "Escribe primer apellido"
      }
      else if(seclastname == '') {
        error = true
        message = "Escribe segundo apellido"
      }
      else if(profesion == '') {
        error = true
        message = "Escribe profesión"
      }
      else if(estadoCivil == '') {
        error = true
        message = "Elige estado civil"
      }
      else if(departamentSelect == '') {
        error = true
        message = "Elige departamento"
      }
      else if(municipioSelect == '') {
        error = true
        message = "Elige municipio"
      }
      else if(addres == '') {
        error = true
        message = "Escribe direccion"
      }
      else if(res == false) {
        error = true
        message = "El correo no es correcto o está vacío"
      }
      else if(phone2 == '') {
        error = true
        message = "Escribe numero de celular"
      }
      else if(tipo_inmueblee == '' || tipo_inmueblee == null ) {
        error = true
        message = "Elige tipo de inmueble"
      }
      else if(unidadres == null || unidadres == '') {
        error = true
        message = "Elige una torre / interior - número"
      }
      /*else if(file == null||file.name == null) {
        error = true
        message = "Debe subir el documento"
      }*/

      /*else if(numeroPer == '') {
        error = true
        message = "Digite el numero de personas que viven en el inmueble"
      }*/
   }
   ////este es validación de arrendatario
   else if(showForm == 2){

     var tipo_inmueblee = otraUni.value
     var prop = propietarios.value
     var depart = 0
     var munici = 0
     var vivir  = uniArrend.value


    var torreArrende = JSON.stringify(uniArrend)

    if(vehiculo == 0){
      var vehicule = 0
    }else {
      var vehicule = vehiculo.value
    }

    if(documento == null) {
      error = true
      message = "Escoge tipo de documento"
    }
    else if(numeroDoc == '') {
      error = true
      message = "Escribe el numero de documento de la persona"
    }
    else if(expedition == '') {
      error = true
      message = "Escribe el lugar de expedición del documento"
    }
    else if(img == null) {
      error = true
      message = "Debe subir una foto"
    }
    else if(name == '') {
      error = true
      message = "Escribe tu nombre"
    }
    else if(lastname == '') {
      error = true
      message = "Escribe primer apellido"
    }
    else if(seclastname == '') {
      error = true
      message = "Escribe segundo apellido"
    }
    else if(profesion == '') {
      error = true
      message = "Escribe profesión"
    }
    else if(estadoCivil == '') {
      error = true
      message = "Elige estado civil"
    }
    else if(res == false) {
      error = true
      message = "El correo no es correcto o está vacío"
    }
    else if(phone2 == '') {
      error = true
      message = "Escribe numero de celular"
    }
    else if(propietarios == '') {
      error = true
      message = "Elige el propietario del inmueble"
    }
    else if(otraUni == '') {
      error = true
      message = "Elige tipo de inmueble"
    }
    else if(uniArrend == null) {
      error = true
      message = "Elige una torre"
    }
    {/*else if(numeroPer == '') {
      error = true
      message = "Digite el numero de personas que viven en el inmueble"
    }*/}
   }
  if (error) {
    MessageError(message)
  }
  else{
    const data = new FormData()
    data.append('id', props.id)
    data.append('tip_prop', propietario.value)
    data.append('tip_ident',documento.value)
    data.append('n_document',numeroDoc)
    data.append('name',name)
    data.append('secname',seconame)
    data.append('lastname',lastname)
    data.append('seclastname',seclastname)
    data.append('departamento', depart)
    data.append('municipio', munici)
    data.append('expedido',expedition)
    if(img){
      data.append('foto',img) //Foto
    }
    data.append('addres',addres)
    data.append('city',city)
    data.append('phone',phone)
    data.append('mobile',phone2)
    data.append('profesion', profesion)
    data.append('estado_civil', estadoCivil.value)
    data.append('tip_inmueble',tipo_inmueblee)
    data.append('torre', torreArrende)
    data.append('numero_res', numeroRes)
    data.append('vehiculo', vehicule)
    data.append('marca', marca)
    data.append('placa', placa)
    data.append('color', color)
    data.append('numero_per', numeroPer)
    data.append('correo',email)
    if(file.name){
      console.log("Envio documento")
      console.log(file)
      data.append('documento', file) ///Certificado libertad
      data.append('name_file', file.name) ///Nombre del certificado
    }
    data.append('prop_inmu', prop)
    data.append('arrendatario', arrend)
    data.append('vive', vivir)
    axios.post(ip+'admon/guard_propietario',data).then(response=>{
      if(response.data == 2){
        MessageError("Ya hay una persona con el mismo número de cédula");
      }else{
        if (props.id == null) {
          MessageSuccess("Tipo persona creada correctamente")
            props.gobackSave()
        }
        else{
          MessageSuccess("Tipo persona editada correctamente")
            props.gobackSave()

        }
      }
    })
  }

}

/// función para escojer tipo persona
    const tipo_per = (e)=>{
      if(e.value == 1){
        setShowForm(3)
        var casa = [];
        const data ={
          value:'4',
          label:'otros'
        }
        setDocumento(data)
        const data2 ={
          value:'1',
          label:'Jurídica'
        }
        setType(data2)
      }
      if(e.value == 2 ){
        setShowForm(2)
        const data ={
          value:'2',
          label:'Natural'
        }
        setType(data)
      }
    }

/// tipo persona propietario o  arrendatario
    const tipo_prop = (e) =>{
      if(e.value == 1){
        setShowForm(1)
        const data ={
          value:'1',
          label:'Propietario'
        }
        setPropietario(data)
      }
      if(e.value == 2){
        setShowForm(2)
        const data ={
          value:'2',
          label:'Arrendatario'
        }
        setPropietario(data)
      }

    }

    const arrendatarioData = async (data) => {
      if(data.target.checked == true){
        setEstado(2)
        setArrend(1)
        setVariable(true)
      }
      else if (data.target.checked == false){
        setArrend(0)
        setEstado(1)
        setVariable(false)
        setUnidadVive(null)
        setVehiculo(0)
        setNumeroPer(0)
        setMarca("")
        setPlaca("")
        setColor("")
      }
    }

    return(
      <div className="mt-2 col-12 mt-5" >
      <div className="btn container col-1 " onClick = {()=>props.goback()}>
      <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
      </div>
        <div  className="form-group">
          <div className="row col-12">

            {
              props.id > 0 ?
              <div></div>
              :
              <div className= "row  col-12 justify-content-center">
                <p style={{marginLeft: '18px'}}>Selecciona tipo de persona</p>
                <div className = "col-12 d-flex">
                  <Select
                    className="col-6"
                    value = {propietario}
                    components={animatedComponents}
                    options={propietarioList}
                    onChange={(e)=>tipo_prop(e)}
                    placeholder = "Seleccione tipo de persona"
                    name="colors"
                  />
                </div>
              </div>
            }

          {
          showForm == 1?
          <div className="row col-12 mt-2">

            <div className="row col-12 mt-2">
              <div className="my-2 d-flex col-md-9">
                <h4 className="mr-3">{props.id > 0 ? 'Editar propietario - residente' : 'Agregar propietario - residente'}</h4>
              </div>

              <div className="my-2 d-flex col-md-9">
                <h4 className="my-2 mr-3">Información personal</h4>
              </div>
              <div className="col-md-6">
                <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '225px'}}>Foto del propietario</label>
                <div className=" d-flex justify-content-center align-items-center mt-2 " style={{marginRight:'73%'}}>
                  <label htmlFor="activa">
                    {
                      urlImg == null ?
                      <div className="material-icons border radius-10 muestra">
                        <div className="img-foto">camera_alt</div>
                        </div>
                        :<img style = {{
                          maxWidth: 100,
                          maxHeight: 100
                        }} src = {urlImg}/>
                    }
                  </label>

                  </div>
                  <div className="justify-Content-center flie">
                    <input onChange = {(e)=>loadImage(e)} className="file" id = "activa" type="file"/>
                  </div>
              </div>

              {
                props.id > 0 ?
                <div style={{marginLeft: '-8px'}}>
                  <div className="col-md-9 d-flex ">
                    <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '225px'}}>Tipo de documento: </label>
                    <Select
                    isDisabled
                    className="col-5"
                    value={documento}
                    closeMenuOnSelect={true}
                    components={animatedComponents}
                    options={docuList}
                    onChange={(e)=>setDocumento(e)}
                    placeholder = "Seleccionar tipo documento"
                    name="colors"
                    />
                  </div>
                  <div className = "col-md-10 d-flex">
                      <label className="d-flex col-6 color mr-3" style={{width: '225px'}}
                      >Número de documento: </label>
                    <input className="form-control col-8" disabled type="number" pattern="[0-9]{0,13}" value = {numeroDoc} placeholder="Número de documento" onChange = {(e)=>setNumeroDoc(e.target.value)} style={{width: '328px'}}/>
                  </div>
                </div>

                :
              <div  style={{marginLeft: '-8px'}}>
                <div className="col-md-9 d-flex ">
                  <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '225px'}}>Tipo de documento: </label>
                  <Select
                  className="col-5"
                  value={documento}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={docuList}
                  onChange={(e)=>setDocumento(e)}
                  placeholder = "Seleccionar tipo documento"
                  name="colors"
                  />
                </div>
                <div className = "col-md-10 d-flex">
                    <label className="d-flex col-6 color mr-3" style={{width: '225px'}}
                    >Número de documento: </label>
                  <input className="form-control col-8"  type="number" pattern="[0-9]{0,13}" value = {numeroDoc} placeholder="Número de documento" onChange = {(e)=>setNumeroDoc(e.target.value)} style={{width: '328px'}}/>
                </div>
              </div>

              }

              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Expedido en: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text"   value = {expedition} placeholder="Lugar de expedición del documento" onChange = {(e)=>setExpedition(e.target.value)} />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}> Primer nombre: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text"  value = {name} placeholder="Primer nombre" onChange = {(e)=>setName(e.target.value)}  ç/>
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Segundo nombre: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text"  value = {seconame} placeholder="Segundo nombre" onChange = {(e)=>setSecoName(e.target.value)} />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Primer apellido: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text"  value = {lastname} placeholder="Primer apellido"  onChange = {(e)=>setLastname(e.target.value)} />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Segundo  apellido: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text"  value = {seclastname} placeholder="Segundo apellido" onChange = {(e)=>setSeclastname(e.target.value)}  />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Profesión: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text"  value = {profesion} placeholder="Profesión" onChange = {(e)=>setProfesion(e.target.value)} />
              </div>
              <div className = "col-md-9 d-flex">
                  <label className="d-flex col-6 color mr-2" style={{width: '225px'}}>Estado civil: </label>
                  <Select
                  className="col-5"
                  value={estadoCivil}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={estadoCivilList}
                  onChange={(e)=>setEstadoCivil(e)}
                  placeholder = "Seleccionar estado civil"
                  name="colors"
                  />
              </div>
              <div className="my-2 d-flex col-md-9">
                <h4 className="my-2 mr-3">Información de ubicación</h4>
              </div>
              <div className="col-md-9 d-flex">
                <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '225px'}}>Departamento: </label>
              <Select
              className="col-md-5"
              value={departamentSelect}
              components={animatedComponents}
              options={departamentlist}
              onChange={(e)=>departame(e)}
              placeholder = "Seleccione el departamento"
              />
              </div>
              <div className="col-md-9 d-flex">
                <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '225px'}}>Ciudad: </label>
                <Select
                  className="col-md-5"
                  value={municipioSelect}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={municipiolist}
                  onChange={(e)=>setMunicipioSelect(e)}
                  placeholder = "Seleccione el municipio"
                  name="colors"
                  />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Direccion residencia: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text"  value = {addres} placeholder="Direccion residencia" onChange = {(e)=>setAddres(e.target.value)} />
              </div>
              <div className="my-2 d-flex col-md-9">
                <h4 className="my-2 mr-3">Información de contacto</h4>
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Correo: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="email" value = {email} placeholder="Correo " onChange = {(e)=>setEmail(e.target.value)}  />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Teléfono: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="number"  value = {phone} placeholder="Teléfono " onChange = {(e)=>setPhone(e.target.value)} />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Teléfono Celular: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="number" value = {phone2} placeholder="Teléfono Celular" onChange = {(e)=>setPhone2(e.target.value)}  />
              </div>
              <div className="my-2 d-flex col-md-9">
                <h4 className="my-2 mr-3">Información del inmueble</h4>
              </div>
              <div className="col-md-9 d-flex">
                <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '225px'}}>Unidad residencial</label>
                <Select
                  className="col-md-5"
                  value={inmuebleSelect}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={inmueble}
                  onChange={(e)=>residencias(e)}
                  placeholder="Seleccione unidad residencial"
                  name="colors"
                />
              </div>

              <div className="col-md-9 d-flex">
                <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '225px'}}>Torre / Interior - número</label>
                <Select
                  className="basic-multi-select col-md-5"
                  classNamePrefix="select"
                  isMulti
                  value={unidadres}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={unidadResSelect}
                  onChange={(e)=>setUnidadres(e)}
                  placeholder="Seleccione torre"
                  name="colors"
                />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Certificado libertad: </label>
                  <input class="form-control" type="file" id="formFile" onChange = {(e)=>validateDocument(e)} style={{width: '328px'}}/>
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>¿Usted vive en el inmueble?</label>
                  <div className="form-check form-switch" >
                      <input className="form-check-input" onClick={(arrend)=>arrendatarioData(arrend)} checked={variable} type="checkbox" id="flexSwitchCheckDefault" style={{width: '50px' , height: '25px'}}/>
                      <label className="form-check-label" for="flexSwitchCheckDefault"></label>
                  </div>
              </div>
              {
                estado == 2 ?

                <div>
                  <div className = "col-md-10 d-flex" style={{marginLeft: '-9px'}}>
                    <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>¿En que inmueble vive?</label>
                    <Select
                      className="col-md-5 select-prop"
                      classNamePrefix="select"
                      value={unidadVive}
                      closeMenuOnSelect={true}
                      components={animatedComponents}
                      options={unidadres}
                      onChange={(e)=>setUnidadVive(e)}
                      placeholder="Seleccione inmueble"
                      name="colors"
                    />
                  </div>
                  <div className="my-2 d-flex col-md-9" style={{marginLeft: '-8px'}}>
                    <h4 className="my-2 mr-3">Información adicional</h4>
                  </div>

                  <div className="col-md-10 d-flex" style={{marginLeft: '-9px'}}>
                    <label className="d-flex col-6 mr-3 color" style={{width: '225px'}}>Tipo de vehículo</label>
                    <Select
                      className="col-md-5 select-prop"
                      value={vehiculo}
                      closeMenuOnSelect={true}
                      components={animatedComponents}
                      options={vehiculoList}
                      onChange={(e)=>setVehiculo(e)}
                      placeholder="Seleccionar tipo de vehículo"
                      name="colors"
                    />
                  </div>

                </div>

                : null
              }

              {
                vehiculo.value == 1 || vehiculo.value == 2?

                <div style={{marginLeft:'-8px'}}>
                  <div className = "col-md-10 d-flex">
                    <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Marca: </label>
                    <input className="form-control col-8" style={{width: '328px'}} type="text" value = {marca} placeholder="Digite la marca del vehículo" onChange = {(e)=>setMarca(e.target.value)}  />
                  </div>

                  <div className = "col-md-10 d-flex">
                    <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Placa: </label>
                    <input className="form-control col-8" style={{width: '328px'}} type="text" value = {placa} placeholder="Digite la placa del vehículo" onChange = {(e)=>setPlaca(e.target.value)}  />
                  </div>

                  <div className = "col-md-10 d-flex">
                    <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Color: </label>
                    <input className="form-control col-8" style={{width: '328px'}} type="text" value = {color} placeholder="Digite el color del vehículo" onChange = {(e)=>setColor(e.target.value)}  />
                  </div>
                </div>


                : null


              }

              {
                estado == 2 ?

                <div style={{marginLeft:'-8px'}}>
                  <div className = "col-md-10 d-flex">
                    <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>N° de personas: </label>
                    <input className="form-control col-8" style={{width: '328px'}} type="number" value = {numeroPer} placeholder="Número de personas que habitan" onChange = {(e)=>setNumeroPer(e.target.value)}  />
                  </div>
                </div>

                : null
              }

              <div className="my-2 col-md-4 d-flex">
                <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}} onClick = {()=>save_per()}>Guardar propietario</button>
              </div>
            </div>

          </div>

          : showForm == 2 ?

          ////aqui comienza arrendatario

          <div className="row col-12 mt-2">

            <div className="row col-12 mt-2">
              <div className="my-2 d-flex col-md-9">
                <h4 className="mr-3">{props.id > 0 ? 'Editar arrendatario' : 'Agregar arrendatario'}</h4>
              </div>

              <div className="my-2 d-flex col-md-9">
                <h4 className="my-2 mr-3">Información personal</h4>
              </div>
              <div className="col-md-6">
                <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '225px'}}>Foto del arrendatario</label>
                <div className=" d-flex justify-content-center align-items-center mt-2 " style={{marginRight:'73%'}}>
                  <label htmlFor="activa">
                    {
                      urlImg == null ?
                      <div className="material-icons border radius-10 muestra">
                        <div className="img-foto">camera_alt</div>
                        </div>
                        :<img style = {{
                          maxWidth: 100,
                          maxHeight: 100
                        }} src = {urlImg}/>
                    }
                  </label>

                  </div>
                  <div className="justify-Content-center flie">
                    <input onChange = {(e)=>loadImage(e)} className="file" id = "activa" type="file"/>
                  </div>
              </div>

              {
                props.id > 0 ?
                <div  style={{marginLeft: '-8px'}}>
                  <div className="col-md-9 d-flex ">
                    <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '225px'}}>Tipo de documento: </label>
                    <Select
                    isDisabled
                    className="col-5"
                    value={documento}
                    closeMenuOnSelect={true}
                    components={animatedComponents}
                    options={docuList}
                    onChange={(e)=>setDocumento(e)}
                    placeholder = "Seleccionar tipo documento"
                    name="colors"
                    />
                  </div>
                  <div className = "col-md-10 d-flex">
                      <label className="d-flex col-6 color mr-3" style={{width: '225px'}}
                      >Número de documento: </label>
                    <input className="form-control col-8" disabled type="number" value = {numeroDoc} placeholder="Número de documento" onChange = {(e)=>setNumeroDoc(e.target.value)} style={{width: '328px'}}/>
                  </div>
                </div>
                :
                <div style={{marginLeft: '-8px'}}>
                  <div className="col-md-9 d-flex ">
                    <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '225px'}}>Tipo de documento: </label>
                    <Select
                    className="col-5"
                    value={documento}
                    closeMenuOnSelect={true}
                    components={animatedComponents}
                    options={docuList}
                    onChange={(e)=>setDocumento(e)}
                    placeholder = "Seleccionar tipo documento"
                    name="colors"
                    />
                  </div>
                  <div className = "col-md-10 d-flex">
                      <label className="d-flex col-6 color mr-3" style={{width: '225px'}}
                      >Número de documento: </label>
                      <input className="form-control col-8" type="number" value = {numeroDoc} placeholder="Número de documento" onChange = {(e)=>setNumeroDoc(e.target.value)} style={{width: '328px'}}/>
                  </div>
                </div>

              }

              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Expedido en: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text" value = {expedition} placeholder="Lugar de expedición del documento" onChange = {(e)=>setExpedition(e.target.value)} />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}> Primer nombre: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text"  value = {name} placeholder="Primer nombre" onChange = {(e)=>setName(e.target.value)}  ç/>
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Segundo nombre: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text"  value = {seconame} placeholder="Segundo nombre" onChange = {(e)=>setSecoName(e.target.value)} />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Primer apellido: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text"  value = {lastname} placeholder="Primer apellido"  onChange = {(e)=>setLastname(e.target.value)} />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Segundo  apellido: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text"  value = {seclastname} placeholder="Segundo apellido" onChange = {(e)=>setSeclastname(e.target.value)}  />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Profesión: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="text"  value = {profesion} placeholder="Profesión" onChange = {(e)=>setProfesion(e.target.value)} />
              </div>
              <div className = "col-md-9 d-flex">
                  <label className="d-flex col-6 color mr-2" style={{width: '225px'}}>Estado civil: </label>
                  <Select
                  className="col-5"
                  value={estadoCivil}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={estadoCivilList}
                  onChange={(e)=>setEstadoCivil(e)}
                  placeholder = "Seleccionar estado civil"
                  name="colors"
                  />
              </div>
              <div className="my-2 d-flex col-md-9">
                <h4 className="my-2 mr-3">Información de contacto</h4>
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Correo: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="email" value = {email} placeholder="Correo " onChange = {(e)=>setEmail(e.target.value)}  />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Teléfono: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="number"  value = {phone} placeholder="Teléfono " onChange = {(e)=>setPhone(e.target.value)} />
              </div>
              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Teléfono Celular: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="number" value = {phone2} placeholder="Teléfono Celular" onChange = {(e)=>setPhone2(e.target.value)}  />
              </div>
              <div className="my-2 d-flex col-md-9">
                <h4 className="my-2 mr-3">Información del inmueble</h4>
              </div>
              <div className="col-md-9 d-flex ">
                <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '225px'}}>Propietario del inmueble: </label>
                <Select
                  className="col-5"
                  value={propietarios}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={propietariosList}
                  onChange={(e)=>propvive(e)}
                  placeholder = "Seleccionar propietario"
                  name="colors"
                />
              </div>
              <div className="col-md-9 d-flex">
                <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '225px'}}>Unidad residencial</label>
                <Select
                  className="col-md-5"
                  value={otraUni}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={otraUniSelect}
                  onChange={(e)=>setOtraUni(e)}
                  placeholder="Seleccione unidad residencial"
                  name="colors"
                />
              </div>

              <div className="col-md-9 d-flex">
                <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '225px'}}>Torre / Interior - número</label>
                <Select
                  className="col-md-5"
                  value={uniArrend}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={uniArrendSelect}
                  onChange={(e)=>setUniArrend(e)}
                  placeholder="Seleccione torre/interior - numero"
                  name="colors"
                />
              </div>
              <div className="my-2 d-flex col-md-9">
                <h4 className="my-2 mr-3">Información adicional</h4>
              </div>
              <div className="col-md-9 d-flex">
                <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '225px'}}>Tipo de vehículo</label>
                <Select
                  className="col-md-5"
                  value={vehiculo}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={vehiculoList}
                  onChange={(e)=>setVehiculo(e)}
                  placeholder="Seleccionar tipo de vehículo"
                  name="colors"
                />
              </div>

              {
                vehiculo.value == 1 || vehiculo.value == 2?

                <div style={{marginLeft:'-8px'}}>
                  <div className = "col-md-10 d-flex">
                    <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Marca: </label>
                    <input className="form-control col-8" style={{width: '328px'}} type="text" value = {marca} placeholder="Digite la marca del vehículo" onChange = {(e)=>setMarca(e.target.value)}  />
                  </div>

                  <div className = "col-md-10 d-flex">
                    <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Placa: </label>
                    <input className="form-control col-8" style={{width: '328px'}} type="text" value = {placa} placeholder="Digite la placa del vehículo" onChange = {(e)=>setPlaca(e.target.value)}  />
                  </div>

                  <div className = "col-md-10 d-flex">
                    <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>Color: </label>
                    <input className="form-control col-8" style={{width: '328px'}} type="text" value = {color} placeholder="Digite el color del vehículo" onChange = {(e)=>setColor(e.target.value)}  />
                  </div>
                </div>


                : null


              }

              <div className = "col-md-10 d-flex">
                  <label className="d-flex col-6 color mr-3" style={{width: '225px'}}>N° de personas: </label>
                  <input className="form-control col-8" style={{width: '328px'}} type="number" value = {numeroPer} placeholder="Número de personas que habitan" onChange = {(e)=>setNumeroPer(e.target.value)}  />
              </div>

              <div className="my-2 col-md-4 d-flex">
                <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}} onClick = {()=>save_per()}>Guardar arrendatario</button>
              </div>
            </div>

          </div>

          :null
        }
        </div>

      </div>
    </div>


    );
}

export default NewPropietarios;
if(document.getElementById('NewPropietarios')) {
    ReactDOM.render(<NewPropietarios />, document.getElementById('NewPropietarios'));
}
