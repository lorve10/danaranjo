import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import NewPropietarios from './NewPropietarios';
import Swal from 'sweetalert2';
import Select from 'react-select'
import Header from '../../../Ui/Header';
import makeAnimated from 'react-select/animated'
import { Button, Modal } from 'react-bootstrap';
import { Tooltip,  Grid  } from '@material-ui/core';

import util from "../../util";
import Pagination from '../../componentes/paginate';

function Propietarios() {

  const [detalles, setDetalles] = useState([])
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [loading, setLoading] = useState(false);
  const [text, setText] = useState('')
  const [id, setId] = useState(null)
  const [showForm, setShowForm] = useState(1)
  const [data, setData] = useState({})
  const [person, setPerson] = useState([])

  const [currentPage, setCurrentPage] = useState(1)
  const [perPage, setPerPage] = useState(6)
  const [marginval, setMarginVal] = useState(false)
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
  const [type, setType] = useState(null)
  const [listPropietario, setListPropietario] = useState([])
  const [listPropietario2, setListPropietario2] = useState([])
  const [listPropietario3, setListPropietario3] = useState([])
  const [ vive, setVive ] = useState([])
  const [ apar, setApar ] = useState([])
  const [ torre, setTorre ] = useState([])

  const [tipo, setTipo] = useState([
    {
    value:'3',
    label:'Todos',
    },
    {
    value:'1',
    label:'Propietarios',
    },
    {
    value:'2',
    label:'Arrendatarios',
    }

]);

const [ torreDet, setTorreDet ] = useState([])

  useEffect(()=>{
    obtain_terce()
    obtener_vive()
    obtener_tipo()
  },[]);

  const obtain_terce = () =>{
    axios.get(ip+'admon/obtain_propietarios').then(response=>{
      setListPropietario3(response.data)
      setListPropietario2(response.data)
      const dataNew = util.paginate(response.data,currentPage,perPage)
      setPerson(dataNew)
      setListPropietario(dataNew)

      var res = response.data

      res.map(item=>{
        console.log("aqui torreeee");
        var res2 = JSON.parse(item.torre);

          setTorre(res2)

      })


    })
  }

  const updateCurrentPage = async (number) => {
    await setPerson([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listPropietario3,number,perPage)
    await setPerson(dataNew)
    await setListPropietario(dataNew)
  }

  const searchInput = async(value) => {
    await setText(value)
    await setLoading(true)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData2 = listPropietario2.filter(function(item2){
      var n_document = ((item2.n_document).toString()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var name = ((item2.name).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")

      var resN_document = n_document.indexOf(inputSearch) > -1
      var resN_documentLasT = n_document.indexOf(inputSearch) > -1
      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var res = false
      if(resNameLasT||resNameLasT){
        res = true
      }
      if(resN_document||resN_documentLasT){
        res = true
      }
      return res;
    })
    await setCurrentPage(1)
    await setListPropietario3(newData2)
    const dataNew = util.paginate(newData2,1,perPage)
    await setListPropietario(dataNew);

  }
    const goback = async () => {
    await setShowForm(1)
    setId(null)
    obtain_terce()

  }
  const gobackSave = () => {
  setShowForm(1)
  setId(null)
  obtain_terce()
  setData({})
}

const obtener_vive = (data) =>{
  axios.get(ip+'admon/obtain_Inmu').then(response=>{
      var res = response.data;
      var filtro = res.filter(e=>e.id == data )
      console.log("Aqui filtro vive")
      console.log(filtro);
      setVive(filtro)
  })
}
const obtener_tipo = (data) =>{
  axios.get(ip+'admon/obtain_tipo').then(response=>{
      var res = response.data;
      var filtro = res.filter(e=>e.id == data )
      console.log("Aqui filtro tipo")
      console.log(filtro);
      setApar(filtro)
  })
}


const dataDet = async (data) => {


  setShow(true);

  setDetalles(data);
  var torre = JSON.parse(data.torre)
  setTorreDet(torre)

  var vive = data.vive
  obtener_vive(vive, true)
  var tipo = data.tip_inmueble
  obtener_tipo(tipo, true)


}


  const dataUpdate = async (data) => {
    var tip_prop = data.tip_prop
    var tip_ident = data.tip_ident
    var n_document = data.n_document
    var expedido = data.expedido
    var name = data.name
    var secname = data.secname
    var lastname = data.lastname
    var seclastname = data.seclastname
    var departamento = data.departamento
    var municipio = data.municipio
    var addres = data.addres
    var city = data.city
    var correo = data.correo
    var phone = data.phone
    var mobile = data.mobile
    var profesion = data.profesion
    var estado_civil = data.estado_civil
    var tip_inmueble = data.tip_inmueble
    var torre = JSON.parse(data.torre)
    var numero_res = data.numero_res
    var vehiculo = data.vehiculo
    var marca = data.marca
    var placa = data.placa
    var color = data.color
    var numero_per = data.numero_per
    var documento = data.documento
    var name_file = data.name_file
    var foto = data.foto
    var prop_inmu = data.prop_inmu
    var arrendatario = data.arrendatario
    var vive = data.vive

    const dataForAll = {

           tip_prop,
           tip_ident,
           n_document,
           expedido,
           name,
           secname,
           lastname,
           seclastname,
           departamento,
           municipio,
           addres,
           city,
           correo,
           phone,
           mobile,
           profesion,
           estado_civil,
           tip_inmueble,
           torre,
           numero_res,
           vehiculo,
           marca,
           placa,
           color,
           numero_per,
           documento,
           name_file,
           foto,
           prop_inmu,
           arrendatario,
           vive,
    }
    await setId(data.id)
    await setData(dataForAll)
    await setShowForm(2)
    console.log(dataForAll);
  }

  const deshabilitar = (valor) =>{
    if(valor.deleted == 0){
      var message = '¿Quieres deshabilitar este propietario?';
      Swal.fire({
        title: message,
        icon: 'warning',
        showDenyButton: true,
        confirmButtonColor: '#3085d6',
        denyButtonColor: '#d33',
        confirmButtonText: 'Si',
        denyButtonText: 'No'
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          const data = new FormData()
          data.append('id', valor.id)
          axios.post(ip+'admon/deleted_prop',data).then(response=>{

            if(response.data == 1){
              Swal.fire('Deshabilitado correctamente!', '', 'success')
              obtain_terce();
            }
            else{
              Swal.fire('No se puede eliminar por que ya tiene una cuenta de cobro.', '', 'info')
              obtain_terce();
            }

          })

        } else if (result.isDenied) {
          Swal.fire('Acción cancelada', '', 'info')
        }
      })

    }else{
      var message = '¿Quieres habilitar este propietario?';
      Swal.fire({
        title: message,
        icon: 'warning',
        showDenyButton: true,
        confirmButtonColor: '#3085d6',
        denyButtonColor: '#d33',
        confirmButtonText: 'Si',
        denyButtonText: 'No'
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          const data = new FormData()
          data.append('id', valor.id)
          axios.post(ip+'admon/habilited_prop',data).then(response=>{
            if(response.data == 1){
              Swal.fire('Habilitado correctamente!', '', 'success')
              obtain_terce();
            }
          })

        } else if (result.isDenied) {
          Swal.fire('Acción cancelada', '', 'info')
        }
      })
    }


  }

  const propHeader = (value) => {
    setMarginVal(value)
  }



  const tipo_per = (e) =>{

      const data2 ={
        value:e.value,
        label:e.label
      }
      setType(data2)
      setCurrentPage(1)
      if (e.value == 3){

        const dataNew = util.paginate(listPropietario3,1,perPage)
        setListPropietario(dataNew);
        setListPropietario2(listPropietario3)
      }
      else{
        var data = []
        console.log("antes de filtrar =>"+listPropietario3.length)
        data =  listPropietario3.filter((item, i)  => item.tip_prop == e.value);
        setListPropietario2(data)
        console.log("Despues de filtrar =>"+data.length)
        const dataNew = util.paginate(data,1,perPage)
        setListPropietario(dataNew);
        setLoading(false)
      }





    // if(e.value == 2){
    //   const data2 ={
    //     value:'2',
    //     label:'Arrendatario'
    //   }
    //   setType(data2)
    //   var listPropietario1 = []
    //   listPropietario1 = person.filter(e=>e.tip_prop == 2)
    //   setListPropietario3(listPropietario1)
    //   const dataNew = util.paginate(listPropietario1,1,perPage)
    //   setListPropietario(dataNew);
    //   setLoading(false)
    //
    // }
    // if(e.value == 3){
    //   const data2 ={
    //     value:'3',
    //     label:'Todos'
    //   }
    //   setType(data2)
    //   setListPropietario(listPropietario3)
    //   setLoading(false)
    //
    // }
  }



  return(
  <div className="content-wrapper " style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}}>
    <Header section={2}  marginval = {(value)=>propHeader(value)}/>

    <div className="verticar-footer-2">
      {
        showForm == 1?
        <Tooltip title="Agregar propietario" placement="left">
          <button className="btn" >
            <h5 onClick = {()=>setShowForm(2)}><img src = {ip+'images/Asambleas_2.png'} style= {{width:'56px', marginLeft: '0px', marginTop: '35px'}} /> </h5>
          </button>
         </Tooltip>

        :null
      }

         <img className="imagen" src={ip+'images/footer.png'}/>
    </div>

  {
    showForm == 1 ?
    <div>
      <div className="row">
        <div className="col-md-9 " style={{marginLeft:47}}>
        <div className="btn container mt-3 d-flex" style={{marginLeft:'-155px'}}>
          <div className="form-section col-md-8 "><strong><h6 className="mt-2 ruta" style={{marginLeft:'-108px'}}>/ <a href = {ip+'admon/conjunto'}>Módulo conjunto </a>/ Propietarios</h6></strong></div>
        </div>
        <div className="mt-3">
          <h4 className="text-center" style={{marginLeft:"-87%"}}>Propietarios</h4>
        </div>
          <div className="d-flex mt-2" style={{marginTop: 9}}>
              <label className="mr-3 d-flex mt-2">Tipo persona: </label>
              <Select
              className="col-3"
              defaultValue={tipo[0]}
              closeMenuOnSelect={false}
              components={animatedComponents}
              options={tipo}
              onChange={(e)=>tipo_per(e)}
              name="colors"
              />
              <div className="col-7  mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #D9D9D9 ', backgroundColor:'white',borderRadius:12}}>
              <input  onChange = {(e)=>searchInput(e.target.value)} style = {{width:'490px',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Consultar cédula, nombre "/>
              {
                text.length == 0 ?
                <span style = {{color:'black', cursor:'pointer'}}  className= "material-icons">search</span>
                :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
              }
              </div>
          </div>

        </div>
      </div>
    </div>
    :null

  }
  {
    showForm == 1?

    <div className="col-md-10" style={{marginLeft:35}}>
    <table className="table table-striped">
        <thead style={{backgroundColor: '#FFDE59' }}>
          <tr>
              <th>Tipo Persona</th>
              <th>Cédula</th>
              <th>Nombre de la persona</th>
              <th>Correo</th>
              <th>Descargar certificado libertad</th>
              <th>Detalles</th>
              <th>Editar</th>
              <th>Deshabilitar</th>

          </tr>
        </thead>
          <tbody>
          {
            listPropietario.map((item,index)=>{
              if(item.tip_prop == 1){
                var per = 'Propietario';
              }else if(item.tip_prop == 2){
                var per = 'Arrendatario';
              }
              return (
                <tr key={index}>
                  <td>{per}</td>
                  <td>{item.n_document}</td>
                  <td>{item.name} {item.secname == null ? '': item.secname } {item.lastname == null ? '': item.lastname} {item.seclastname == null ? '': item.seclastname}</td>
                  <td>{item.correo}</td>
                  {
                    item.tip_prop == 1 ?
                    <td>{item.name_file}<a href = {ip+"storage/"+item.documento} target="_blank"><button className="btn d-flex"><i class = "material-icons">file_download</i></button></a> </td>

                    :
                    <td>No tiene certificado libertad </td>
                  }

                  <td>
                    <div className = "">
                      <button onClick = {()=>dataDet(item)} className="btn "><i class = "material-icons">visibility</i></button>
                    </div>
                  </td>
                  <td>
                    <div className = "">
                      <button onClick = {()=>dataUpdate(item)} className="btn "><i class = "material-icons">edit</i></button>
                    </div>
                  </td>
                  <td>
                  <div className="form-check form-switch">
                      <input className="form-check-input cursor-pointer" onClick={()=>deshabilitar(item)} checked={item.deleted == 0 ? true : false} type="checkbox" id="flexSwitchCheckDefault"/>
                      <label className="form-check-label" for="flexSwitchCheckDefault"></label>
                  </div>
                  </td>
                </tr>
              );
            })

          }
          </tbody>
     </table>
      <div className="d-flex col-md-12 col-12 justify-content-end">
          <Pagination currentPage={currentPage} perPage={perPage} countdata={listPropietario2.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
      </div>
      <div>
         <Modal show={show} onHide={handleClose}>
           <Modal.Header closeButton>
             <Modal.Title>Información detallada</Modal.Title>
           </Modal.Header>
             <Modal.Body>
             <div class="container-fluid">
              <div class="row">
                <div class="col-md-8">
                  <img style = {{
                    maxWidth: 150,
                    maxHeight: 150,
                    border: '5px solid #D9D9D9'
                  }} src = {ip+"storage/"+detalles.foto}/>
                </div>
              </div>
              <hr></hr>
              <div class="row">
                <div class="col-md-8">
                <strong>Tipo de propietario:</strong> {detalles.tip_prop == 1 ? 'Propietario' : 'Arrendatario'}
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <strong>Tipo de documento:</strong> {detalles.tip_ident == 1 ? 'Cedula' : detalles.tip_ident == 2 ? 'Cedula extranjera' : detalles.tip_ident == 3 ? 'Pasaporte' : detalles.tip_ident == 4 ? 'Otros' : null}
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                <strong>Numero de documento:</strong> {detalles.n_document}
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                <strong>Expedido en:</strong> {detalles.expedido}
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                <strong>Nombre completo:</strong> {detalles.name} {detalles.secname == null ? '': detalles.secname } {detalles.lastname == null ? '': detalles.lastname} {detalles.seclastname == null ? '': detalles.seclastname}
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                <strong>Profesion:</strong> {detalles.profesion}
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                <strong>Estado civil:</strong> {detalles.estado_civil == 1 ? 'Soltero/a' : detalles.estado_civil == 2 ? 'Casado/a' : detalles.estado_civil == 3 ? 'Unión libre o unión de hecho' : detalles == 3 ? 'Separado/a' : detalles.estado_civil == 4 ? 'Divorciado/a' : detalles.estado_civil ? 'Viudo/a' : null}
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                <strong>Correo:</strong> {detalles.correo}
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                <strong>Celular:</strong> {detalles.mobile}
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                <strong>Tipo de inmueble:</strong>
                 {
                  apar.map(item=>{
                  return(
                    <div>
                      <p>{item.nombre}</p>
                    </div>
                  )
                })}
                </div>
              </div>
              <div class="row">
                { torreDet.length > 0 ?
                  <div class="col-md-8">
                  <strong>Torre / interior - número:</strong>
                    {
                      torreDet.map((item2,index2)=>{
                      return (
                        <p key = {index2} >{item2.label}</p>
                      );
                    })
                  }
                  </div>
                  :null
                }

              </div>
              {
                detalles.arrendatario == 1 ?
                <div className="container" style={{marginLeft: '-11px'}}>
                  <div class="row">
                  <div class="col-md-8">
                    <strong>¿Vive en el inmueble? :</strong> {detalles.arrendatario == 0 ? 'No': detalles.arrendatario == 1 ? 'Si': detalles.arrendatario}
                  </div>
                  <div class="col-md-8">
                    <strong>¿En que inmueble vive?:</strong> { vive.length < 0 ? 'No vive aca':
                      vive.map(item=>{
                        return(
                            <div>
                              <p>{item.tipo_unidad.nombre_res} - {item.numero} </p>
                            </div>
                        )

                      })}
                  </div>
                  <div class="col-md-12">
                    <strong>N° de personas que habitan en el inmueble:</strong> {detalles.numero_per}
                  </div>
                  <div class="col-md-8">
                    <strong>Vehiculo:</strong> {detalles.vehiculo == 3 ? 'No tiene vehiculo': detalles.vehiculo == 1 ? 'Carro' : detalles.vehiculo == 2 ? 'Moto' : null}
                  </div>
                  {
                    detalles.vehiculo == 3 ?
                    <div></div>
                    :
                    <div className="row">
                      <div class="col-md-8">
                        <strong>Marca:</strong> {detalles.marca == null ? 'No tiene marca': detalles.marca}
                      </div>
                      <div class="col-md-8">
                        <strong>Placa:</strong> {detalles.placa == null ? 'No tiene placa': detalles.placa}
                      </div>
                      <div class="col-md-8">
                        <strong>Color:</strong> {detalles.color == null ? 'No tiene' : detalles.color}
                      </div>

                  </div>
                  }


                </div>
                </div>

                : detalles.tip_prop == 2 ?

                <div className="container" style={{marginLeft: '-11px'}}>
                  <div class="row">
                  <div class="col-md-8">
                    <strong>¿En que inmueble vive?:</strong> { vive.length > 0 ?
                      vive.map(item=>{
                        return(
                            <div>
                              <p>{item.tipo_unidad.nombre_res} - {item.numero} </p>
                            </div>
                        )

                      })
                      : null
                    }
                  </div>
                  <div class="col-md-12">
                    <strong>N° de personas que habitan en el inmueble:</strong> {detalles.numero_per}
                  </div>
                  <div class="col-md-8">
                    <strong>Vehiculo:</strong> {detalles.vehiculo == 3 ? 'No tiene vehiculo': detalles.vehiculo == 1 ? 'Carro' : detalles.vehiculo == 2 ? 'Moto' : null}
                  </div>

            {
              detalles.vehiculo == 3 ?
              <div></div>
              :
              <div className="row">
                <div class="col-md-8">
                  <strong>Marca:</strong> {detalles.marca == null ? 'No tiene marca': detalles.marca}
                </div>
                <div class="col-md-8">
                  <strong>Placa:</strong> {detalles.placa == null ? 'No tiene placa': detalles.placa}
                </div>
                <div class="col-md-8">
                  <strong>Color:</strong> {detalles.color == null ? 'No tiene' : detalles.color}
                </div>
              </div>
            }
                </div>
                </div>

                :

                <div className="container" style={{marginLeft: '-16px'}}>
                  <strong>Vive en el inmueble:</strong> No
                </div>
              }
            </div>
             </Modal.Body>
             <Modal.Footer>
              <Button variant="danger" onClick={handleClose}>Cerrar</Button>
            </Modal.Footer>
         </Modal>
      </div>
    </div>
      :null
    }

    { showForm == 2 ?
      <div style={{marginLeft:40}}>
          <NewPropietarios data={data} torre={torre} gobackSave = {()=>gobackSave()} goback = {()=>goback()}  id = {id}  />
      </div>
      :null
    }

</div>
);

}
export default Propietarios;
if (document.getElementById('Propietarios')) {
  ReactDOM.render(<Propietarios />, document.getElementById('Propietarios'))
}
