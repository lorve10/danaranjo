import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import Select from 'react-select'
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2'
import '../../../../css/app.scss';
import makeAnimated from 'react-select/animated'
import { Divider } from '@material-ui/core';
import { CompareArrowsTwoTone } from '@material-ui/icons';

function NewTerceros (props) {
    const [showForm, setShowForm] = useState(1)
    const [loading, setLoading] = useState(false);
    const [text, setText] = useState('');
    const [numeroDoc, setNumeroDoc] = useState('');///
    const [nit, setNit ] = useState('');
    const [expedition, setExpedition] = useState('');///
    const [name, setName] = useState('');//
    const [seconame, setSecoName] = useState('');//
    const [lastname, setLastname] = useState('');//
    const [secolastname, setSecolastname] = useState('');//
    const [addres, setAddres] = useState('')//
    const [city, setCity] = useState('')//
    const [phone, setPhone] = useState('')//telefono
    const [phoneRep, setPhoneRep] = useState('')
    const [phone2, setPhone2] = useState('')//celular
    const [email, setEmail] = useState('')//correo
    const [emailRep, setEmailRep] = useState('')//correo del representante legal
    const [local, setLocal] = useState('')//nombre del centro comercial o local
    const [direEmpresa, setDireEmpresa] = useState('')//
    const [sigla, setSigla] = useState('')
    const [type, setType] = useState(null);//tipo persona juridica o Natural
    const [contra, setContra] = useState(null)//tipo de Arrendatario
    const [documento, setDocumento] = useState(null);//
    const [bank, setBank] = useState(null)
    const [bankSelect, setBankSelect] = useState([])
    const [pais, setPais] = useState(null)
    const [paisSelect, setPaisSelect] = useState([])
    const [actividad, setActividad] = useState(null)
    const [actividadSelect, setActividadSelect] = useState([])
    const [fiscal, setFiscal] = useState(null)
    const [fiscalSelect, setFiscalSelect] = useState([])
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
    const [cuenta, setCuenta] = useState(null);
    const [tipoRegimen, setTipoRegimen] = useState(null);
    const [cuentaBanco, setCuentaBanco] = useState('');
    const [nomTitular, setNomTitular] = useState('');
    const [nomrepre, setNomRepre] = useState('');
    const [nomcom, setNomCom] = useState('');

    const [cuentaList, setCuentaList] = useState([
      {
        value:'1',
        label:'Cuenta de ahorros',
      },
      {
        value:'2',
        label:'Cuenta corriente',
      }
    ]);

    const [tipoRegimenList, setTipoRegimenList] = useState([
      {
        value:'1',
        label:'Régimen simple',
      },
      {
        value:'2',
        label:'Régimen especial',
      },
      {
        value:'3',
        label:'Contributiva iva',
      },
      {
        value:'4',
        label:'No contributiva iva',
      },
      {
        value:'5',
        label:'Grandes contribuyentes',
      },
    ]);

    const [typeList, setTypeList] = useState([
      {
    value:'1',
    label:'Jurídica',
    },
    {
    value:'2',
    label:'Natural',
    }
  ]);

  const [docuList, setDocuList] = useState([
    {
    value:'1',
    label:'Cédula',
    },
    {
    value:'2',
    label:'Cédula Extranjera',
    },
    {
    value:'3',
    label:'Pasaporte ',
    },
    {
    value:'4',
    label:'otros ',
    }

  ]);
    const [contraList, setContraList] = useState([
      {
    value:'1',
    label:'Proveedor',
    },
    {
    value:'2',
    label:'Contratista',
    },
    {
    value:'3',
    label:'Cliente'
    }
  ]);
  const [departamentlist, setDepartamentlist] = useState(null)
  const [departamentSelect,  setDepartamentSelect] = useState([])
  const [municipiolist, setMunicipiolist] = useState(null)
  const [municipioSelect,  setMunicipioSelect] = useState([])

  useEffect(()=>{
    validar()
    obtain_departamento()
    obtain_bank()
    obtain_pais()
    obtain_actividad()
    obtain_fiscal()
  },[]);

  const validar = async () =>{
    if(props.id > 0 ){
    console.log(props.data.tip_per);
    if(props.data.tip_per == 2){
      setType({
            value:'2',
            label:'Natural',
          })
      setShowForm(2)
    }
    if(props.data.tip_per == 1){
      setType({
            value:'1',
            label:'Jurídica',
          })
      setShowForm(3)
    }
    if(props.data.contra == 3){
      setContra({
        value:'3',
        label:'Cliente'
      })
    }
    if(props.data.contra == 2){
      setContra({
        value:'2',
        label:'Contratista'
      })
    }
    if(props.data.contra == 1){
      setContra({
        value:'1',
        label:'Proovedor'
      })
    }
    if(props.data.tip_ident == 1){
      setDocumento({
        value:'1',
        label:'Cedula'
      })
    }
    if(props.data.tip_ident == 2){
      setDocumento({
        value:'2',
        label:'Cedula Extranjera'
      })
    }
    if(props.data.tip_ident == 3){
      setDocumento({
        value:'3',
        label:'Pasaporte '
      })
    }
    if(props.data.tip_ident == 4){
      setDocumento({
        value:'4',
        label:'otros'
      })
    }

    if(props.data.tipo_cuenta == 1){
      setCuenta({
        value:'1',
        label:'Cuenta de ahorros'
      })
    }

    if(props.data.tipo_cuenta == 2){
      setCuenta({
        value:'2',
        label:'Cuenta corriente'
      })
    }

    if(props.data.regimen == 1){
      setTipoRegimen({
        value:'1',
        label:'Natural'
      })
    }

    if(props.data.regimen == 2){
      setTipoRegimen({
        value:'2',
        label:'Jurídica'
      })
    }

    if(props.data.regimen == 3){
      setTipoRegimen({
        value:'3',
        label:'Contributivo iva'
      })
    }

    if(props.data.regimen == 4){
      setTipoRegimen({
        value:'4',
        label:'No contributivo iva'
      })
    }

    if(props.data.regimen == 5){
      setTipoRegimen({
        value:'5',
        label:'Grandes contribuyentes'
      })
    }


      setExpedition(props.data.expedido)
      setName(props.data.name)
      setSecoName(props.data.secname == null ? '' : props.data.secname)
      setLastname(props.data.lastname)
      setSecolastname(props.data.seclastname)
      setNomRepre(props.data.nom_representante)
      setNomCom(props.data.nom_comercial)
      setAddres(props.data.addres)
      setCity(props.data.city)
      setEmail(props.data.correo)
      setEmailRep(props.data.correo_representante)
      setPhone(props.data.phone)
      setPhoneRep(props.data.phone_representante)
      setPhone2(props.data.mobile)
      setDireEmpresa(props.data.direlocal)
      setNit(props.data.nit)
      setNumeroDoc(props.data.n_documet)
      setCuentaBanco(props.data.cuenta_bancaria)
      setNomTitular(props.data.nombre_titular)
    }
  }

  const obtain_departamento = async () => {
    axios.get(ip+'admon/departament').then(response=>{
      var res = response.data
      var departamento = [];
      res.map(item=>{
        const data = {
          value:item.id_departamento,
          label:item.departamento
        }
        departamento.push(data)
        setDepartamentlist(departamento)
      })
      var filtro = res.filter(e=>e.id_departamento == props.data.departamento);
      var depar = []
      filtro.map(item=>{
        const data = {
          value:item.id_departamento,
          label:item.departamento
        }
        obtain_municipio(item.id_departamento, false)
        depar.push(data)
        setDepartamentSelect(data)
      })

    })
  }

  const obtain_municipio = async (data, value) => {
        setMunicipioSelect([])
        axios.get(ip+'admon/municipio').then(response=>{
          var res = response.data
          var municipio =[]
          var filtro = response.data.filter(e=>e.departamento_id == data);
          var filtro2 = filtro.filter(e=>e.id_municipio == props.data.municipio);
          filtro.map(item=>{
            const data = {
              value:item.id_municipio,
              label:item.municipio
            }
            municipio.push(data)
          })
          setMunicipiolist(municipio)

          filtro2.map(item=>{
            const data = {
              value:item.id_municipio,
              label:item.municipio
            }
            setMunicipioSelect(data)
          })

        })
  }

  const obtain_bank = async () => {
    axios.get(ip+'admon/bank').then(response=>{
      var res = response.data
      var banco = []
      res.map(item=>{
        const data = {
          value:item.bank_id,
          label:item.bank_name
        }
        banco.push(data)
      })
      setBank(banco)
      if(props.id > 0){
        var filterbanco = res.filter(e=>e.bank_id == props.data.banco)
        console.log("Aqui filter banco")
        console.log(filterbanco)
        filterbanco.map(item=>{
          const databanco = {
            value:item.bank_id,
            label:item.bank_name
          }
          setBankSelect(databanco)
        })
      }
    })
  }

  const obtain_pais = async () => {
    axios.get(ip+'admon/pais').then(response=>{
      var res = response.data
      var pais = []
      res.map(item=>{
        const data = {
          value:item.id,
          label:item.paisnombre
        }
        pais.push(data)
      })
      setPais(pais)
      if(props.id > 0){
        var filterpais = res.filter(e=>e.id == props.data.pais)
        filterpais.map(item=>{
          const datapais = {
            value:item.id,
            label:item.paisnombre
          }
          setPaisSelect(datapais)
        })
      }
    })
  }

  const obtain_actividad = async () => {
    axios.get(ip+'admon/actividadEconomica').then(response=>{
      var res = response.data
      var actividad = []
      res.map(item=>{
        const data = {
          value:item.codigo,
          label:item.codigo + " - " + item.nombre
        }
        actividad.push(data)
      })
      setActividad(actividad)
      if(props.id > 0){
        var filteractividad = res.filter(e=>e.codigo == props.data.actividad_economica)
        console.log("Aqui filter actividad")
        console.log(filteractividad)
        filteractividad.map(item=>{
          const dataactividad = {
            value:item.codigo,
            label:item.codigo + " - " + item.nombre
          }
          setActividadSelect(dataactividad)
        })
      }
    })
  }

  const obtain_fiscal = async () => {
    axios.get(ip+'admon/ResponsabilidadFiscal').then(response=>{
      var res = response.data
      var fiscal = []
      res.map(item=>{
        const data = {
          value:item.id,
          label:item.id + " - " + item.nombre_res
        }
        fiscal.push(data)
      })
      setFiscal(fiscal)
      if(props.id > 0){
        var filterfiscal = res.filter(e=>e.id == props.data.res_fiscal)
        console.log("Aqui filter actividad")
        console.log(filterfiscal)
        filterfiscal.map(item=>{
          const datafiscal = {
            value:item.id,
            label:item.id + " - " + item.nombre_res
          }
          setFiscalSelect(datafiscal)
        })
      }
    })
  }

  const departame = (value)=>{
      setDepartamentSelect(value);
      obtain_municipio(value.value, true)
  }


  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const validateEmail = async (data) => {
    var regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    console.log("validacion email");
    console.log(regex.test(data));
    return regex.test(data);

  }

 const save_per = async () => {
   var message = ''
   var error = false
   var res = await validateEmail(email)

  if(showForm == 2){
    var casa = 0;

    if(contra == null ){
       error = true
       message = "Por favor escoje un tipo de tercero"
    }
    else if(documento == null) {
      error = true
      message = "Escoje tipo de documento"
    }
    else if(numeroDoc == '') {
      error = true
      message = "Escribe el número de documento de la persona"
    }
    else if(expedition == '') {
      error = true
      message = "Escribe lugar donde fue expedido"
    }
    else if(name == '') {
      error = true
      message = "Escribe primer nombre"
    }
    else if(lastname == '') {
      error = true
      message = "Escribe primer apellido"
    }
    else if(secolastname == '') {
      error = true
      message = "Escribe segundo apellido"
    }
    else if(paisSelect == ''){
      error = true
      message = "Seleccione país"
    }
    else if(departamentSelect == ''){
      error = true
      message = "Seleccione departamento"
    }
    else if(municipioSelect == ''){
      error = true
      message = "Seleccione municipio"
    }
    else if(addres == '') {
      error = true
      message = "Escribe Direccion"
    }
    else if(res == false) {
      error = true
      message = "El correo no es correcto o está vacío"
    }
    else if(phone2 == '') {
      error = true
      message = "Escribe número de celular"
    }
    else if(bankSelect == ''){
      error = true
      message = "Seleccione banco"
    }
    else if(cuenta == null){
      error = true
      message = "Seleccione tipo de cuenta"
    }
    else if(cuentaBanco == ''){
      error = true
      message = "Digite la cuenta bancaria"
    }
    else if(nomTitular == ''){
      error = true
      message = "Escribe el titular de la cuenta"
    }
    else if(tipoRegimen == null){
      error = true 
      message = "Elige tipo de régimen"
    }
    else if(actividadSelect == ''){
      error = true
      message = "Elige el tipo de actividad económica"
    }
    else if(fiscalSelect == ''){
      error = true
      message = "Elige responsabilidad fiscal"
    }
  }
  else if(showForm == 3){
    var casa = nit;

    if(contra == null){
      error = true
      message = "Por favor escoje un tipo de terceros"
    }
    else if(nit == '') {
      error = true
      message = "Escribe el nit de la empresa"
    }
    else if(name == '') {
      error = true
      message = "Escribe la razón social"
    }
    else if(nomcom == ''){
      error = true
      message = "Escribe el nombre comercial"
    }
    else if(paisSelect == ''){
      error = true
      message = "Seleccione país"
    }
    else if(departamentSelect == ''){
      error = true
      message = "Seleccione departamento"
    }
    else if(municipioSelect == ''){
      error = true
      message = "Seleccione municipio"
    }
    else if(addres == '') {
      error = true
      message = "Escribe dirección"
    }
    else if(res == false) {
      error = true
      message = "El correo no es correcto o está vacío"
    }
    else if(phone2 == '') {
      error = true
      message = "Escribe número de celular"
    }
    else if(bankSelect == ''){
      error = true
      message = "Seleccione banco"
    }
    else if(cuenta == null){
      error = true
      message = "Seleccione tipo de cuenta"
    }
    else if(cuentaBanco == ''){
      error = true
      message = "Digite la cuenta bancaria"
    }
    else if(nomTitular == ''){
      error = true
      message = "Escribe el titular de la cuenta"
    }
    else if(nomrepre == ''){
      error = true
      message = "Escribe el nombre del representante legal"
    }
    else if(emailRep == ''){
      error = true
      message = "El correo del representante legal no es correcto o está vacío"
    }
    else if(phoneRep == ''){
      error = true
      message = "Escribe el número celular del representante legal"
    }
    else if(tipoRegimen == null){
      error = true 
      message = "Elige tipo de régimen"
    }
    else if(actividadSelect == ''){
      error = true
      message = "Elige el tipo de actividad económica"
    }
    else if(fiscalSelect == ''){
      error = true
      message = "Elige responsabilidad fiscal"
    }
    
  }
  if (error) {
    MessageError(message)
  }
  else{
    const data = new FormData()
    data.append('id', props.id)
    data.append('tip_per',type.value)
    data.append('tip_ident',documento.value)
    data.append('N_documet',numeroDoc)
    data.append('nit' , casa)
    data.append('name',name)
    data.append('nom_comercial' ,nomcom)
    data.append('secname',seconame)
    data.append('lastname',lastname)
    data.append('seclastname',secolastname)
    data.append('pais', paisSelect.value)
    data.append('departamento', departamentSelect.value)
    data.append('municipio', municipioSelect.value)
    data.append('expedido',expedition)
    data.append('addres',addres)
    data.append('city',city)
    data.append('phone',phone)
    data.append('mobile',phone2)
    data.append('correo',email)
    data.append('banco', bankSelect.value)
    data.append('tipo_cuenta', cuenta.value)
    data.append('cuenta_bancaria', cuentaBanco)
    data.append('nombre_titular', nomTitular)
    data.append('nom_representante' ,nomrepre)
    data.append('correo_representante' ,emailRep)
    data.append('phone_representante' ,phoneRep)
    data.append('regimen', tipoRegimen.value)
    data.append('actividad_economica', actividadSelect.value)
    data.append('res_fiscal', fiscalSelect.value)
    data.append('sigla', sigla)
    data.append('contra', contra.value)
    axios.post(ip+'admon/guard_person',data).then(response=>{
      if(response.data == 2){
        MessageError("Ya hay una persona con el mismo número de cédula");
      }else{
        if (props.id == null) {
        MessageSuccess("Tipo persona creada correctamente")
          props.gobackSave()
        }
        else{
        MessageSuccess("Tipo persona editada correctamente")
          props.gobackSave()

        }
      }
    })
  }

}

    const tipo_per = (e)=>{
      if(e.value == 1){
        setShowForm(3)
        var casa = [];
        const data ={
          value:'4',
          label:'otros'
        }
        setDocumento(data)
        const data2 ={
          value:'1',
          label:'Jurídica'
        }
        setType(data2)
      }
      if(e.value == 2 ){
        setShowForm(2)
        const data ={
          value:'2',
          label:'Natural'
        }
        setType(data)
      }
    }

    const solo = async (e) =>{
      if(e.target.value.length > 9){
        MessageError("Solo se permiten 9 digitos")
      }
      else{
      await  setNumeroDoc(e.target.value)
      }

    }
    const nits = async (valor) =>{
      console.log(valor.target.value);
      if(valor.target.value.length > 1){
        MessageError("Solo se permite 1 digito")
      }
      else{
      await  setNit(valor.target.value)
      }

    }
    return(
      <div className="mt-2 col-12 mt-5" >
      <div className="btn container col-1 " onClick = {()=>props.goback()}>
      <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
      </div>
        <div  className="form-group">
          <div className="row col-12">

          {
            props.id > 0 ?
            <div></div>
            :
            <div className= "row col-12 mt-2">
            <div className = "col-md-6 d-flex">
                <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '230px'}}>Selecciona tipo de persona: </label>
                <Select
                className="col-7"
                value = {type}
                components={animatedComponents}
                options={typeList}
                onChange={(e)=>tipo_per(e)}
                placeholder = "Seleccione tipo de persona"
                name="colors"
                />
            </div>
            </div>
          }
        {
          showForm == 2?
          <div className="row col-12 mt-2">
          <div className = "col-md-6 d-flex">
              <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '230px'}}>Seleccione tipo de tercero: </label>
              <Select
              className="col-7"
              value={contra}
              components={animatedComponents}
              options={contraList}
              onChange={(e)=>setContra(e)}
              placeholder = "Seleccione tipo de tercero"
              name="colors"
              />
          </div>

          <Divider className="col-md-12 my-2" style={{backgroundColor:'#d9d9d9', marginLeft: '13px', width: '585px', height:'2px'}} />

          <div className="my-2 d-flex col-md-9">
            <h5 className="my-2 mr-3">Información básica</h5>
          </div>

          {
            props.id > 0 ?

            <div style={{marginLeft: '-8px'}}>
            <div className="col-md-9 d-flex ">
            <label className="d-flex col-6 align-items-center mr-2 color select-ter" style={{width: '200px'}}>Tipo de documento: </label>
            <Select
            isDisabled
            className="col-5"
            value={documento}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={docuList}
            onChange={(e)=>setDocumento(e)}
            placeholder = "Seleccionar tipo documento"
            name="colors"
            />
            </div>
            <div className = "col-md-9 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '200px'}}
                >Número Documento: </label>
                <input className="form-control col-8" disabled type="number" value = {numeroDoc} placeholder="Número de documento" onChange = {(e)=>setNumeroDoc(e.target.value)} style={{width: '335px'}}/>
            </div>
            </div>
            :
            <div style={{marginLeft: '-8px'}}>
            <div className="col-md-9 d-flex ">
            <label className="d-flex col-6 align-items-center mr-2 color select-ter" style={{width: '200px'}}>Tipo de documento: </label>
            <Select
            className="col-5"
            value={documento}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={docuList}
            onChange={(e)=>setDocumento(e)}
            placeholder = "Seleccionar tipo documento"
            name="colors"
            />
            </div>
            <div className = "col-md-9 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '200px'}}
                >Número Documento: </label>
                <input className="form-control col-8" type="number" value = {numeroDoc} placeholder="Número de documento" onChange = {(e)=>setNumeroDoc(e.target.value)} style={{width: '335px'}}/>
            </div>
            </div>
          }
          
          <div className = "col-md-9 d-flex">
              <label className="d-flex col-6 color mr-3" style={{width: '200px'}}>Expedido en: </label>
              <input className="form-control col-7" style={{width: '335px'}} type="text"   value = {expedition} placeholder="Expedido en" onChange = {(e)=>setExpedition(e.target.value)} />
          </div>
          <div className = "col-md-9 d-flex">
              <label className="d-flex col-6 color mr-3" style={{width: '200px'}}> Primer nombre: </label>
              <input className="form-control col-7" style={{width: '335px'}} type="text"  value = {name} placeholder="Primer nombre" onChange = {(e)=>setName(e.target.value)}  ç/>
          </div>
          <div className = "col-md-9 d-flex">
              <label className="d-flex col-6 color mr-3" style={{width: '200px'}}>Segundo nombre: </label>
              <input className="form-control col-7" style={{width: '335px'}} type="text"  value = {seconame} placeholder="Segundo nombre" onChange = {(e)=>setSecoName(e.target.value)} />
          </div>
          <div className = "col-md-9 d-flex">
              <label className="d-flex col-6 color mr-3" style={{width: '200px'}}>Primer apellido: </label>
              <input className="form-control col-7" style={{width: '335px'}} type="text"  value = {lastname} placeholder="Primer apellido"  onChange = {(e)=>setLastname(e.target.value)} />
          </div>
          <div className = "col-md-9 d-flex">
              <label className="d-flex col-6 color mr-3" style={{width: '200px'}}>Segundo  apellido: </label>
              <input className="form-control col-7" style={{width: '335px'}} type="text"  value = {secolastname} placeholder="Segundo apellido" onChange = {(e)=>setSecolastname(e.target.value)}  />
          </div>

          <div className="my-2 d-flex col-md-9">
            <h5 className="my-2 mr-3">Información de ubicación</h5>
          </div>

          <div className="col-md-9 d-flex">
          <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '200px'}}>País: </label>
          <Select
          className="col-md-5"
          value={paisSelect}
          components={animatedComponents}
          options={pais}
          onChange={(e)=>setPaisSelect(e)}
          placeholder = "Seleccione el país"
          />
          </div>

          <div className="col-md-9 d-flex">
          <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '200px'}}>Departamento: </label>
          <Select
          className="col-md-5"
          value={departamentSelect}
          components={animatedComponents}
          options={departamentlist}
          onChange={(e)=>departame(e)}
          placeholder = "Seleccione el departamento"
          />
          </div>

          <div className="col-md-9 d-flex">
          <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '200px'}}>Ciudad: </label>
          <Select
          className="col-md-5"
          value={municipioSelect}
          closeMenuOnSelect={true}
          components={animatedComponents}
          options={municipiolist}
          onChange={(e)=>setMunicipioSelect(e)}
          placeholder = "Seleccione el municipio"
          name="colors"
          />
          </div>

          <div className = "col-md-9 d-flex">
              <label className="d-flex col-6 color mr-3" style={{width: '200px'}}>Dirección residencia: </label>
              <input className="form-control col-7" style={{width: '335px'}} type="text"  value = {addres} placeholder="Dirección residencia" onChange = {(e)=>setAddres(e.target.value)} />
          </div>

          <div className="my-2 d-flex col-md-9">
            <h5 className="my-2 mr-3">Información de contacto</h5>
          </div>

          <div className = "col-md-9 d-flex">
              <label className="d-flex col-6 color mr-3" style={{width: '200px'}}>Correo: </label>
              <input className="form-control col-7" style={{width: '335px'}} type="email" value = {email} placeholder="Correo " onChange = {(e)=>setEmail(e.target.value)}  />
          </div>
          <div className = "col-md-9 d-flex">
              <label className="d-flex col-6 color mr-3" style={{width: '200px'}}>Teléfono: </label>
              <input className="form-control col-7" style={{width: '335px'}} type="number"  value = {phone} placeholder="Teléfono " onChange = {(e)=>setPhone(e.target.value)} />
          </div>
          <div className = "col-md-9 d-flex">
              <label className="d-flex col-6 color mr-3" style={{width: '200px'}}>Teléfono Celular: </label>
              <input className="form-control col-7" style={{width: '335px'}} type="number" value = {phone2} placeholder="Teléfono Celular" onChange = {(e)=>setPhone2(e.target.value)}  />
          </div>

          <div className="my-2 d-flex col-md-9">
            <h5 className="my-2 mr-3">Información bancaria</h5>
          </div>

          <div className="col-md-9 d-flex">
          <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '200px'}}>Banco: </label>
          <Select
          className="col-md-5"
          value={bankSelect}
          closeMenuOnSelect={true}
          components={animatedComponents}
          options={bank}
          onChange={(e)=>setBankSelect(e)}
          placeholder = "Seleccione el banco"
          name="colors"
          />
          </div>

          <div className="col-md-9 d-flex">
          <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '200px'}}>Tipo de cuenta: </label>
          <Select
          className="col-md-5"
          value={cuenta}
          closeMenuOnSelect={true}
          components={animatedComponents}
          options={cuentaList}
          onChange={(e)=>setCuenta(e)}
          placeholder = "Seleccione el tipo de cuenta"
          name="colors"
          />
          </div>

          <div className = "col-md-9 d-flex">
              <label className="d-flex col-6 color mr-3" style={{width: '200px'}}>Cuenta bancaria: </label>
              <input className="form-control col-7" style={{width: '335px'}} type="number"  value = {cuentaBanco} placeholder="Ingrese cuenta bancaria " onChange = {(e)=>setCuentaBanco(e.target.value)} />
          </div>
          <div className = "col-md-9 d-flex">
              <label className="d-flex col-6 color mr-3" style={{width: '200px'}}>Nombre del titular: </label>
              <input className="form-control col-7" style={{width: '335px'}} type="text" value = {nomTitular} placeholder="Ingrese nombre del titular" onChange = {(e)=>setNomTitular(e.target.value)}  />
          </div>

          <div className="my-2 d-flex col-md-9">
            <h5 className="my-2 mr-3">Información adicional</h5>
          </div>

          <div className="col-md-9 d-flex">
          <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '200px'}}>Tipo de régimen: </label>
          <Select
          className="col-md-5"
          value={tipoRegimen}
          closeMenuOnSelect={true}
          components={animatedComponents}
          options={tipoRegimenList}
          onChange={(e)=>setTipoRegimen(e)}
          placeholder = "Seleccione tipo de régimen"
          name="colors"
          />
          </div>

          <div className="col-md-9 d-flex">
          <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '200px'}}>Actividad económica: </label>
          <Select
          className="col-md-5"
          value={actividadSelect}
          closeMenuOnSelect={true}
          components={animatedComponents}
          options={actividad}
          onChange={(e)=>setActividadSelect(e)}
          placeholder = "Seleccione actividad económica"
          name="colors"
          />
          </div>

          <div className="col-md-9 d-flex">
          <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '200px'}}>Responsabilidad fiscal: </label>
          <Select
          className="col-md-5"
          value={fiscalSelect}
          closeMenuOnSelect={true}
          components={animatedComponents}
          options={fiscal}
          onChange={(e)=>setFiscalSelect(e)}
          placeholder = "Seleccione responsabilidad fiscal"
          name="colors"
          />
          </div>

          <div className="col-md-4 justify-content-end " style={{marginTop: '10px'}}>
            <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}} onClick = {()=>save_per()}>Guardar tercero</button>
          </div>
          </div>

          :showForm == 3?
          <div className="row col-12 mt-2">
          <div className = "col-md-6 d-flex">
              <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '230px'}}>Seleccione tipo de tercero: </label>
              <Select
              className="col-7"
              value={contra}
              components={animatedComponents}
              options={contraList}
              onChange={(e)=>setContra(e)}
              placeholder = "Seleccione tipo de tercero"
              name="colors"
              />
          </div>

          <Divider className="col-md-12 my-2" style={{backgroundColor:'#d9d9d9', marginLeft: '13px', width: '585px', height:'2px'}} />

          <div className="my-2 d-flex col-md-9">
            <h5 className="my-2 mr-3">Información básica juridica</h5>
          </div>

            <div className = "col-md-9 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '260px'}}> Nit:</label>
                <input className="form-control col-7" style={{width: '257px'}}
                  type="number"
                  value = {numeroDoc}
                  placeholder="Nit empresa"
                  onChange = {(e)=>solo(e)} />
                  <p className=""> _ </p>
                  <input className="d-flex form-control col-2" style={{width: '71px'}} value={nit} onChange={(e)=>nits(e)}  maxLength={1} type="number"  placeholder="data" />
            </div>
            <div className = "col-md-9 d-flex">
                <label className = "d-flex col-6 color mr-3" style={{width: '260px'}}>Razón social:</label>
                <input className="form-control col-7" type="text" style={{width: '335px'}} value = {name} placeholder="Razón social" onChange = {(e)=>setName(e.target.value)} />
            </div>
            <div className = "col-md-9 d-flex">
                <label className = "d-flex col-6 color mr-3" style={{width: '260px'}}>Nombre comercial:</label>
                <input className="form-control col-7" type="text" style={{width: '335px'}} value = {nomcom} placeholder="Nombre comercial" onChange = {(e)=>setNomCom(e.target.value)} />
            </div>

            <div className="my-2 d-flex col-md-9">
              <h5 className="my-2 mr-3">Información de ubicación</h5>
            </div>

            <div className="col-md-9 d-flex">
            <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '260px'}}>País: </label>
            <Select
            className="col-md-5"
            value={paisSelect}
            components={animatedComponents}
            options={pais}
            onChange={(e)=>setPaisSelect(e)}
            placeholder = "Seleccione el país"
            />
            </div>

            <div className="col-md-9 d-flex">
            <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '260px'}}>Departamento: </label>
            <Select
            className="col-md-5"
            value={departamentSelect}
            components={animatedComponents}
            options={departamentlist}
            onChange={(e)=>departame(e)}
            placeholder = "Seleccione el departamento"
            />
            </div>

            <div className="col-md-9 d-flex">
            <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '260px'}}>Ciudad: </label>
            <Select
            className="col-md-5"
            value={municipioSelect}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={municipiolist}
            onChange={(e)=>setMunicipioSelect(e)}
            placeholder = "Seleccione el municipio"
            name="colors"
            />
            </div>

            <div className = "col-md-9 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '260px'}}>Dirección de la empresa: </label>
                <input className="form-control col-7" style={{width: '335px'}} type="text"  value = {addres} placeholder="Dirección de la empresa" onChange = {(e)=>setAddres(e.target.value)} />
            </div>

            <div className="my-2 d-flex col-md-9">
              <h5 className="my-2 mr-3">Información de contacto</h5>
            </div>

            <div className = "col-md-9 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '260px'}}>Correo: </label>
                <input className="form-control col-7" style={{width: '335px'}} type="email" value = {email} placeholder="Correo " onChange = {(e)=>setEmail(e.target.value)}  />
            </div>
            <div className = "col-md-9 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '260px'}}>Teléfono: </label>
                <input className="form-control col-7" style={{width: '335px'}} type="number"  value = {phone} placeholder="Teléfono " onChange = {(e)=>setPhone(e.target.value)} />
            </div>
            <div className = "col-md-9 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '260px'}}>Teléfono Celular: </label>
                <input className="form-control col-7" style={{width: '335px'}} type="number" value = {phone2} placeholder="Teléfono Celular" onChange = {(e)=>setPhone2(e.target.value)}  />
            </div>

            <div className="my-2 d-flex col-md-9">
              <h5 className="my-2 mr-3">Información bancaria</h5>
            </div>

            <div className="col-md-9 d-flex">
            <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '260px'}}>Banco: </label>
            <Select
            className="col-md-5"
            value={bankSelect}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={bank}
            onChange={(e)=>setBankSelect(e)}
            placeholder = "Seleccione el banco"
            name="colors"
            />
            </div>

            <div className="col-md-9 d-flex">
            <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '260px'}}>Tipo de cuenta: </label>
            <Select
            className="col-md-5"
            value={cuenta}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={cuentaList}
            onChange={(e)=>setCuenta(e)}
            placeholder = "Seleccione el tipo de cuenta"
            name="colors"
            />
            </div>

            <div className = "col-md-9 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '260px'}}>Cuenta bancaria: </label>
                <input className="form-control col-7" style={{width: '335px'}} type="number"  value = {cuentaBanco} placeholder="Ingrese cuenta bancaria " onChange = {(e)=>setCuentaBanco(e.target.value)} />
            </div>
            <div className = "col-md-9 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '260px'}}>Nombre del titular: </label>
                <input className="form-control col-7" style={{width: '335px'}} type="text" value = {nomTitular} placeholder="Ingrese nombre del titular" onChange = {(e)=>setNomTitular(e.target.value)}  />
            </div>

            <div className="my-2 d-flex col-md-9">
              <h5 className="my-2 mr-3">Información del representante legal</h5>
            </div>

            <div className = "col-md-9 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '260px'}}>Nombre del representante legal: </label>
                <input className="form-control col-7" type="text" style={{width: '335px'}}  value={nomrepre} placeholder=" Nombre del representante legal" onChange = {(e)=>setNomRepre(e.target.value)} />
            </div>

            <div className = "col-md-9 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '260px'}}>Correo: </label>
                <input className="form-control col-7" style={{width: '335px'}} type="email" value = {emailRep} placeholder="Correo " onChange = {(e)=>setEmailRep(e.target.value)}  />
            </div>
            <div className = "col-md-9 d-flex">
                <label className="d-flex col-6 color mr-3" style={{width: '260px'}}>Teléfono: </label>
                <input className="form-control col-7" style={{width: '335px'}} type="number"  value = {phoneRep} placeholder="Teléfono " onChange = {(e)=>setPhoneRep(e.target.value)} />
            </div>

            <div className="my-2 d-flex col-md-9">
              <h5 className="my-2 mr-3">Información adicional</h5>
            </div>

            <div className="col-md-9 d-flex">
            <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '260px'}}>Tipo de régimen: </label>
            <Select
            className="col-md-5"
            value={tipoRegimen}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={tipoRegimenList}
            onChange={(e)=>setTipoRegimen(e)}
            placeholder = "Seleccione tipo de régimen"
            name="colors"
            />
            </div>

            <div className="col-md-9 d-flex">
            <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '260px'}}>Actividad económica: </label>
            <Select
            className="col-md-5"
            value={actividadSelect}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={actividad}
            onChange={(e)=>setActividadSelect(e)}
            placeholder = "Seleccione actividad económica"
            name="colors"
            />
            </div>

            <div className="col-md-9 d-flex">
            <label className="d-flex col-6 mr-2 align-items-center color" style={{width: '260px'}}>Responsabilidad fiscal: </label>
            <Select
            className="col-md-5"
            value={fiscalSelect}
            closeMenuOnSelect={true}
            components={animatedComponents}
            options={fiscal}
            onChange={(e)=>setFiscalSelect(e)}
            placeholder = "Seleccione responsabilidad fiscal"
            name="colors"
            />
            </div>
            
            <div className="col-md-4 justify-content-end" style={{marginTop: '10px'}}>
              <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}} onClick = {()=>save_per()}>Guardar tercero</button>
            </div>
            </div>

          :null
        }
        </div>

      </div>
    </div>


    );
}

export default NewTerceros;
if(document.getElementById('NewTerceros')) {
    ReactDOM.render(<NewTerceros />, document.getElementById('NewTerceros'));
}
