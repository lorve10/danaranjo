import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import NewTerceros from './NewTerceros';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2';
import Select from 'react-select'
import Header from '../../Ui/Header';
import makeAnimated from 'react-select/animated'
import Pagination from '../../pagination/Pagination'
import PaginationButton from '../../pagination/Pagination-button'


function Terceros() {
  const [loading, setLoading] = useState(false);
  const [text, setText] = useState('')
  const [id, setId] = useState(null)
  const [showForm, setShowForm] = useState(1)
  const [data, setData] = useState({})
  const [person, setPerson] = useState([])
  const [backTerce, setBackTerce] = useState([])
  const [backTerce2, setBackTerce2] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [marginval, setMarginVal] = useState(false)
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
  const [type, setType] = useState(null)
  const [typeTer, setTypeTer] = useState(null)
  const [filtro, setFiltro] = useState([])

  const [tipo, setTipo] = useState([
    {
    value:'3',
    label:'Todos',
    },
    {
    value:'1',
    label:'Jurídica',
    },
    {
    value:'2',
    label:'Natural',
    },

  ]);

  const [tipoTer, setTipoTer] = useState([
  {
    value:'4',
    label:'Todos',
  },
  {
    value:'1',
    label:'Proveedor',
  },
  {
    value:'2',
    label:'Contratista',
  },
  {
    value:'3',
    label:'Cliente',
  },

  ]);

  useEffect(()=>{
    obtain_terce()

  },[]);

  const obtain_terce = async () =>{
    axios.get(ip+'admon/obtain_terce').then(response=>{
      setBackTerce(response.data)
      setBackTerce2(response.data)
      const dataNew = Pagination.paginate(response.data,currentPage,postsPerPage)
      setPerson(dataNew)
      setFiltro(dataNew)
      console.log("entroo a la funciòn");
    })
  }

  const updateCurrentPage = async (number) => {
  await setPerson([])
  await setCurrentPage(number)
  const dataNew = Pagination.paginate(backTerce,number,postsPerPage)
  await setPerson(dataNew)
  await setFiltro(dataNew)
}

  const searchInput = async(value) => {
    await setText(value)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData2 = backTerce2.filter(function(item2){
      var name = ((item2.name).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var document = ((item2.N_documet).toString()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var resDocument = document.indexOf(inputSearch) > -1
      var resDocumentLast = document.indexOf(inputSearch) > -1
      var res = false
      if(resName||resNameLasT){
        res = true
      }
      if(resDocument||resDocumentLast){
        res = true
      }
      return res;
    })
    await setCurrentPage(1)
    await setBackTerce(newData2)
    const dataNew = Pagination.paginate(newData2,1,postsPerPage)
    await setFiltro(dataNew);

  }
    const goback = async () => {
    await setShowForm(1)
    setId(null)
    obtain_terce();

  }
  const gobackSave = () => {
  setShowForm(1);
  setId(null);
  obtain_terce();
  setData({})
}


  const dataUpdate = async (data) => {
    console.log(data);

    var tip_per = data.tip_per
    var tip_ident = data.tip_ident
    var n_documet = data.N_documet
    var expedido = data.expedido
    var nit = data.nit
    var name = data.name
    var secname = data.secname
    var lastname = data.lastname
    var seclastname = data.seclastname
    var nom_representante = data.nom_representante
    var nom_comercial = data.nom_comercial
    var addres = data.addres
    var pais = data.pais
    var city = data.city
    var correo = data.correo
    var correo_representante = data.correo_representante
    var phone = data.phone
    var phone_representante = data.phone_representante
    var mobile = data.mobile
    var direlocal = data.direlocal
    var contra = data.contra
    var sigla = data.sigla
    var departamento = data.departamento
    var municipio = data.municipio
    var banco = data.banco
    var tipo_cuenta = data.tipo_cuenta
    var cuenta_bancaria = data.cuenta_bancaria
    var nombre_titular = data.nombre_titular
    var regimen = data.regimen
    var actividad_economica = data.actividad_economica
    var res_fiscal = data.res_fiscal

    const dataForAll = {

           tip_per,
           tip_ident,
           n_documet,
           expedido,
           nit,
           name,
           secname,
           lastname,
           seclastname,
           nom_representante,
           nom_comercial,
           addres,
           pais,
           city,
           correo,
           correo_representante,
           phone,
           phone_representante,
           mobile,
           contra,
           sigla,
           departamento,
           municipio,
           banco,
           tipo_cuenta,
           cuenta_bancaria,
           nombre_titular,
           regimen,
           actividad_economica,
           res_fiscal,
    }
    await setId(data.id)
    await setData(dataForAll)
    await setShowForm(2)
    console.log(dataForAll);
  }

  const deshabilitar = (value) =>{

    if(value.deleted == 0){
        var message = '¿Quieres deshabilitar esta persona?'
        Swal.fire({
          title: message,
          icon: 'warning',
          showDenyButton: true,
          confirmButtonColor: '#3085d6',
          denyButtonColor: '#d33',
          confirmButtonText: 'Si',
          denyButtonText: 'No'
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          const data = new FormData()
          data.append('id', value.id)
          axios.post(ip+'admon/deleted_ter',data).then(response=>{

            if(response.data == 1){
              Swal.fire('Deshabilitado correctamente!', '', 'success')
              obtain_terce();
            }

          })

        } else if (result.isDenied) {
            Swal.fire('Acción cancelada', '', 'info')
        }
      })
    }else{
        var message = '¿Quieres habilitar esta persona?';
        Swal.fire({
          title: message,
          icon: 'warning',
          showDenyButton: true,
          confirmButtonColor: '#3085d6',
          denyButtonColor: '#d33',
          confirmButtonText: 'Si',
          denyButtonText: 'No'
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            const data = new FormData()
            data.append('id', value.id)
            axios.post(ip+'admon/habilited_ter',data).then(response=>{
              if(response.data == 1){
                Swal.fire('Habilitado correctamente!', '', 'success')
                obtain_terce();
              }
            })

          } else if (result.isDenied) {
            Swal.fire('Acción cancelada', '', 'info')
          }
        })
      }
    }

  const propHeader = (value) => {
    console.log("entro",value);
    setMarginVal(value)
  }



  const tipo_per = async (e) =>{
    console.log("esta validandox")

    if(e.value == 1){
      const data2 ={
        value:'1',
        label:'Jurídica'
      }
      setType(data2)
      ///filtro persona juridica y paso a la lista
      var filtro = []
      filtro =  person.filter(e=>e.tip_per == 1)
      /////
      await setCurrentPage(1)
      await setBackTerce(filtro)///sepasa los filtros
      const dataNew = Pagination.paginate(filtro,1,postsPerPage)
      await setFiltro(dataNew);
    }
    if(e.value == 2){
      const data2 ={
        value:'2',
        label:'Natural'
      }
      setType(data2)
      ////filtro persona natural
      var filtro1 = person.filter(e=>e.tip_per == 2)
      await setCurrentPage(1)
      await setBackTerce(filtro1)
      const dataNew = Pagination.paginate(filtro1,1,postsPerPage)
      await setFiltro(dataNew);
    }
    if(e.value == 3){
      const data2 ={
        value:'3',
        label:'Todos'
      }
      setType(data2)
      await setBackTerce(person)
      const dataNew = Pagination.paginate(person,1,postsPerPage)
      await setFiltro(dataNew);
    }
  }

  const tipo_ter = async (e) =>{
    console.log("esta validandox")

    if(e.value == 1){
      const data2 ={
        value:'1',
        label:'Proveedor'
      }
      setTypeTer(data2)
      ///filtro persona juridica y paso a la lista
      var filtro = []
      filtro =  person.filter(e=>e.contra == 1)
      /////
      await setCurrentPage(1)
      await setBackTerce(filtro)///sepasa los filtros
      const dataNew = Pagination.paginate(filtro,1,postsPerPage)
      await setFiltro(dataNew);
    }
    if(e.value == 2){
      const data2 ={
        value:'2',
        label:'Contratista'
      }
      setTypeTer(data2)
      ////filtro persona natural
      var filtro1 = person.filter(e=>e.contra == 2)
      await setCurrentPage(1)
      await setBackTerce(filtro1)
      const dataNew = Pagination.paginate(filtro1,1,postsPerPage)
      await setFiltro(dataNew);
    }
    if(e.value == 3){
      const data2 ={
        value:'3',
        label:'Cliente'
      }
      setTypeTer(data2)
      ////filtro persona natural
      var filtro1 = person.filter(e=>e.contra == 3)
      await setCurrentPage(1)
      await setBackTerce(filtro1)
      const dataNew = Pagination.paginate(filtro1,1,postsPerPage)
      await setFiltro(dataNew);
    }
    if(e.value == 4){
      const data2 ={
        value:'4',
        label:'Todos'
      }
      setTypeTer(data2)
      await setBackTerce(person)
      const dataNew = Pagination.paginate(person,1,postsPerPage)
      await setFiltro(dataNew);
    }
  }



  return(
  <div className="content-wrapper " style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}}>
    <Header section={4}  marginval = {(value)=>propHeader(value)}/>
    <div className="verticar-footer">
          <div className="deshabilitar" onClick = {()=>setShowForm(2)} style={{marginTop:50}}>
              <h6 className="form-section d-flex align-items-center  my-3" style={{borderRadius:15, marginTop:50, cursor:'pointer'}}><i className="material-icons" style= {{fontSize:'170%'}}>control_point</i> Agregar Terceros</h6>
          </div>
          <h5 className="form-section habilitar" onClick = {()=>setShowForm(2)}><i className="material-icons" style= {{fontSize:'170%', marginLeft:14, marginTop:44, backgroundColor:'#FFDE59', borderRadius:15, width:66}}> control_point</i></h5>
          <img className="imagen" src={ip+'images/footer.png'}/>
    </div>

  {
    showForm == 1 ?
    <div>
      <div className="row">
        <div className="col-md-9 " style={{marginLeft:47}}>
        <div className="btn container mt-3 d-flex" style={{marginLeft:'-135px'}}>
          <div className="form-section col-md-8 "><strong><h6 className="mt-2 ruta" style={{marginLeft:'-108px'}}>/ <a href = {ip+'admon/Financiera'}>Módulo financiero </a>/ Terceros</h6></strong></div>
        </div>
        <div className="mt-3">
          <h4 className="text-center" style={{marginLeft:"-91%"}}>Terceros</h4>
        </div>
          <div className="d-flex mt-2" style={{marginTop: 9}}>
              <label className="mr-3 d-flex mt-2">Tipo persona: </label>
                <Select
                className="col-3"
                defaultValue={tipo[0]}
                closeMenuOnSelect={false}
                components={animatedComponents}
                options={tipo}
                onChange={(e)=>tipo_per(e)}
                name="colors"
                />

              <label className="mr-3 d-flex mt-2">Tipo tercero: </label>
                <Select
                className="col-3"
                defaultValue={tipoTer[0]}
                closeMenuOnSelect={false}
                components={animatedComponents}
                options={tipoTer}
                onChange={(e)=>tipo_ter(e)}
                name="colors"
                />
          </div>
          <div className="d-flex mt-2" style={{marginTop: 9}}>
              <div className="col-9 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #D9D9D9 ', backgroundColor:'white',borderRadius:12, width: '691px'}}>
              <input  onChange = {(e)=>searchInput(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Consultar cédula o nit, nombre de la persona o empresa "/>
              {
                text.length == 0 ?
                <span style = {{color:'black', cursor:'pointer'}}  className= "material-icons">search</span>
                :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
              }
              </div>

          </div>

        </div>
      </div>
    </div>
    :null

  }
  {
    showForm == 1?

    <div className="col-md-10" style={{marginLeft:35}}>
    <table className="table table-striped">
        <thead style={{backgroundColor: '#FFDE59' }}>
          <tr>
              <th>Tipo persona</th>
              <th>Tipo tercero</th>
              <th>Número de documento / NIT </th>
              <th>Nombre persona / Empresa</th>
              <th>Telefono</th>
              <th>Editar</th>
              <th>Deshabilitar</th>

          </tr>
        </thead>
          <tbody>
          {
            filtro.map((item,index)=>{
                if(item.tip_per == 1){
                  var per = 'Jurídica';
                }
                else {
                  var per = 'Natural'
                }

                if(item.contra == 1){
                  var con = 'Proveedor';
                }else if (item.contra == 2){
                  var con = 'Contratista';
                }else {
                  var con = 'Cliente';
                }
              return (
                <tr key={index}>
                  <td>{per} </td>
                  <td>{con} </td>
                  {
                    item.tip_per == 1 ?
                    <td>{item.N_documet} - {item.nit}</td>
                    :
                    <td>{item.N_documet}</td>
                  }

                  {
                    item.tip_per == 1 ?
                    <td>{item.name}</td>
                    :
                    <td>{item.name} {item.secname == null ? '': item.secname} {item.lastname == null ? '': item.lastname} {item.seclastname == null ? '': item.seclastname}</td>
                  }
                  <td>{item.mobile}</td>
                  <td>
                    <div className = "">
                      <button onClick = {()=>dataUpdate(item)} className="btn "><i class = "material-icons">edit</i></button>
                    </div>
                  </td>
                  <td>
                    <div className="form-check form-switch">
                      <input className="form-check-input cursor-pointer" onClick={()=>deshabilitar(item)} checked={item.deleted == 0 ? true : false} type="checkbox" id="flexSwitchCheckDefault"/>
                      <label className="form-check-label" for="flexSwitchCheckDefault"></label>
                    </div>
                  </td>
                </tr>
              );
            })

          }
          </tbody>
     </table>
      <div className="d-flex col-md-12 col-12 justify-content-end">
          <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backTerce.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
      </div>
    </div>
      :null
    }

    { showForm == 2 ?
      <div style={{marginLeft:40}}>
          <NewTerceros data={data} gobackSave = {()=>gobackSave()} goback = {()=>goback()}  id = {id}  />
      </div>
      :null
    }

</div>
);

}
export default Terceros;
if (document.getElementById('Terceros')) {
  ReactDOM.render(<Terceros />, document.getElementById('Terceros'))
}
