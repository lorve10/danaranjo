import React, {useState} from 'react'
import ReactDOM from 'react-dom'
import {ip} from '../ApiRest';
import Header from '../Ui/Header';
import '../../../css/app.scss';

function Contabilidad () {
  const [marginval, setMarginVal] = useState(false)

  const propHeader = (value) => {
      console.log("entro",value);
      setMarginVal(value)
  }


  return(
    <div className="content-wrapper " style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}}>
      <Header section={4}  marginval = {(value)=>propHeader(value)}/>
      <div className="verticar-footer">
            <img className="imagen" src={ip+'images/footer.png'} style={{ marginTop:367}}/>
      </div>
      <div className="container mt-5"  >
      <div className="form-section col-md-8 "><strong><h6 className="mt-2 ruta" style={{marginLeft:'103px'}}  >/ <a href = {ip+'admon/Financiera'}>Módulo financiero </a>/ Contabilidad</h6></strong>
      </div>
          <div className="text-center">
          <h4><strong>Módulo Contabilidad</strong></h4>
          </div>
        <div className="row mb-3">
        <div className="col-md-9 row mb-3 p-0 mt-4" style={{marginLeft: '110px'}}>

        <div className="col-md-4 mb-3">
        <a href={ip+"admon/parametrizacion"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/informes.png'}/>
                </div>
                <div className="mt-2">
                  <p className="m-0 interlineado">Parametrización</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>

        <div className="col-md-4 mb-3">
        <a href={ip+"admon/crear-documentos"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/CrearDocumento.png '}/>
                </div>
                <div className="mt-2">
                  <p className="m-0 interlineado">Crear documentos</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>

        <div className="col-md-4 mb-3 ">
          <a href={ip+"admon/servicios"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/en-construccion.png'}/>
                </div>
                <div className="mt-2">
                  <p className="m-0 interlineado">Página en construcción</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>

        <div className="col-md-4 mb-3">
          <a href={ip+"admon/cuentas"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/en-construccion.png'}/>
                </div>
                <div className="mt-2">
                  <p className="m-0 interlineado">Página en construcción</p>
                </div>
              </div>
            </div>
          </div>
          </a>
        </div>

        {/*<div className="row mt-5 " style={{marginLeft:'31%'}}>
          <a className="col-md-3" href={ip+"admon/pagos"}>
            <div className="cajas" >
                <img src={ip+'images/Metodo.png'} style = {{marginLeft: 10, width:100, marginTop: '10px'}}/>
                <p className="text-center" style={{fontWeight: "bold", marginTop: '10px'}}>Pagos Recibidos</p>
            </div>
          </a>
          <a className="col-md-3" href={ip+"admon/cobros"}>
            <div className="cajas ">
                <img src={ip+'images/pagos.png'} style = {{marginLeft: 10, width:100, marginTop: '10px'}}/>
                <p className="text-center" style={{fontWeight: "bold", marginTop: '10px'}}>Cobros</p>
            </div>
            </a>
            <a className="col-md-3" href={ip+"admon/egresos"}>
            <div className="cajas ">
                <img src={ip+'images/Tarjeta.png'} style = {{marginLeft: 10, width:100, marginTop: '10px'}}/>
                <p className="text-center" style={{fontWeight: "bold", marginTop: '10px'}}>Egresos</p>
            </div>
            </a>
        </div>
        <div className="row mt-2" style={{marginLeft:'31%'}} >
        <a className="col-md-3" href={ip+"admon/cuentas"}>
            <div className="cajas">
                <img src={ip+'images/solicitud.png'} style = {{marginLeft: 10, width:100, marginTop: '10px'}}/>
                <p className="text-center" style={{fontWeight: "bold", marginTop: '10px'}}>Catalogo Cuenta</p>
            </div>
        </a>
        </div>*/}
        </div>
        </div>
    </div>
    </div>
  )

}

export default Contabilidad;

if(document.getElementById('Contabilidad')){
  ReactDOM.render(<Contabilidad />, document.getElementById('Contabilidad'));

}
