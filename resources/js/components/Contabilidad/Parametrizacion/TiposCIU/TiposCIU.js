import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Header from '../../../Ui/Header';

import util from '../../componentes/util';
import Pagination from '../../componentes/paginate';

function TiposCIU (){

    const [loading, setLoading] = useState(false);
    const [showForm, setShowForm] = useState(1)
    const [currentPage, setCurrentPage] = useState(1)
    const [perPage, setPerPage] = useState(9)
    const [tipoCiu, setTipoCiu] = useState([])
    const [tipoCiu2, setTipoCiu2] = useState([])
    const [text, setText] = useState('')
    const [marginval, setMarginVal] = useState(false)
    const [filtro, setFiltro] = useState([])

    useEffect(()=>{ 
        obtain_actividad() 
    },[]);

    const obtain_actividad = async () => {
        axios.get(ip+'admon/actividadEconomica').then(response=>{
            console.log(response);
            setTipoCiu(response.data)
            setTipoCiu2(response.data)
            const dataNew = util.paginate(response.data,currentPage,perPage)
            setFiltro(dataNew)
        })
    }

    const searchInput = async(value) => {
        await setText(value)
        const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
        var newData2 = tipoCiu2.filter(function(item2){
          var nombre = ((item2.nombre).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
          var codigo = ((item2.codigo).toString()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
          var resNombre = nombre.indexOf(inputSearch) > -1
          var resNombreLasT = nombre.indexOf(inputSearch) > -1
          var resCodigo = codigo.indexOf(inputSearch) > -1
          var resCodigoLast = codigo.indexOf(inputSearch) > -1
          var res = false
          if(resNombre||resNombreLasT){
            res = true
          }
          if(resCodigo||resCodigoLast){
            res = true
          }
          return res;
        })
        await setCurrentPage(1)
        const dataNew = util.paginate(newData2,1,perPage)
        await setFiltro(dataNew);
    
    }

    const updateCurrentPage = async (number) => {
        await setFiltro([])
        await setCurrentPage(number)
        const dataNew = util.paginate(tipoCiu2,number,perPage)
        await setFiltro(dataNew)
    }

    return(
        <div className="content-wrapper " style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}}>
            <Header section={4}  marginval = {(value)=>propHeader(value)}/>

            <div className="verticar-footer-2">
            {
                showForm == 1?
                <img className="imagen" src={ip+'images/footer.png'}/>

                :null
            }
            </div>

        {
            showForm == 1 ?
            <div>
            <div className="row">
                <div className="col-md-9 " style={{marginLeft:47}}>
                <div className="btn container mt-3 d-flex" style={{marginLeft:'-155px'}}>
                <div className="form-section col-md-8 "><strong><h6 className="mt-2 ruta" style={{marginLeft:'-30px'}}>/ <a href = {ip+'admon/parametrizacion'}>Módulo parametrización </a>/ Tipos de ciiu</h6></strong></div>
                </div>
                <div className="mt-3">
                <h4 className="text-center" style={{marginLeft:"-86%"}}>Tipos de CIIU</h4>
                </div>
                <div className="d-flex mt-2" style={{marginTop: 9}}>
                    <div className="col-7  mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #D9D9D9 ', backgroundColor:'white',borderRadius:12}}>
                    <input  onChange = {(e)=>searchInput(e.target.value)} style = {{width:'490px',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Consultar ciiu "/>
                    {
                        text.length == 0 ?
                        <span style = {{color:'black', cursor:'pointer'}}  className= "material-icons">search</span>
                        :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
                    }
                    </div>
                </div>

                </div>
            </div>
            </div>
            :null

        }
        {
            showForm == 1?

            <div className="col-md-10" style={{marginLeft:35}}>
            <table className="table table-striped">
                <thead style={{backgroundColor: '#FFDE59' }}>
                <tr>
                    <th>Código</th>
                    <th>Ciiu</th>

                </tr>
                </thead>
                <tbody>
                    {
                        filtro.map((item, index)=>{
                            return(
                                <tr key={index}>
                                    <td>{item.codigo}</td>
                                    <td>{item.nombre}</td>
                                </tr>
                            );
                        })
                    }
                </tbody>
                </table>
                <div className="d-flex col-md-12 col-12 justify-content-end">
                    <Pagination currentPage={currentPage} perPage={perPage} countdata={tipoCiu.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
                </div>
            </div>
            :null
            }
        </div>
    );
}

export default TiposCIU;
if(document.getElementById('TiposCIU')) {
    ReactDOM.render(<TiposCIU />, document.getElementById('TiposCIU'));
}
