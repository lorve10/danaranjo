import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Header from '../../../Ui/Header';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { Button, Modal } from 'react-bootstrap';
import { Tooltip,  Grid  } from '@material-ui/core';

import util from '../../componentes/util';
import Pagination from '../../componentes/paginate';

function PeriodosContables (){

    const [ loading, setLoading ] = useState(false);
    const [ showForm, setShowForm ] = useState(1)
    const [ currentPage, setCurrentPage ] = useState(1)
    const [ perPage, setPerPage ] = useState(9)
    const [ periodoCon, setPeriodoCon ] = useState([])
    const [ periodoCon2, setPeriodoCon2 ] = useState([])
    const [ text, setText ] = useState('')
    const [ marginval, setMarginVal ] = useState(false)
    const [ filtro, setFiltro ] = useState([])
    const [ show, setShow ] = useState(false);
    const handleShow = () => setShow(true);

    const [ id, setId ] = useState(null);
    const [ fechaIni, setFechaIni ] = useState(null);
    const [ fechaFin, setFechaFin ] = useState(null);
    const [ fecha1, setFecha1 ] = useState('');
    const [ fecha2, setFecha2 ] = useState('');

    useEffect(()=>{ 
        obtainPeriodoContable() 
    },[]);

    const addPeriodo = async () => {
        setShow(true);
    }

    const handleClose = () => {
        setShow(false);
        setFechaIni(null);
        setFechaFin(null);
    }

    const obtainPeriodoContable = async () => {
        axios.get(ip+'admon/parametrizacion/obtain_PeriodosContables').then(response=>{
            console.log(response);
            setPeriodoCon(response.data)
            setPeriodoCon2(response.data)
            const dataNew = util.paginate(response.data,currentPage,perPage)
            setFiltro(dataNew)
        })
    }

    const GuardPeriodoContable = async () => {
        var message = ''
        var error = false

        if(fechaIni == null){
            error = true
            message = "Debe seleccionar fecha inicial"
        }else if(fechaFin  == null){
            error = true
            message = "Debe seleccionar fecha final"
        }
        if(error){
            MessageError(message)
        }
        else{
            const data = new FormData()
            data.append('id',id)
            data.append('fecha_inicial', fechaIni)
            data.append('fecha_final', fechaFin)
            axios.post(ip+'admon/parametrizacion/guardPeriodoContable',data).then(response=>{
                console.log("aqui data");
                console.log(response.data);
                if(response.data == 2){
                    MessageError("Ya hay un período contable con esa fecha inicial");
                }else if(response.data == 3){
                    MessageError("Ya hay un período contable con esa fecha final");
                }else{
                    MessageSuccess("Período contable creado correctamente");
                    obtainPeriodoContable();
                    handleClose(false);
                    
                }
                
            })
        }
        
    }

    const MessageError = async (data) => {
        Swal.fire({
          title: 'Error',
          text: data,
          icon: 'warning',
        })
    }
  
      const MessageSuccess = async (data) => {
        Swal.fire({
          text: data,
          icon: 'success',
        })
    }

    const deshabilitar = (valor) =>{

        if(valor.deleted == 0){
          var message = '¿Quieres deshabilitar este período contable?';
          Swal.fire({
            title: message,
            icon: 'warning',
            showDenyButton: true,
            confirmButtonColor: '#3085d6',
            denyButtonColor: '#d33',
            confirmButtonText: 'Si',
            denyButtonText: 'No'
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              const data = new FormData()
              data.append('id', valor.id)
              axios.post(ip+'admon/parametrizacion/deleted_periodoContable',data).then(response=>{
    
                if(response.data == 1){
                  Swal.fire('Deshabilitado correctamente!', '', 'success')
                  obtainPeriodoContable();
                }
                else{
                  Swal.fire('No se puede eliminar por que ya tiene una cuenta de cobro.', '', 'info')
                  obtainPeriodoContable();
                }
    
              })
    
            } else if (result.isDenied) {
              Swal.fire('Acción cancelada', '', 'info')
            }
          })
    
        }else{
          var message = '¿Quieres habilitar este período contable?';
          Swal.fire({
            title: message,
            icon: 'warning',
            showDenyButton: true,
            confirmButtonColor: '#3085d6',
            denyButtonColor: '#d33',
            confirmButtonText: 'Si',
            denyButtonText: 'No'
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              const data = new FormData()
              data.append('id', valor.id)
              axios.post(ip+'admon/parametrizacion/habilited_periodoContable',data).then(response=>{
                if(response.data == 1){
                  Swal.fire('Habilitado correctamente!', '', 'success')
                  obtainPeriodoContable();
                }
              })
    
            } else if (result.isDenied) {
              Swal.fire('Acción cancelada', '', 'info')
            }
          })
        }
    
    }

    const dataFecha = async () => {
        console.log(fecha1);
        console.log(fecha2);
        var fecha1back = moment(fecha1, 'YYYY-MM-DD')
        var fecha2back = moment(fecha2, 'YYYY-MM-DD')
            if(fecha1back._isValid && fecha2back._isValid){
                
            console.log("Escriba aca algooooo");
            console.log(fecha1back);
            console.log(fecha2back);
            var arrayTopush = []
            console.log("object")
            periodoCon2.map(item=>{
            var fecha_creacion2 = moment(item.fecha_inicial, 'YYYY-MM-DD')
            var fecha_creacion = moment(item.fecha_final, 'YYYY-MM-DD')
          
            console.log(fecha_creacion2);
            var fecha_1 = fecha1back.diff(fecha_creacion2, 'minutes');
            var fecha_2 = fecha2back.diff(fecha_creacion, 'minutes');
            if (fecha_1 <= 0 && fecha_2 >= 0) {
                arrayTopush.push(item)
            }
          
            })
            setFecha1("")
            setFecha2("")                
            console.log(arrayTopush);
            await setCurrentPage(1)
            await setPeriodoCon(arrayTopush)
            const dataNew = util.paginate(arrayTopush,1,perPage)
            await setFiltro(dataNew)

            }else{
                console.log("aquiii filtrooooo")
                console.log(periodoCon2);
                await setCurrentPage(1)
                await setPeriodoCon(periodoCon2)
                const dataNew = util.paginate(periodoCon2,1,perPage)
                await setFiltro(dataNew)
            }
    }

    return(
        <div className="content-wrapper " style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}}>
            <Header section={4}  marginval = {(value)=>propHeader(value)}/>

            <div className="verticar-footer-2">
            {
                showForm == 1?
                <Tooltip title="Agregar periodo contable" placement="left">
                <button className="btn" >
                    <h5 onClick = {()=>addPeriodo()}><img src = {ip+'images/AgregarPeriodoContable.png'} style= {{width:'56px', marginLeft: '0px', marginTop: '35px'}} /> </h5>
                </button>
                </Tooltip>

                :null
            }

                <img className="imagen" src={ip+'images/footer.png'}/>
            </div>

        {
            showForm == 1 ?
            <div>
            <div className="row">
                <div className="col-md-9 " style={{marginLeft:47}}>
                <div className="btn container mt-3 d-flex" style={{marginLeft:'-155px'}}>
                <div className="form-section col-md-8 "><strong><h6 className="mt-2 ruta" style={{marginLeft:'25px'}}>/ <a href = {ip+'admon/parametrizacion'}>Módulo parametrización </a>/ Períodos contables</h6></strong></div>
                </div>
                <div className="mt-3">
                <h4 className="text-center" style={{marginLeft:"-78%"}}>Períodos contables</h4>
                </div>
                <div className="d-flex mt-2" style={{marginTop: 9}}>

                    <label className="d-flex mr-3 col-6 align-items-center color2" style={{width: '100px'}}>Fecha de: </label>
                    <input className="form-control mr-3 col-3 d-flex" style={{width: '180px'}} value={fecha1} type="date" onChange = {(e)=>setFecha1(e.target.value)}/*value={fecha} onChange={(event)=>setFecha(event.target.value)}*/ />

                    <label className="d-flex mr-3 col-6 align-items-center color2" style={{width: '120px'}}>Fecha hasta: </label>
                    <input className="form-control col-3 d-flex" style={{width: '180px'}} value={fecha2} type="date" onChange = {(e)=>setFecha2(e.target.value)} /*value={fecha} onChange={(event)=>setFecha(event.target.value)}*/ />

                    <div className="col-4 mb-3">
                    <button className="btn btn-filtro" style={{backgroundColor:'#FFDE59', color:'black', width: '46px', fontWeight: "bold"}} onClick={()=>dataFecha()}><span className = "material-icons align-items-center justify-content-between" >filter_alt</span></button>
                    </div>
                </div>

                </div>
            </div>
            </div>
            :null

        }
        {
            showForm == 1?

            <div className="col-md-10" style={{marginLeft:35}}>
            <table className="table table-striped">
                <thead style={{backgroundColor: '#FFDE59' }}>
                <tr>
                    <th>Fecha inicial</th>
                    <th>Fecha final</th>
                    <th>Estado</th>
                </tr>
                </thead>
                <tbody>
                    {
                        filtro.map((item, index)=>{
                            return(
                                <tr key={index}>
                                    <td>{item.fecha_inicial}</td>
                                    <td>{item.fecha_final}</td>
                                    <td>
                                    <div>

                                        {
                                            item.deleted == 0 ?
                                            <i className="material-icons cursor-pointer" onClick={()=>deshabilitar(item)}>lock_open</i>
                                            :
                                            <i className="material-icons cursor-pointer" onClick={()=>deshabilitar(item)}>lock</i>
                                        }

                                    </div>
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
                </table>
                <div className="d-flex col-md-12 col-12 justify-content-end">
                    <Pagination currentPage={currentPage} perPage={perPage} countdata={periodoCon.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
                </div>
                <div>
                    <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Crear periodo contable</Modal.Title>
                    </Modal.Header>
                        <Modal.Body>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-10 d-flex">
                                <label className="d-flex col-4 align-items-center mr-2 color" style={{width: '225px'}}>Fecha inicial: </label>
                                <input className="form-control col-6" type="date" value = {fechaIni} placeholder="Fecha inicial" onChange = {(e)=>setFechaIni(e.target.value)}/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-10 d-flex">
                                <label className="d-flex col-4 align-items-center mr-2 color" style={{width: '225px'}}>Fecha final: </label>
                                <input className="form-control col-6" type="date" value = {fechaFin} placeholder="Fecha final" onChange = {(e)=>setFechaFin(e.target.value)}/>
                                </div>
                            </div>
                        </div>
                        </Modal.Body>
                        <Modal.Footer>
                        <Button style={{backgroundColor: '#FFDE59', border:'solid 0px', borderColor: '#FFDE59', color: 'black'}} onClick={()=>GuardPeriodoContable()}>Guardar</Button>
                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
            :null
            }
        </div>
    );
}

export default PeriodosContables;
if(document.getElementById('PeriodosContables')) {
    ReactDOM.render(<PeriodosContables />, document.getElementById('PeriodosContables'));
}