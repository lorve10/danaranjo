import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Header from '../../../Ui/Header';
import Swal from 'sweetalert2';
import util from '../../../Admon/util';
import Pagination from '../../componentes/paginate';
import { Button, Modal } from 'react-bootstrap';
import { Tooltip,  Grid  } from '@material-ui/core';
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css';
import { store } from 'react-notifications-component';


function TipoDoc (){
  const [marginval, setMarginVal] = useState(false)

    const [ loading, setLoading ] = useState(false);
    const [ text, setText ] = useState('')
    const [ showForm, setShowForm ] = useState(1)
    const [ id, setId ] = useState(null)
    const [ prefijo, setPrefijo ] =useState(null)
    const [ nombre, setNombre ] = useState(null)
    const [ titulo, setTitulo ] = useState(null)
    const [ conInicial, setConInicial ] = useState(null)
    const [ conFinal, setConFinal ] = useState(null)
    const [ comprobante, setComprobante ] = useState(null)
    const [ resolucion, setResolucion ] = useState(null)
    const [ numIni, setNumIni ] = useState('')
    const [ numFinal, setNumFinal ] = useState('')
    const [ fechaVig, setFechaVig ] = useState('')
    const [ fechaRes, setFechaRes ] = useState('')
    const [ variable, setVariable ] = useState(false)
    const [ estado, setEstado ] = useState(1);
    const [ show, setShow ] = useState(false);
    const handleShow = () => setShow(true);
    const [ filtro, setFiltro ] = useState([])
    const [ currentPage, setCurrentPage ] = useState(1)
    const [ perPage, setPerPage ] = useState(6)
    const [ documento, setDocumento ] = useState([])
    const [ documento2, setDocumento2 ] = useState([])

    useEffect(()=>{
      obtainDocumento();
      handleOnClickDefault();
    },[]);


    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

    const addTipoDoc = async () => {
      setShow(true);
    }

    const handleClose = () => {
      setShow(false);
      setPrefijo("")
      setNombre("")
      setTitulo("")
      setConFinal("")
      setConInicial("")
      setComprobante("")
      setResolucion(0)
      setNumIni("")
      setNumFinal("")
      setFechaVig("")
      setFechaRes("")
    }

    const obtainDocumento = async () => {
      axios.get(ip+'admon/parametrizacion/obtain_documento').then(response=>{
          console.log(response);
          setDocumento(response.data)
          setDocumento2(response.data)
          const dataNew = util.paginate(response.data,currentPage,perPage)
          setFiltro(dataNew)
      })
    }

    const save_data = async () => {
        var message = ''
        var error = false

        if(variable == false){
          var res = 0
        }
        else {
          var res = resolucion
        }

        if(fechaVig == ''){
          var resFechaVig = ''
        }
        else {
          var resFechaVig = fechaVig
        }

        if(fechaRes == ''){
          var resFechaRes = ''
        }
        else {
          var resFechaRes = fechaRes
        }

        if(numIni == ''){
          var numInicial = ''
        }
        else {
          var numInicial = numIni
        }

        if(numFinal == ''){
          var numFin = ''
        }
        else {
          var numFin = numFinal
        }

        if(variable == true){
          if (prefijo == null) {
            error = true
            message = "El prefijo no puede ir vacío"
          }
          else if (nombre == null) {
            error = true
            message = "El nombre del documento no puede estar vacío"
          }
          else if (titulo == null ){
            error = true
            message = "El título del documento no puede estar vacío"
          }
          else if (conInicial == null ){
            error = true
            message = "Debe digitar consecutivo inicial"
          }
          else if (conFinal == null ){
            error = true
            message = "Debe digitar consecutivo final"
          }
          else if (comprobante == null){
            error = true
            message = "Debe digitar comprobante"
          }
          else if (numIni == ''){
            error = true
            message = "Debe digitar número inicial"
          }
          else if (numFinal == ''){
            error = true
            message = "Debe digitar número final"
          }
          else if (fechaVig == ''){
            error = true
            message = "Debe elegir fecha de vigencia"
          }
          else if (fechaRes == ''){
            error = true
            message = "Debe elegir fecha de resolución"
          }
        }else if(variable == false){
          if (prefijo == null) {
            error = true
            message = "El prefijo no puede ir vacío"
          }
          else if (nombre == null) {
            error = true
            message = "El nombre del documento no puede estar vacío"
          }
          else if (titulo == null ){
            error = true
            message = "El título del documento no puede estar vacío"
          }
          else if (conInicial == null ){
            error = true
            message = "Debe digitar consecutivo inicial"
          }
          else if (conFinal == null ){
            error = true
            message = "Debe digitar consecutivo final"
          }
        }
        
      if (error) {
        MessageError(message)
      }
      else{
        const data = new FormData()
        data.append('id', id)
        data.append('prefijo', prefijo)
        data.append('nombre', nombre)
        data.append('titulo', titulo)
        data.append('conInicial', conInicial)
        data.append('conFinal', conFinal)
        data.append('comprobante', comprobante)
        data.append('resolucion', res)
        data.append('num_inicial', numInicial)
        data.append('num_final', numFin)
        data.append('fecha_vig', resFechaVig)
        data.append('fecha_res', resFechaRes)
        axios.post(ip+'admon/parametrizacion/guardDoct',data).then(response=>{
          if (id == null) {
            MessageSuccess("Datos creados correctamente")
            obtainDocumento();
            handleClose(false);
            setVariable(false);
          }
          else{
            MessageSuccess("Editado correctamente")
          }
          })
        }
      }

      const resolucionData = async (data) => {
        if(data.target.checked == true){
          setEstado(2)
          setResolucion(1)
          setVariable(true)
        }
        else if (data.target.checked == false){
          setEstado(1)
          setResolucion(0)
          setVariable(false)
        }
      }

      const updateCurrentPage = async (number) => {
        await setFiltro([])
        await setCurrentPage(number)
        const dataNew = util.paginate(documento2,number,perPage)
        await setFiltro(dataNew)
      }

      const searchInput = async(value, otro) => {
        await setText(value)
        await setLoading(true)
        const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
        var newData = documento2.filter(function(item2){
          var Nombre  = ((item2.nombre).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
          var Prefijo2 = ((item2.prefijo).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")

          var resPrefijo = Prefijo2.indexOf(inputSearch) > -1
          var resPrefijoLasT = Prefijo2.indexOf(inputSearch) > -1
          var resNombre = Nombre.indexOf(inputSearch) > -1
          var resNombreLasT = Nombre.indexOf(inputSearch) > -1
          var res = false
          if(resNombre||resNombreLasT){
            res = true
          }
          if(resPrefijo||resPrefijoLasT){
            res = true
          }
    
          return res;
        })
    
        if(otro){
          console.log("entrooo aquiii");
          console.log(otro);
          await setCurrentPage(1)
          await setDocumento(otro)
          const dataNew = util.paginate(otro,1,perPage)
          await setFiltro(otro)
          await setLoading(false)
        }
        else {
          await setCurrentPage(1)
          await setDocumento(newData)
          const dataNew = util.paginate(newData,1,perPage)
          await setFiltro(dataNew)
          await setLoading(false)
        }
    
    
      }

      const handleOnClickDefault = () => {
        axios.get(ip+'admon/parametrizacion/notification').then(response=>{
          if(response.data >= 2){
            store.addNotification({
              title: "Alerta!",
              message: 'El tipo de documento ' + {nombre} + ' esta a punto de vencerse',
              type: "warning",
              container: "top-left",
              animationIn: ["animate__animated", "animate__fadeIn"],
              animationOut: ["animate__animated", "animate__fadeOut"],
              dismiss: {
                duration: 5000,
                onScreen: true,
                showIcon: true
              }
            });
          }
        })
      }

    return(
        <div className="content-wrapper " style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}}>
            <ReactNotification/>
            <Header section={4}  marginval = {(value)=>propHeader(value)}/>

            <div className="verticar-footer-2">
            {
                showForm == 1?
                <Tooltip title="Agregar tipo de documento" placement="left">
                <button className="btn" >
                    <h5 onClick = {()=>addTipoDoc()}><img src = {ip+'images/AgregarPeriodoContable.png'} style= {{width:'56px', marginLeft: '0px', marginTop: '35px'}} /> </h5>
                </button>
                </Tooltip>

                :null
            }

                <img className="imagen" src={ip+'images/footer.png'}/>
            </div>

        {
            showForm == 1 ?
          <div className="container">
            <div className="row">
              <div className="btn container mt-3 d-flex" style={{marginLeft:'-155px'}}>
                  <div className="form-section col-md-8 "><strong><h6 className="mt-2 ruta" style={{marginLeft:'-80px'}}>/ <a href = {ip+'admon/parametrizacion'}>Módulo parametrización </a>/ Tipos de documento </h6></strong></div>
              </div>
                <div className="col-md-12">
                  <h4>Tipos de documento</h4>
                </div>


                <div className="d-flex mt-2" style={{marginTop: 9}}>
                    <div className="col-7  mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #D9D9D9 ', backgroundColor:'white',borderRadius:12}}>
                    <input  onChange = {(e)=>searchInput(e.target.value)} style = {{width:'490px',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Consultar tipos de documentos "/>
                    {
                        text.length == 0 ?
                        <span style = {{color:'black', cursor:'pointer'}}  className= "material-icons">search</span>
                        :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
                    }
                    </div>
                </div>

                
              
                <div className="col-md-10">
                  <table className="table table-striped">
                  <thead style={{backgroundColor: '#FFDE59' }}>
                  <tr>
                      <th>Prefijo</th>
                      <th>Nombre del documento</th>
                      <th>Consecutivo inicial</th>
                      <th>Consecutivo final</th>
                      <th>Fecha de vigencia</th>
                  </tr>
                  </thead>
                  <tbody>
                      {
                          filtro.map((item, index)=>{
                              return(
                                  <tr key={index}>
                                      <td>{item.prefijo}</td>
                                      <td>{item.nombre}</td>
                                      <td>{item.conInicial}</td>
                                      <td>{item.conFinal}</td>
                                      <td>{item.fecha_vig}</td>
                                  </tr>
                              );
                          })
                      }
                  </tbody>
                  </table>
                <div className="d-flex col-md-12 col-12 justify-content-end">
                    <Pagination currentPage={currentPage} perPage={perPage} countdata={documento.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
                </div>

              <div>
                    <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Crear tipos de documentos</Modal.Title>
                    </Modal.Header>
                        <Modal.Body>
                        <div class="container-fluid">
                            <div class="row">
                              <div class="col-md-12 d-flex mt-1">
                              <label className="d-flex col-6 align-items-center mr-2 color float-right" style={{width: '175px'}}>Prefijo: </label>
                                <input className="form-control col-md-8" style={{width: '250px'}} placeholder="Ingrese prefijo" type="text"  value={prefijo}  onChange = {(e)=>setPrefijo(e.target.value)} />
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-12 d-flex mt-1">
                                <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '175px'}}>Nombre documento: </label>
                                <input className="form-control col-md-8" style={{width: '250px'}} placeholder="Ingrese nombre del documento" type="text"  value={nombre}  onChange = {(e)=>setNombre(e.target.value)} />
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-12 d-flex mt-1">
                                <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '175px'}}>Título: </label>
                                <input className="form-control col-md-8" style={{width: '250px'}}  placeholder="Ingrese título del documento" type="text"  value={titulo}  onChange = {(e)=>setTitulo(e.target.value)} />
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-12 d-flex mt-1">
                                <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '175px'}}>Consecutivo inicial: </label>
                                <input className="form-control col-md-8" style={{width: '250px'}} placeholder="Ingrese consecutivo inicial" type="number"  value={conInicial}  onChange = {(e)=>setConInicial(e.target.value)} />
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-12 d-flex mt-1">
                                <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '175px'}}>Consecutivo final: </label>
                                <input className="form-control col-md-8" style={{width: '250px'}} placeholder="Ingrese consecutivo final" type="number"  value={conFinal}  onChange = {(e)=>setConFinal(e.target.value)} />
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-12 d-flex mt-1">
                                <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '175px'}}>Resolución: </label>
                                <div className="form-check form-switch" >
                                    <input className="form-check-input" onClick={(resolucion)=>resolucionData(resolucion)} checked={variable} type="checkbox" id="flexSwitchCheckDefault" style={{width: '50px' , height: '25px'}}/>
                                    <label className="form-check-label" for="flexSwitchCheckDefault"></label>
                                </div>
                              </div>
                            </div>

                            {
                              estado == 2 ?
                              <div>
                              <div class="row">
                                <div class="col-md-12 d-flex mt-1">
                                  <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '175px'}}>Comprobante: </label>
                                  <input className="form-control col-md-8" style={{width: '250px'}} placeholder="Ingrese comprobante" type="text"  value={comprobante}  onChange = {(e)=>setComprobante(e.target.value)} />
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12 d-flex mt-1">
                                  <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '175px'}}>Número inicial: </label>
                                  <input className="form-control col-md-8" style={{width: '250px'}} placeholder="Ingrese número inicial" type="number"  value={numIni}  onChange = {(e)=>setNumIni(e.target.value)} />
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-12 d-flex mt-1">
                                  <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '175px'}}>Número final: </label>
                                  <input className="form-control col-md-8" style={{width: '250px'}} placeholder="Ingrese número final" type="number"  value={numFinal}  onChange = {(e)=>setNumFinal(e.target.value)} />
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-12 d-flex mt-1">
                                  <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '175px'}}>Fecha de vigencia: </label>
                                  <input className="form-control col-md-8" style={{width: '250px'}} placeholder="Ingrese fecha de vigencia" type="date"  value={fechaVig}  onChange = {(e)=>setFechaVig(e.target.value)} />
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-12 d-flex mt-1">
                                  <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '175px'}}>Fecha de resolución: </label>
                                  <input className="form-control col-md-8" style={{width: '250px'}} placeholder="Ingrese fecha de resolucion" type="date"  value={fechaRes}  onChange = {(e)=>setFechaRes(e.target.value)} />
                                </div>
                              </div>
                              </div>
                              :
                              null
                            }

                            

                        </div>
                        </Modal.Body>
                        <Modal.Footer>
                        <Button style={{backgroundColor: '#FFDE59', border:'solid 0px', borderColor: '#FFDE59', color: 'black'}} onClick = {()=>save_data()}>Guardar tipo de documento</Button>
                        </Modal.Footer>
                    </Modal>
                </div>
              </div>                 
            </div>
          </div>
          :null

        }

        </div>
    );
}

export default TipoDoc;
if(document.getElementById('TipoDoc')) {
    ReactDOM.render(<TipoDoc />, document.getElementById('TipoDoc'));
}
