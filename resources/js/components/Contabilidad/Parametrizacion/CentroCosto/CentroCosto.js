import React,{useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Swal from 'sweetalert2';
import Header from '../../../Ui/Header';
import { Button, Modal } from 'react-bootstrap';


function CentroCosto() {
  const [ loading, setLoading] = useState(false)
  const [ text, setText] = useState('')
  const [ listCostos, setListCostos ] = useState([]);
  const [ marginval, setMarginVal ] = useState(false)
  const [ showForm, setShowForm ] = useState(1)
  const [ show, setShow ] = useState(false);
  const handleShow = () => setShow(true);
  const [ id, setId] = useState(null);
  const [ codigo, setCodigo ] = useState(null)
  const [ nombre, setNombre ] = useState(null)
  const [ muestra, setMuestra ] = useState(1)
  const [ idCentro, setIdCentro ] = useState(null)
  const [ code, setCode ] = useState(null)
  const [ name, setName ] = useState(null)

  const [ nombreSub, setNombreSub ] = useState(null)
  const [ codigoSub, setCodigoSub ] = useState(null)

  const [ centro, setCentro ] = useState(null);
  const [ listCentro, setListCentro ] = useState([]);

  useEffect(()=>{
    obtainData();
    obtainCentro();
  },[])

  const obtainData = async () => {
    axios.get(ip+'admon/parametrizacion/obtener_costo').then(response=>{
      var res = response.data
      setListCostos(res)
      console.log(res);

    })
  }

  const obtainCentro = async () => {
    axios.get(ip+'admon/parametrizacion/obtener_centro').then(response=>{
      var res = response.data
      var filtrado = res.filter(e=>e.deleted == 0);
      setListCentro(filtrado)
      console.log("Aquiii centros de costos")
      console.log(res);

    })
  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  
  const addSubcentro = (id, codigo, nombre) => {
    setShow(true)
    setShowForm(2)
    setIdCentro(id)
    setCode(codigo)
    setName(nombre)
    setMuestra(2)
  
  }

  const save_data = async () => {
    var message = ''
    var error = false

    if(muestra == 1){
      var res = ''
    }
    else {
        var res = code
    }

    if(showForm == 1){
      var codig = codigo;
      var tipo = 1
      if (codigo == null) {
        error = true
        message = "El código no puede ir vacío"
      }
      else if (nombre == null) {
        error = true
        message = "El campo nombre no puede estar vacío"
      }
    }else if(showForm == 2){
      var codig = (code+ "-" + codigo);
      var tipo = 2
      if (codigo == null) {
        error = true
        message = "El código no puede ir vacío"
      }
      else if (nombre == null) {
        error = true
        message = "El campo nombre no puede estar vacío"
  
      }
    }
    
    if (error) {
      MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('id', id)
      data.append('codigo', codig)
      data.append('nombre', nombre)
      data.append('tipo', tipo)
      data.append('id_centro', res)
      axios.post(ip+'admon/parametrizacion/guard_costo',data).then(response=>{
        if (id == null) {
          MessageSuccess("Datos creados correctamente")
          setCodigo("")
          setNombre("")
          setMuestra(0)
          obtainData()
          obtainCentro()
          handleClose(false);
        }
        else{
          MessageSuccess("Editado correctamente")
          obtainData()
          obtainCentro()
        }
        })
      }
    }





  const deshabilitar = (value) =>{
    var message = '¿Quieres eliminar este centro de costos?'
    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,

    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value.id)
        axios.post(ip+'admon/parametrizacion/deletedCentro',data).then(response=>{

          Swal.fire('Eliminado correctamente!', '', 'success')
          obtainData();
          obtainCentro();
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }


  const goback = async () => {
    await setEstado(1)
    setId(null)
  }

  const gobackSave = () => {
    setEstado(1)
    setId(null)
    setData({})
    mostrarCuotas();
  }

    const propHeader = (value) => {
      console.log("entro",value);
      setMarginVal(value)
    }


    console.log(muestra);

    const handleClose = () => {
      setShow(false);
      setShowForm(1);
      setCodigo("");
      setNombre("");
    }

  return(
    <div className="content-wrapper"  style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:74}} >
    <Header section={2}  marginval = {(value)=>propHeader(value)}/>

    <div className="verticar-footer-2">
          <img className="imagen" src={ip+'images/footer.png'}/>
    </div>



      <div>
        <div style={{marginLeft:-31}} className="form-section col-md-8 mt-3"><strong><h6 className="mt-2 ruta" style={{marginLeft:'103px'}}  >/ <a href = {ip+'admon/Financiera'}>Módulo financiero </a>/ <a href = {ip+'admon/contabilidad'}>Contabilidad </a> / <a  href = {ip+'admon/parametrizacion'} > Parametrización</a> /Centro de costos </h6></strong>
        </div>
        {
          showForm == 1 ?
          <div className="container">
          <div className="row mt-3 d-flex">
            <h4>Crear centro de costos</h4>
              <div class="col-md-2 ">
                  <label>Código</label>
                  <input className="form-control" placeholder="Ingrese código" type="text"  value={codigo}  onChange = {(e)=>setCodigo(e.target.value.toUpperCase())} />
              </div>
              <div class="col-md-5 ">
                  <label>Nombre centro</label>
                  <input className="form-control" placeholder="Ingrese nombre del centro" type="text"  value={nombre}  onChange = {(e)=>setNombre(e.target.value.toUpperCase())} />
              </div>
                <img class="col-md-2" style={{width:'86px', marginTop: '16px', marginLeft: '-10px'}} src={ip+'images/centroCosto.png'} onClick={()=>save_data()}/>
            </div>
          </div>
          :
          null
        }
        
      </div>


      <div className="row" style={{marginLeft:52}}>
        <div class ="col-md-10">
        <hr style={{marginLeft:'9px'}}></hr>
        <table className="table table-striped" style={{marginLeft: '9px'}}>
            <thead style={{backgroundColor: '#FFDE59' }}>
              <tr>
                  <th>Código</th>
                  <th>Nombre</th>
                  <th>Centro / Subcentro </th>
                  <th>Agregar</th>
                  <th>Eliminar</th>
              </tr>
            </thead>
            <tbody>
              {
                listCostos.map(item=>{
                  return(
                    <tr>
                      <td>{item.codigo}</td>
                      <td>{item.nombre}</td>
                      <td>{item.tipo == 1 ? 'Centro' : 'Subcentro'}</td>
                      <td>
                        {
                          item.tipo == 1 ?
                          <span class="material-icons align-items-center" onClick={()=>addSubcentro(item.id, item.codigo, item.nombre)} style={{cursor:'pointer'}} >
                          add_circle
                          </span>
                          :
                          null
                        }
                      </td>
                      <td>
                        <span class="material-icons align-items-center" onClick={()=>deshabilitar(item)} style={{cursor:'pointer'}} >
                        cancel
                        </span>

                      </td>

                    </tr>
                  )

                })
                }
            </tbody>
          </table>
        </div>
        {
          showForm == 2 ? 
          <div>
          <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
              <Modal.Title>Crear subcentro</Modal.Title>
          </Modal.Header>
              <Modal.Body>
              <div class="container-fluid">
              <p style={{marginLeft:'-2px'}}> <strong>Pertenece a :</strong> {name} - {code}</p>
                  <div class="row">
                      <div class="col-md-10 d-flex">
                      <label className="d-flex col-8 align-items-center mr-2 color" style={{width: '200px'}}>Código: </label>
                      <input className="form-control col-8" type="text" value = {codigo} placeholder="Código del subcentro" onChange = {(e)=>setCodigo(e.target.value.toUpperCase())}/>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-10 d-flex">
                      <label className="d-flex col-8 align-items-center mr-2 color" style={{width: '200px'}}>Nombre del subcentro: </label>
                      <input className="form-control col-8" type="text" value = {nombre} placeholder="Nombre del subcentro" onChange = {(e)=>setNombre(e.target.value.toUpperCase())}/>

                      </div>
                  </div>

              </div>
              </Modal.Body>
              <Modal.Footer>
              <Button style={{backgroundColor: '#FFDE59', border:'solid 0px', borderColor: '#FFDE59', color: 'black'}} onClick={()=>save_data()}>Guardar</Button>
              </Modal.Footer>
            </Modal>
          </div>
          :
          null
        }
        
      </div>




  </div>



);

}
export default CentroCosto;
if (document.getElementById('CentroCosto')) {
  ReactDOM.render(<CentroCosto />, document.getElementById('CentroCosto'));
}
