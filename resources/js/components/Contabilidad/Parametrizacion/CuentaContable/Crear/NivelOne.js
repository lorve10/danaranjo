import React,{ useState, useEffect }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../../ApiRest';
import Swal from 'sweetalert2'
import Select from 'react-select'
import makeAnimated from 'react-select/animated'
import '../../../../../../css/app.scss';
import { Button, Accordion, Card, Modal } from 'react-bootstrap';
import obtener from '../Componentes/Cuenta_conex';

 function NivelOne(props) {

   const [ nombre, setNombre ] = useState("")
   const [ descripcion, setDescripcion ] = useState("")
   const [ codigo, setCodigo ] = useState("")
   const [ tipo, setTipo ] = useState(null);
   const [ naturaleza, setNaturaleza ] = useState(null)
   const [ retefuente, setRetefuente ] = useState(null)
   ////primeros checkbox
   const [ factura, setFactura ] = useState(false)/// este es retenciones
   const [ cxc, setCxc ] = useState(false)
   const [ nit, setNit ] = useState(false)
   const [ provee, setProvee ] = useState(false)
   const [ invetario, setInventario ] = useState(false)
   ///segundos checkbox
   const [ mvot, setMvot ] = useState(false)
   const [ activo, setActivo ] = useState(false)
   const [ saldo, setSaldo ] = useState(false)// este me dice si afecta o no el presupuesto
   const [ interes, setInteres ] = useState(false)
   const [ mora, setMora ] = useState(false)
   const [ descuento, setDescuento ] = useState(false)
   const [ efectivo, setEfectivo ] = useState(false)
   /////
   const [ base, setBase ] = useState(null)
   const [ porcentaje, setPorcentaje ] = useState(null)
   const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
   const [naturalezaSelect, setNaturalezaSelect] = useState([
     {
     value:'1',
     label:'Débito',
     },
     {
     value:'2',
     label:'Crédito',
     }
   ]);
   const [tipoSelect, setTipoSelect] = useState([
     {
     value:'1',
     label:'Si',
     },
     {
     value:'0',
     label:'No',
     }
   ]);

   const [tipo2, setTipo2 ] = useState(null)
   const [tipoSelect2, setTipoSelect2] = useState([
     {
     value:'1',
     label:'Activo',
     },
     {
     value:'2',
     label:'Pasivo',
     },
     {
     value:'3',
     label:'Patrimonio',
     },
     {
     value:'4',
     label:'Ingresos',
     },
     {
     value:'5',
     label:'Costos',
      },
     {
     value:'6',
     label:'Gastos',
     }
   ]);

   /// estas constantes te dicen en donde estas ubicado

   const [ clase, setClase ] = useState(null)

   console.log(props.tipoCuent);

   useEffect(()=>{
     validar();
   },[])

   const MessageError = async (data) => {
     Swal.fire({
       title: 'Error',
       text: data,
       icon: 'warning',
     })
   }
   const MessageSuccess = async (data) => {
     Swal.fire({
       text: data,
       icon: 'success',
     })
   }


  const validar = async () =>{



     if(props.id > 0){
      setNombre(props.data.nombre)
      setCodigo(props.data.codigo)
      setDescripcion(props.data.descripcion)
      var f = props.data.retefuente == 0 ? "" : props.data.retefuente;
      setRetefuente(f)
      var b = props.data.base == 0 ? "" :props.data.base
      setBase(b)
      var p = props.data.porcentaje == 0 ? "" :props.data.porcentaje
      setPorcentaje(b)

      var fact = props.data.factura == 1 ? true : false
      setFactura(fact)
      var cuen = props.data.cxc == 1 ? true : false
      setCxc(cuen)

      var nitt = props.data.nit == 1 ? true : false
      setNit(nitt)

      var prov = props.data.proveedores == 1 ? true : false
      setProvee(prov)

      var invent = props.data.inventario == 1 ? true : false
      setInventario(invent)

      var movi = props.data.mvot == 1 ? true : false
      setMvot(movi)
      var acti = props.data.activos == 1 ? true : false
      setActivo(acti)
      var sal = props.data.saldo == 1 ? true : false
      setSaldo(sal)
      var inte = props.data.interes == 1 ? true : false
      setInteres(inte)
      var moraa = props.data.mora == 1 ? true : false
      setMora(moraa)
      var desc = props.data.descuento == 1 ? true : false
      setDescuento(desc)

      var efect = props.data.efectivo == 1 ? true : false
      setEfectivo(efect)

      if(props.data.tipo == 1){
        setTipo({
          value:'1',
          label:'Nominales',
        })
      }
      else {
        setTipo({
          value:'2',
          label:'Reales',
        })
      }

      if(props.data.naturaleza == 1){
        setNaturaleza({
          value:'1',
          label:'Debito',
          })
      }
      else{
        setNaturaleza({
          value:'2',
          label:'Credito',
          })
      }
    }

      if(props.tipoCuent == 1){
        setClase("Activo")
      }
      else if (props.tipoCuent == 2) {
        setClase("Pasivo")
      }
      else if (props.tipoCuent == 3) {
        setClase("Patrimonio")
      }
      else if (props.tipoCuent == 4) {
        setClase("Ingresos")
      }
      else if (props.tipoCuent == 5) {
        setClase("Gastos")
      }
      else if (props.tipoCuent == 6) {
        setClase("Costo de venta")
      }
      else if (props.tipoCuent == 7) {
        setClase("costo de producción y operación")
      }
      else if (props.tipoCuent == 8) {
        setClase("cuentas de orden deudoras")
      }
      else if (props.tipoCuent == 9) {
        setClase("cuentas de orden acreedoras")
      }


      if(props.data.tipo == 1){
        setTipo2({
          value:'1',
          label:'Activo',
          })
      }
      else if (props.data.tipo == 2) {
        setTipo2({
          value:'2',
          label:'Pasivo',
          })
      }
      else if (props.data.tipo == 3) {
        setTipo2({
          value:'3',
          label:'Patrimonio',
          })
      }
      else if (props.data.tipo == 4) {
        setTipo2({
          value:'4',
          label:'Ingresos',
          })
      }
      else if (props.data.tipo == 5) {
        setTipo2({
          value:'5',
          label:'Costos',
          })
      }
      else if (props.data.tipo == 6) {
        setTipo2({
          value:'6',
          label:'Gastos',
          })
      }




  }
  console.log("aqui nivel donde se Guardar");
  console.log(props.nivel);

const crear = () =>{
     var message = ''
     var error = false

     var code = props.codigo + codigo

   if(nombre == ''){
     error = true
     message = "Escribe nombre "
   }else if(codigo == ''){
     error = true
     message = "El valor código no puede ser vacío "
   }
   if(error){
     MessageError(message)
   }
   else{
     const data = new FormData()
     data.append('id',props.id)
     data.append('nombre', nombre)
     data.append('codigo', code)
     data.append('naturaleza', naturaleza.value)
     data.append('nit', nit == false ? 0 : 1 )
     data.append('factura', factura  == false ? 0 : 1)
     data.append('cxc', cxc  == false ? 0 : 1)
     data.append('proveedores', provee  == false ? 0 : 1)
     data.append('inventario', invetario  == false ? 0 : 1)
     data.append('activos', activo  == false ? 0 : 1)
     data.append('saldo', saldo  == false ? 0 : 1)
     data.append('cxc', cxc  == false ? 0 : 1)
     data.append('mvot', mvot  == false ? 0 : 1)
     data.append('interes', interes  == false ? 0 : 1)
     data.append('mora', mora  == false ? 0 : 1)
     data.append('descuento', descuento  == false ? 0 : 1)
     data.append('retefuente', retefuente == null ? 0 :retefuente)
     data.append('base', base == null ? 0 :base )
     data.append('tipo', tipo2.value)
     data.append('porcentaje', porcentaje == null ? 0 :porcentaje)
     data.append('efectivo', efectivo  == false ? 0 : 1)
     data.append('idSubCuentas',  props.codigo)
     data.append('nivel', props.nivel)
     axios.post(ip+'admon/parametrizacion/guard_Niveles',data).then(response=>{
       if (props.id == null) {
         MessageSuccess("Cuenta creada correctamente");
         props.handleClose();
         obtener.cuentas();
        }
       else{
         MessageSuccess("Cuenta editada correctamente");
         props.handleClose();
         obtener.cuentas();
        }

     })
   }

   }

   const inputCodigo = (event) =>{
     var value = event.target.value
     if(value.length > 2){
     }else{
      setCodigo(event.target.value);
     }
   }


   return(
     <div className = "row">
         <div className = "container">
           <p className="m-2"> <strong>Pertenece a :</strong> {clase} - {props.codigo} - {props.nombreGrupo}   </p>
           <div className="col col-md-12 d-flex">
               <label className="d-flex col-12 mr-2 color" style={{width: '170px'}} >Nombre de cuenta:</label>
               <input className="form-control col-md-8 d-flex" style={{width: '480px'}} placeholder="Ingrese el nombre de la cuenta contable" value={nombre} type="text" onChange={(event)=>setNombre(event.target.value)}/>
           </div>
           <div className="col col-md-12 d-flex">
               <label className="d-flex col-12 mr-2 color" style={{width: '170px'}} >Codigo:</label>
               <input className="form-control col-md-8 d-flex" style={{width: '480px'}} placeholder="Ingrese el código" maxLength={2} value={codigo} type="number" onChange={(event)=>inputCodigo(event)}/>
           </div>
           {/*<div className="col col-md-12 d-flex">
               <label className="d-flex col-12 mr-2 color" style={{width: '170px'}} >Descripción:</label>
               <input className="form-control col-md-8 d-flex" style={{width: '480px'}} placeholder="Ingrese la descripción de la cuenta" value={descripcion} type="text" onChange={(event)=>setDescripcion(event.target.value)}/>
           </div>*/}

           <div className = "col col-md-12 d-flex">
               <label className="d-flex col-12 color " style={{width: '170px'}}>Naturaleza: </label>
               <Select
               className="col-8"
               value={naturaleza}
               closeMenuOnSelect={true}
               components={animatedComponents}
               options={naturalezaSelect}
               onChange={(e)=>setNaturaleza(e)}
               placeholder = "Seleccionar naturaleza"
               name="colors"
               />
           </div>
           <div className = "col col-md-12 d-flex">
               <label className="d-flex col-12 color " style={{width: '170px'}}>Tipo: </label>
               <Select
               className="col-8"
               value={tipo2}
               closeMenuOnSelect={true}
               components={animatedComponents}
               options={tipoSelect2}
               onChange={(e)=>setTipo2(e)}
               placeholder = "Seleccionar tipo"
               name="colors"
               />
           </div>
           {
             props.tipoCuent == 4 ||  props.tipoCuent == 5 || props.tipoCuent == 6 ?

             <div className = "col col-md-12 d-flex">
                 <label className="d-flex col-12 color " style={{width: '170px'}}>Afecta presupuesto: </label>
                 <Select
                 className="col-8"
                 value={saldo}
                 closeMenuOnSelect={true}
                 components={animatedComponents}
                 options={tipoSelect}
                 onChange={(e)=>setSaldo(e)}
                 placeholder = "Seleccionar naturaleza"
                 name="colors"
                 />
             </div>
             :null
           }

           <div className = "col col-md-12 d-flex">
               <label className="d-flex col-12 color " style={{width: '170px'}}>Requiere NIT </label>
               <input type="checkbox" checked={nit} style={{ marginTop: '12px', width: '49px',height:'20px'}} onChange={()=>setNit(!nit)} />
           </div>
           <div className = "col col-md-12 d-flex">
               <label className="d-flex col-12 color " style={{width: '170px'}}>Requiere Mvot </label>
               <input type="checkbox" style={{ marginTop: '12px', width: '49px',height:'20px'}} checked={mvot} onChange={()=>setMvot(!mvot)} />
           </div>
           <div className = "col col-md-12 d-flex">
               <label className="d-flex col-12 color " style={{width: '170px'}}>Flujo de efectivo </label>
               <input type="checkbox" style={{ marginTop: '12px', width: '49px',height:'20px'}} checked={efectivo} onChange={()=>setEfectivo(!efectivo)} />
           </div>

           <div className = "col col-md-12 d-flex">

                <label className="col-3"><input type="checkbox"  style={{height:'20px',width: '50px',marginTop: '1px'}}  checked={cxc} onChange={()=>setCxc(!cxc)}   />CxC</label>
                <label className="col-3"><input type="checkbox"  style={{height:'20px',width: '50px',marginTop: '1px', marginLeft: '-24px'}} checked={provee} onChange={()=>setProvee(!provee)} />Proveedores</label>
                <label className="col-3"><input type="checkbox"  style={{height:'20px',width: '50px',marginTop: '1px'}} checked={invetario}  onChange={()=>setInventario(!invetario)} />Inventario</label>
           </div>

           <div className = "col col-md-12 d-flex">

                <label className="col-3"><input type="checkbox"  style={{height:'20px',width: '50px',marginTop: '1px'}} checked={activo} onChange={()=>setActivo(!activo)} />Activo fijo</label>
                <label className="col-3"><input type="checkbox"  style={{height:'20px',width: '50px',marginTop: '1px',  marginLeft: '-24px'}} checked={interes} onChange={()=>setInteres(!interes)}  />Interés</label>
                <label className="col-2"><input type="checkbox"  style={{height:'20px',width: '50px',marginTop: '1px'}} checked={mora} onChange={()=>setMora(!mora)} />Mora</label>
                <label className="col-5"><input type="checkbox"  style={{height:'20px',width: '50px',marginTop: '1px', marginLeft: '47px'}} checked={descuento} onChange={()=>setDescuento(!descuento)}  />Dscto</label>

           </div>
           <div className = "col col-md-12 d-flex">
               <label className="d-flex col-12 color " style={{width: '170px'}}>Retenciones</label>
               <input type="checkbox" checked={factura} style={{ marginTop: '12px', width: '49px',height:'20px'}} onChange={()=>setFactura(!factura)} />
           </div>

           {
             factura == true ?
             <div>

               <div class="row" style={{marginLeft: '-5px'}}>
                   <div className="col-6 d-flex">
                     <label className="d-flex col-6 mr-2 color" style={{width: '170px'}} >Base:</label>
                     <input className="form-control col-md-6 d-flex" style={{width: '480px'}} placeholder="Digite Base" value={base} type="number" onChange={(event)=>setBase(event.target.value)}/>
                   </div>
                   <div className = "col-md-6 d-flex">
                       <label className="d-flex col-3 color mr-3" >% </label>
                       <input className="form-control col-6" style={{width: '173px'}} type="email" value = {porcentaje} placeholder="Porcentaje " onChange = {(e)=>setPorcentaje(e.target.value)}  />
                   </div>
               </div>

             </div>

             :
             <div>
               <div class="row" style={{marginLeft: '-5px'}}>
                   <div className="col-6 d-flex">
                     <label className="d-flex col-6 mr-2 color" style={{width: '170px'}} >Base:</label>
                     <input className="form-control col-md-6 d-flex" style={{width: '480px'}} disabled placeholder="Digite Base" value={base} type="number" onChange={(event)=>setBase(event.target.value)}/>
                   </div>
                   <div className = "col-md-6 d-flex">
                       <label className="d-flex col-3 color mr-3" >% </label>
                       <input className="form-control col-6" style={{width: '173px'}} type="email" disabled value = {porcentaje} placeholder="Porcentaje " onChange = {(e)=>setPorcentaje(e.target.value)}  />
                   </div>
               </div>

             </div>

           }

        </div>

        <Modal.Footer>
           <Button variant="danger" style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}} onClick = {()=>crear()}>Guardar</Button>
        </Modal.Footer>

       </div>
   )

 }

 export default NivelOne;

 if(document.getElementById('NivelOne')){
   ReactDOM.render(<NivelOne/>, document.getElementById('NivelOne'))
 }
