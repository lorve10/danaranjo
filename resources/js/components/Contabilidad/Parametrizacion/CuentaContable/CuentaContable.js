import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Button, Accordion, Card, Modal } from 'react-bootstrap';
import axios from 'axios';
import {ip} from '../../../ApiRest';
import Header from '../../../Ui/Header';
import '../../../../../css/app.scss';
import Divider from '@material-ui/core/Divider';
import desactivar from './Componentes/Desactivar';
import obtener from './Componentes/Cuenta_conex';
import Lista from './Componentes/Lista';
import NivelOne from './Crear/NivelOne';
import desac from './Componentes/Desactivar';
import Swal from 'sweetalert2'




function CuentaContable () {


  const [ showForm, setShowForm ] = useState(1)
  const [ marginval, setMarginVal] = useState(false)
  const [ text, setText] = useState('')
  const [ cuentas, setCuentas] =useState([])
  const [ filtro, setFiltro ] = useState([]);
  /// este es el que recoje el tipo de cuenta contable
  const [ tipoCuent, setTipoCuent] = useState(null);
  ///
  const [ ser, setSer ] = useState("");
  const [ consul, setConsul ] = useState([]);
  const [ subCuent, setSubCuent] = useState([]);
  ////
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(false)
  const [ id, setId ] = useState(null);
  const [ data, setData ] = useState({})
  const [ validar, setValidar ] = useState(false)
  const [ idSub, setIdSub ] = useState(null)
  const [ nombreGrupo, setNombreGrupo ] = useState(null)
  const [ nivel, setNivel ] = useState(null)
  const [ codigo, setCodigo ] = useState(null)


  useEffect(()=>{
    obj();
  },[])



  const handleClose = () =>{
    setShow(false);
    setValidar(false)
    setNivel(null)
    setData({})
    setId(null)
  }

  const handleShow = () => setShow(true);

  const modal = async () => {
  setShow(true);
  }

  const crear = (id, codig, nom,  nivel) => {
    setShow(true)
    setIdSub(id)
    setCodigo(codig)
    setNombreGrupo(nom)
    setNivel(nivel)
  }


  const obj = async (valor, res) => {
      var data = await obtener.cuentas(valor);
    setTipoCuent(valor)
    setFiltro(data)
    setShowForm(res)
  }



  const propHeader = (value) => {
    setMarginVal(value)
  }

  const searchInput  = async () => {
    console.log("holaaa");
    console.log(ser);
    var data = await obtener.filtroCodigo(ser);
    setConsul(data)
  }

  const dataUpdate = async (data, valor, vali) => {
    setLoading(true)

    var nombre = data.nombre
    var codigo = data.codigo
    var descripcion = data.descripcion
    var tipo = data.tipo
    var naturaleza = data.naturaleza
    var nit = data.nit
    var mvot = data.mvot
    var factura = data.factura
    var cxc = data.cxc
    var proveedores = data.proveedores
    var inventario = data.inventario
    var activos = data.activos
    var saldo = data.saldo
    var interes = data.interes
    var mora = data.mora
    var descuento = data.descuento
    var retefuente = data.retefuente
    var base = data.base
    var porcentaje = data.porcentaje

    const dataForAll = {
      nombre,
      codigo,
      descripcion,
      tipo,
      naturaleza,
      nit,
      mvot,
      factura,
      cxc,
      proveedores,
      inventario,
      activos,
      saldo,
      interes,
      mora,
      descuento,
      retefuente,
      base,
      porcentaje
    }
    await setId(data.id)
    await setData(dataForAll)
    setShow(true)
    setValidar(vali)
    setNivel(valor)
    setData({})
    setLoading(false)
  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }

  console.log(showForm);


  return(
<div className="content-wrapper " style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}}>
  <Header  marginval = {(value)=>propHeader(value)}/>
    <div className="verticar-footer">
        <img className="imagen" src={ip+'images/footer.png'}/>
    </div>


    <div>
      <div className="row">
        <div className="col-md-8 mt-5" style={{marginLeft:47}}>
              <div className="btn container d-flex" style={{marginLeft:'-155px'}}>
                <div className="form-section col-md-12 "><strong><h6 className="ruta" style={{marginLeft:'-8px'}}>/ <a href={ip+'admon/contabilidad'}>Módulo de contabilidad </a>/ <a href = {ip+'admon/parametrizacion'}>Módulo parametrización </a>/ Cuentas contables</h6></strong></div>
              </div>
          <strong className="mt-4"><h4>Cuentas Contables</h4></strong>
        <div className="col-7 mt-5 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid ', backgroundColor:'white',borderRadius:12}}>
            <input type = "text" onChange = {(e)=>setSer(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {ser} placeholder="Consultar tipo de cuenta contable "/>
            {
              ser.length == 0 ?
              <span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput()} className= "material-icons-round">Search</span>
              :
              <span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput()} className= "material-icons-round">Search</span>

            }
        </div>
        </div>
      </div>
    </div>

  <div class="container">
    <div class="row" style={{marginLeft:'41px'}}>
      <div class="col-md-10">
      {
        consul.length > 0 ?
        consul.map((item ,index)=>{
          console.log(item.nombre_subcuenta);
        if(item.codigo == null){
          var numero =  JSON.stringify(item.codigo_subcuenta);
          var no = item.nombre_subcuenta;
          var code = item.codigo_subcuenta
        }
        else {
          var numero =  JSON.stringify(item.codigo);
          var no = item.nombre
          var code = item.codigo
        }
          var resul = numero.length == 2;
          var n = parseInt(item.nivel);
          var res;
          if(n == 1){
            res = 2
          }
          else if (n == 2) {
            res = 3
          }
          else if (n == 3) {
            res = 4
          }
          else if (n == 4) {
            res = 5
          }
          else {
            res = 1
          }
          console.log(res);
          return (
            <Accordion defaultActiveKey={index} >
          <Accordion.Item eventKey="1">
            <Accordion.Header>
              <div className = "row" style = {{width:'inherit'}}>
              <div className = "col-md-6">
              {item.codigo == null ? item.codigo_subcuenta : item.codigo} - {item.nombre == null ? item.nombre_subcuenta : item.nombre}
              </div>
              {
                resul == true ?
                <div></div>
                :
              <div className = "col-md-6 justify-content-end d-flex">
              {
              item.mvot == 1 ?
              <button  className="btn "><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas subcuentas")} >add_circle</i></button>
              :
              <button  className="btn "><i className = "material-icons"  onClick={()=>crear(item.id, code  , no, res)} >add_circle</i></button>
              /*<button  className="btn "><i className = "material-icons"  onClick={()=>dataUpdate(item, item.nivel ,true)}>edit</i></button>
                <button  className="btn" onClick={()=>desac.dato(item, item.nivel)} ><i className = "material-icons" style={{color:'red'}}>cancel</i></button>*/
              }
              </div>
            }
              </div>
            </Accordion.Header>
            <Accordion.Body>
              {

              }
            </Accordion.Body>
          </Accordion.Item>
          </Accordion>
          );
        })
          :null
      }
      </div>
    </div>
  </div>

    <div className="row mt-5">
      <div className="container" style={{marginLeft:'56px'}}>
        <div className="col-md-12" >
            <div>
              <strong className="d-flex">
                  <p className="d-flex col-4">Titulo</p>
                  <p className="d-flex col-4"></p>
                  <p className="">Acciones</p>
              </strong>
            </div>
            <div className="col-md-10">

              <div class="primerComponente">
                <div class="d-flex">
                  {
                    showForm == 2 ?
                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setShowForm(1)}>
                        expand_less
                    </span>
                    :
                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>obj(1,2)}>
                        expand_more
                    </span>

                  }
                  <strong>1.Activo</strong>
                </div>
                <hr></hr>
                  {
                    showForm == 2 ?
                    <Lista filtro={filtro}  tipoCuent={tipoCuent}/>
                    :null
                  }
              </div>

              <div class="SegundoComponente">
                <div class="d-flex">

                  {
                    showForm == 3 ?
                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setShowForm(1)}>
                        expand_less
                    </span>
                    :
                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>obj(2, 3)}>
                        expand_more
                    </span>

                  }

                  <strong>2.Pasivo</strong>
                </div>
                <hr></hr>
                  {
                    showForm == 3 ?
                    <Lista filtro={filtro}  tipoCuent={tipoCuent}/>
                    :null
                  }
              </div>

              <div class="TercerComponente">
                <div class="d-flex">

                  {
                    showForm == 4 ?
                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setShowForm(1)}>
                        expand_less
                    </span>
                    :
                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}}  onClick={()=>obj(3, 4)}>
                        expand_more
                    </span>
                  }
                  <strong>3.Patrimonio</strong>
                </div>
                <hr></hr>
                  {
                    showForm == 4 ?
                    <Lista filtro={filtro}  tipoCuent={tipoCuent}/>
                    :null
                  }
              </div>

              <div class="cuatroComponente">
                <div class="d-flex">
                  {
                    showForm == 5 ?
                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setShowForm(1)}>
                        expand_less
                    </span>
                    :
                  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>obj(4, 5)}>
                      expand_more
                  </span>
                }
                  <strong>4.Ingresos</strong>
                </div>
                <hr></hr>
                  {
                    showForm == 5 ?
                    <Lista filtro={filtro}  tipoCuent={tipoCuent}/>
                    :null
                  }
              </div>

              <div class="cincoComponente">
                <div class="d-flex">
                  {
                    showForm == 6 ?
                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setShowForm(1)}>
                        expand_less
                    </span>
                    :
                  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>obj(5, 6)}>
                      expand_more
                  </span>
                }
                  <strong>5.Gastos</strong>
                </div>
                <hr></hr>
                  {
                    showForm == 6 ?
                    <Lista filtro={filtro}  tipoCuent={tipoCuent}/>
                    :null
                  }
              </div>

              <div class="seisComponente">
                <div class="d-flex">
                  {
                    showForm == 7 ?
                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setShowForm(1)}>
                        expand_less
                    </span>
                    :
                  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>obj(6, 7)}>
                      expand_more
                  </span>
                }
                  <strong>6.Costos de venta</strong>
                </div>
                <hr></hr>
                  {
                    showForm == 7 ?
                    <Lista filtro={filtro}  tipoCuent={tipoCuent}/>
                    :null
                  }
              </div>

              <div class="sieteComponente">
                <div class="d-flex">
                  {
                    showForm == 8 ?
                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setShowForm(1)}>
                        expand_less
                    </span>
                    :
                  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>obj(7, 8)}>
                      expand_more
                  </span>
                }
                  <strong>7.Costos de producción o de operación</strong>
                </div>
                <hr></hr>
                  {
                    showForm == 8 ?
                    <Lista filtro={filtro}  tipoCuent={tipoCuent}/>
                    :null
                  }
              </div>

              <div class="ochoComponente">
                <div class="d-flex">
                  {
                    showForm == 9 ?
                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setShowForm(1)}>
                        expand_less
                    </span>
                    :
                  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>obj(8, 9)}>
                      expand_more
                  </span>
                }
                  <strong>8.Cuentas de orden deudoras</strong>
                </div>
                <hr></hr>
                  {
                    showForm == 9 ?
                    <Lista filtro={filtro}  tipoCuent={tipoCuent}/>
                    :null
                  }
              </div>

              <div class="nueveComponente">
                <div class="d-flex">
                  {
                    showForm == 10 ?
                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setShowForm(1)}>
                        expand_less
                    </span>
                    :
                  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>obj(9, 10)}>
                      expand_more
                  </span>
                }
                  <strong>9.Cuentas de orden acreedoras</strong>
                </div>
                <hr></hr>
                  {
                    showForm == 10 ?
                    <Lista filtro={filtro}  tipoCuent={tipoCuent}/>
                    :null
                  }
              </div>


            </div>
        </div>
      </div>
    </div>
    <Modal show={show} onHide={handleClose} size="lg">
       <Modal.Header closeButton>
         <Modal.Title>Crear cuenta contable</Modal.Title>
       </Modal.Header>
       <Modal.Body>
         <NivelOne id={id} data = {data} nom={validar} idSub={idSub} codigo={codigo} nombreGrupo = {nombreGrupo} nivel={nivel} tipoCuent={tipoCuent} handleClose={()=>handleClose()}/>
       </Modal.Body>
   </Modal>
  </div>
)
}


export default CuentaContable;

if(document.getElementById('CuentaContable')){
  ReactDOM.render(<CuentaContable />, document.getElementById('CuentaContable'))
}
