import axios from 'axios';
import {ip} from '../../../../ApiRest'



const data = {}


//// obtener grupos
data.cuentas = async (valor) =>{
  const data = new FormData()
  data.append('id', valor)
  var res = await axios.post(ip+'admon/parametrizacion/obtain_grupo',data).then(response=>{
      return response.data;
  })
  return res
}

////obtener cuentas
data.cuentasSub = async () =>{
  var res = await axios.get(ip+'admon/parametrizacion/obtain_Cuentas').then(response=>{
      return response.data;
  })
  return res
}
//subcuentas
data.obtenerSubcuentas = async () =>{
  var res = await axios.get(ip+'admon/parametrizacion/obtain_Subcuentas').then(response=>{
      return response.data;
  })
  return res
}
//auxiliar == nivel 4
data.obtenerAuxiliar = async () =>{
  var res = await axios.get(ip+'admon/parametrizacion/obtain_Nivel').then(response=>{
      return response.data;
  })
  return res
}




////filtro de datos por codigo
data.filtroCodigo = async (valor) =>{
  const data = new FormData()
  data.append('codigo', valor)
  var res = await axios.post(ip+'admon/parametrizacion/obtenerCodigo',data).then(response=>{
      return response.data;
  })
  return res
}






export default  data;
