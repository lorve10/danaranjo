import axios from 'axios';
import {ip} from '../../../../ApiRest';
import Swal from 'sweetalert2'

const MessageError = async (data) => {
  Swal.fire({
    title: 'Error',
    text: data,
    icon: 'warning',
  })
}
const MessageSuccess = async (data) => {
  Swal.fire({
    text: data,
    icon: 'success',
  })
}

const deshabilitar = {}



deshabilitar.dato = async (value, nivel) => {
  var message = '¿Deseas eliminarlo?'
  Swal.fire({
    title: message,
    showDenyButton: true,
    confirmButtonText: `Sí`,
    denyButtonText: `No`,
  }).then((result) => {
    /* Read more about isConfirmed, isDenied below */
    if (result.isConfirmed) {
      const data = new FormData()
      data.append('id', value.id)
      data.append('tipo_nivel', nivel)
      axios.post(ip+'admon/contabilidad/deshabilitar_One',data).then(response=>{

        Swal.fire('Eliminado correctamente!', '', 'success')
        props.gobackSave()
      })

    } else if (result.isDenied) {
      Swal.fire('Acción cancelada', '', 'info')
    }
  })
}



export default  deshabilitar;
