import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Swal from 'sweetalert2'
import obtener from './Cuenta_conex';
import {ip} from '../../../../ApiRest';
import '../../../../../../css/app.scss';
import NivelOne from '../Crear/NivelOne';
import desactivar from './Desactivar';

import { Button, Accordion, Card, Modal } from 'react-bootstrap';



function Lista(props){

  const [ grupo, setGrupo ] =  useState([])
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(false)

  //variables para envio al Modal
  const [ data, setData ] = useState({})
  const [ id, setId ] = useState(null)
  const [ idSub, setIdSub ] = useState(null)
  const [ nombreGrupo, setNombreGrupo ] = useState(null)
  const [ codigo, setCodigo ] = useState(null)
  const [ nivel, setNivel ] = useState(null)

  ///este resive el tipo cuent de cuenta contable (setTipoCuent)
  const [ level, setLevel ] = useState(null)
  ////este se encarga de limpiar modal
  const [ validar, setValidar ] = useState(false)

  const [ resiveNivel, setResiveNivel ] = useState(props.todosNiveles)
  const [ filtro2, setFiltro2 ] = useState([])

  const handleClose = () =>{
    setShow(false);
    setValidar(false)
    setNivel(null)
  }

  const handleShow = () => setShow(true);

  const modal = async () => {
  setShow(true);
  }

  const crear = (id, codig, nom, nivel) => {
    setShow(true)
    setIdSub(id)
    setCodigo(codig)
    setNombreGrupo(nom)
    setNivel(nivel)
  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }


  const dataUpdate = async (data, valor, vali) => {
    setLoading(true)

    var nombre = data.nombre
    var codigo = data.codigo
    var descripcion = data.descripcion
    var tipo = data.tipo
    var naturaleza = data.naturaleza
    var nit = data.nit
    var mvot = data.mvot
    var factura = data.factura
    var cxc = data.cxc
    var proveedores = data.proveedores
    var inventario = data.inventario
    var activos = data.activos
    var saldo = data.saldo
    var interes = data.interes
    var mora = data.mora
    var descuento = data.descuento
    var retefuente = data.retefuente
    var base = data.base
    var porcentaje = data.porcentaje
    var efectivo = data.efectivo


    const dataForAll = {
      nombre,
      codigo,
      descripcion,
      tipo,
      naturaleza,
      nit,
      mvot,
      factura,
      cxc,
      proveedores,
      inventario,
      activos,
      saldo,
      interes,
      mora,
      descuento,
      retefuente,
      base,
      porcentaje,
      efectivo

    }
    await setId(data.id)
    await setData(dataForAll)
    setShow(true)
    setValidar(vali)
    setNivel(valor)
    setData({})
    setLoading(false)
  }

useEffect(()=>{
filtrarNivel()
setFiltro2(props.filtro)

},[])

const filtrarNivel = (nivel,idNivel) => {
  if(props.todosNiveles){
    const data = props.todosNiveles.filter((item)=> item.nivel==nivel && item.idSubCuentas==idNivel )
    return data;
  }



}



  return(
    <div>
          {
            filtro2.map((item,index)=>{
            var numero =  JSON.stringify(item.codigo);
            var resul = numero.length == 2;

              return (
                <Accordion defaultActiveKey={index} >
              <Accordion.Item eventKey="0">
                <Accordion.Header>
                <div className = "row" style = {{width:'inherit'}}>
                <div className = "col-md-6">
                {item.codigo + " - " + item.nombre}
                </div>
                {
                  resul == true ?
                  <div></div>
                  : 
                  
                  item.mvot == 1 ?
                  <div className = "col-md-6 justify-content-end d-flex">
                  
                    <button  className="btn "><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas subcuentas ")} >add_circle</i></button>
                    :
                    <button  className="btn "><i className = "material-icons"    onClick={()=>crear(item.id, item.codigo, item.nombre, 1)} >add_circle</i></button>
                    {/*<button  className="btn "><i className = "material-icons"    onClick = {()=>dataUpdate1(item, 5)}>edit</i></button>
                    <button  className="btn" onClick={()=>desactivar.dato(item)}><i className = "material-icons" style={{color:'red'}}>cancel </i></button>*/}
                  </div>

                  : null
                  
                }


                </div>
                </Accordion.Header>
                <Accordion.Body>
              {


                filtrarNivel(1,item.id).map((item1 ,index1)=>{
                  return (
                    <Accordion defaultActiveKey={index1} >
                  <Accordion.Item eventKey="0">
                    <Accordion.Header>
                      <div className = "row" style = {{width:'inherit'}}>
                      <div className = "col-md-6">
                      {item1.codigo + " - " + item1.nombre}
                      </div>

                        <div className = "col-md-6 justify-content-end d-flex">
                        {
                          item1.mvot == 1 ?
                          <button  className="btn "><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas subcuentas ")} >add_circle</i></button>

                          :
                          <button  className="btn "><i className = "material-icons"  onClick={()=>crear(item1.id, item1.codigo , item1.nombre, 2)} >add_circle</i></button>
                        }
                        {/*<button  className="btn "><i className = "material-icons"  onClick={()=>dataUpdate(item1 ,1, true )}>edit</i></button>
                        <button  className="btn" onClick={()=>desactivar.dato(item1, 1)} ><i className = "material-icons" style={{color:'red'}}>cancel</i></button>*/}
                        </div>

                      </div>
                    </Accordion.Header>
                    <Accordion.Body>
                      {
                        filtrarNivel(2,item1.id).map((item2 ,index2)=>{
                          return (
                            <Accordion defaultActiveKey={index2} >
                          <Accordion.Item eventKey="0">
                            <Accordion.Header>
                              <div className = "row" style = {{width:'inherit'}}>
                              <div className = "col-md-6">
                              {item2.codigo + " - " + item2.nombre}
                              </div>
                              <div className = "col-md-6 justify-content-end d-flex">
                                {
                                  item2.mvot == 1 ?
                                  <button  className="btn "><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas subcuentas")} >add_circle</i></button>
                                  :
                                  <button  className="btn "><i className = "material-icons"  onClick={()=>crear(item1.id, item1.codigo , item1.nombre, 2)} >add_circle</i></button>
                                }                              
                                {/*<button  className="btn "><i className = "material-icons"  onClick={()=>dataUpdate(item2,2, true)}>edit</i></button>
                                <button  className="btn" onClick={()=>desactivar.dato(item2, 2)} ><i className = "material-icons" style={{color:'red'}}>cancel</i></button>*/}
                              </div>
                              </div>
                            </Accordion.Header>
                            <Accordion.Body>
                              {
                                filtrarNivel(3, item2.id).map((item3 ,index3)=>{
                                  return (
                                    <Accordion defaultActiveKey={index3} >
                                  <Accordion.Item eventKey="0">
                                    <Accordion.Header>
                                      <div className = "row" style = {{width:'inherit'}}>
                                      <div className = "col-md-6">
                                      {item3.codigo + " - " + item3.nombre}
                                      </div>
                                      <div className = "col-md-6 justify-content-end d-flex">
                                        {
                                          item3.mvot == 1 ?
                                          <button  className="btn "><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas subcuentas")} >add_circle</i></button>
                                          :
                                          <button  className="btn "><i className = "material-icons"  onClick={()=>crear(item1.id, item1.codigo , item1.nombre, 2)} >add_circle</i></button>
                                        }                                      
                                        {/*<button  className="btn "><i className = "material-icons"  onClick={()=>dataUpdate(item3,3,true)}>edit</i></button>
                                        <button  className="btn" onClick={()=>desactivar.dato(item3, 3)} ><i className = "material-icons" style={{color:'red'}}>cancel</i></button>*/}
                                      </div>
                                      </div>
                                    </Accordion.Header>
                                    <Accordion.Body>
                                      {
                                        filtrarNivel(4,item3.id).map((item4 ,index4)=>{
                                          return (
                                            <Accordion defaultActiveKey={index4} >
                                          <Accordion.Item eventKey="0">
                                            <Accordion.Header>
                                              <div className = "row" style = {{width:'inherit'}}>
                                              <div className = "col-md-6">
                                              {item4.codigo + " - " + item4.nombre}
                                              </div>
                                              <div className = "col-md-6 justify-content-end d-flex">
                                                {
                                                  item4.mvot == 1 ?
                                                  <button  className="btn "><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas subcuentas")} >add_circle</i></button>
                                                  :
                                                  <button  className="btn "><i className = "material-icons"  onClick={()=>crear(item1.id, item1.codigo , item1.nombre, 2)} >add_circle</i></button>
                                                }                                              
                                                {/*<button  className="btn "><i className = "material-icons"  onClick={()=>dataUpdate(item4,4,true)}>edit</i></button>
                                                <button  className="btn" onClick={()=>desactivar.dato(item4,4)} ><i className = "material-icons" style={{color:'red'}}>cancel</i></button>*/}
                                              </div>
                                              </div>
                                            </Accordion.Header>
                                            <Accordion.Body>
                                              {
                                                filtrarNivel(5,item4.id).map((item5 ,index5)=>{
                                                  return (
                                                    <Accordion defaultActiveKey={index5} >
                                                  <Accordion.Item eventKey="0">
                                                    <Accordion.Header>
                                                      <div className = "row" style = {{width:'inherit'}}>
                                                      <div className = "col-md-6">
                                                      {item5.codigo + " - " + item5.nombre}
                                                      </div>
                                                      <div className = "col-md-6 justify-content-end d-flex">
                                                        {
                                                          item5.mvot == 1 ?
                                                          <button  className="btn "><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas subcuentas")} >add_circle</i></button>
                                                          :
                                                          <button  className="btn "><i className = "material-icons"  onClick={()=>crear(item1.id, item1.codigo , item1.nombre, 2)} >add_circle</i></button>
                                                        }                                                      {/*<button  className="btn "><i className = "material-icons"  onClick={()=>dataUpdate(item5,5,true)}>edit</i></button>
                                                        <button  className="btn" onClick={()=>desactivar.dato(item5, 5)} ><i className = "material-icons" style={{color:'red'}}>cancel</i></button>*/}
                                                      </div>
                                                      </div>
                                                    </Accordion.Header>
                                                    <Accordion.Body>
                                                      {

                                                      }
                                                    </Accordion.Body>
                                                  </Accordion.Item>
                                                  </Accordion>
                                                  );
                                                })
                                              }
                                            </Accordion.Body>
                                          </Accordion.Item>
                                          </Accordion>
                                          );
                                        })
                                      }
                                    </Accordion.Body>
                                  </Accordion.Item>
                                  </Accordion>
                                  );
                                })
                              }
                            </Accordion.Body>
                          </Accordion.Item>
                          </Accordion>
                          );
                        })
                      }
                    </Accordion.Body>
                  </Accordion.Item>
                  </Accordion>
                  );
                })

              }

                </Accordion.Body>
              </Accordion.Item>
              </Accordion>
              );
            })

          }
      <Modal show={show} onHide={handleClose} size="lg">
         <Modal.Header closeButton>
           <Modal.Title>Crear cuenta contable</Modal.Title>
         </Modal.Header>
         <Modal.Body>
           <NivelOne id={id} data = {data} nom={validar} idSub={idSub} codigo={codigo} nombreGrupo = {nombreGrupo} nivel={nivel} tipoCuent={props.tipoCuent} handleClose={()=>handleClose()}/>
         </Modal.Body>
     </Modal>

    </div>
  )

}

export default Lista;
if(document.getElementById('Lista')){
  ReactDOM.render(<Lista />, document.getElementById('Lista'))
}
