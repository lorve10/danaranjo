import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Swal from 'sweetalert2'
import obtener from './Cuenta_conex';
import {ip} from '../../../../ApiRest';
import '../../../../../../css/app.scss';
import NivelOne from '../Crear/NivelOne';
import desactivar from './Desactivar';

import { Button, Accordion, Card, Modal } from 'react-bootstrap';



function Lista(props){

  const [ grupo, setGrupo ] =  useState([])
  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);

  const [loading, setLoading] = useState(false)

  //variables para envio al Modal
  const [ data, setData ] = useState({})
  const [ id, setId ] = useState(null)
  const [ idSub, setIdSub ] = useState(null)
  const [ nombreGrupo, setNombreGrupo ] = useState(null)
  const [ codigo, setCodigo ] = useState(null)
  const [ nivel, setNivel ] = useState(null)

  ///este resive el tipo cuent de cuenta contable (setTipoCuent)
  const [ level, setLevel ] = useState(null)
  ////este se encarga de limpiar modal
  const [ validar, setValidar ] = useState(false)

  const [ resiveNivel, setResiveNivel ] = useState(props.todosNiveles)
  const [ filtro2, setFiltro2 ] = useState([])

  ///
  const [ detalles, setDetalles ] = useState([])
  const handleClose2 = () => setShow2(false);



  const handleClose = () =>{
    setShow(false);
    setValidar(false)
    setNivel(null)
    filtroAux()
  }

  const handleShow = () => setShow(true);

  const modal = async () => {
  setShow(true);
  }

  const [cuen, setCuen ] = useState([])
  const [subCuen, setSubCuen ] = useState([])
  const [ auxiliar, setAuxiliar ] = useState([])
  ///
  const [ estado, setEstado ] = useState(0)
  const [ estado2, setEstado2 ] = useState(0)
  const [ estado3, setEstado3 ] = useState(0)
  const [ estado4, setEstado4 ] = useState(0)
  const [ estado5, setEstado5 ] = useState(0)
  const [ estado6, setEstado6 ] = useState(0)
  const [ estado7, setEstado7 ] = useState(0)
  const [ estado8, setEstado8 ] = useState(0)
  const [ estado9, setEstado9 ] = useState(0)
  const [ estado10, setEstado10 ] = useState(0)


  useEffect(()=>{
    cargarCuentas()
    SubCuentas()
    todoniveles()
  },[])

///cuentas
  const cargarCuentas = async () => {
    var data = await obtener.cuentasSub();
    setCuen(data)
  }
  //subCuentas
  const SubCuentas = async () => {
    var data = await obtener.obtenerSubcuentas();
    todoniveles()
    setSubCuen(data)
  }

  const todoniveles = async () => {
    var data = await obtener.obtenerAuxiliar();
    setAuxiliar(data)
  }
  // const obtener = async ()=>{
  //   const da = await obtener.cuentasSub()
  //   console.log("data");
  //   console.log(da);
  //
  // }


  const crear = (id, codig, nom, nivel) => {
    setShow(true)
    setIdSub(id)
    setCodigo(codig)
    setNombreGrupo(nom)
    setNivel(nivel)
  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }


  const dataUpdate = async (data, valor, vali) => {
    setLoading(true)

    var nombre = data.nombre
    var codigo = data.codigo
    var descripcion = data.descripcion
    var tipo = data.tipo
    var naturaleza = data.naturaleza
    var nit = data.nit
    var mvot = data.mvot
    var factura = data.factura
    var cxc = data.cxc
    var proveedores = data.proveedores
    var inventario = data.inventario
    var activos = data.activos
    var saldo = data.saldo
    var interes = data.interes
    var mora = data.mora
    var descuento = data.descuento
    var retefuente = data.retefuente
    var base = data.base
    var porcentaje = data.porcentaje
    var efectivo = data.efectivo


    const dataForAll = {
      nombre,
      codigo,
      descripcion,
      tipo,
      naturaleza,
      nit,
      mvot,
      factura,
      cxc,
      proveedores,
      inventario,
      activos,
      saldo,
      interes,
      mora,
      descuento,
      retefuente,
      base,
      porcentaje,
      efectivo

    }
    await setId(data.id)
    await setData(dataForAll)
    setShow(true)
    setValidar(vali)
    setNivel(valor)
    setData({})
    setLoading(false)
  }

useEffect(()=>{
filtrarSub()
setFiltro2(props.filtro)
console.log(props.subCuent);

},[])

const dataDet = async (data) => {
  setShow2(true);
  setDetalles(data);
}




const filtrarSub = (valor) => {
    const data = subCuen.filter((item)=> item.cuenta_id == valor)
    return data;

}

const filtroCuen  = (valor) => {
  const  res = cuen.filter((item)=>item.grupo_id == valor)
    return res;
}

const filtroAux  = (nivel ,valor) => {
  const  res = auxiliar.filter((item)=>item.nivel == nivel && item.idSubCuentas == valor)
    return res;
}

  return(
    <div>
          {
            filtro2.map((item,index)=>{
            var numero =  JSON.stringify(item.codigo);
            var resul = numero.length == 2;
              return (
                <div>
                  <div class="componente" style={{marginLeft:5}}>
                    <div class="d-flex">
                      {
                        estado == 0?
                        <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado(item.codigo)}>
                            expand_more
                        </span>
                        :estado == item.codigo?
                        <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado(0)}>
                            expand_less
                        </span>
                        :  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado(item.codigo)}>
                              expand_more
                          </span>

                      }

                      <p>{item.codigo} - {item.nombre_grupo}</p>
                    </div>
                    <hr></hr>
                    {

                      filtroCuen(item.codigo).map((item1, index1)=>{
                        return(
                          <div>
                            {
                              estado == item1.grupo_id?

                              <div class="Componente" style={{marginLeft:40}}>
                                <div class="d-flex">
                                  {
                                    estado2 == 0 ?
                                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado2(item1.codigo_cuenta)}>
                                        expand_more
                                    </span>
                                    :estado2 == item1.codigo_cuenta ?
                                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado2(0)}>
                                        expand_less
                                    </span>
                                    :
                                    <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado2(item1.codigo_cuenta)}>
                                        expand_more
                                    </span>

                                  }

                                    <p>  {item1.codigo_cuenta} - {item1.nombre_cuenta}</p>
                                </div>
                                <hr></hr>
                                {
                                  filtrarSub(item1.codigo_cuenta).map((item2, index2)=>{
                                    return(
                                    <div>
                                      {
                                        estado2 == item2.cuenta_id ?

                                        <div class="Componente" style={{marginLeft:40}}>
                                          <div class="d-flex">
                                            {
                                              estado3 == 0 ?
                                              <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado3(item2.codigo_subcuenta)}>
                                                  expand_more
                                              </span>
                                              : estado3 == item2.codigo_subcuenta ?
                                              <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado3(0)}>
                                                  expand_less
                                              </span>
                                              :
                                              <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado3(item2.codigo_subcuenta)}>
                                                  expand_more
                                              </span>

                                            }

                                            <p class="col-md-10">{item2.codigo_subcuenta} - {item2.nombre_subcuenta}</p>
                                            <button className="btn "><i className = "material-icons"  onClick={()=>crear(item2.id, item2.codigo_subcuenta , item2.nombre_subcuenta, 1)} >add_circle</i></button>
                                          </div>
                                          <hr></hr>

                                        {
                                          filtroAux(1, item2.codigo_subcuenta).map((item3, index3)=>{
                                            return(
                                              <div>
                                              {
                                                estado3 == item3.idSubCuentas ?

                                                <div class="SegundoComponente" style={{marginLeft:40}}>
                                                  <div class="d-flex">
                                                    {
                                                      estado4 == 0 ?
                                                      <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado4(item3.codigo)}>
                                                          expand_more
                                                      </span>
                                                      :estado4 == item3.codigo ?
                                                      <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado4(0)}>
                                                          expand_less
                                                      </span>
                                                      :
                                                      <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado4(item3.codigo)}>
                                                          expand_more
                                                      </span>

                                                    }

                                                      <p class="col-md-9">{item3.codigo} - {item3.nombre}</p>
                                                      {
                                                          item3.mvot == 1 ?
                                                          <div style={{marginLeft:"17px"}}>
                                                            <button  className="btn "><i class = "material-icons" onClick = {()=>dataDet(item3)} >visibility</i></button>
                                                              <button className="btn"><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas subcuentas.")} >add_circle</i></button>
                                                          </div>

                                                          :
                                                          <div style={{marginLeft:"17px"}}>
                                                            <button  className="btn "><i class = "material-icons" onClick = {()=>dataDet(item3)} >visibility</i></button>
                                                            <button className="btn"><i className = "material-icons"  onClick={()=>crear(item3.id, item3.codigo , item3.nombre, 2)} >add_circle</i></button>
                                                          </div>

                                                      }
                                                  </div>
                                                  <hr></hr>
                                                    {
                                                      filtroAux(2, item3.codigo).map((item4, index4)=>{
                                                        return(
                                                          <div>
                                                          {
                                                            estado4 == item4.idSubCuentas ?

                                                            <div class="SegundoComponente" style={{marginLeft:40}}>
                                                              <div class="d-flex">
                                                                {
                                                                  estado5 == 0 ?
                                                                  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado5(item4.codigo)}>
                                                                      expand_more
                                                                  </span>
                                                                  :estado5 == item4.codigo ?
                                                                  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado5(0)}>
                                                                      expand_less
                                                                  </span>
                                                                  :
                                                                  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado5(item4.codigo)}>
                                                                      expand_more
                                                                  </span>

                                                                }

                                                                  <p class="col-md-9">{item4.codigo} - {item4.nombre}</p>
                                                                    {
                                                                        item4.mvot == 1 ?
                                                                        <div style={{marginLeft:"7px"}}>
                                                                          <button onClick = {()=>dataDet(item4)} className="btn "><i class = "material-icons">visibility</i></button>
                                                                            <button className="btn"><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas subcuentas.")} >add_circle</i></button>
                                                                        </div>
                                                                      :
                                                                      <div style={{marginLeft:"7px"}}>
                                                                        <button onClick = {()=>dataDet(item4)} className="btn "><i class = "material-icons">visibility</i></button>
                                                                        <button className="btn"><i className = "material-icons"  onClick={()=>crear(item4.id, item4.codigo , item4.nombre, 3)} >add_circle</i></button>

                                                                      </div>
                                                                    }
                                                            </div>
                                                              <hr></hr>
                                                                {
                                                                  filtroAux(3, item4.codigo).map((item5, index5)=>{
                                                                    return(
                                                                      <div>
                                                                      {
                                                                        estado5 == item5.idSubCuentas ?

                                                                        <div class="SegundoComponente" style={{marginLeft:40}}>
                                                                          <div class="d-flex">
                                                                            {
                                                                              estado6 == 0 ?
                                                                              <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado6(item5.codigo)}>
                                                                                  expand_more
                                                                              </span>
                                                                              :estado6 == item5.codigo ?
                                                                              <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado6(0)}>
                                                                                  expand_less
                                                                              </span>
                                                                              :
                                                                              <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado6(item5.codigo)}>
                                                                                  expand_more
                                                                              </span>

                                                                            }

                                                                              <p class="col-md-9">{item5.codigo} - {item5.nombre}</p>
                                                                              {
                                                                                item5.mvot == 1 ?
                                                                                <div style={{marginLeft:'-2px'}}>
                                                                                  <button onClick = {()=>dataDet(item5)} className="btn "><i class = "material-icons">visibility</i></button>
                                                                                    <button className="btn"><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas subcuentas.")} >add_circle</i></button>
                                                                                </div>
                                                                              :
                                                                              <div style={{marginLeft:'-2px'}}>
                                                                                <button onClick = {()=>dataDet(item5)} className="btn "><i class = "material-icons">visibility</i></button>
                                                                                  <button className="btn"><i className = "material-icons"  onClick={()=>crear(item5.id, item5.codigo , item5.nombre, 4)} >add_circle</i></button>

                                                                              </div>
                                                                              }
                                                                        </div>
                                                                          <hr></hr>
                                                                            {
                                                                              filtroAux(4, item5.codigo).map((item6, index6)=>{
                                                                                return(
                                                                                  <div>
                                                                                  {
                                                                                    estado6 == item6.idSubCuentas ?

                                                                                    <div class="SegundoComponente" style={{marginLeft:40}}>
                                                                                      <div class="d-flex">
                                                                                        {
                                                                                          estado7 == 0 ?
                                                                                          <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado7(item6.codigo)}>
                                                                                              expand_more
                                                                                          </span>
                                                                                          :estado7 == item6.codigo ?
                                                                                          <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado7(0)}>
                                                                                              expand_less
                                                                                          </span>
                                                                                          :
                                                                                          <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado7(item6.codigo)}>
                                                                                              expand_more
                                                                                          </span>

                                                                                        }

                                                                                          <p class="col-md-9">{item6.codigo} - {item6.nombre}</p>
                                                                                            {
                                                                                                item6.mvot == 1 ?
                                                                                                <div style={{marginLeft: '-11px'}}>
                                                                                                  <button onClick = {()=>dataDet(item6)} className="btn "><i class = "material-icons">visibility</i></button>
                                                                                                    <button className="btn"><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas subcuentas.")} >add_circle</i></button>
                                                                                                </div>
                                                                                                :
                                                                                                <div style={{marginLeft: '-11px'}}>
                                                                                                  <button onClick = {()=>dataDet(item6)} className="btn "><i class = "material-icons">visibility</i></button>
                                                                                                    <button className="btn"><i className = "material-icons"  onClick={()=>crear(item6.id, item6.codigo , item6.nombre, 5)} >add_circle</i></button>

                                                                                                </div>
                                                                                            }

                                                                                    </div>
                                                                                      <hr></hr>
                                                                                        {
                                                                                          filtroAux(5, item6.codigo).map((item7, index7)=>{
                                                                                            return(
                                                                                              <div>
                                                                                              {
                                                                                                estado7 == item7.idSubCuentas ?

                                                                                                <div class="SegundoComponente" style={{marginLeft:40}}>
                                                                                                  <div class="d-flex">
                                                                                                    {
                                                                                                      estado8 == 0 ?
                                                                                                      <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado8(item7.codigo)}>
                                                                                                          expand_more
                                                                                                      </span>
                                                                                                      :estado8 == item6.codigo ?
                                                                                                      <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado8(0)}>
                                                                                                          expand_less
                                                                                                      </span>
                                                                                                      :
                                                                                                      <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado8(item7.codigo)}>
                                                                                                          expand_more
                                                                                                      </span>

                                                                                                    }

                                                                                                      <p class="col-md-9">{item7.codigo} - {item7.nombre}</p>
                                                                                                        {
                                                                                                            item7.mvot == 1 ?
                                                                                                            <div style={{marginLeft: '-20px'}}>
                                                                                                              <button onClick = {()=>dataDet(item7)} className="btn "><i class = "material-icons">visibility</i></button>
                                                                                                                <button className="btn"><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas subcuentas.")} >add_circle</i></button>
                                                                                                            </div>
                                                                                                            :
                                                                                                            <div style={{marginLeft: '-20px'}}>
                                                                                                              <button onClick = {()=>dataDet(item7)} className="btn "><i class = "material-icons">visibility</i></button>
                                                                                                              <button className="btn"><i className = "material-icons"  onClick={()=>crear(item7.id, item7.codigo , item7.nombre, 6)} >add_circle</i></button>
                                                                                                            </div>
                                                                                                      }
                                                                                                </div>
                                                                                                  <hr></hr>
                                                                                                    {
                                                                                                      filtroAux(6, item7.codigo).map((item8, index8)=>{
                                                                                                        return(
                                                                                                          <div>
                                                                                                          {
                                                                                                            estado8 == item8.idSubCuentas ?

                                                                                                            <div class="SegundoComponente" style={{marginLeft:40}}>
                                                                                                              <div class="d-flex">
                                                                                                                {
                                                                                                                  estado9 == 0 ?
                                                                                                                  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado9(item8.codigo)}>
                                                                                                                      expand_more
                                                                                                                  </span>
                                                                                                                  :estado9 == item6.codigo ?
                                                                                                                  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado9(0)}>
                                                                                                                      expand_less
                                                                                                                  </span>
                                                                                                                  :
                                                                                                                  <span class="material-icons" style={{fontSize:'200%', marginTop:'-4px'}} onClick={()=>setEstado9(item8.codigo)}>
                                                                                                                      expand_more
                                                                                                                  </span>

                                                                                                                }

                                                                                                                  <p class="col-md-9">{item8.codigo} - {item8.nombre}</p>
                                                                                                                  <div style={{marginLeft:'-30px'}}>
                                                                                                                  <button onClick = {()=>dataDet(item8)} className="btn "><i class = "material-icons">visibility</i></button>
                                                                                                                  <button className="btn"><i className = "material-icons"  onClick={()=>MessageError("No puedes crear mas cuentas")} >add_circle</i></button>
                                                                                                                  </div>
                                                                                                              </div>
                                                                                                              <hr></hr>
                                                                                                            </div>

                                                                                                            :null
                                                                                                          }
                                                                                                          </div>
                                                                                                        )
                                                                                                      })
                                                                                                    }
                                                                                                </div>

                                                                                                :null
                                                                                              }
                                                                                              </div>
                                                                                            )
                                                                                          })
                                                                                        }
                                                                                    </div>

                                                                                    :null
                                                                                  }
                                                                                  </div>
                                                                                )
                                                                              })
                                                                            }
                                                                        </div>

                                                                        :null
                                                                      }
                                                                      </div>
                                                                    )
                                                                  })
                                                                }
                                                            </div>

                                                            :null
                                                          }
                                                          </div>
                                                        )
                                                      })
                                                    }
                                                </div>

                                                :null
                                              }
                                              </div>
                                            )
                                          })
                                        }


                                        </div>

                                        :null
                                      }
                                      </div>


                                    )
                                  })
                                }
                              </div>

                              :null
                            }

                          </div>
                        )
                      })

                    }
                  </div>
                </div>
              )
            })

          }
          <Modal show={show2} onHide={handleClose2} >
             <Modal.Header closeButton>
               <Modal.Title>Detalle Cuenta contable</Modal.Title>
             </Modal.Header>
             <Modal.Body>
               <div class="container-fluid">
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Codigo:</strong> {detalles.codigo}
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Nombre:</strong> {detalles.nombre}
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Naturaleza:</strong> {detalles.naturaleza == 1? "Débito" : "Crédito"}
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Tipo:</strong> {detalles.tipo == 1 ? "Activo" :detalles.tipo == 2? "Pasivo" :detalles.tipo == 3? "Patrimonio": detalles.tipo == 4? "Ingresos" :detalles.tipo ==5 ? "Costos" :detalles.tipo == 6? "Gastos" :null}
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Requiere NIT:</strong> {detalles.nit == 1 ? "Requiere nit" :"No requiere nit" }
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Requiere Mvot:</strong> {detalles.mvot == 1 ? "Requiere moviemiento" : "No requiere moviemiento" }
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Flujo efectivo:</strong> {detalles.efectivo == 1 ? "Flujo de efectivo activado" :"Flujo de efectivo Desactivado"}
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Cuentas por cobrar (cxc):</strong> {detalles.cxc == 1 ? "Requiere Cuenta por cobrar" :"No Requiere Cuenta por cobrar"}
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Proveedores:</strong> {detalles.proveedores == 1 ? "Requiere proveedores" :"No requiere proveedores"}
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Inventario:</strong> {detalles.invetario == 1 ? "Requiere invetario" :"No requiere invetario"}
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Activo fijo:</strong> {detalles.activos == 1 ? "Requiere activo fijo" :"No requiere activo fijo"}
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Interes:</strong> {detalles.interes == 1 ? "Requiere interes" :"No requiere interes"}
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Mora:</strong> {detalles.mora == 1 ? "Requiere mora" :"No requiere Mora"}
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-8">
                   <strong>Descuento(Dscto):</strong> {detalles.descuento == 1 ? "Requiere descuento" :"No requiere descuento"}
                   </div>
                 </div>
                 {
                   detalles.factura == 1?
                   <div>
                     <div class="row">
                       <div class="col-md-8">
                       <strong>Base:</strong> {detalles.base > 0? detalles.base : 0 }
                       </div>
                     </div>
                     <div class="row">
                       <div class="col-md-8">
                       <strong>Base:</strong> {detalles.porcentaje > 0? detalles.porcentaje : 0 }
                       </div>
                     </div>
                   </div>
                   :null
                 }



               </div>
             </Modal.Body>
         </Modal>




          <Modal show={show} onHide={handleClose} size="lg">
             <Modal.Header closeButton>
               <Modal.Title>Crear cuenta contable</Modal.Title>
             </Modal.Header>
             <Modal.Body>
               <NivelOne id={id} data = {data} nom={validar} idSub={idSub} codigo={codigo} nombreGrupo = {nombreGrupo} nivel={nivel} tipoCuent={props.tipoCuent} handleClose={()=>handleClose()}/>
             </Modal.Body>
         </Modal>

    </div>
  )

}

export default Lista;
if(document.getElementById('Lista')){
  ReactDOM.render(<Lista />, document.getElementById('Lista'))
}
