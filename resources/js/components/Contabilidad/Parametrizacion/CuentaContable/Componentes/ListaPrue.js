import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import obtener from './Cuenta_conex';
import {ip} from '../../../../ApiRest';
import '../../../../../../css/app.scss';
import NivelOne from '../Crear/NivelOne';
import desactivar from './Desactivar';

import { Button, Accordion, Card, Modal } from 'react-bootstrap';



function Lista(props){

  const [ grupo, setGrupo ] =  useState([])
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(false)

  //variables para envio al Modal
  const [ data, setData ] = useState({})
  const [ id, setId ] = useState(null)
  const [ idSub, setIdSub ] = useState(null)
  const [ nombreGrupo, setNombreGrupo ] = useState(null)
  const [ codigo, setCodigo ] = useState(null)
  const [ nivel, setNivel ] = useState(null)

  ///este resive el tipo cuent de cuenta contable (setTipoCuent)
  const [ level, setLevel ] = useState(null)




  const [ validar, setValidar ] = useState(false)

  const handleClose = () =>{
    setShow(false);
    setValidar(false)
    setNivel(null)
  }

  const handleShow = () => setShow(true);

  const modal = async () => {
  setShow(true);
  }

  const crear = (id, codig, nom, nivel) => {
    setShow(true)
    setIdSub(id)
    setCodigo(codig)
    setNombreGrupo(nom)
    setNivel(nivel)
  }


  const dataUpdate = async (data, valor, vali) => {
    setLoading(true)

    var nombre = data.nombre
    var codigo = data.codigo
    var descripcion = data.descripcion
    var tipo = data.tipo
    var naturaleza = data.naturaleza
    var nit = data.nit
    var mvot = data.mvot
    var factura = data.factura
    var cxc = data.cxc
    var proveedores = data.proveedores
    var inventario = data.inventario
    var activos = data.activos
    var saldo = data.saldo
    var interes = data.interes
    var mora = data.mora
    var descuento = data.descuento
    var retefuente = data.retefuente
    var base = data.base
    var porcentaje = data.porcentaje

    const dataForAll = {
      nombre,
      codigo,
      descripcion,
      tipo,
      naturaleza,
      nit,
      mvot,
      factura,
      cxc,
      proveedores,
      inventario,
      activos,
      saldo,
      interes,
      mora,
      descuento,
      retefuente,
      base,
      porcentaje
    }
    await setId(data.id)
    await setData(dataForAll)
    setShow(true)
    setValidar(vali)
    setNivel(valor)
    setData({})
    setLoading(false)
  }



  const filtrarNivel = (nivel, idNivel) => {
    const data = props.todosNiveles.filter((item)=> item.nivel== nivel && item.idSubCuentas==idNivel)
    console.log(data)
    return data;
  }


  return(
    <div>
          {
            props.filtro.map((item,index)=>{
            var numero =  parseInt(item.codigo);
              return (
                <Accordion defaultActiveKey={index} >
              <Accordion.Item eventKey="0">
                <Accordion.Header>
                <div className = "row" style = {{width:'inherit'}}>
                <div className = "col-md-6">
                {item.codigo + " - " + item.nombre}
                </div>
                {
                  item.codigo == 11 || item.codigo == 12 || item.codigo == 21 || item.codigo == 31 || item.codigo == 41 || item.codigo == 51 || item.codigo == 61 || item.codigo == 71 || item.codigo == 81 || item.codigo == 91 ?
                  <div></div>
                  :
                  <div className = "col-md-6 justify-content-end d-flex">
                  <button  className="btn "><i className = "material-icons"    onClick={()=>crear(item.id, item.codigo, item.nombre, 1)} >add_circle</i></button>
                  <button  className="btn "><i className = "material-icons"    onClick = {()=>dataUpdate1(item, 5)}>edit</i></button>
                  <button  className="btn" onClick={()=>desactivar.dato(item)}><i className = "material-icons" style={{color:'red'}}>cancel </i></button>
                  </div>
                }


                </div>
                </Accordion.Header>
                <Accordion.Body>
              {
                filtrarNivel(1,item.id).map((item1 ,index1)=>{
                  return (
                    <Accordion defaultActiveKey={index1} >
                  <Accordion.Item eventKey="0">
                    <Accordion.Header>
                      <div className = "row" style = {{width:'inherit'}}>
                      <div className = "col-md-6">
                      {item1.codigo + " - " + item1.nombre}
                      </div>

                        <div className = "col-md-6 justify-content-end d-flex">
                        <button  className="btn "><i className = "material-icons"  onClick={()=>crear(item1.id, item1.codigo , item1.nombre, 2)} >add_circle</i></button>
                        <button  className="btn "><i className = "material-icons"  onClick={()=>dataUpdate(item1 ,1, true )}>edit</i></button>
                        <button  className="btn" onClick={()=>desactivar.dato(item1, 1)} ><i className = "material-icons" style={{color:'red'}}>cancel</i></button>
                        </div>

                      </div>
                    </Accordion.Header>
                    <Accordion.Body>
                      {
                        filtrarNivel(2, item1.id).map((item2 ,index2)=>{
                          return (
                            <Accordion defaultActiveKey={index2} >
                          <Accordion.Item eventKey="0">
                            <Accordion.Header>
                              <div className = "row" style = {{width:'inherit'}}>
                              <div className = "col-md-6">
                              {item2.codigo + " - " + item2.nombre}
                              </div>
                              <div className = "col-md-6 justify-content-end d-flex">
                              <button  className="btn "><i className = "material-icons"  onClick={()=>crear(item2.id, item2.codigo , item2.nombre, 3)} >add_circle</i></button>
                              <button  className="btn "><i className = "material-icons"  onClick={()=>dataUpdate(item2,2, true)}>edit</i></button>
                              <button  className="btn" onClick={()=>desactivar.dato(item2, 2)} ><i className = "material-icons" style={{color:'red'}}>cancel</i></button>
                              </div>
                              </div>
                            </Accordion.Header>
                            <Accordion.Body>
                            </Accordion.Body>
                          </Accordion.Item>
                          </Accordion>
                          );
                        })
                      }
                    </Accordion.Body>
                  </Accordion.Item>
                  </Accordion>
                  );
                })
              }

                </Accordion.Body>
              </Accordion.Item>
              </Accordion>
              );
            })
          }
      <Modal show={show} onHide={handleClose} size="lg">
         <Modal.Header closeButton>
           <Modal.Title>Crear cuenta contable</Modal.Title>
         </Modal.Header>
         <Modal.Body>
           <NivelOne id={id} data = {data} nom={validar} idSub={idSub} codigo={codigo} nombreGrupo = {nombreGrupo} nivel={nivel} tipoCuent={props.tipoCuent} handleClose={()=>handleClose()}/>
         </Modal.Body>
     </Modal>

    </div>
  )

}

export default Lista;
if(document.getElementById('Lista')){
  ReactDOM.render(<Lista />, document.getElementById('Lista'))
}
