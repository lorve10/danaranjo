import React,{useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Swal from 'sweetalert2';
import Header from '../../../Ui/Header';
import { Button, Accordion, Card, Modal } from 'react-bootstrap';
import { Tooltip,  Grid  } from '@material-ui/core';
import obtener from '../CuentaContable/Componentes/Cuenta_conex.js'

function Concepto() {
  const [ id, setId ] = useState(null)
  const [ loading, setLoading] = useState(false)
  const [marginval, setMarginVal] = useState(false)
  const [show, setShow] = useState(false);
  const [ showForm, setShowForm ] = useState(1);
  const [ nombre, setNombre ] = useState(null)
  const [tipo2, setTipo2 ] = useState(null)
  const [tipoSelect2, setTipoSelect2] = useState([
    {
    value:'1',
    label:'Activo',
    },
    {
    value:'2',
    label:'Pasivo',
    },
    {
    value:'3',
    label:'Patrimonio',
    },
    {
    value:'4',
    label:'Ingresos',
    },
    {
    value:'5',
    label:'Costos',
     },
    {
    value:'6',
    label:'Gastos',
    }
  ]);
  const [ grupo, setGrupo ] = useState(null)
  const [ grupoSelect, setGrupoSelect ] = useState([])
  const [ todosNiveles, setTodosNiveles ] = useState([])
  ///constante que recoje el nivel
  const [ nivel, setNivel ] = useState(null)
  const [ grupo1, setGrupo1 ] = useState(null)
  const [ grupo2, setGrupo2 ] = useState(null)
  const [ grupo3, setGrupo3 ] = useState(null)
  const [ grupo4, setGrupo4 ] = useState(null)
  const [ grupo5, setGrupo5 ] = useState(null)
  ////list conceptos
  const [ listCon, setListCon ] = useState([])



  useEffect(()=>{
    obj();
    obtainInfo();
  },[])

  const obtainInfo = async () => {
    axios.get(ip+'admon/parametrizacion/getConcep').then(response=>{
      var res = response.data
      setListCon(res)

      console.log(response.data);
  })
}

  const handleClose = () =>{
    setShow(false);
  }
  const handleShow = () => setShow(true);
  const modal = async () => {
  setShow(true);
  }
  const obj = async () => {
    var data = await obtener.obtenerAuxiliar();
    console.log("aqui niveles");
    console.log(data);
    var filtro = data.filter(e=>e.mvot == 1);
    setGrupoSelect(filtro)

  }


  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }

  const filtrarNivel = (nivel,idNivel) => {
    if(todosNiveles){
      const data = todosNiveles.filter((item)=> item.nivel==nivel && item.idSubCuentas==idNivel )
      return data;
    }
  }



  const save_data = async () => {
    console.log(grupo);
    if (nombre == null) {
      MessageError("El campo nombre no puede esta vacio")
    }

    else{
      const data = new FormData()
      data.append('id', id)
      data.append('nombre', nombre)
      data.append('pertenNivel', grupo)
      axios.post(ip+'admon/parametrizacion/guardConcepto',data).then(response=>{
        if (id == null) {
          MessageSuccess("Datos creados correctamente")
          setNombre("")
          obtainInfo()
          setShow(false);

        }
        else{
          MessageSuccess("Editado correctamente")
          obtainInfo()
          setShow(false);

        }
        })
      }
    }





  const deshabilitar = (value) =>{
    var message = '¿Quieres eliminar este concepto?'

    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,

    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value.id)
        axios.post(ip+'admon/parametrizacion/deleted',data).then(response=>{

          Swal.fire('Eliminado correctamente!', '', 'success')
          obtainInfo();
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }


  const goback = async () => {
    await setEstado(1)
    setId(null)
  }

  const gobackSave = () => {
    setEstado(1)
    setId(null)
    setData({})
    mostrarCuotas();
  }

    const propHeader = (value) => {
      console.log("entro",value);
      setMarginVal(value)
    }





  return(
    <div className="content-wrapper"  style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:74}} >
    <Header section={2}  marginval = {(value)=>propHeader(value)}/>
      <script src="" charset="utf-8"></script>
    <div className="verticar-footer-2">
                {
                    showForm == 1?
                    <Tooltip title="Agregar concepto" placement="left">
                    <button className="btn" >
                        <h5 onClick = {()=>setShow(true)}><img src = {ip+'images/AgregarPeriodoContable.png'} style= {{width:'56px', marginLeft: '0px', marginTop: '35px'}} /> </h5>
                    </button>
                    </Tooltip>

                    :null
                }

                    <img className="imagen" src={ip+'images/footer.png'}/>
    </div>


      <div>
        <div style={{marginLeft:-31}} className="form-section col-md-8 mt-3"><strong><h6 className="mt-2 ruta" style={{marginLeft:'103px'}}  >/ <a href = {ip+'admon/Financiera'}>Módulo financiero </a>/ <a href = {ip+'admon/contabilidad'}>Contabilidad </a> / <a  href = {ip+'admon/parametrizacion'} > Parametrización</a> /Conceptos contables </h6></strong>
        </div>
        <div className="container">
          <div className="row mt-3 d-flex">
            <h4>Conceptos contables</h4>


              <div className="row" style={{marginLeft:-22}}>
                <div class ="col-md-6">
                <hr style={{marginLeft:'9px'}}></hr>
                <table className="table table-striped" style={{marginLeft: '9px'}}>
                    <thead style={{backgroundColor: '#FFDE59' }}>
                      <tr>
                          <th>Concepto</th>
                          <th>Pertenece a:</th>
                          <th></th>

                      </tr>
                    </thead>
                    <tbody>
                      {
                        listCon.map(item=>{
                          return(
                            <tr>
                              <td>{item.nombre}</td>
                              {
                                item.pertenNivel == 0?
                                <td>{item.grupo.codigo + " - "+ item.grupo.nombre}</td>
                                  :
                                <td>{item.pertenNivel == null ?""+item.grupo.codigo + " - " + item.grupo.nombr + "" :""+ item.niveles.codigo + " - "+ item.niveles.nombre + ""}</td>
                              }
                              <td>
                                <span class="material-icons align-items-center" onClick={()=>deshabilitar(item)} style={{cursor:'pointer'}} >
                                cancel
                                </span>

                              </td>

                            </tr>
                          )

                        })
                        }
                    </tbody>
                  </table>
                </div>
              </div>



          </div>
        </div>
      </div>


      <Modal show={show} onHide={handleClose}>
         <Modal.Header closeButton>
           <Modal.Title>Crear concepto</Modal.Title>
         </Modal.Header>
         <Modal.Body>
              <div className="container">
                  <div className="row">
                  <div class="col-md-12 d-flex">
                      <label class="col-md-5">Nombre concepto</label>
                      <input className="form-control col-md-7" placeholder="Ingrese nombre del concepto" type="text"  value={nombre}  onChange = {(e)=>setNombre(e.target.value)} />
                  </div>
                  <div class="col-md-12 d-flex mt-2" >
                    <label  class="col-md-5">Tipo cuenta contable </label>
                    <select name="select" id="select" class="form-select col-md-7"   value={grupo}
                    onChange={(event)=>setGrupo(event.target.value)}>
                        <option value="seleccionar">Seleccionar...</option>
                        {
                          grupoSelect.map((item, i)=>{
                          return(
                              <option value={item.id} key={i}>{item.codigo} - {item.nombre}</option>
                          )
                          })
                        }
                    </select>
                  </div>





                  </div>
              </div>
         </Modal.Body>
         <Modal.Footer>
         <Button style={{backgroundColor: '#FFDE59', border:'solid 0px', borderColor: '#FFDE59', color: 'black'}} onClick = {()=>save_data()}>Guardar</Button>
         </Modal.Footer>
     </Modal>




  </div>



);

}
export default Concepto;
if (document.getElementById('Concepto')) {
  ReactDOM.render(<Concepto />, document.getElementById('Concepto'));
}
