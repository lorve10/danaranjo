import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Header from '../../Ui/Header';
import '../../../../css/app.scss';
import Select from 'react-select';
import makeAnimated from 'react-select/animated'
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { Button, Modal } from 'react-bootstrap';
import { Tooltip,  Grid  } from '@material-ui/core';
import { Divider } from '@material-ui/core';
import PrintDocumentos from './PrintDocumentos';

import util from '../componentes/util';
import Pagination from '../componentes/paginate';

function CrearDocumentos (){

    const [ loading, setLoading ] = useState(false);
    const [ animatedComponents, setAnimatedComponents ] = useState(makeAnimated)
    const [ showForm, setShowForm ] = useState(1)
    const [ currentPage, setCurrentPage ] = useState(1)
    const [ perPage, setPerPage ] = useState(9)
    const [ periodoCon, setPeriodoCon ] = useState([])
    const [ periodoCon2, setPeriodoCon2 ] = useState([])
    const [ text, setText ] = useState('')
    const [ marginval, setMarginVal ] = useState(false)
    const [ filtro, setFiltro ] = useState([])
    const [ show, setShow ] = useState(false);
    const handleShow = () => setShow(true);
    const [ id, setId ] = useState(null)
    const [ fecha, setFecha ] = useState(null);
    const [ terceroSelect, setTerceroSelect ] = useState([]);
    const [ detalle, setDetalle ] = useState('');
    const [ tercero, setTercero ] = useState(null);
    const [ cuentaSelect, setCuentaSelect ] = useState([])
    const [ tipoDoc, setTipoDoc ] = useState(null);
    const [ tipoDocList, setTipoDocList ] = useState([]);

    const [ valor, setValor ] = useState({
      cuenta: '',
      tercero:'',
      detalle:'',
      base:0,
      debito: 0,
      credito: 0
    });
    /// este es el contador
    const [count, setCount] = useState(null)
    const [countCre, setCountCre] = useState(null)
    const [ list, setList ] = useState([]);
    const [ base2, setBase2 ] = useState(null)
    const [ factura, setFactura ] = useState(false)

    //
    const [ listCuenta, setListCuenta ] = useState([])

    useEffect(()=>{
        obtain_terce();
        obtainInfo();
        obtaNivel();

    },[]);

    const CrearDocumento = async () => {
        setShow(true);
    }

    const handleClose = () => {
        setShow(false);
    }

    const descPlantilla = async ()=>{
        $("#site").on('click', function(){
          window.open(ip+"plantilla/Plantilla.xlsx","_blank");
        });
    }
    const goback = async () => {
      await setShowForm(1)
    }

    const obtaNivel = async () => {
      axios.get(ip+'admon/obtain_nivel').then(response=>{
        var res = response.data
        var filtro = res.filter(e=>e.mvot == 1)
        setListCuenta(filtro)
        var tes = []
        filtro.map(item=>{
          const data = {
              value: item.id,
              label: item.codigo + " - " + item.nombre
          }
          tes.push(data)
          setCuentaSelect(tes)

        })
        console.log("Aqui filtro")
        console.log(filtro);
    })
  }

    const obtainInfo = async () => {
      axios.get(ip+'admon/parametrizacion/obtain_documento').then(response=>{
        var res = response.data
        var tes = []
          res.map(item=>{
            const data = {
                value: item.id,
                label: item.nombre + " - " + item.prefijo
            }
            tes.push(data)
          })
          setTipoDocList(tes)
        console.log(response.data);
    })
  }
    const obtain_terce = async () => {
        axios.get(ip+'admon/obtain_terce').then(response=>{
            var res = response.data;
            var terceros = []
            var filtrado = res.filter(e=>e.deleted == 0);
            filtrado.map(item=>{
                const data = {
                    value:item.id,
                    label: item.tip_per == 2 ? item.N_documet  + " - " + item.name + " " + (item.secname == null ? '':item.secname)+ " " + (item.lastname == null ? '':item.lastname) + " " + (item.seclastname == null ? '':item.seclastname) : item.N_documet + "-" + item.nit + " - " + item.name
                }
                terceros.push(data)
            })
            setTerceroSelect(terceros)
        })
    }


    const MessageError = async (data) => {
        Swal.fire({
          title: 'Error',
          text: data,
          icon: 'warning',
        })
      }
      const MessageSuccess = async (data) => {
        Swal.fire({
          text: data,
          icon: 'success',
        })
      }

    const save_data = async () => {
      var message = ''
      var error = false
      var res = count-countCre;
      console.log("aqui la res");
      console.log(res);
      if (tipoDoc == null || tipoDoc == "" || tipoDoc.value == null) {
        error = true
        message = "Debe agregar el tipo de documento"
      }
      else if (fecha == null) {
        error = true
        message = "Debe agregar la fecha"
      }
      else if (list.length < 0  || list == null || list == ''){
        error = true
        message = "Verifique si tiene algún campo vacío"
      }
      else if(res < 0 || res > 0){
        error = true
        message = "Verifique que la diferencia sea igual a 0 para poder guardar"
      }
      if (error) {
        MessageError(message)
      }
      else{
        const data = new FormData()
        data.append('id', id)
        data.append('tipo', tipoDoc.value)
        data.append('fecha', fecha)
        data.append('lista', JSON.stringify(list))
        axios.post(ip+'admon/crear',data).then(response=>{
            if(response.data == 2){
            MessageError("No puede crear documentos, este período contable ya se encuentra cerrado.");
            }else if (id == null) {
            MessageSuccess("Documento creado correctamente")
              setList([])
              setTipoDoc("")
              setFecha("")
              setBase2(0)
              setCount(0)
              setCountCre(0)
              setValor({
                cuenta: '',
                tercero:'',
                detalle:'',
                base:0,
                debito: 0,
                credito: 0
              })
            }

          })
        }
      }

    const onClickAddContable = async () =>{
      var message = ''
      var error = false

      if(factura == true){
        if(valor.cuenta == ""){
          error = true
          message = "Debe llenar el campo cuenta"
        }
        else if (valor.tercero == "") {
          error = true
          message = "Debe llenar el campo tercero"
        }
        else if (valor.base == "") {
          error = true
          message = "Debe llenar el campo base"
        }
      }else if(factura == false){
        if(valor.cuenta == ""){
          error = true
          message = "Debe llenar el campo cuenta"
        }
        else if (valor.tercero == "") {
          error = true
          message = "Debe llenar el campo tercero"
        }
      }

      if (error) {
        MessageError(message)
      }
      else{
        const lista = list
        lista.push(valor)
        setValor({
            cuenta: '',
            tercero:'',
            detalle:'',
            base:0,
            debito: 0,
            credito: 0
        })
        setBase2(0)
        setList(lista)
        setCount(count + parseInt(valor.debito))
        setCountCre(countCre + parseInt(valor.credito))
      }

    }

    const numberFormat = (amount) => {
      if(amount>0){
        amount = amount.toString()
        const res = amount.replace(/./g, function(c, i, a) {
          return i && c !== "." && ((a.length - i) % 3 === 0) ? '.' + c : c;
        });
        return res;
      }
      return amount

    }

    const inputNumber = (e) => {
      var value = e.target.value
      value = value.replace(".","")
      value = value.replace(".","")
      value = value.replace(".","")
      value = value.replace(".","")
      console.log(value);

      setValor({ ...valor, [e.target.name]: value })
    }
    
    const inputBase = (e) => {

      var data = e.target.value
      data = data.replace(".","")
      data = data.replace(".","")
      data = data.replace(".","")
      data = data.replace(".","")

      var cuent = valor.cuenta
      console.log("aqui input base");
      console.log(data);
      var final = parseInt(data)
      setBase2(final)
      var res = listCuenta.filter((item)=>item.id == cuent.value)

    var data2 = res.map(item=>{
      var result;
       if(item.factura == 1){
          var multi = ((final*item.porcentaje)/100);
          result = multi + parseInt(final);
          }else {
          result = data
           }
           console.log(result);
          return result

      })

      console.log(data2);
      setValor({ ...valor, 'base': data2[0] })

    }

    const filterBase = (e) => {
      setValor({...valor,'cuenta': e})
      var event = e
      var res = listCuenta.filter((e)=>e.id == event.value)

      var data = res.map(item=>{
        if(item.factura == 1){
          setFactura(true)
        }else{
          setFactura(false)
        }
      })

      console.log("Aqui data filter base")
      console.log(data)
    }

    return(
        <div className="content-wrapper " style={marginval ? {marginLeft:210,backgroundColor:'#ffffff'}:{backgroundColor:'#ffffff', marginLeft:100}}>


            <Header section={4}  marginval = {(value)=>propHeader(value)}/>

            <div className="verticar-footer-2">
            {
                showForm == 1?
                <div>
                  <Tooltip title="Imprimir" placement="left">
                  <button className="btn" >
                      <h5 onClick = {()=>setShowForm(2)}><img src = {ip+'images/AgregarPeriodoContable.png'} style= {{width:'56px', marginLeft: '0px', marginTop: '35px'}} /> </h5>
                  </button>
                  </Tooltip>

                </div>


                :null
            }

                <img className="imagen" src={ip+'images/footer.png'}/>
            </div>
            {
            showForm == 1 ?
            <div>
            <div className="row">
                <div className="col-md-9 " style={{marginLeft:47}}>
                <div className="btn container mt-3 d-flex" style={{marginLeft:'-155px'}}>
                <div className="form-section col-md-8 "><strong><h6 className="mt-2 ruta" style={{marginLeft:'-1px'}}>/ <a href = {ip+'admon/contabilidad'}>Módulo contabilidad </a>/ Crear documentos</h6></strong></div>
                </div>
                <div className="mt-3">
                <h4 className="text-center" style={{marginLeft:"-78%"}}>Crear documentos</h4>
                </div>
                </div>
            </div>
            </div>
            :null

        }
        {
            showForm == 1?


            <div className="col-md-10" style={{marginLeft:35}}>

            <Divider className="col-md-12 my-2" style={{backgroundColor:'#FFDE59', marginLeft: '8px', width: '965px', border:'1px solid', borderColor:'#FFDE59'}} />

            <div className="col-md-10 d-flex" style={{marginTop: '20px'}}>
              <label className="d-flex col-6 align-items-center color" style = {{width:'180px'}}>Tipo de documento: </label>
              <Select
              className="col-5"
              value={tipoDoc}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={tipoDocList}
              isClearable={true}
              onChange={(e)=>setTipoDoc(e)}
              placeholder = "Seleccionar tipo de documento"
              name="colors"
              />
            </div>

            <div class="col-md-10 d-flex">
                <label className="d-flex col-6 align-items-center mr-2 color" style={{width: '180px'}}>Fecha de elaboración: </label>
                <input className="form-control col-5" style={{width:'339px'}} type="date" value = {fecha} placeholder="Ingrese fecha" onChange = {(e)=>setFecha(e.target.value)}/>
            </div>

            <table className="table table-striped" style={{marginTop: '20px'}} >
                <thead style={{backgroundColor: '#FFDE59' }}>
                <tr>
                    <th>Cuenta contable</th>
                    <th>Tercero</th>
                    <th>Detalle</th>
                    <th>Base</th>
                    <th>Débito</th>
                    <th>Crédito</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                    <td>
                        {/*<select name="cuenta" id="cuenta" class="form-control claseCrea"  value={valor.cuenta}
                          onChange={(e)=>setValor({ ...valor, [e.target.name]: e.target.value })}>
                          <option selected>Seleccionar...</option>
                            {
                              cuentaSelect.map((item, i)=>{
                                return(
                                  <option value={item.value} key={i}>{item.label}</option>
                              )
                              })
                            }
                          </select>*/}

                        <Select
                          className="select-docu"
                          value={valor.cuenta}
                          components={animatedComponents}
                          options={cuentaSelect}
                          onChange={(e)=>filterBase(e)}
                          placeholder = "Seleccionar..."
                          name="cuenta"
                          id="cuenta"
                          />
                    </td>
                    <td >
                      {/*<select name="tercero" id="tercero" class="form-select claseCrea" data-live-search="true" value={valor.tercero}
                        onChange={(e)=>setValor({...valor, [e.target.name]: e.target.value })}>
                        <option selected>Seleccionar...</option>
                          {
                            terceroSelect.map((item, i)=>{
                              return(
                                <option data-tokens={item.label} value={item.value} key={i}>{item.label}</option>
                            )
                            })
                          }
                      </select>*/}

                      <Select
                        value={valor.tercero}
                        className="select-docu"
                        components={animatedComponents}
                        options={terceroSelect}
                        onChange={(e)=>setValor({...valor,'tercero': e})}
                        placeholder = "Seleccionar..."
                        name="tercero"
                        id="tercero"
                        />
                    </td>
                    <td>
                        <textarea className="form-control col-11" style={{width: '164px'}} value={valor.detalle}  name = "detalle"  placeholder="Detalle" onChange={(e)=>setValor({ ...valor, [e.target.name]: e.target.value })}></textarea>

                    </td>
                    {
                      factura == false ?
                      <td>
                        <input className="form-control col-9" type="text" style={{width: '164px'}} value={util.convertMoney(base2)} disabled name = "base" id = "base" placeholder="Base" onChange={(e)=>inputBase(e)} />
                      </td>
                      :
                      <td>
                        <input className="form-control col-9" type="text" style={{width: '164px'}} value={util.convertMoney(base2)}  name = "base" id = "base" placeholder="Base" onChange={(e)=>inputBase(e)} />
                      </td>
                    }
                    <td>
                        <input className="form-control col-9" type="text" min={0} style={{width: '164px'}} value={numberFormat(valor.debito)}  name = "debito"  placeholder="Débito" onChange={(e)=>inputNumber(e)}/>
                    </td>
                    <td>
                        <input className="form-control col-9" type="text" min={0} style={{width: '164px'}} value={numberFormat(valor.credito)}  name = "credito"  placeholder="Crédito" onChange={(e)=>inputNumber(e)} />
                    </td>
                    <td>
                        <img className="cursor-pointer" onClick={()=>onClickAddContable()} src = {ip+'images/maxx.png'} style={{width:'26px'}} />
                    </td>
                    </tr>
                    {
                      list.map(item=>{
                        return(
                          <tr>
                            <td>
                            <select name="cuenta" id="cuenta" class="form-control claseCrea" disabled value={item.cuenta.value} style={{width:'150px'}}
                                onChange={(e)=>setValor({ ...valor, [e.target.name]: e.target.value })}>
                                <option selected>Seleccionar</option>
                                  {
                                    cuentaSelect.map((item, i)=>{
                                      return(
                                        <option value={item.value} key={i}>{item.label}</option>
                                    )
                                    })
                                  }
                                </select>


                            </td>
                            <td >
                              <select name="tercero" id="tercero" class="form-control claseCrea"  disabled  value={item.tercero.value} style={{width:'150px'}}
                                onChange={(e)=>setValor({ ...valor, [e.target.name]: e.target.value })}>
                                <option selected>Seleccionar</option>
                                  {
                                    terceroSelect.map((item, i)=>{
                                      return(
                                        <option value={item.value} key={i}>{item.label}</option>
                                    )
                                    })
                                  }
                              </select>
                            </td>
                            <td>
                                <textarea className="form-control col-11" disabled style={{width: '164px'}} value={item.detalle}  name = "Detalle"  ></textarea>

                            </td>
                            <td>
                                <input className="form-control col-11" disabled type="number" style={{width: '125px'}} value={numberFormat(item.base)}  name = "base"  />
                            </td>
                            <td>
                                <input className="form-control col-11" disabled type="number" style={{width: '125px'}} value={numberFormat(item.debito)}  name = "debito"/>
                            </td>
                            <td>
                                <input className="form-control col-11" disabled type="number" style={{width: '125px'}} value={numberFormat(item.credito)}  name = "credito" />
                            </td>
                          </tr>
                        )
                      })
                    }
                </tbody>
                <tfoot>
                    <tr >
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Total:</td>
                        <td>{numberFormat(count)}</td>
                        <td>{numberFormat(countCre)}</td>

                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td style={count-countCre == 0 ? {color: "green"} : {color: "red"}}>Diferencia: </td>
                      <td style={count-countCre == 0 ? {color: "green"} : {color: "red"}}>
                        {
                          count > countCre ?
                          <p></p>
                          :
                          <p style={{color: "red"}}>{count-countCre == 0 ? "" : numberFormat(count-countCre)}</p>
                        }

                      </td>
                      <td>
                        {
                          countCre > count ?
                          <p></p>
                          :
                          <p style={{color: "red"}}>{ count-countCre == 0 ? "" :numberFormat(countCre-count)}</p>
                        }
                      </td>

                      <td></td>
                    </tr>
                </tfoot>
                </table>
                <div className="my-2 d-flex col-md-9 justify-content-end px-0 mt-3" style = {{left:'32%'}}>
                  <button  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}} onClick = {()=>save_data()}>Guardar</button>
                </div>

                <div>
                    <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Importar documentos adicionales</Modal.Title>
                    </Modal.Header>
                        <Modal.Body>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-10 d-flex">
                                    <button className="btn btn-success" id="site" style={{border:'1px solid #FFDE59', backgroundColor:'#FFDE59', color:'black', fontWeight: "bold"}} onClick = {()=>descPlantilla()}> <img className="icon-btn" src = {ip+'images/descargarPlantilla.png'}></img>Descargar plantilla</button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-10 d-flex">
                                <label className="mr-3 d-flex align-items-center justify-content-center caja-subir-archivo cursor-pointer" style = {{height: '110px', width: '350px', cursor: 'pointer', marginTop:20}}>
                                <div className="text-center">
                                    <i className="material-icons color-degradado-danaranjo" style={{fontSize: '60px'}}>cloud_upload</i>
                                    <p>Seleccionar documento</p>
                                </div>
                                <input className="ocultar" type = "file" /*onChange = {(e)=>validateDocument(e)}*/ id = "ChargeMasive"/>
                                </label>
                                </div>
                            </div>
                        </div>
                        </Modal.Body>
                        <Modal.Footer>
                        <Button style={{backgroundColor: '#FFDE59', border:'solid 0px', borderColor: '#FFDE59', color: 'black'}} /*onClick={()=>GuardPeriodoContable()}*/>Guardar</Button>
                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
            :showForm == 2?
            <PrintDocumentos goback={()=>goback()}/>
            :null
            }
        </div>
    );
}

export default CrearDocumentos;
if(document.getElementById('CrearDocumentos')) {
    ReactDOM.render(<CrearDocumentos />, document.getElementById('CrearDocumentos'));
}
