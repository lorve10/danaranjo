import React, {useState, useEffect} from 'react';
import {ip} from '../../ApiRest';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Swal from 'sweetalert2'
import '../../../../css/app.scss';
import makeAnimated from 'react-select/animated';
import * as moment from 'moment';
import { data } from 'jquery';
import downloadjs from 'downloadjs';

import util from '../../Admon/util';
import Pagination from '../componentes/paginate';

function PrintDocumentos (props) {
    const [ loading, setLoading ] = useState(false);
    const [ listDoc, setListDoc ] = useState([])
    const [ token, setToken ] = useState(null)
    const [ token2, setToken2 ] = useState(null)
    const [ data, setData ] = useState([])
    const [ currentPage, setCurrentPage ] = useState(1)
    const [ perPage, setPerPage ] = useState(6)
    const [ filtro, setFiltro ] = useState([])
    const [ text, setText ] = useState('')
    const [ documento, setDocumento ] = useState([])
    const [ listDoc2, setListDoc2 ] = useState([])


useEffect(()=>{
  mostrarDoc()
  var csrf = document.querySelector('meta[name="csrf-token"]').content;
  var csrf2 = document.querySelector('meta[name="csrf-token"]').content;

  console.log("algoooo");
  console.log(csrf);
  setToken(csrf);
  setToken2(csrf2)
},[])

const goback = async () => {
    await setEstado(1)
    setId(null)
}


    const mostrarDoc = () =>{
      axios.get(ip+'admon/obtain_doc').then(response=>{
          var res = response.data
          setListDoc(res)
          setListDoc2(res)
          const dataNew = util.paginate(response.data,currentPage,perPage)
          setFiltro(dataNew)
      })
    }

    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }

    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

    const updateCurrentPage = async (number) => {
      await setFiltro([])
      await setCurrentPage(number)
      const dataNew = util.paginate(listDoc2,number,perPage)
      await setFiltro(dataNew)
    }

    const searchInput = async(value, otro) => {
      await setText(value)
      await setLoading(true)
      const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var newData = listDoc2.filter(function(item2){
        var Tipodoc  = ((item2.tipo.nombre).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
        var Prefijo = ((item2.tipo.prefijo).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")

        var resTipodoc = Tipodoc.indexOf(inputSearch) > -1
        var resTipodocLasT = Tipodoc.indexOf(inputSearch) > -1
        var resPrefijo = Prefijo.indexOf(inputSearch) > -1
        var resPrefijoLasT = Prefijo.indexOf(inputSearch) > -1
        var res = false
        if(resTipodoc||resTipodocLasT){
          res = true
        }
        if(resPrefijo||resPrefijoLasT){
          res = true
        }
  
        return res;
      })
  
      if(otro){
        console.log("entrooo aquiii");
        console.log(otro);
        await setCurrentPage(1)
        await setListDoc(otro)
        const dataNew = util.paginate(otro,1,perPage)
        await setFiltro(otro)
        await setLoading(false)
      }
      else {
        await setCurrentPage(1)
        await setListDoc(newData)
        const dataNew = util.paginate(newData,1,perPage)
        await setFiltro(dataNew)
        await setLoading(false)
      }
  
  
    }



    return(
      <div className="mt-5 col-10" style={{marginLeft: '45px'}}>
      <div className="btn container col-1" onClick = {()=>props.goback()}>
      <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4>
      </div>
      <div  className="container col-10 mt-4" style={{marginLeft:'-11px'}}>
      <h5 className="col-4"><strong>Imprimir:</strong> </h5>
      </div>
      <div className="d-flex mt-2" style={{marginTop: 9}}>
          <div className="col-7  mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #D9D9D9 ', backgroundColor:'white',borderRadius:12}}>
          <input  onChange = {(e)=>searchInput(e.target.value)} style = {{width:'490px',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Consultar tipos de documentos "/>
          {
              text.length == 0 ?
              <span style = {{color:'black', cursor:'pointer'}}  className= "material-icons">search</span>
              :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
          }
          </div>
      </div>
          <div className="col-md-12 mt-3">
          <table className="table table-hover" style={{backgroundColor: '#FFDE59'}}>
            <thead>
              <tr>
                <th scope="col">Tipo Docuemto</th>
                <th scope="col">Fecha</th>
                <th scope="col">Cuenta</th>
                <th scope="col">Tercero</th>
                <th scope="col">Imprimir</th>
              </tr>
            </thead>
            <tbody style={{backgroundColor: 'white'}}>
              {
                filtro.map((item,index)=>{
                  var data = JSON.parse(item.datos)
                  console.log(data);
                  return(
                    <tr>
                      <td>{item.tipo.nombre} - {item.tipo.prefijo}</td>
                      <td>{item.fecha}</td>
                      <td>
                        {
                          data.map(item2=>{
                            return(
                              <tr>
                                <td>{item2.cuenta.label}</td>
                              </tr>
                            )
                          })
                        }
                      </td>
                      <td>
                        {
                          data.map(item3=>{
                            return(
                              <tr>
                                <td>{item3.tercero.label}</td>
                              </tr>
                            )
                          })
                        }
                      </td>
                      <td>   <form className="d-flex" action={ip+"admon/imprimir"} method="POST" target="_blank">
                                <input type= "hidden"  name= "data"  value={JSON.stringify(item)} />
                                <input type="hidden" name="_token" value={token}/>
                                <button type="submit"  className="btn " style={{backgroundColor:'#FFDE59', borderRadius:10, color:'black'}}>
                                  <span class="material-icons">
                                    print
                                  </span>
                                </button>
                            </form>
                        </td>
                    </tr>
                  )
                })
              }


            </tbody>
          </table>
          <div className="d-flex col-md-12 col-12 justify-content-end">
            <Pagination currentPage={currentPage} perPage={perPage} countdata={listDoc.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
          </div>
        </div>
    </div>

    );
}

export default PrintDocumentos;
if(document.getElementById('PrintDocumentos')) {
    ReactDOM.render(<PrintDocumentos />, document.getElementById('PrintDocumentos'));
}
