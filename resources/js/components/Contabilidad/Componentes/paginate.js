import React, { Component } from 'react';


function paginate (props){

  const next = () => {
    const page = props.currentPage
    var nPage = page + 1
    props.updateCurrentPage(nPage)
  }
  const prev = () =>{
    const page = props.currentPage
    var nPage = page - 1
    props.updateCurrentPage(nPage)
    // console.log(nPage)
  }

  const paginationButton = () => {
    const { perPage, countdata } = props
    // console.log("Total: "+totalData)
    console.log("Limite es "+perPage)
    const contador = [];
    for (var i = 1; i <= Math.ceil(countdata/perPage); i++) {
      console.log("Pagina "+i);
      contador.push(i)
    }

    return contador;
  }

    const pagina = props.currentPage
    const limitpage = Math.ceil(props.countdata/props.perPage)

    return(
      <div>

        <nav aria-label="Page navigation example">
          <ul className="pagination">
            <li className={props.currentPage<=1 ? 'page-item disabled':'page-item cursor-pointer'}>
              <a className="page-link button-pagination" aria-label="Previous"
              onClick={()=>props.currentPage==1?null:prev()} >
                <span aria-hidden="true">&laquo;</span>
              </a>
            </li>
            {paginationButton().map((data,index)=>{
              if((props.currentPage+5)>data && (props.currentPage-5)<data){
                return(
                  <li onClick={()=>props.updateCurrentPage(data)} className="page-item cursor-pointer" key={index}>
                    <a className={props.currentPage==data ? 'page-link button-pagination button-pagination-select':'page-link button-pagination'}>
                      {data}
                    </a>
                  </li>
                )
              }
            })
            }
            <li className={props.currentPage< limitpage ? 'page-item cursor-pointer':'page-item disabled'}>
              <a className="page-link button-pagination" aria-label="Next"
              onClick={()=>props.currentPage< limitpage?next():null} >
                <span aria-hidden="true">&raquo;</span>
              </a>
            </li>
          </ul>
        </nav>

      </div>
    )
}

export default paginate;
if (document.getElementById('paginate')) {
  ReactDOM.render(<paginate />, document.getElementById('paginate'))
}
