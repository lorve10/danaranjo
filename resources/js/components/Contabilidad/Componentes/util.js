const util = {}

// paginacion
util.paginate = (data,currentPage,postsPerPage) => {
  const indexLastPost = currentPage * postsPerPage
  const indexFirstPost = indexLastPost - postsPerPage
  const objeto = data.slice(indexFirstPost, indexLastPost)
  return objeto
}


util.convertMoney = (amount) =>{

    try {
        var thousands = "."
        var decimalCount = 0
        var decimal = "."
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
      } catch (e) {
        console.log("Error de moneda "+e)
        return null
      }

}

export default util;
