import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import Header from './Ui/Header';
import Footer from './Ui/Footer';
import '../../css/app.scss'
import {ip} from './ApiRest';
import Swal from 'sweetalert2'
function HomePage() {

  const [loading, setLoading] = useState(false)


  return (
  <section className = "container-app">
    <Header section = {1}/>
    <Footer/>
  </section>
    );
  }
  export default HomePage;

  if (document.getElementById('HomePage')) {
    ReactDOM.render(<HomePage />, document.getElementById('HomePage'));
  }
