import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../ApiRest';
import '../../../css/app.scss';
import Swal from 'sweetalert2';
import RecoveyPassword from './RecoveyPassword';
import { Button, Modal } from 'react-bootstrap';

function LoginAdmon() {
  const [user, setUser] = useState('');
  const [pass, setPass] = useState('');
  const [loading, setLoading] = useState(false);
  const [input, setInput] = useState(false);
  const [count, setCount] = useState(0);

  /// recuperar pass
  const [ log, setLog ] = useState(1)
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const Login = async () => {
    if (user != '' && pass != '') {
      setLoading(true)
      const data = new FormData()
      data.append('email', user)
      data.append('password', pass)
      axios.post(ip+'admon/login_administrador',data).then(response=>{
        window.location.href = ip+"admon/inicio"
      })
      .catch(error=>{
        setLoading(false)
        setCount(count + 1);
        MessageError("El usuario o la contraseña son incorrectos")

        if(count == 3){
          MessageConfirmation("¿Deseas cambiar contraseña?");
          setCount(0);
        }

      })

    }
    else{
      MessageError("El usuario y la contraseña no pueden estar vacios")


    }
  }


  const MessageConfirmation = async (data) =>{
    Swal.fire({
    title: 'Has excedido el limite de intentos',
    text: data,
    icon: 'warning',
    showDenyButton: true,
    confirmButtonText: `Si`,
    denyButtonText: `No`,
  }).then((result) => {
    /* Read more about isConfirmed, isDenied below */
    if (result.isConfirmed) {
        setShow(true);
    } else if (result.isDenied) {

    }
  })
  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }

  const clickLog = (value) =>{
    setLog(value)
    $('#Login').modal('show')
  }

  const showPwd = async() => {
    setInput(!input)

    if(input){
      type = "password"
    }else{
      type = "text";

    }
  }

  return (
    <div className="container" style={{marginRight:0}}>
        <div className="d-flex mt-3">
            <div className=" mb-3 col-3" >
                <img src={ip+'images/OneturpialHome.png'} style={{height: 125, marginTop:35, marginLeft: '-52px'}}/>
            </div>
            <div class="verticalLine d-flex"> </div>
            <div className="separador  col-6">
                    <div className="mt-5">
                          <input type="text" className="form-control mt-3" placeholder="Usuario" value={user} onChange={(event)=>setUser(event.target.value)} style={{borderRadius:30, backgroundColor:'#f1f1f1', border: 'aqua'}} />
                    </div>
                    <div className=" ">
                          <input  type={input ? "text":"password" } className="form-control mt-3" placeholder="Contraseña" value={pass} onChange={(event)=>setPass(event.target.value)}  style={{borderRadius:30, backgroundColor:'#f1f1f1', border: 'aqua'}} />
                      <div>

                        <div>
                          {input ?
                            <i className="material-icons style-clear-input cursor-pointer" onClick={()=>showPwd()}>visibility_off</i>
                          :
                            <i className="material-icons style-clear-input cursor-pointer" onClick={()=>showPwd()}>visibility</i>

                          }
                        </div>

                      </div>
                    <div className="justify-content-center" style={{marginLeft:180}}>
                        <u onClick={handleShow} style={{cursor:'pointer'}} > <strong>Recuperar contraseña</strong></u>
                    </div>
                    </div>
                    <div className="my-2 d-flex col-md-9 justify-content-end px-0 mt-3" style = {{marginLeft:120}}>
                    {
                      !loading?
                      <button className="btn btn-success" style={{border:'1px solid #FFDE59', backgroundColor:'#FFDE59', color:'black', borderRadius:30}} onClick = {()=>Login()}>Iniciar sesión</button>
                      :<div className="spinner-border text-warning" role="status">
                      </div>
                    }
                      
                    </div>
                  </div>

          <div>
         <Modal show={show} onHide={handleClose}>
           <Modal.Header closeButton>
             <Modal.Title>Recuperar Contraseña</Modal.Title>
           </Modal.Header>
             <Modal.Body>
                    <RecoveyPassword/>
             </Modal.Body>
         </Modal>
          </div>




        </div>

    </div>
);
}

export default LoginAdmon;

if (document.getElementById('LoginAdmon')) {
  ReactDOM.render(<LoginAdmon />, document.getElementById('LoginAdmon'));
}
