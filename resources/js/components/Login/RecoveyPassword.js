import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import Footer from '../Ui/Footer';
import '../../../css/app.scss'
import {ip} from '../ApiRest';
import Swal from 'sweetalert2'
function RecoveyPassword() {
  const [code, setCode] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [password2, setPassword2] = useState('')

  const [loading, setLoading] = useState(false)
  const [showForm, setShowForm] = useState(2)

  const Login = async () => {
    if (code == '') {
      MessageError("El código no debe ir vacío")
    }

    else {
      const dataform = new FormData()
      dataform.append('email',email)
      dataform.append('code',code)
      axios.post(ip+'admon/valida_use',dataform).then(response=>{
        if (response.data.success) {
          MessageSuccess("Código validado correctamente")
          setCode('')
          setShowForm(3)
          console.log("estas en el paso 3");
        }else {
        MessageError("Código no es correcto")
        }

      })
      .catch(error => {

        MessageError("Código no es correcto")
      })
    }
  }

  const Email = async () =>{
    setLoading(true)
    if (email == '') {
      MessageError("El campo de correo  no debe ir vacío")
    }
    else {
      const dataform = new FormData()
      dataform.append('correo', email)
      axios.post(ip+'admon/obtain_user',dataform).then(response=>{
        setLoading(false)
        if(response.data.success){
          MessageSuccess("Revisa tu correo, se ha enviado un código de verificación")
          setShowForm(1)
        }
        else {
          MessageError("Correo no es correcto")
        }
      })
      .catch(error => {

        MessageError("Correo no es correcto")
      })

    }
  }

  const validateEmail = async (data) => {
    var regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    console.log("validacion email");
    console.log(regex.test(data));
    return regex.test(data);

  }

  const editpass = async () => {
    if (password == '' && password2 == '') {
      MessageError("El campo de correo no debe ir vacío")
    }
    else if (password.length<6) {
      MessageError("La contraseña no es correcta debe tener mínimo 6 caracteres")
    }
    else if (password != password2){
      MessageError("Las contraseñas no coinciden")

    }
    else {
      const dataform = new FormData()
      dataform.append('correo', email)
      dataform.append('password',password)
      axios.post(ip+'admon/edit_pass',dataform).then(response=>{
        MessageSuccessend("Contraseña editada correctamente");


      })
      .catch(error => {
        MessageError("Fallo no se guardaron cambios")
      })

    }


  }
  const MessageSuccessend = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',

    }).then(response=>{
      window.location.href="/"
    })

  }


  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  return (
  <div>
  {
    showForm == 2 ?

    <div className = "row">

      <div className = "col-md-12 col-12 pt-1">
        <h5>
          Escribe tu direccion de correo electronico
        </h5>
        <p className = "pt-2">
          <input className = "form-control" onChange = {(e)=>setEmail(e.target.value)} type="email" placeholder = " Tu usuario o email " />
        </p>
      </div>

      <div className = "col-md-12 col-12 pt-1 d-flex justify-content-end" >
      {
        !loading?
        <button className="btn-primary" style={{borderRadius:10, cursor:'pointer'}} onClick = {()=>Email()} >Validar</button>
        :<div className="spinner-border text-warning" role="status">
        </div>
      }
      </div>

    </div>
    :showForm == 1 ?

    <div className = "row">

      <div className = "col-md-12 col-12 pt-1">
        <h5>
          Enviamos un código de verificación a tu correo electronico
        </h5>
        <p className = "pt-2">
          <input className = "form-control"  onChange = {(e)=>setCode(e.target.value)} type="number" placeholder="Escribe el código" />
        </p>
      </div>
      <div className = "col-md-12 col-12 pt-1">
        <button className="btn-primary"  onClick = {()=>Login()} >Validar</button>
      </div>

    </div>
    :showForm == 3 ?
    <div className = "row">

      <div className = "col-md-12 col-12 pt-1">
        <h5>
          Escribe una nueva contraseña
        </h5>
        <p className = "pt-2">
          <input className = "form-control" value={password} onChange = {(e)=>setPassword(e.target.value)} autocomplete="new-text" type  = "password" placeholder = "contraseña"/>
        </p>
      </div>
      <div className = "col-md-12 col-12 pt-1">
        <p className = "pt-2">
        <input className = "form-control" value={password2} onChange = {(e)=>setPassword2(e.target.value)} autocomplete="new-text" type  = "password" placeholder = "repite contraseña"/>
        </p>
      </div>

      <div className = "col-md-12 col-12 pt-1">
        <button className="btn-primary" onClick = {()=>editpass()} >Validar</button>
      </div>

    </div>
    :null
  }
</div>
  );
}

export default RecoveyPassword;

if (document.getElementById('RecoveyPassword')) {
  ReactDOM.render(<RecoveyPassword />, document.getElementById('RecoveyPassword'));
}
