import React,{ useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../ApiRest';
import Swal from 'sweetalert2';
import '../../../css/app.scss';



function Header(props){
  const [showForm, setShowForm] = useState(1)
  const [menu, setMenu] = useState(true)
  useEffect(()=>{
    setShowForm(props.section)

  })

  return(
    <div className = "container">
    <div className="d-flex">
        <nav className = {menu ? 'navbar  navbar-expand-xl navbar-light d-flex style_sidebar_dashboard':'navbar style_sidebar_dashboard_active navbar-expand-xl navbar-light d-flex'}>
  {menu ?  <div className = "list-group">
                <div>
                <div className= "d-flex list-group-item ">
                <div className=" d-flex mt-3 col-md-12  justify-content-end marca">
                    <nav> <img className=""  style={{height:80}} src={Img} onClick = {()=>setMenu(!menu)}/></nav>
                </div>
                </div>

                    { showForm == 2?
                    <a href="/admon/inmueble">
                    <div className= "d-flex list-group-item sumadre ">
                        <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                            <img class="size-icon-sidebar-dashboard" src={Imu} style = {{ width:35}}/>
                        </div>
                        <div>
                        <p className = "negro" >Inmueble</p>
                        </div>
                    </div>
                    </a>
                    :
                    <a href="/admon/inmueble">
                    <div className= "d-flex list-group-item ">

                        <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                            <img class="size-icon-sidebar-dashboard" src={Imu} style = {{ width:35}}/>
                        </div>
                        <div>
                        <p className = "negro" >Inmuebles</p>
                        </div>
                    </div>
                    </a>
                  }
                    { showForm == 3?
                      <a href="/admon/terceros">
                        <div className= "d-flex list-group-item sumadre">

                            <div class="d-flex mr-1 align-items-center justify-content-start tamaño">
                                <img class="size-icon-sidebar-dashboard" src={person} style = {{ width:30}}/>
                            </div>
                            <div>
                            <p className = "negro" >Terceros</p>
                            </div>
                        </div>
                      </a>
                    :
                    <a href="/admon/terceros">
                      <div className= "d-flex list-group-item ">

                          <div class="d-flex mr-1 align-items-center justify-content-start tamaño">
                              <img class="size-icon-sidebar-dashboard" src={person} style = {{ width:30}}/>
                          </div>
                          <div>
                          <p className = "negro" >Terceros</p>
                          </div>
                      </div>
                    </a>
                  }
                  { showForm == 4?
                    <a href="/admon/pagos">
                    <div className= "d-flex list-group-item sumadre">
                        <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                            <img class="size-icon-sidebar-dashboard" src={Metodo} style = {{ width:35}}/>
                        </div>
                        <div>
                        <p className = "negro">Pagos</p>
                        </div>
                    </div>
                    </a>
                    :
                    <a href="/admon/pagos">
                    <div className= "d-flex list-group-item ">
                        <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                            <img class="size-icon-sidebar-dashboard" src={Metodo} style = {{ width:35}}/>
                        </div>
                        <div>
                        <p className = "negro">Pagos</p>
                        </div>
                    </div>
                    </a>
                  }
                  { showForm == 5?
                    <a href="/admon/cobros">
                    <div className= "d-flex list-group-item sumadre">
                        <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                            <img class="size-icon-sidebar-dashboard" src={Cobro} style = {{ width:35}}/>
                        </div>
                        <div>
                        <p className = "negro">Cobros</p>
                        </div>
                    </div>
                    </a>
                    :
                    <a href="/admon/cobros">
                    <div className= "d-flex list-group-item ">
                        <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                            <img class="size-icon-sidebar-dashboard" src={Cobro} style = {{ width:35}}/>
                        </div>
                        <div>
                        <p className = "negro">Cobros</p>
                        </div>
                    </div>
                    </a>
                  }
                    <div className= "d-flex  list-group-item ">

                        <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                            <img class="size-icon-sidebar-dashboard" src={Gasto} style = {{ width:35}}/>
                        </div>
                        <div>
                        <p className = "negro" >Egresos</p>
                        </div>
                    </div>
                    { showForm == 7?
                    <a href="/admon/servicios">
                    <div className= "d-flex list-group-item sumadre">
                        <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                            <img class="size-icon-sidebar-dashboard" src={Servi} style = {{ width:35}}/>
                        </div>
                        <div>
                        <p className = "negro">Servicios</p>
                        </div>
                    </div>
                    </a>
                    :
                    <a href="/admon/servicios">
                    <div className= "d-flex list-group-item ">
                        <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                            <img class="size-icon-sidebar-dashboard" src={Servi} style = {{ width:35}}/>
                        </div>
                        <div>
                        <p className = "negro">Servicios</p>
                        </div>
                    </div>
                    </a>
                  }

                    { showForm == 8?
                    <a href="/admon/cuotas">
                    <div className= "d-flex list-group-item sumadre">
                        <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                            <img class="size-icon-sidebar-dashboard" src={Tarjeta} style = {{ width:35}}/>
                        </div>
                        <div>
                        <p className = "negro">Cuotas</p>
                        </div>
                    </div>
                    </a>
                    :
                    <a href="/admon/cuotas">
                    <div className= "d-flex list-group-item ">
                        <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                            <img class="size-icon-sidebar-dashboard" src={Tarjeta} style = {{ width:35}}/>
                        </div>
                        <div>
                        <p className = "negro">Cuotas</p>
                        </div>
                    </div>
                    </a>
                  }
                </div>

            </div>
          :<div className = "list-group">
                        <div>
                        <div className= "d-flex list-group-item ">
                        <div className=" d-flex mt-3 col-md-12  justify-content-end marca">
                            <nav> <img className=""  style={{height:50}} src={Img} onClick = {()=>setMenu(!menu)}/></nav>
                        </div>
                        </div>

                            { showForm == 2?
                            <a href="/admon/inmueble">
                            <div className= "d-flex list-group-item sumadre ">
                                <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                    <img class="size-icon-sidebar-dashboard" src={Imu} style = {{ width:35}}/>
                                </div>
                                <div>
                                </div>
                            </div>
                            </a>
                            :
                            <a href="/admon/inmueble">
                            <div className= "d-flex list-group-item ">

                                <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                    <img class="size-icon-sidebar-dashboard" src={Imu} style = {{ width:35}}/>
                                </div>
                                <div>
                                </div>
                            </div>
                            </a>
                          }
                            { showForm == 3?
                              <a href="/admon/terceros">
                                <div className= "d-flex list-group-item sumadre">

                                    <div class="d-flex mr-1 align-items-center justify-content-start tamaño">
                                        <img class="size-icon-sidebar-dashboard" src={person} style = {{ width:30}}/>
                                    </div>
                                    <div>
                                    </div>
                                </div>
                              </a>
                            :
                            <a href="/admon/terceros">
                              <div className= "d-flex list-group-item ">

                                  <div class="d-flex mr-1 align-items-center justify-content-start tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={person} style = {{ width:30}}/>
                                  </div>
                                  <div>
                                  </div>
                              </div>
                            </a>
                          }
                          { showForm == 4?
                            <a href="/admon/pagos">
                            <div className= "d-flex list-group-item sumadre">
                                <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                    <img class="size-icon-sidebar-dashboard" src={Metodo} style = {{ width:35}}/>
                                </div>
                                <div>
                                </div>
                            </div>
                            </a>
                            :
                            <a href="/admon/pagos">
                            <div className= "d-flex list-group-item ">
                                <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                    <img class="size-icon-sidebar-dashboard" src={Metodo} style = {{ width:35}}/>
                                </div>
                                <div>
                                </div>
                            </div>
                            </a>
                          }
                          { showForm == 5?
                            <a href="/admon/cobros">
                            <div className= "d-flex list-group-item sumadre">
                                <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                    <img class="size-icon-sidebar-dashboard" src={Cobro} style = {{ width:35}}/>
                                </div>
                                <div>
                                </div>
                            </div>
                            </a>
                            :
                            <a href="/admon/cobros">
                            <div className= "d-flex list-group-item ">
                                <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                    <img class="size-icon-sidebar-dashboard" src={Cobro} style = {{ width:35}}/>
                                </div>
                                <div>
                                </div>
                            </div>
                            </a>
                          }
                            <div className= "d-flex  list-group-item ">

                                <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                    <img class="size-icon-sidebar-dashboard" src={Gasto} style = {{ width:35}}/>
                                </div>
                                <div>
                                </div>
                            </div>
                            { showForm == 7?
                            <a href="/admon/servicios">
                            <div className= "d-flex list-group-item sumadre">
                                <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                    <img class="size-icon-sidebar-dashboard" src={Servi} style = {{ width:35}}/>
                                </div>
                                <div>
                                </div>
                            </div>
                            </a>
                            :
                            <a href="/admon/servicios">
                            <div className= "d-flex list-group-item ">
                                <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                    <img class="size-icon-sidebar-dashboard" src={Servi} style = {{ width:35}}/>
                                </div>
                                <div>
                                </div>
                            </div>
                            </a>
                          }

                            { showForm == 8?
                            <a href="/admon/cuotas">
                            <div className= "d-flex list-group-item sumadre">
                                <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                    <img class="size-icon-sidebar-dashboard" src={Tarjeta} style = {{ width:35}}/>
                                </div>
                                <div>
                                </div>
                            </div>
                            </a>
                            :
                            <a href="/admon/cuotas">
                            <div className= "d-flex list-group-item ">
                                <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                    <img class="size-icon-sidebar-dashboard" src={Tarjeta} style = {{ width:35}}/>
                                </div>
                                <div>
                                </div>
                            </div>
                            </a>
                          }
                        </div>

                    </div>
                  }
        </nav>
        </div>
    </div>
  )

}

export default Header;
if (document.getElementById('Header')) {
  ReactDOM.render(<Header />, document.getElementById('Header'));
}
