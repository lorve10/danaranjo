import React,{ useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../ApiRest';
import Swal from 'sweetalert2';
import '../../../css/app.scss';
import { Tooltip } from '@material-ui/core';
import { withStyles, makeStyles } from '@material-ui/core/styles';


function Header(props){
  const [showForm, setShowForm] = useState(1)
  const [menu, setMenu] = useState(false)
  const [estado, setEstado] = useState(1)

  useEffect(()=>{
    setShowForm(props.section)

  })

const clickMenu = async (value) => {

  await setMenu(value)
  props.marginval(value)
}

const LightTooltip = makeStyles((theme) => ({
    tooltip: {
      fontSize: 100,
    },
  }));

  const classes = LightTooltip();
  
  return(
    <div className = "container">
    <div className="d-flex">
        <nav className = {menu ? 'navbar  navbar-expand-xl navbar-light d-flex style_sidebar_dashboard':'navbar style_sidebar_dashboard_active navbar-expand-xl navbar-light d-flex'}>

          <div className = "row align-items-center justify-content-center pl-3">
            {menu ?  <div className = "list-group">
                          <div>
                          <div className= "d-flex list-group-item ">
                          <img onClick = {()=>clickMenu(!menu)} className={!menu ? 'imagen_dashboard_slider':'imagen_dashboard_slider_active'} src={ip+'images/Oneturpial.png'} />

                          </div>

                          { showForm == 1?
                              <a href = {ip+"admon/inicio"}>
                              <div className= "d-flex list-group-item sumadre ">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/home.png'} style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro" >Inicio</p>
                                  </div>
                              </div>
                              </a>
                              :
                              <a href = {ip+"admon/inicio"}>
                              <div className= "d-flex list-group-item ">

                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/home.png'} style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro" >Inicio</p>
                                  </div>
                              </div>
                              </a>
                          }

                            { showForm == 2?
                              <a href= {ip+"admon/conjunto"}>
                              <div className= "d-flex list-group-item sumadre">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/edificio.png'}  style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Conjunto</p>
                                  </div>
                              </div>
                              </a>
                              :
                              <a href= {ip+"admon/conjunto"}>
                              <div className= "d-flex list-group-item ">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/edificio.png'}  style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Conjunto</p>
                                  </div>
                              </div>
                              </a>
                            }

                            { showForm == 3?
                              <a href = {ip+"admon/#"}>
                              <div className= "d-flex list-group-item sumadre ">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/contrato.png'} style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro" >Contratos</p>
                                  </div>
                              </div>
                              </a>
                              :
                              <a href = {ip+"admon/#"}>
                              <div className= "d-flex list-group-item ">

                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/contrato.png'} style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro" >Contratos</p>
                                  </div>
                              </div>
                              </a>
                            }
                            
                              { showForm == 4?
                                <a href= {ip+"admon/Financiera"}>
                                  <div className= "d-flex list-group-item sumadre">

                                      <div class="d-flex mr-1 align-items-center justify-content-start tamaño">
                                          <img class="size-icon-sidebar-dashboard" src={ip+'images/finanzas.png'} style = {{ width:40}}/>
                                      </div>
                                      <div>
                                      <p className = "negro" >Financiera</p>
                                      </div>
                                  </div>
                                </a>
                              :
                              <a href= {ip+"admon/Financiera"}>
                                <div className= "d-flex list-group-item ">

                                    <div class="d-flex mr-1 align-items-center justify-content-start tamaño">
                                        <img class="size-icon-sidebar-dashboard" src={ip+'images/finanzas.png'} style = {{ width:40}}/>
                                    </div>
                                    <div>
                                    <p className = "negro" >Financiera</p>
                                    </div>
                                </div>
                              </a>
                            }
                            { showForm == 5?
                              <a href= {ip+"admon/#"}>
                              <div className= "d-flex list-group-item sumadre">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/consult.png'}  style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Servicios</p>
                                  </div>
                              </div>
                              </a>
                              :
                              <a href= {ip+"admon/Financiera"}>
                              <div className= "d-flex list-group-item ">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/consult.png'}  style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Servicios</p>
                                  </div>
                              </div>
                              </a>
                            }

                            { showForm == 6?
                              <a href= {ip+"admon/#"}>
                              <div className= "d-flex list-group-item sumadre">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/presupuesto.png'}  style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Presupuesto</p>
                                  </div>
                              </div>
                              </a>
                              :
                              <a href= {ip+"admon/Financiera"}>
                              <div className= "d-flex list-group-item ">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/presupuesto.png'}  style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Presupuesto</p>
                                  </div>
                              </div>
                              </a>
                            }

                            { showForm == 7?
                              <a href= {ip+"admon/#"}>
                              <div className= "d-flex list-group-item sumadre">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/carrito.png'}  style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Compras</p>
                                  </div>
                              </div>
                              </a>
                              :
                              <a href= {ip+"admon/Financiera"}>
                              <div className= "d-flex list-group-item ">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/carrito.png'}  style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Compras</p>
                                  </div>
                              </div>
                              </a>
                            }

                            { showForm == 8?
                              <a href= {ip+"admon/#"}>
                              <div className= "d-flex list-group-item sumadre">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/mantenimiento.png'}  style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Mantenimiento</p>
                                  </div>
                              </div>
                              </a>
                              :
                              <a href= {ip+"admon/Financiera"}>
                              <div className= "d-flex list-group-item ">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/mantenimiento.png'}  style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Mantenimiento</p>
                                  </div>
                              </div>
                              </a>
                            }
                            
                            { showForm == 9?
                              <a href= {ip+"admon/#"}>
                              <div className= "d-flex list-group-item sumadre">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/reunion.png'}  style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Asambleas</p>
                                  </div>
                              </div>
                              </a>
                              :
                              <a href= {ip+"admon/Financiera"}>
                              <div className= "d-flex list-group-item ">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/reunion.png'}  style = {{ width:40}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Asambleas</p>
                                  </div>
                              </div>
                              </a>
                            }

                            { showForm == 10?
                              <a href= {ip+"admon/Configuracion"}>
                              <div className= "d-flex list-group-item sumadre">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/configuración.png'} style = {{ width:35}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Configuración</p>
                                  </div>
                              </div>
                              </a>
                              :
                              <a href= {ip+"admon/Configuracion"}>
                              <div className= "d-flex list-group-item ">
                                  <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                      <img class="size-icon-sidebar-dashboard" src={ip+'images/configuración.png'} style = {{ width:35}}/>
                                  </div>
                                  <div>
                                  <p className = "negro">Configuración</p>
                                  </div>
                              </div>
                              </a>
                            }
                          </div>

                      </div>
                    :<div className = "list-group">
                                  <div className= "d-flex list-group-item">
                                      <img onClick = {()=>clickMenu(!menu)} className={!menu ? 'imagen_dashboard_slider':'imagen_dashboard_slider_active'}  src={ip+'images/pajaro.png'} />
                                  </div>
                                  { showForm == 1?
                                    <Tooltip title="Inicio" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/inicio"}>
                                      <div className= "d-flex list-group-item sumadre ">
                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/home.png'} style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                    </Tooltip>
                                      :
                                    <Tooltip title="Inicio" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/inicio"}>
                                      <div className= "d-flex list-group-item ">

                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/home.png'} style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                    </Tooltip>
                                    }

                                    { showForm == 2?
                                    <Tooltip title="Módulo conjunto" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/conjunto"}>
                                      <div className= "d-flex list-group-item sumadre">
                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/edificio.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                      </Tooltip>
                                      :
                                      <Tooltip title="Módulo conjunto" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/conjunto"}>
                                      <div className= "d-flex list-group-item ">
                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/edificio.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                      </Tooltip>
                                    }

                                    { showForm == 3?
                                    <Tooltip title="Contratos" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/#"}>
                                      <div className= "d-flex list-group-item sumadre ">
                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/contrato.png'} style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                      </Tooltip>
                                      :
                                      <Tooltip title="Contratos" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/#"}>
                                      <div className= "d-flex list-group-item ">

                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/contrato.png'} style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                      </Tooltip>
                                    }
                                      { showForm == 4?
                                      <Tooltip title="Financiera" placement="right" arrow className="class-tooltip">
                                        <a href= {ip+"admon/Financiera"}>
                                          <div className= "d-flex list-group-item sumadre">

                                              <div class="d-flex mr-1 align-items-center justify-content-start tamaño">
                                                  <img class="size-icon-sidebar-dashboard" src={ip+'images/finanzas.png'}style = {{ width:40, marginTop: "15px"}}/>
                                              </div>
                                              <div>
                                              </div>
                                          </div>
                                        </a>
                                        </Tooltip>
                                      :
                                      <Tooltip title="Financiera" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/Financiera"}>
                                        <div className= "d-flex list-group-item ">

                                            <div class="d-flex mr-1 align-items-center justify-content-start tamaño">
                                                <img class="size-icon-sidebar-dashboard" src={ip+'images/finanzas.png'}style = {{ width:40, marginTop: "15px"}}/>
                                            </div>
                                            <div>
                                            </div>
                                        </div>
                                      </a>
                                      </Tooltip>
                                    }
                                    { showForm == 5?
                                    <Tooltip title="Servicios" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/#"}>
                                      <div className= "d-flex list-group-item sumadre">
                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/consult.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                      </Tooltip>
                                      :
                                      <Tooltip title="Servicios" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/#"}>
                                      <div className= "d-flex list-group-item ">
                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/consult.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                      </Tooltip>
                                    }
                                    { showForm == 6?
                                    <Tooltip title="Presupuesto" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/#"}>
                                      <div className= "d-flex list-group-item sumadre">
                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/presupuesto.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                      </Tooltip>
                                      :
                                      <Tooltip title="Presupuesto" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/#"}>
                                      <div className= "d-flex list-group-item ">
                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/presupuesto.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                      </Tooltip>
                                    }
                                    { showForm == 7?
                                    <Tooltip title="Compras" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/#"}>
                                      <div className= "d-flex list-group-item sumadre">
                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/carrito.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                      </Tooltip>
                                      :
                                      <Tooltip title="Compras" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/#"}>
                                      <div className= "d-flex list-group-item ">
                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/carrito.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                      </Tooltip>
                                    }
                                    { showForm == 8?
                                    <Tooltip title="Mantenimiento" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/#"}>
                                      <div className= "d-flex list-group-item sumadre">
                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/mantenimiento.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                      </Tooltip>
                                      :
                                      <Tooltip title="Mantenimiento" placement="right" arrow className="class-tooltip">
                                      <a href= {ip+"admon/#"}>
                                      <div className= "d-flex list-group-item ">
                                          <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                              <img class="size-icon-sidebar-dashboard" src={ip+'images/mantenimiento.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                          </div>
                                          <div>
                                          </div>
                                      </div>
                                      </a>
                                      </Tooltip>
                                    }
                                    
                                    {
                                        estado == 1 ?
                                        <div className= "d-flex">
                                          <div class="d-flex mr-1 align-items-center justify-content-center">
                                            <span class="material-icons" style={{marginLeft:'25px', fontSize:'300%', marginTop:'-30px'}} onClick={()=>setEstado(2)}>
                                                expand_more
                                            </span>
                                          </div>
                                          <div>
                                          </div>
                                        </div>

                                        : null
                                    }

                                    {
                                        estado == 2 ? 
                                        <div>
                                        { showForm == 9?
                                        <Tooltip title="Asambleas" placement="right" arrow className="class-tooltip">
                                        <a href= {ip+"admon/#"}>
                                        <div className= "d-flex list-group-item sumadre">
                                            <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                                <img class="size-icon-sidebar-dashboard" src={ip+'images/reunion.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                            </div>
                                            <div>
                                            </div>
                                        </div>
                                        </a>
                                        </Tooltip>
                                        :
                                        <Tooltip title="Asambleas" placement="right" arrow className="class-tooltip">
                                        <a href= {ip+"admon/#"}>
                                        <div className= "d-flex list-group-item ">
                                            <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                                <img class="size-icon-sidebar-dashboard" src={ip+'images/reunion.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                            </div>
                                            <div>
                                            </div>
                                        </div>
                                        </a>
                                        </Tooltip>
                                        }

                                        { showForm == 10?
                                        <Tooltip title="Configuración" placement="right" arrow className="class-tooltip">
                                        <a href={ip+"admon/Configuracion"}>
                                        <div className= "d-flex list-group-item sumadre">
                                            <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                                <img class="size-icon-sidebar-dashboard" src={ip+'images/configuración.png'} style = {{ width:40, marginTop: "15px"}}/>
                                            </div>
                                            <div>
                                            </div>
                                        </div>
                                        </a>
                                        </Tooltip>
                                        :
                                        <Tooltip title="Configuración" placement="right" arrow className="class-tooltip">
                                        <a href={ip+"admon/Configuracion"}>
                                        <div className= "d-flex list-group-item ">
                                            <div class="d-flex mr-1 align-items-center justify-content-center tamaño">
                                                <img class="size-icon-sidebar-dashboard" src={ip+'images/configuración.png'}  style = {{ width:40, marginTop: "15px"}}/>
                                            </div>
                                            <div>
                                            </div>
                                        </div>
                                        </a>
                                        </Tooltip>
                                        }
                                    </div>
                                        : null
                                    }
                                    
                                    
                                  </div>
                            }
          </div>
        </nav>
        </div>
    </div>
  )

}

export default Header;
if (document.getElementById('Header')) {
  ReactDOM.render(<Header />, document.getElementById('Header'));
}
