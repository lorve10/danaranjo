import  React from 'react'
import ReactDOM from 'react-dom';
import {ip} from '../ApiRest';
import Swal from 'sweetalert2';
import '../../../css/app.scss';


const InicioAdmon = () =>{

  return(
    <div>
    <div className="col-md-12 mt-4" >
        <div className="form-section d-flex "  style={{backgroundColor:'#FFEDA6', borderRadius:10, marginLeft:'86%', padding: '6px'}}> <i  className="material-icons align-items-center my-1" style={{marginLeft: '20px'}}>logout</i> <a href={ip+"admon/logout"}> <h5 style={{padding: '6px', marginBottom: 0}}>Cerrar Sesión</h5> </a></div>
    </div>
    <div className="container mt-1"  >
        <div className="text-center">
            <h5>Bienvenido al gestor administrativo de centros comerciales</h5>
        </div>


        <div className="col-md-9 justify-content-center align-items-center"  >
        <div className="row mt-5 " style={{marginLeft:'31%'}}>
          <a className="col-md-3" href={ip+"admon/inmueble"}>
            <div className="cajas" >
                <img src={ip+'images/edificio.png'} style = {{marginLeft: 29, width:56, marginTop:9}}/>
                <p className="text-center" style={{fontWeight: "bold"}}>Inmuebles</p>
            </div>
          </a>
          <a className="col-md-3" href={ip+"admon/terceros"}>
            <div className="cajas ">
                <img src={ip+'images/group.png'} style = {{marginLeft: 31, width:56, marginTop:14}}/>
                <p className="text-center" style={{fontWeight: "bold", marginTop:'-8px'}}>Terceros</p>
            </div>
            </a>
            <a className="col-md-3" href={ip+"admon/Financiera"}>
            <div className="cajas ">
                <img src={ip+'images/finanzas.png'} style = {{marginLeft: 31, width:56, marginTop:9}}/>
                <p className="text-center" style={{fontWeight: "bold"}}>Financiera </p>
            </div>
            </a>
            <a className="col-md-3" href={ip+"admon/Configuracion"}>
            <div className="cajas">
                <img src={ip+'images/configuración.png'} style ={{marginLeft: 36, width:48, marginTop:12}}/>
                <p className="text-center" style={{fontWeight: "bold" }}>Configuracion </p>
            </div>
            </a>
            {/*<a className="col-md-3" href={ip+"admon/contabilidad"}>
                <div className="cajas">
                    <img src={ip+'images/Contabilidad.png'} style ={{marginLeft: 32, width:56, marginTop:9}}/>
                    <p className="text-center" style={{fontWeight: "bold"}}>Contabilidad</p>
                </div>
            </a>*/}
        </div>
        {/*<div className="row mt-2" style={{marginLeft:'31%'}} >
        <a className="col-md-3" href={ip+"admon/Configuracion"}>
            <div className="cajas">
                <img src={ip+'images/configuración.png'} style ={{marginLeft: 36, width:48, marginTop:12}}/>
                <p className="text-center" style={{fontWeight: "bold" }}>Configuracion </p>
            </div>
        </a>
        </div>*/}
        </div>
    </div>
    </div>
  )
}

export default InicioAdmon;

if(document.getElementById('InicioAdmon')){
  ReactDOM.render(<InicioAdmon />, document.getElementById('InicioAdmon'))
}
