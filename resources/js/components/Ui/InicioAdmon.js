import  React,{useState, useEffect} from 'react'
import ReactDOM from 'react-dom';
import {ip} from '../ApiRest';
import Swal from 'sweetalert2';
import '../../../css/app.scss';
import Pagination from '../pagination/Pagination';


const InicioAdmon = () =>{

  const [listConjunto, setListConjunto ] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(6);

  useEffect(()=>{
    mostrarConjunto();
  }, [])

  const mostrarConjunto = async() =>{
    axios.get(ip+'admon/obtain_conjunto').then(response=>{
      console.log(response);
      var res = response.data;
      const dataNew = Pagination.paginate(response.data,currentPage,postsPerPage)
      setListConjunto(dataNew)
    })
  }

  return(
    <div>
    <div className="container mt-5"  >
        <div className="text-center">
          {
            listConjunto.map((item)=>{
              return(
                <h5>Bienvenido al gestor de conjuntos residenciales | {item.nombre}</h5>
              )
            })
          }
            
        </div>


        <div className="col-md-12 justify-content-center align-items-center"  >
        <div className="row" style={{marginLeft:'31%'}}>

          <a className="col-md-3 mb-5" href={ip+"admon/conjunto"} style={{marginLeft:-52}}>
            <div className="d-flex poner-hover" style={{border:'1px solid #FFDE59', width:'140%', borderRadius:12}}>
              <div className="cajas d-flex" >
                  <img className="cajas-img" src={ip+'images/edificio.png'}/>
              </div>
              <div className="d-flex align-items-center" >
                  <p className="text-center" style={{fontWeight: "bold", marginTop:10}}><strong>Conjunto</strong></p>
              </div>
            </div>
          </a>
          <a className="col-md-3 mb-5" href={ip+"admon/terceros"} style={{marginLeft:98}}>
            <div className="d-flex poner-hover" style={{border:'1px solid #FFDE59', width:'140%', borderRadius:12}}>
              <div className="cajas d-flex" >
                  <img className="cajas-img" src={ip+'images/contrato.png'}/>
              </div>
              <div className="d-flex align-items-center" >
                  <p className="text-center" style={{fontWeight: "bold", marginTop:10}}><strong>Contratos</strong></p>
              </div>
            </div>
          </a>
        </div>
        <div className="row " style={{marginLeft:'31%', marginTop:-30}} >
        <a className="col-md-3 mb-5" href={ip+"admon/Financiera"} style={{marginLeft:-52}}>
          <div className="d-flex poner-hover" style={{border:'1px solid #FFDE59', width:'140%', borderRadius:12}}>
            <div className="cajas d-flex" >
                <img className="cajas-img" src={ip+'images/finanzas.png'}/>
            </div>
            <div className="d-flex align-items-center" >
                <p className="text-center" style={{fontWeight: "bold", marginTop:10}}><strong>Financiera</strong></p>
            </div>
          </div>
        </a>

        <a className="col-md-3 mb-5" href={ip+"admon/#"} style={{marginLeft:98}}>
            <div className="d-flex poner-hover" style={{border:'1px solid #FFDE59', width:'140%', borderRadius:12}}>
              <div className="cajas d-flex" >
                  <img className="cajas-img" src={ip+'images/consult.png'}/>
              </div>
              <div className="d-flex align-items-center" >
                  <p className="text-center" style={{fontWeight: "bold", marginTop:10}}><strong>Servicios</strong></p>
              </div>
            </div>
          </a>
        
        </div>

        <div className="row " style={{marginLeft:'31%', marginTop:-30}} >
        <a className="col-md-3 mb-5" href={ip+"admon/#"} style={{marginLeft:-52}}>
          <div className="d-flex poner-hover" style={{border:'1px solid #FFDE59', width:'140%', borderRadius:12}}>
            <div className="cajas d-flex" >
                <img className="cajas-img" src={ip+'images/presupuesto.png'}/>
            </div>
            <div className="d-flex align-items-center" >
                <p className="text-center" style={{fontWeight: "bold", marginTop:10}}><strong>Presupuesto</strong></p>
            </div>
          </div>
        </a>

        <a className="col-md-3 mb-5" href={ip+"admon/#"} style={{marginLeft:98}}>
            <div className="d-flex poner-hover" style={{border:'1px solid #FFDE59', width:'140%', borderRadius:12}}>
              <div className="cajas d-flex" >
                  <img className="cajas-img" src={ip+'images/carrito.png'}/>
              </div>
              <div className="d-flex align-items-center" >
                  <p className="text-center" style={{fontWeight: "bold", marginTop:10}}><strong>Compras</strong></p>
              </div>
            </div>
          </a>
        
        </div>

        <div className="row " style={{marginLeft:'31%', marginTop:-30}} >
        <a className="col-md-3 mb-5" href={ip+"admon/#"} style={{marginLeft:-52}}>
          <div className="d-flex poner-hover" style={{border:'1px solid #FFDE59', width:'140%', borderRadius:12}}>
            <div className="cajas d-flex" >
                <img className="cajas-img" src={ip+'images/mantenimiento.png'}/>
            </div>
            <div className="d-flex align-items-center" >
                <p className="text-center" style={{fontWeight: "bold", marginTop:10}}><strong>Mantenimiento</strong></p>
            </div>
          </div>
        </a>

        <a className="col-md-3 mb-5" href={ip+"admon/#"} style={{marginLeft:98}}>
            <div className="d-flex poner-hover" style={{border:'1px solid #FFDE59', width:'140%', borderRadius:12}}>
              <div className="cajas d-flex" >
                  <img className="cajas-img" src={ip+'images/reunion.png'}/>
              </div>
              <div className="d-flex align-items-center" >
                  <p className="text-center" style={{fontWeight: "bold", marginTop:10}}><strong>Asambleas</strong></p>
              </div>
            </div>
          </a>
        
        </div>

        <div className="row " style={{marginLeft:'31%', marginTop:-30}} >
        <a className="col-md-3 mb-5" href={ip+"admon/Configuracion"} style={{marginLeft:-52}}>
          <div className="d-flex poner-hover" style={{border:'1px solid #FFDE59', width:'140%', borderRadius:12}}>
            <div className="cajas d-flex" >
                <img className="cajas-img" src={ip+'images/configuración.png'}/>
            </div>
            <div className="d-flex align-items-center" >
                <p className="text-center" style={{fontWeight: "bold", marginTop:10}}><strong>Configuración</strong></p>
            </div>
          </div>
        </a>
        </div>

        </div>
    </div>
    <div className="col-md-12 mt-5"  >
        <div className="form-section d-flex "  style={{backgroundColor:'#FFDE59', borderRadius:10, marginLeft:'95%', padding: '6px'}}><a href={ip+"admon/logout"}> <i  className="material-icons align-items-center my-1" style={{marginLeft: '20px'}}>logout</i> </a></div>
    </div>
    </div>
  )
}

export default InicioAdmon;

if(document.getElementById('InicioAdmon')){
  ReactDOM.render(<InicioAdmon />, document.getElementById('InicioAdmon'))
}
