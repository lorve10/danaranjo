/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./components/Example');
require('./components/Login/LoginAdmon');
require('./components/Admon/Property/Property');
require('./components/Admon/Terceros/Terceros');
require('./components/Admon/Cartera/Purse/Purse');
require('./components/Admon/Cuotas/Cuotas');
require('./components/Admon/Cartera/Cobros/Cobros');
require('./components/Admon/Cartera/Cartera');
require('./components/Contabilidad/Contabilidad');
require('./components/Contabilidad/Parametrizacion/Parametrizacion');
require('./components/Admon/Financiera');
require('./components/Admon/Tesoreria/Tesoreria');

require('./components/Admon/Cartera/Conceptos');
require('./components/Admon/config/Configuracion');
require('./components/Admon/Cartera/PrintCobros/PrintCobro');
require('./components/Admon/Cartera/Cobros/PrintCobroLocal');
require('./components/Admon/Cartera/Egresos/Egresos');
require('./components/Admon/Conceptos/Conceptos');
require('./components/Ui/Header');
require('./components/Ui/Footer');
require('./components/Ui/InicioAdmon');
require('./components/HomePage');
/////
require('./components/Admon/Conjunto/Conjunto');
require('./components/Admon/Conjunto/DatosConjunto/DatosConjunto');
require('./components/Admon/Conjunto/Propietarios/Propietarios');
////Modulo Finaciero
require('./components/Contabilidad/Parametrizacion/CentroCosto/CentroCosto');
require('./components/Contabilidad/Parametrizacion/Concepto/Concepto');
require('./components/Contabilidad/Parametrizacion/TiposCIU/TiposCIU');
require('./components/Contabilidad/Parametrizacion/PeriodosContables/PeriodosContables');
require('./components/Contabilidad/Parametrizacion/TipoDoc/TipoDoc');
require('./components/Contabilidad/Crear-documentos/CrearDocumentos');
require('./components/Contabilidad/Parametrizacion/CuentaContable/CuentaContable');
